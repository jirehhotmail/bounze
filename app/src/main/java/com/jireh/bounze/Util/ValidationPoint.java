package com.jireh.bounze.Util;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by Muthamizhan C on 21-11-2017.
 */


public class ValidationPoint {

    private static final String EMAIL_REG = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REG = "\\d{10}";
    private static final String EMAIL_MSG = "Invalid email";
    private static final String REQUIRED_MSG = "Field required";
    private static final String PHONE_MSG = "Phone no invalid";
    private static final String PASSWORD_MSG = "pwd should be max 5 chars";

    public static boolean isEmailIdValid(EditText edtValue, boolean isRequired) {
        return isValid(edtValue, EMAIL_REG, EMAIL_MSG, isRequired);
    }

    public static boolean isValidPhone(EditText edtValue, boolean isRequired) {
        return isValidPhoneno(edtValue, PHONE_REG, PHONE_MSG, isRequired);
    }

    public static boolean isValidPassword(EditText password, boolean isRequired) {
        return isValidPwd(password, PASSWORD_MSG, isRequired);
    }

    public static boolean isValidPhoneno(EditText edtValue, String phone_no, String phoneMsg, boolean isRequired) {
        String edit_text = edtValue.getText().toString().trim();
        edtValue.setError(null);
        if (isRequired && !hasText(edtValue)) {
            return false;
        } else if (isRequired && (edit_text.length() != 10)) {
            edtValue.setError(phoneMsg);
            return false;
        }
        return true;
    }

    public static boolean isValid(EditText edtValue, String emailReg, String emailMsg, boolean isRequired) {
        String edit_text = edtValue.getText().toString().trim();
        edtValue.setError(null);
        if (isRequired && !hasText(edtValue)) {
            return false;
        } else if (isRequired && !Pattern.matches(emailReg, edit_text)) {
            edtValue.setError(emailMsg);
            return false;
        }
        return true;
    }

    public static boolean hasText(EditText edtValue) {
        String editText = edtValue.getText().toString();
        edtValue.setError(null);

        if (editText.length() == 0) {
            edtValue.setError(REQUIRED_MSG);
            return false;
        }
        return true;
    }

    public static boolean isValidPwd(EditText password_edt, String passwordMsg, boolean isRequired) {
        String password = password_edt.getText().toString().trim();
        password_edt.setError(null);
        if (isRequired && !hasText(password_edt)) {
            return false;
        } else if (isRequired && (password.length() < 6)) {
            password_edt.setError(passwordMsg);
            return false;
        }
        return true;

    }
}

