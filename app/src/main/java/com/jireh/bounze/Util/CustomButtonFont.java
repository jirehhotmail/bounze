package com.jireh.bounze.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Muthamizhan C on 25-07-2017.
 */

public class CustomButtonFont extends Button {

    public CustomButtonFont(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomButtonFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);

    }

    public CustomButtonFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontChache.getTypeface("fonts/Montserrat-Thin.ttf", context);
        setTypeface(customFont);
    }


}
