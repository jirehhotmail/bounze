package com.jireh.bounze.Util;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Muthamizhan C on 25-10-2017.
 */

public class WorkaroundNestedScrollView extends NestedScrollView {


        public WorkaroundNestedScrollView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                // Explicitly call computeScroll() to make the Scroller compute itself
                computeScroll();
            }
            return super.onInterceptTouchEvent(ev);
        }
    }