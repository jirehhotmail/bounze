package com.jireh.bounze.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class CustomTFBold extends TextView {


    public CustomTFBold(Context context) {
        super(context);
        init();
    }

    public CustomTFBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTFBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Bold.otf");
        setTypeface(tf, 1);
    }


}
