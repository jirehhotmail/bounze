package com.jireh.bounze.Util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.jireh.bounze.Intents.IntentHelper;

/**
 * Created by Muthamizhan C on 05-01-2018.
 */
public class UriToIntentMapper {
    private Context mContext;
    private IntentHelper mIntents;

    public UriToIntentMapper(Context context, IntentHelper intentHelper) {
        mContext = context;
        mIntents = intentHelper;
    }

    public void dispatchIntent(Intent intent) {
        final Uri uri = intent.getData();
        Intent dispatchIntent = null;

        if (uri == null) throw new IllegalArgumentException("Uri cannot be null");

        final String scheme = uri.getScheme().toLowerCase();
        final String host = uri.getHost().toLowerCase();
        final String path = uri.getPath().toLowerCase();

        if (("http".equals(scheme) || "https".equals(scheme)) &&
                ("www.bounze.in".equals(host) || "www.bounze.in".equals(host))) {
            dispatchIntent = mapAppLink(uri);
        }
/*
         if (("http".equals(scheme) || "https".equals(scheme)) &&
                ("www.bounze.in".equals(host) || "www.bounze.in".equals(host))) {
            dispatchIntent = mapWebLink(uri);
        }*/

        if (dispatchIntent != null) {
            mContext.startActivity(dispatchIntent);
        }
    }

    private Intent mapAppLink(Uri uri) {
       // final String host = uri.getHost().toLowerCase();
        final String path = uri.getLastPathSegment().toLowerCase();
        switch (path) {
            case "programs.php":
                String aQuery = uri.getQueryParameter("shopid");
                return mIntents.newAActivityIntent(mContext, aQuery);
            case "coach_programs.php":
                String bQuery = uri.getQueryParameter("coachid");
                return mIntents.newBActivityIntent(mContext, bQuery);
            case "event_programs.php":
                String cQuery = uri.getQueryParameter("eid");
                //         int choice = Integer.parseInt(uri.getQueryParameter("choice"));
                return mIntents.newCActivityIntent(mContext, cQuery);
            case "pass_programs":
                String dQuery = uri.getQueryParameter("pass_id");
                //         int choice = Integer.parseInt(uri.getQueryParameter("choice"));
                return mIntents.newDActivityIntent(mContext, dQuery);
            case "blog.php":
                String eQuery = uri.getQueryParameter("blogid");
                //         int choice = Integer.parseInt(uri.getQueryParameter("choice"));
                return mIntents.newEActivityIntent(mContext, eQuery);

        }
        return null;
    }

  /*  private Intent mapWebLink(Uri uri) {
        final String path = uri.getPath();

        switch (path) {
            case "/a":
                //   String aQuery = uri.getQueryParameter("query");
                String aQuery = "https://www.bounze.in/programs.php?sid=4&shopid=31&suid=23";
                return mIntents.newAWebIntent(mContext, aQuery);
            case "/b":
                String bQuery = uri.getQueryParameter("query");

                return mIntents.newBWebIntent(mContext, bQuery);
            case "/c":
                //   String cQuery = uri.getQueryParameter("query");
                String cQuery = "https://www.bounze.in/programs.php?sid=4&shopid=31&suid=23";
                //       int choice = Integer.parseInt(uri.getQueryParameter("choice"));
                return mIntents.newCWebIntent(mContext, cQuery);
        }
        return null;
    }*/
}