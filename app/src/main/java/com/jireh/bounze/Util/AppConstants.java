package com.jireh.bounze.Util;

/**
 * Created by Muthamizhan C on 18-08-2017.
 */

public class AppConstants {
    public static final String isUserLoggedIn = "isUserLoggedIn";
    public static final String isallDataDownloaded = "isallDataDownloaded";
    public static final String UserId = "UserId";
    public static final String attachmentRealPath = "attachmentPath";

    public static final String paymentuid = "payment_unique_id";
    public static final String version = "current_version";
    public static String isUserLoggedFacebook = "facebook_pic";
    public static String isUserLoggedGoogle = "google_pic";

    public static String tokenId="token_id";
    public static String wallet="wallet";
    public static String reward_points="reward_points";
}
