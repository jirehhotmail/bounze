package com.jireh.bounze.Util;

import android.app.Activity;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.EditText;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 21-11-2017.
 */

public class SendOTP {

    public int random_no = 0;

    protected boolean validation = true;
    SendSMS sendsms_msg91 = new SendSMS();

    public SendOTP() {
    }

    public boolean validation_phoneno(int res_id, Activity activity) {
        this.validation = true;
        String class_name = activity.findViewById(res_id).getClass().getName().toLowerCase();
        if (class_name.contains("edittext")) {
            EditText textView1 = (EditText) activity.findViewById(res_id);
            if (!ValidationPoint.hasText(textView1)) {
                this.validation = false;
            }
        }

        return this.validation;
    }

    public boolean send_otp_msg91(String mobile_number, int random_no, Activity activity) {
        this.random_no = random_no;
        this.sendsms_msg91.setMobiles(mobile_number);
        this.sendsms_msg91.setMessage(random_no + this.sendsms_msg91.getMessage_txt());
        this.sendsms_msg91.send_sms();
        return true;
    }


    public boolean checkNumber(String user_no) {
        return this.random_no == 0 ? false : user_no.trim().equals(String.valueOf(this.random_no));
    }

    public int getRandomNumber(int Min, int Max) {
        return (int) Math.floor(Math.random() * (double) (Max - Min + 1)) + Min;
    }

    public void showcounter(int res_id, Activity activity, final long milliseconds, int interval, final String tick_part2) {
        String class_name = activity.findViewById(res_id).getClass().getName().toLowerCase();

        if (class_name.contains("button")) {
            final Button textView2 = (Button) activity.findViewById(res_id);
            (new CountDownTimer(milliseconds, interval) {
                public void onTick(long millisUntilFinished) {
                    textView2.setText(millisUntilFinished / 1000L + tick_part2);
                }

                public void onFinish() {
                    textView2.setText("OK");

                }
            }).start();
        }

    }

}
