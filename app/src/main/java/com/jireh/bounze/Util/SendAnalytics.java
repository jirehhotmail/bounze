package com.jireh.bounze.Util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.jireh.bounze.data.Analytics;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by vishnu on 25-12-2016.
 */

public class SendAnalytics {
    Context c;
     public SendAnalytics(Context c1, String userid, String description, String serviceid, String shopid, String viewedport, String viewtime, String clicked, String lat, String lon) {
        c = c1;


         if(isInternetOn()) {
             ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
             Call<Analytics> call = stationClient.analyticsdate(userid, description, serviceid, shopid, viewedport, viewtime, clicked,lat,lon);
             call.enqueue(new Callback<Analytics>() {
                 @Override
                 public void onResponse(Call<Analytics> call, Response<Analytics> response) {
                     int response_code = response.code();
                     Analytics result = response.body();

                     if (response_code == 200) {

                     } else if (response_code == 400) {

                         Log.e("analytics", "400 error");
                     } else if (response_code == 500) {

                         Log.e("analytics", "500 error");
                     }
                 }

                 @Override
                 public void onFailure(Call<Analytics> call, Throwable t) {

                     Log.d("Registration_eqnn", t + "");
                     Log.d("Registration_eqnn", call + "");

                 }
             });

         }
    }

    public final boolean isInternetOn()
    {
        ConnectivityManager connec = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        //ARE WE CONNECTED TO THE NET
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED )
        {
            //MESSAGE TO SCREEN FOR TESTING (IF REQ)
            //Toast.makeText(this, connectionType + � connected�, Toast.LENGTH_SHORT).show();
            return true;
        }
        else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  )
        {
            return false;
        }

        return false;
    }
}
