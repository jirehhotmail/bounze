package com.jireh.bounze.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jireh.bounze.data.BlogData;
import com.jireh.bounze.data.BookmarkPojo;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.Shop;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Shubham on 25-09-2017.
 */
public class SessionManager {
    public static final String PREF_NAME = "Bounze";
    public final String Pass_Result = "Pass_Result";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public PassArr[] getPass_Result() {
        PassArr[] passPojos;
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Pass_Result", "");
        if (json.isEmpty()) {
            passPojos = new PassArr[1];
        } else {
            Type type = new TypeToken<PassArr[]>() {
            }.getType();
            passPojos = gson.fromJson(json, type);
        }
        return passPojos;
    }

    public void setPass_Result(PassArr[] result) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(result);
        editor.putString("Pass_Result", jsonString);
        editor.commit();
    }

    public PassArr getSinglePass() {
        PassArr singlePass;
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Single_Pass", "");
        if (json.isEmpty()) {
            singlePass = new PassArr();
        } else {
            Type type = new TypeToken<PassArr>() {
            }.getType();
            singlePass = gson.fromJson(json, type);
        }
        return singlePass;
    }


    public void setSinglePass(PassArr Single_Pass) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(Single_Pass);
        editor.putString("Single_Pass", jsonString);
        editor.commit();
    }

    public ArrayList<BookmarkPojo> getBookmarkPass() {
        ArrayList<BookmarkPojo> bookmarkPassList;
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("Bookmark_Pass", "");
        if (jsonString.isEmpty()) {
            bookmarkPassList = new ArrayList<BookmarkPojo>();
        } else {
            Type type = new TypeToken<ArrayList<BookmarkPojo>>() {
            }.getType();
            bookmarkPassList = gson.fromJson(jsonString, type);
        }
        return bookmarkPassList;
    }

    public void setBookmarkBlog(ArrayList<BookmarkPojo> blogList) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(blogList);
        editor.putString("Bookmark_Blog", jsonString);
        editor.commit();
    }

    public ArrayList<BookmarkPojo> getBookmarkBlog() {
        ArrayList<BookmarkPojo> bookmarkBlogList;
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("Bookmark_Blog", "");
        if (jsonString.isEmpty()) {
            bookmarkBlogList = new ArrayList<BookmarkPojo>();
        } else {
            Type type = new TypeToken<ArrayList<BookmarkPojo>>() {
            }.getType();
            bookmarkBlogList = gson.fromJson(jsonString, type);
        }
        return bookmarkBlogList;
    }

    public void setBookmarkPass(ArrayList<BookmarkPojo> passList) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(passList);
        editor.putString("Bookmark_Pass", jsonString);
        editor.commit();
    }

    public ArrayList<Shop> getBookmarkShop() {
        ArrayList<Shop> bookmarkShopList;
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("Bookmark_Shop", "");
        if (jsonString.isEmpty()) {
            bookmarkShopList = new ArrayList<Shop>();
        } else {
            Type type = new TypeToken<ArrayList<Shop>>() {
            }.getType();
            bookmarkShopList = gson.fromJson(jsonString, type);
        }
        return bookmarkShopList;
    }

    public ArrayList<Coach> getBookmarkCoach() {
        ArrayList<Coach> bookmarkCoachList;
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("Bookmark_Coach", "");
        if (jsonString.isEmpty()) {
            bookmarkCoachList = new ArrayList<Coach>();
        } else {
            Type type = new TypeToken<ArrayList<Coach>>() {
            }.getType();
            bookmarkCoachList = gson.fromJson(jsonString, type);
        }
        return bookmarkCoachList;
    }

    public ArrayList<Event> getBookmarkEvent() {
        ArrayList<Event> bookmarkEventList;
        Gson gson = new Gson();
        String jsonString = sharedPreferences.getString("Bookmark_Event", "");
        if (jsonString.isEmpty()) {
            bookmarkEventList = new ArrayList<Event>();
        } else {
            Type type = new TypeToken<ArrayList<Event>>() {
            }.getType();
            bookmarkEventList = gson.fromJson(jsonString, type);
        }
        return bookmarkEventList;
    }
    public void setBookmarkEvent(ArrayList<Event> eventList) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(eventList);
        editor.putString("Bookmark_Event", jsonString);
        editor.commit();
    }
    public void setBookmarkShop(ArrayList<Shop> shopList) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(shopList);
        editor.putString("Bookmark_Shop", jsonString);
        editor.commit();
    }

    public void setBookmarkCoach(ArrayList<Coach> coachList) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(coachList);
        editor.putString("Bookmark_Coach", jsonString);
        editor.commit();
    }

    public String getUserId() {
        Log.d("sessionmanager", " userid: " + sharedPreferences.getString("userId", ""));
        return sharedPreferences.getString("userId", "");
    }

    public void setUserId(String userId) {
        Log.d("sessionmanager", " userid: " + userId);
        editor.putString("userId", userId);
        editor.commit();
    }

    public String getFCMId() {
        Log.d("sessionmanager", " fmcId: " + sharedPreferences.getString("fmcId", ""));
        return sharedPreferences.getString("fmcId", null);
    }

    public void setFCMId(String fmcId) {
        Log.d("sessionmanager", " fmcId: " + fmcId);
        editor.putString("fmcId", fmcId);
        editor.commit();
    }


    public BlogData[] getBlogResult() {
        BlogData[] blogPojos;
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Blog_Result", "");
        if (json.isEmpty()) {
            blogPojos = new BlogData[1];
        } else {
            Type type = new TypeToken<BlogData[]>() {
            }.getType();
            blogPojos = gson.fromJson(json, type);
        }
        return blogPojos;
    }

    public void setBlogResult(BlogData[] result) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(result);
        editor.putString("Blog_Result", jsonString);
        editor.commit();
    }
}
