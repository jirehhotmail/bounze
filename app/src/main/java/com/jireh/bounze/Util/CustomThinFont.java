package com.jireh.bounze.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muthamizhan C on 21-07-2017.
 */

public class CustomThinFont extends TextView {


    public CustomThinFont(Context context) {
        super(context);
        init();
    }

    public CustomThinFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomThinFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Thin.ttf");
        setTypeface(tf, 1);
    }


}
