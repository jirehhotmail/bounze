package com.jireh.bounze.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class CustomTFSemiBold extends TextView {


    public CustomTFSemiBold(Context context) {
        super(context);
        init();
    }

    public CustomTFSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTFSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-SemiBold.otf");
        setTypeface(tf, 1);
    }


}
