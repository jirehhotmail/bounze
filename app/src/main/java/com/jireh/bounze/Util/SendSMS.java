package com.jireh.bounze.Util;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;


public class SendSMS{

    // authentication key
    String authkey = "121913AmFcUhY157aad35c";
    String mobiles;
    // Sender ID,While using route4 sender id should be 6 characters long.
    String senderId = "Bounze";
    // Your message to send, Add URL endcoding here.
    String message = "";
    String message_txt = " is your login OTP. Treat this as confidential. Sharing it with anyone gives them full access to your Bounze Account including reward points. Bounze never calls you to verify your OTP.";
    // define route
    String route = "4";
    // Send SMS API
    String mainUrl = "https://control.msg91.com/sendhttp.php?";
    String encoded_message;
    URLConnection myURLConnection = null;
    URL myURL = null;
    BufferedReader reader = null;

    public String getAuthkey() {
        return authkey;
    }

    public void setAuthkey(String authkey) {
        this.authkey = authkey;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_txt() {
        return message_txt;
    }

    public void setMessage_txt(String message_txt) {
        this.message_txt = message_txt;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getMainUrl() {
        return mainUrl;
    }

    public void setMainUrl(String mainUrl) {
        this.mainUrl = mainUrl;
    }

    public String getEncoded_message() {
        return encoded_message;
    }

    public void setEncoded_message(String encoded_message) {
        this.encoded_message = encoded_message;
    }

    public void send_sms() {
        Send_SMS myTask = new Send_SMS();
        myTask.execute();
    }

    public class Send_SMS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // encoding message
            encoded_message = URLEncoder.encode(message);
            StringBuilder sbPostData = new StringBuilder(mainUrl);
            sbPostData.append("authkey=" + authkey);
            sbPostData.append("&mobiles=" + mobiles);
            sbPostData.append("&message=" + encoded_message);
            sbPostData.append("&route=" + route);
            sbPostData.append("&sender=" + senderId);
            mainUrl = sbPostData.toString();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected Void doInBackground(Void... params) {
            // do your work here
            // final string

            try {
                // prepare connection
                myURL = new URL(mainUrl);
                myURLConnection = myURL.openConnection();
                myURLConnection.connect();
                reader = new BufferedReader(new InputStreamReader(
                        myURLConnection.getInputStream()));

                // reading response
                String response;
                while ((response = reader.readLine()) != null)
                    // print response


                // finally close connection
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


        }

    }
}
