package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.PassHistoryPojo;

import java.util.List;

/**
 * Created by jireh_16 on 04-10-2017.
 */

public class PassHistoryAdapter  extends RecyclerView.Adapter<PassHistoryAdapter.ViewHolder> {
    Context context;
    List<PassHistoryPojo> passHistoryPojoList;

    public PassHistoryAdapter(Context context, List<PassHistoryPojo> passHistoryPojoList) {
        this.context = context;
        this.passHistoryPojoList = passHistoryPojoList;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pass_history_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        PassHistoryAdapter.ViewHolder vh = new PassHistoryAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        PassHistoryPojo passHistoryPojo = passHistoryPojoList.get(position);

        holder.tv_date.setText(passHistoryPojo.getDate());
        holder.tv_name.setText(passHistoryPojo.getPassName());
        holder.tv_service.setText(passHistoryPojo.getServiceName());
    }


    @Override
    public int getItemCount() {
        return passHistoryPojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_date, tv_name, tv_service;

        public ViewHolder(View view) {
            super(view);

            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_service = (TextView) view.findViewById(R.id.tv_service);
        }
    }
}
