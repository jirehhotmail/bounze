package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.data.FilterModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class FilterByCoachAdapter extends RecyclerView.Adapter<FilterByCoachAdapter.ViewHolder> {

    private final Set<Pair<Long, Long>> mCheckedItems = new HashSet<Pair<Long, Long>>();
    Activity activity;
    ArrayList<FilterModel> filterTitle;
    FilterByCoachAdapter.onItemClickListener listener;
    String groupPosition;
    DBFunctions dbFunctions;
    private int lastSelectedPosition = -1;
    private boolean onBind;
    private boolean calledOnce = true;

    public FilterByCoachAdapter(Activity activity, String groupPosition, ArrayList<FilterModel> filterTitle, FilterByCoachAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.filterTitle = filterTitle;
        this.groupPosition = groupPosition;
        this.listener = listener;
    }

    public FilterByCoachAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_by_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        FilterByCoachAdapter.ViewHolder vh = new FilterByCoachAdapter.ViewHolder(v);
        dbFunctions = new DBFunctions(activity);
        return vh;
    }


    @Override
    public void onBindViewHolder(final FilterByCoachAdapter.ViewHolder holder, final int position) {
        //set the heading to the textview based on scroll position and change the background color on click

        final FilterModel header = filterTitle.get(position);
        //based on the group position clicked get the amenities list and get location and get name
       if (groupPosition.equals("4")) {
            holder.filter_row_txt.setText(header.getLocation());
        } else {
            holder.filter_row_txt.setText(header.getName());
        }

        if (groupPosition.equals("2")) {
            onBind = true;
            //check if it is already check if checked make it check else make it false
            //and also allow check only one
            if (calledOnce == true) {
                if (header.isCheckIt()) {
                    holder.row_checkbox.setChecked(true);
                } else {
                    holder.row_checkbox.setChecked(false);

                }

            }
            holder.row_checkbox.setChecked(lastSelectedPosition == position);


            onBind = false;

        } else {
            onBind = true;
            //check if it is already check if checked make it check else make it false
            if (header.isCheckIt()) {
                holder.row_checkbox.setChecked(true);
            } else {
                holder.row_checkbox.setChecked(false);

            }
        }

        holder.row_checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final CheckBox cb = (CheckBox) v;

                if (cb.isChecked()) {
                    //get the name and get the index from it
                    //insert the details into filterdb if checked or not  based on the category selected and the values checked position.
                    if (groupPosition.equals("0") && (position == 0 || position == 1)) {
                        dbFunctions.insertintoFilterCDb(groupPosition, "1", String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::1" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("0") && (position == 2)) {
                        dbFunctions.insertintoFilterCDb(groupPosition, "1", String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::1" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("1")) {
                        dbFunctions.insertintoFilterCDb(groupPosition, "" + filterTitle.get(position).getSubServiceId(), String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::" + filterTitle.get(position).getSubServiceId() + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("2")) {
                        //get the value of the name checked
                        if (filterTitle.get(position).getName().equalsIgnoreCase("Low - High")) {
                            ArrayList<FilterModel> details = dbFunctions.getFilterCoachDetails("2");
                            if (details != null) {
                                dbFunctions.deleteFilterByCategoryCDB("2");
                                dbFunctions.insertintoFilterCDb(groupPosition, "ASC", String.valueOf(filterTitle.get(position).getPosition()));
                            } else {
                                dbFunctions.insertintoFilterCDb(groupPosition, "ASC", String.valueOf(filterTitle.get(position).getPosition()));
                            }

                        } else {
                            ArrayList<FilterModel> details = dbFunctions.getFilterCoachDetails("2");
                            if (details != null) {
                                dbFunctions.deleteFilterByCategoryCDB("2");
                                dbFunctions.insertintoFilterCDb(groupPosition, "DESC", String.valueOf(filterTitle.get(position).getPosition()));
                            } else {
                                dbFunctions.insertintoFilterCDb(groupPosition, "DESC", String.valueOf(filterTitle.get(position).getPosition()));
                            }
                        }


                        System.out.println("FilterAdapter::gp" + groupPosition + "value::ASC" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));

                    }  else if (groupPosition.equals("4")) {
                        dbFunctions.insertintoFilterCDb(groupPosition, filterTitle.get(position).getLocation(), String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + " value::" + filterTitle.get(position).getLocation() + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    }

                } else {

                    //delete the details from the filterdb if unchecked

                    if (groupPosition.equals("0") && (position == 0 || position == 1)) {
                        dbFunctions.deleteFilterCDB(groupPosition, "1", String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::-" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("0") && (position == 2)) {
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::-" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                        dbFunctions.deleteFilterCDB(groupPosition, "1", String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("1")) {
                        dbFunctions.deleteFilterCDB(groupPosition, "" + filterTitle.get(position).getSubServiceId(), String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + "value::-" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("4")) {
                        dbFunctions.deleteFilterCDB(groupPosition, filterTitle.get(position).getId(), String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + " value::-" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    } else if (groupPosition.equals("5")) {
                        dbFunctions.deleteFilterCDB(groupPosition, filterTitle.get(position).getLocation(), String.valueOf(filterTitle.get(position).getPosition()));
                        System.out.println("FilterAdapter::gp" + groupPosition + " value::-" + "position::" + String.valueOf(filterTitle.get(position).getPosition()));
                    }

                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return filterTitle.size();
    }

    // method to access in activity after updating selection

    public Set<Pair<Long, Long>> getCheckedItems() {
        return mCheckedItems;
    }

    public interface onItemClickListener {
        public void onItemClick(int view, String groupPosition, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox row_checkbox;

        TextView filter_row_txt;

        public ViewHolder(View view) {
            super(view);
            filter_row_txt = (TextView) view.findViewById(R.id.filter_row_txt);
            row_checkbox = (CheckBox) view.findViewById(R.id.row_checkbox);
            //get if it is bind or not if not bind then call notify data set changed else call it
            row_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!onBind) {

                        if (isChecked) {
                            lastSelectedPosition = getAdapterPosition();
                        } else {
                            lastSelectedPosition = -1;
                        }
                        notifyDataSetChanged();
                        calledOnce = false;
                    } else {
                        if (isChecked) {
                            lastSelectedPosition = getAdapterPosition();
                        }

                    }
                }
            });
        }
    }
}



