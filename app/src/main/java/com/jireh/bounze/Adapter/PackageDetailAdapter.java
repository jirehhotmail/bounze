package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Sessions;

import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class PackageDetailAdapter extends RecyclerView.Adapter<PackageDetailAdapter.ViewHolder> {

    Activity activity;
    List<Sessions> sessionList;
    PackageDetailAdapter.onItemClickListener listener;


    public PackageDetailAdapter(Activity activity, List<Sessions> sessionList, PackageDetailAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.sessionList = sessionList;
        this.listener = listener;
    }

    public PackageDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.session_time, parent, false);
        // set the view's size, margins, paddings and layout parameters
        PackageDetailAdapter.ViewHolder vh = new PackageDetailAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final PackageDetailAdapter.ViewHolder holder, final int position) {
        Sessions sessionTime = sessionList.get(position);


        holder.session_time_txt.setText(sessionTime.getNoOfDays() + " days ");

        holder.no_of_sessions_txt.setText(sessionTime.getSessions() + " sessions");
        //if session have offer amt then strike the cost and put the offer cost
        if (!sessionTime.getSessionsOfferCost().equals("0")) {
            holder.session_cost_txt.setTextColor(activity.getResources().getColor(R.color.red));
            holder.session_cost_txt.setText(activity.getResources().getString(R.string.Rs) + sessionTime.getSessionsCost());
            holder.session_cost_txt.setPaintFlags(holder.session_cost_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.session_offer_txt.setText(activity.getResources().getString(R.string.Rs) + sessionTime.getSessionsOfferCost());
            holder.session_offer_txt.setVisibility(View.VISIBLE);

        } else {
            holder.session_cost_txt.setText(sessionTime.getSessionsCost());
            holder.session_offer_txt.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return sessionList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button session_book;
        TextView session_time_txt, session_cost_txt, session_offer_txt, no_of_sessions_txt;

        public ViewHolder(View view) {
            super(view);
            session_book = (Button) view.findViewById(R.id.session_book);
            session_time_txt = (TextView) view.findViewById(R.id.session_time_txt);
            session_cost_txt = (TextView) view.findViewById(R.id.session_cost_txt);
            session_offer_txt = (TextView) view.findViewById(R.id.session_offer_txt);
            no_of_sessions_txt = (TextView) view.findViewById(R.id.no_of_sessions_txt);
        }
    }
}



