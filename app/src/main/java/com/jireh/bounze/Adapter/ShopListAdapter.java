package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.jireh.bounze.R;
import com.jireh.bounze.data.Shop;

import java.text.NumberFormat;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;


/**
 * Created by Muthamizhan C on 16-08-2017.
 */

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {

    Activity activity;
    List<Shop> shopsList;
    ShopListAdapter.onItemClickListener listener;
    private Animation animationUp, animationDown;
    private int lastSelectedPosition = -1;

    public ShopListAdapter(Activity activity, List<Shop> shopsList, ShopListAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.shopsList = shopsList;
        this.listener = listener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.onthego_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        animationDown = AnimationUtils.loadAnimation(activity, R.anim.slide_in_left);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Shop check1 = shopsList.get(position);

        try {
            if (check1.getSubservices() != null) {
                holder.tag1.setText(check1.getSubservices());
            }

            holder.title.setText("" + check1.getTitle());
            holder.title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
            holder.desc.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            holder.tag1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            //holder.rating.setText(check1.getAvgratings());
            holder.startingcost.setText(activity.getResources().getString(R.string.Rs) + " " + check1.getCost() + "");


            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(1);
            formatter.setMaximumFractionDigits(1);
            holder.shopdist.setText("" + (formatter.format(check1.getResults()[0] / 1000)) + " km");
            holder.ratingbar.setRating(Float.valueOf(check1.getAvgratings()));
            // holder.shopdist.setText(new DecimalFormat("##.##").format(check1.getdistanceonthego()) + " Km");


            holder.title.setText("" + check1.getTitle());
            holder.desc.setText("" + check1.getSmallAddress());

            Picasso.with(activity.getApplicationContext()).load(BASE_URL + check1.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.image);

            holder.shop_view_clayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(R.id.shop_view_clayout, position);
                }
            });

            if (check1.getIsOfferAvailable().equals("1")) {
                holder.offer_image.setVisibility(View.VISIBLE);

            } else {
                holder.offer_image.setVisibility(View.GONE);
            }
            //if the registration avialable show with bg color toolbar else strike it out
            if (check1.getIsRegnAvailable().equals("1")) {
                //    StateListDrawable tvBackground = (StateListDrawable)  holder.registration_txt.getBackground();
                //     tvBackground.setColorFilter(Color.parseColor("#1B76C9"), PorterDuff.Mode.SRC_ATOP);

                //  holder.registration_txt.setBackgroundColor(activity.getResources().getColor(R.color.toolbar_color));
                // holder.registration_txt.setTextColor(activity.getResources().getColor(R.color.white));
                holder.registration_txt.setVisibility(View.VISIBLE);

            } else {
                holder.registration_txt.setPaintFlags(holder.registration_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }
            if (check1.getIsFreeTrailAvailable().equals("1")) {
                holder.free_trial_txt.setVisibility(View.VISIBLE);
                //    holder.free_trial_txt.setBackgroundColor(activity.getResources().getColor(R.color.light_red));
            } else {
                holder.free_trial_txt.setVisibility(View.GONE);
                //     holder.free_trial_txt.setBackgroundColor(activity.getResources().getColor(R.color.grey));
            }

        } catch (Exception e) {
            holder.shopdist.setText("  ");
        }
        //get the position if it clicked then show the shop images and hide the detail layout else show


        //show shop images in recycler view based on shop position click
        holder.shop_images_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.clicked = true;
                holder.shop_detail_layout.setVisibility(View.INVISIBLE);
                holder.shop_image_layout.setVisibility(View.VISIBLE);
                //  activity.overridePendingTransition();
                holder.shop_image_layout.startAnimation(animationDown);

                LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
                holder.shop_view_recycler.setLayoutManager(manager);
                //stop moving when scroll on left or right side
                holder.shop_view_recycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        int action = e.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_MOVE:
                                rv.getParent().requestDisallowInterceptTouchEvent(true);
                                break;
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
                ShopImageAdapter adapter = new ShopImageAdapter(activity, check1, new ShopImageAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, int position) {

                    }
                });
                holder.shop_view_recycler.setAdapter(adapter);

            }
        });
        //is it is not clicked then sow the detail layout else hide it and show the shop image layout
        if (!check1.clicked) {
            holder.shop_detail_layout.setVisibility(View.VISIBLE);
            holder.shop_image_layout.setVisibility(View.INVISIBLE);

        } else {
            holder.shop_detail_layout.setVisibility(View.INVISIBLE);
            holder.shop_image_layout.setVisibility(View.VISIBLE);
            //  activity.overridePendingTransition();
            holder.shop_image_layout.startAnimation(animationDown);

            LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            holder.shop_view_recycler.setLayoutManager(manager);

            ShopImageAdapter adapter = new ShopImageAdapter(activity, check1, new ShopImageAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {

                }
            });
            holder.shop_view_recycler.setAdapter(adapter);
        }


    }


    @Override
    public int getItemCount() {
        return shopsList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, desc, shopdist, startingcost, tag1, tag2, registration_txt, free_trial_txt, pass_txt;
        ImageView image, offer_image, shop_images_img;
        RatingBar ratingbar;
        CardView shop_view_clayout;
        RecyclerView shop_view_recycler;
        LinearLayout shop_detail_layout, shop_image_layout;

        public ViewHolder(View view) {
            super(view);

            ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            image = (ImageView) view.findViewById(R.id.squareimage);
            title = (TextView) view.findViewById(R.id.shopname);
            //  rating = (TextView) view.findViewById(R.id.ratingtext);
            desc = (TextView) view.findViewById(R.id.shopdesc);
            shopdist = (TextView) view.findViewById(R.id.shopdist);
            startingcost = (TextView) view.findViewById(R.id.startingcost);
            tag1 = (TextView) view.findViewById(R.id.tag1);

            registration_txt = (TextView) view.findViewById(R.id.registration_txt);
            free_trial_txt = (TextView) view.findViewById(R.id.free_trial_txt);
            pass_txt = (TextView) view.findViewById(R.id.pass_txt);
            shop_view_clayout = (CardView) view.findViewById(R.id.shop_view_clayout);

            offer_image = (ImageView) view.findViewById(R.id.offer_image);
            shop_images_img = (ImageView) view.findViewById(R.id.shop_images_img);
            shop_view_recycler = (RecyclerView) view.findViewById(R.id.shop_view_recycler);

            shop_detail_layout = (LinearLayout) view.findViewById(R.id.shop_detail_layout);
            shop_image_layout = (LinearLayout) view.findViewById(R.id.shop_image_layout);


        }
    }
}



