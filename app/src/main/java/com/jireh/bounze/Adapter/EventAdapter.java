package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.jireh.bounze.R;
import com.jireh.bounze.data.Event;

import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    Activity activity;
    List<Event> eventsDetails;
    EventAdapter.onItemClickListener listener;


    public EventAdapter(Activity activity, List<Event> eventsDetails, EventAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.eventsDetails = eventsDetails;
        this.listener = listener;
    }

    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        EventAdapter.ViewHolder vh = new EventAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final EventAdapter.ViewHolder holder, final int position) {
        //get row values based on scrolling
        Event event = eventsDetails.get(position);
        //set the event image
        Picasso.with(activity).load(BASE_URL + "../admin/" + event.getImage()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.image);

        //set the title, details, startdate, enddate , location and rupees


        holder.title.setText("" + event.getName());
        holder.ratingbar.setRating(Float.valueOf(event.getAvgratings()));
        holder.event_type.setText("" + event.getEventType());
        holder.location_txt.setText("" + event.getLocation());
        holder.title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        holder.rupees.setText("Rs " + event.getCost() + "/-");
       //on click a row get the id and position of the layout clicked
        holder.event_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(R.id.event_layout, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return eventsDetails.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, event_type, location_txt, rupees;
        RatingBar ratingbar;
        CardView event_layout;

        public ViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            event_type = (TextView) view.findViewById(R.id.event_type);
            location_txt = (TextView) view.findViewById(R.id.location_txt);
            ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            event_layout = (CardView) view.findViewById(R.id.event_layout);
            rupees = (TextView) view.findViewById(R.id.rupees);
        }
    }
}



