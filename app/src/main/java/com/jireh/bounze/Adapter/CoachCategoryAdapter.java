package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 28-08-2017.
 */

public class CoachCategoryAdapter extends RecyclerView.Adapter<CoachCategoryAdapter.ViewHolder> {

    Activity activity;
    List<Category> categoryList;
    CoachCategoryAdapter.onItemClickListener listener;

    public CoachCategoryAdapter(Activity activity, List<Category> categoryList, CoachCategoryAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.categoryList = categoryList;
        this.listener = listener;
    }

    public CoachCategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coach_image, parent, false);
        // set the view's size, margins, paddings and layout parameters
        CoachCategoryAdapter.ViewHolder vh = new CoachCategoryAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final CoachCategoryAdapter.ViewHolder holder, final int position) {
        final Category check1 = categoryList.get(position);


        holder.coachcattext.setText("" + check1.getTitle());
        System.out.println("category image url"+check1.getImageUrl());
        Picasso.with(activity).load(BASE_URL + check1.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.v2u);
        holder.coach_category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           listener.onItemClick(R.id.coach_category_layout,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public interface onItemClickListener {
        void onItemClick(int item, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView v2u;
        TextView coachcattext;
        LinearLayout coach_category_layout;

        public ViewHolder(View view) {
            super(view);

            v2u = (ImageView) view.findViewById(R.id.coach_image);
            coachcattext = (TextView) view.findViewById(R.id.coachcattext);
            coach_category_layout = (LinearLayout) view.findViewById(R.id.coach_category_layout);

        }
    }
}



