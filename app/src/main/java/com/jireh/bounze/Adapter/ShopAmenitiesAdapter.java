package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Amenities;
import com.jireh.bounze.data.Shop;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 18-09-2017.
 */

public class ShopAmenitiesAdapter extends RecyclerView.Adapter<ShopAmenitiesAdapter.ViewHolder> {

    Activity activity;
    ShopAmenitiesAdapter.onItemClickListener listener;
    List<Amenities> amenities;

    public ShopAmenitiesAdapter(Activity activity, List<Amenities> amenities, onItemClickListener listener) {
        this.activity = activity;
        this.amenities = amenities;
        this.listener = listener;
    }

    public ShopAmenitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.amenities_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ShopAmenitiesAdapter.ViewHolder vh = new ShopAmenitiesAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ShopAmenitiesAdapter.ViewHolder holder, final int position) {
        Amenities amenity = amenities.get(position);

        Picasso.with(activity).load(BASE_URL + amenity.getIcon()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.amenity_image);
        holder.amenity_title.setText(amenity.getAmenities());
    }


    @Override
    public int getItemCount() {
        return amenities.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView amenity_image;
        TextView amenity_title;

        public ViewHolder(View view) {
            super(view);
            amenity_image = (ImageView) view.findViewById(R.id.amenity_image);
            amenity_title = (TextView) view.findViewById(R.id.amenity_title);
        }
    }
}



