package com.jireh.bounze.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.CurrentPassBookingPojo;

import java.util.ArrayList;

import at.grabner.circleprogress.CircleProgressView;

/**
 * <h2>CurrentPassGridAdapter</h2>
 * This is an Adapter class that is used for showing all the current pass booking
 * related data into a circular progress bar view and this will be called by
 * @see com.jireh.bounze.Activity.CurrentPassBooking class.
 * @author  Shubham
 * @since 03-10-2017.
 */

public class CurrentPassGridAdapter extends BaseAdapter{

    private static final String TAG = "CurrentPassGridAdapter";
    private Context context;
    private ArrayList <CurrentPassBookingPojo> passBookingPojos;
    private CurrentPassBookingPojo singlePassData;
    private LayoutInflater inflater;
    private ViewHolder holder;

    public CurrentPassGridAdapter(Context context, ArrayList <CurrentPassBookingPojo> passBookingPojos)
    {
        this.context = context;
        this.passBookingPojos = passBookingPojos;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return passBookingPojos.size();
    }

    @Override
    public CurrentPassBookingPojo getItem(int position) {
        return passBookingPojos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        singlePassData = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_current_pass_grid, null);
            holder = new ViewHolder();
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_session = (TextView) convertView.findViewById(R.id.tv_session);
            holder.circleView = (CircleProgressView) convertView.findViewById(R.id.circleView);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_name.setText(singlePassData.getServiceName());
        holder.tv_session.setText(singlePassData.getRemainingSessionCount()+"/"+singlePassData.getTotalSessionCount());
        //value setting.
        holder.circleView.setMaxValue(Integer.parseInt(singlePassData.getTotalSessionCount()));
        holder.circleView.setValue(Integer.parseInt(singlePassData.getRemainingSessionCount()));

        return convertView;
    }

    private class ViewHolder {
        CircleProgressView circleView;
        TextView tv_name, tv_session;
    }
}
