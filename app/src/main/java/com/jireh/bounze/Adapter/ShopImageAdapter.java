package com.jireh.bounze.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Shop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 19-08-2017.
 */

public class ShopImageAdapter extends RecyclerView.Adapter<ShopImageAdapter.ViewHolder> {

    final static long REFRESH_TIME = 100; // miliseconds
    final Handler handler = new Handler();
    Activity activity;
    Shop shop;
    ShopImageAdapter.onItemClickListener listener;
    List myList = new ArrayList();

    Runnable runnable;

    public ShopImageAdapter(Activity activity, Shop shop, ShopImageAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.shop = shop;
        this.listener = listener;
        //seperate the values of image url base on the comma
        String strings[] = shop.getImageUrlRectangle().split(",");
        //convert the string array to arraylist
        for (int i = 0; i < strings.length; i++) {
            //   strings[i] = strings[i].trim();
            Collections.addAll(myList, strings[i].trim());
        }


    }

    public ShopImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_image_row, parent, false);

        View view = activity.getWindow().getDecorView();
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        handler.post(runnable);
                        break;

                    case MotionEvent.ACTION_UP:
                        handler.removeCallbacks(runnable);
                        break;
                }

                return false;
            }
        });
        // set the view's size, margins, paddings and layout parameters
        ShopImageAdapter.ViewHolder vh = new ShopImageAdapter.ViewHolder(v);
        return vh;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //set the images in the adapter


        Picasso.with(activity).load(BASE_URL + myList.get(position).toString()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.image);
        Thread t = new Thread()
        {
            public void run()
            {
                try{
                    sleep(3000);
                }catch(InterruptedException ie)
                {
                    ie.printStackTrace();
                }finally
                {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            holder.shop_image_layout.setVisibility(View.GONE);
                            holder.shop_detail_layout.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }
        }; t.start();

        //gone the layout when clikc the shop image layout
        holder.shop_images_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the existing layout is clicked
                holder.shop_image_layout.setVisibility(View.GONE);
                holder.shop_detail_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return myList.size();
    }


    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image, shop_images_img;
        LinearLayout shop_image_layout, shop_detail_layout;

        public ViewHolder(View view) {
            super(view);


            image = (ImageView) view.findViewById(R.id.shop_round_image);
            shop_images_img = (ImageView) view.findViewById(R.id.shop_images_img);
            shop_image_layout = (LinearLayout) activity.findViewById(R.id.shop_image_layout);
            shop_detail_layout = (LinearLayout) activity.findViewById(R.id.shop_detail_layout);
            runnable = new Runnable() {
                @Override
                public void run() {
                    shop_image_layout.setVisibility(View.INVISIBLE);
                    shop_detail_layout.setVisibility(View.VISIBLE);

                    handler.postDelayed(this, REFRESH_TIME);
                }
            };
        }
    }

}



