package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jireh.bounze.R;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {
    int row_index = 0;
    Activity activity;
    ArrayList<String> filterTitle;
    FilterAdapter.onItemClickListener listener;


    public FilterAdapter(Activity activity, ArrayList<String> filterTitle, FilterAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.filterTitle = filterTitle;
        this.listener = listener;
    }

    public FilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        FilterAdapter.ViewHolder vh = new FilterAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final FilterAdapter.ViewHolder holder, final int position) {
        //set the heading to the textview based on scroll position and selected postion bg color and text color change
        holder.filter_row_txt.setText(filterTitle.get(position));

        //set on click listner for the filter row and send the clicked position to listener
        holder.filter_heading_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index = position;
                listener.onItemClick(R.id.filter_row_txt, position);
                notifyDataSetChanged();
            }
        });
        if (row_index == position) {
            holder.filter_heading_row.setBackgroundColor(activity.getResources().getColor(R.color.light_dark_blue));
            holder.filter_row_txt.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.filter_heading_row.setBackgroundColor(activity.getResources().getColor(R.color.light_blue));
            holder.filter_row_txt.setTextColor(Color.parseColor("#000000"));
        }
    }


    @Override
    public int getItemCount() {
        return filterTitle.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView filter_row_txt;
        LinearLayout filter_heading_row;

        public ViewHolder(View view) {
            super(view);
            filter_row_txt = (TextView) view.findViewById(R.id.filter_row_txt);
            filter_heading_row = (LinearLayout) view.findViewById(R.id.filter_heading_row);
        }
    }
}



