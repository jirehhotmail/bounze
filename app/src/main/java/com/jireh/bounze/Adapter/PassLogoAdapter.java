package com.jireh.bounze.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.jireh.bounze.R;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 29-08-2017.
 */

public class PassLogoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<String> mDataList;
    private Context context;

    public PassLogoAdapter(Context context, ArrayList<String> mDataList) {
        this.context = context;
        this.mDataList = mDataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.pass_logo_layout, parent, false);
        ItemViewHolder holder = new ItemViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rawHolder, int position) {

        ItemViewHolder holder = (ItemViewHolder) rawHolder;

        Picasso.with(context)
                .load(BASE_URL + mDataList.get(position))
                .placeholder(R.drawable.loadingcoa)
                .error(R.drawable.noimagefound)
                .into(holder.logo_image);

        holder.itemView.setTag(position);
    }


    @Override
    public int getItemCount() {
        return (null != mDataList ? mDataList.size() : 0);
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView logo_image;

        public ItemViewHolder(View itemView) {
            super(itemView);
            logo_image = (ImageView) itemView.findViewById(R.id.horizontal_item_text);
        }
    }
}

