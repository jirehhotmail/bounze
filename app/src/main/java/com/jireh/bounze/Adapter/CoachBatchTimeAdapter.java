package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.CoachBatchTimeDetail;

import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class CoachBatchTimeAdapter extends RecyclerView.Adapter<CoachBatchTimeAdapter.ViewHolder> {
    int row_index = -1;
    Activity activity;
    List<CoachBatchTimeDetail> batchList;
    CoachBatchTimeAdapter.onItemBatchClickListener listener;


    public CoachBatchTimeAdapter(Activity activity, List<CoachBatchTimeDetail> batchList, onItemBatchClickListener listener) {
        this.activity = activity;
        this.batchList = batchList;
        this.listener = listener;
    }

    public CoachBatchTimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_time, parent, false);
        // set the view's size, margins, paddings and layout parameters
        CoachBatchTimeAdapter.ViewHolder vh = new CoachBatchTimeAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final CoachBatchTimeAdapter.ViewHolder holder, final int position) {
        CoachBatchTimeDetail batchtime = batchList.get(position);

        holder.batch_name.setText(batchtime.getCoachBatchName());
        holder.batch_time_txt.setText(batchtime.getStartTime() + " - " + batchtime.getEndTime());

        //change the background color when selected
        //set batch time listener for the time selected
        holder.batch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                listener.onItemBatchClick(holder.batch_layout, position, activity);
                notifyDataSetChanged();
            }
        });


        if (row_index == position) {
            holder.batch_layout.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.outer_selected_box));
            holder.batch_name.setTextColor(Color.parseColor("#ffffff"));

            holder.batch_time_txt.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.batch_layout.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.outer_unselected_box));
            holder.batch_name.setTextColor(Color.parseColor("#000000"));

            holder.batch_time_txt.setTextColor(Color.parseColor("#000000"));
        }
    }


    @Override
    public int getItemCount() {
        return batchList.size();
    }

    public interface onItemBatchClickListener {
        public void onItemBatchClick(CardView batch_layout, int position, Activity activity);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView batch_time_txt, batch_name;
        CardView batch_layout;

        public ViewHolder(View view) {
            super(view);

            batch_name = (TextView) view.findViewById(R.id.batch_name);
            batch_time_txt = (TextView) view.findViewById(R.id.batch_time_txt);
            batch_layout = (CardView) view.findViewById(R.id.batch_layout);


        }
    }
}



