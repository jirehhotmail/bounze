package com.jireh.bounze.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.Shop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 19-08-2017.
 */

public class CoachImageAdapter extends RecyclerView.Adapter<CoachImageAdapter.ViewHolder> {

    final static long REFRESH_TIME = 100; // miliseconds
    final Handler handler = new Handler();
    Activity activity;
    Coach coach;
    CoachImageAdapter.onItemClickListener listener;
    List myList = new ArrayList();

    Runnable runnable;

    public CoachImageAdapter(Activity activity, Coach coach, CoachImageAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.coach = coach;
        this.listener = listener;
        //seperate the values of image url base on the comma
        String strings[] = coach.getImageUrlRectangle().split(",");
        //convert the string array to arraylist
        for (int i = 0; i < strings.length; i++) {
            //   strings[i] = strings[i].trim();
            Collections.addAll(myList, strings[i].trim());
        }


    }

    public CoachImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_image_row, parent, false);

        View view = activity.getWindow().getDecorView();
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        handler.post(runnable);
                        break;

                    case MotionEvent.ACTION_UP:
                        handler.removeCallbacks(runnable);
                        break;
                }

                return false;
            }
        });
        // set the view's size, margins, paddings and layout parameters
        CoachImageAdapter.ViewHolder vh = new CoachImageAdapter.ViewHolder(v);
        return vh;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //set the images in the adapter


        Picasso.with(activity).load(BASE_URL + myList.get(position).toString()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.image);
        Thread t = new Thread()
        {
            public void run()
            {
                try{
                    sleep(3000);
                }catch(InterruptedException ie)
                {
                    ie.printStackTrace();
                }finally
                {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            holder.coach_image_layout.setVisibility(View.GONE);
                            holder.coach_detail_layout.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }
        }; t.start();

        //gone the layout when clikc the shop image layout
        holder.coach_images_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the existing layout is clicked
                holder.coach_image_layout.setVisibility(View.GONE);
                holder.coach_detail_layout.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return myList.size();
    }


    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image, coach_images_img;
        LinearLayout coach_image_layout, coach_detail_layout;

        public ViewHolder(View view) {
            super(view);


            image = (ImageView) view.findViewById(R.id.shop_round_image);
            coach_images_img = (ImageView) view.findViewById(R.id.shop_images_img);
            coach_image_layout = (LinearLayout) activity.findViewById(R.id.coach_image_layout);
            coach_detail_layout = (LinearLayout) activity.findViewById(R.id.coach_detail_layout);
            runnable = new Runnable() {
                @Override
                public void run() {
                    coach_image_layout.setVisibility(View.INVISIBLE);
                    coach_detail_layout.setVisibility(View.VISIBLE);

                    handler.postDelayed(this, REFRESH_TIME);
                }
            };
        }
    }

}



