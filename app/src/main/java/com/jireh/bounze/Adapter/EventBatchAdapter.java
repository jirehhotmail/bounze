package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.BatchTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class EventBatchAdapter extends RecyclerView.Adapter<EventBatchAdapter.ViewHolder> {
    int row_index = -1;
    Activity activity;

    List<BatchTime> batchDetails;
    EventBatchAdapter.onItemBatchClickListener listener;


    public EventBatchAdapter(Activity activity, List<BatchTime> batchDetails, onItemBatchClickListener listener) {
        this.activity = activity;
        this.batchDetails = batchDetails;
        this.listener = listener;

    }

    public EventBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_batch_time, parent, false);
        // set the view's size, margins, paddings and layout parameters
        EventBatchAdapter.ViewHolder vh = new EventBatchAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final EventBatchAdapter.ViewHolder holder, final int position) {
        BatchTime batchTime = batchDetails.get(position);
        holder.batch_name.setText(batchTime.getBatchName());

        holder.batch_date_txt.setText(changeDateFormat(batchTime.getStartDate()) + " to " +
                changeDateFormat(batchTime.getEndDate()));
        holder.batch_time_txt.setText(batchTime.getStartTime() + " to " + batchTime.getEndTime());

        //change the background color when selected
        //set batch time listener for the time selected
        holder.batch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                listener.onItemBatchClick(holder.batch_layout, position, activity);
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {
            holder.batch_layout.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.outer_selected_box));
            holder.batch_name.setTextColor(Color.parseColor("#ffffff"));
            holder.batch_date_txt.setTextColor(Color.parseColor("#ffffff"));
            holder.batch_time_txt.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.batch_layout.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.outer_unselected_box));
            holder.batch_name.setTextColor(Color.parseColor("#000000"));
            holder.batch_date_txt.setTextColor(Color.parseColor("#000000"));
            holder.batch_time_txt.setTextColor(Color.parseColor("#000000"));
        }


    }


        private String changeDateFormat(String time) {

            String inputPattern = "dd/MM/yyyy";
            String outputPattern = "dd/MM/yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;



    }


    @Override
    public int getItemCount() {
        return batchDetails.size();
    }

    public interface onItemBatchClickListener {
        public void onItemBatchClick(CardView batch_layout, int position, Activity activity);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView batch_date_txt, batch_time_txt, batch_name;
        CardView batch_layout;


        public ViewHolder(View view) {
            super(view);

            batch_name = (TextView) view.findViewById(R.id.batch_name);
            batch_date_txt = (TextView) view.findViewById(R.id.batch_date_txt);
            batch_time_txt = (TextView) view.findViewById(R.id.batch_time_txt);
            batch_layout = (CardView) view.findViewById(R.id.batch_layout);


        }
    }
}



