package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jireh.bounze.Activity.DisplayImage;
import com.jireh.bounze.R;
import com.jireh.bounze.data.UserProfile;

import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class HealthTrackingAdapter extends RecyclerView.Adapter<HealthTrackingAdapter.ViewHolder> {


    Activity activity;
    List<UserProfile> healthTrackings;
    HealthTrackingAdapter.onItemClickListener listener;


    public HealthTrackingAdapter(Activity activity, List<UserProfile> healthTrackings, HealthTrackingAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.healthTrackings = healthTrackings;
        this.listener = listener;
    }

    public HealthTrackingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_tracking_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        HealthTrackingAdapter.ViewHolder vh = new HealthTrackingAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final HealthTrackingAdapter.ViewHolder holder, final int position) {
        final UserProfile healthTracking = healthTrackings.get(position);
        //Set the values to the views
        holder.serial_no.setText(String.valueOf(position + 1));
        holder.height_txt.setText(String.valueOf(healthTracking.getHeight()));
        holder.weight_txt.setText(healthTracking.getWeight());
        holder.date_entry_txt.setText(healthTracking.getUpdateDate());
        //if the image is available set on click listener else make it - and don't set onclick
        if(healthTracking.getPicture().equals(""))
        {
            holder.viewImage_txt.setText("-");
        }
        else
        {
            holder.viewImage_txt.setText("View Image");
            holder.viewImage_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(activity, DisplayImage.class);
                    in.putExtra("profileImage", healthTracking.getPicture());
                    in.putExtra("ImageFlag","HealthTrack");
                    activity.startActivity(in);
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return healthTrackings.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView serial_no, height_txt, weight_txt, date_entry_txt, viewImage_txt;

        public ViewHolder(View view) {
            super(view);
            viewImage_txt = (TextView) view.findViewById(R.id.viewImage_txt);
            serial_no = (TextView) view.findViewById(R.id.serial_no);
            height_txt = (TextView) view.findViewById(R.id.height_txt);
            weight_txt = (TextView) view.findViewById(R.id.weight_txt);
            date_entry_txt = (TextView) view.findViewById(R.id.date_entry_txt);


        }
    }
}



