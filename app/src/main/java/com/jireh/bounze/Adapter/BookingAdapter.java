package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Event;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 28-08-2017.
 */

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> {
    Activity activity;
    List<Bookhistory> listdata;
    BookingAdapter.onItemClickListener listener;

    List<Event> eventlist;


    public BookingAdapter(Activity activity, List<Bookhistory> listdata, List<Event> eventlist, BookingAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.listdata = listdata;
        this.listener = listener;
        this.eventlist = eventlist;
    }

    public BookingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookinghistory, parent, false);


        // set the view's size, margins, paddings and layout parameters
        BookingAdapter.ViewHolder vh = new BookingAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final BookingAdapter.ViewHolder holder, final int position) {

        Bookhistory check = listdata.get(position);
        holder.booking_history_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(R.id.shop_view_clayout, position);
            }
        });
        //if the booking type is 3 get the event start and end date else get the shop or trainer booking start date and end date
        if (listdata.get(position).getBookingType().equals("3")) {
            holder.shopname.setText("" + check.getTitle());
            holder.period_txt.setText(check.getEventStartDate() + " to " + check.getEventEndDate());
            //if the booking type is pay per use don't show the end date for shop
        } else if (listdata.get(position).getBookingType().equals("5")) {
            holder.shopname.setText("" + check.getShop_name());
            holder.period_txt.setText(check.getStartDate());
        }
        //if the booking type is pay per use don't show the end date for coach
        else if (listdata.get(position).getBookingType().equals("7")) {
            holder.shopname.setText("" + check.getShop_name());
            holder.period_txt.setText(check.getStartDate());
        } else {
            holder.shopname.setText("" + check.getShop_name());
            holder.period_txt.setText(check.getStartDate() + " to " + check.getEndDate());
        }

        //if the cost is zero make it free trial else show the cost
        if (check.getCost().equals("0") || check.getCost().equals("")) {
            holder.startingcost.setText("Free Trial");
        } else {
            holder.startingcost.setText(activity.getResources().getString(R.string.Rs) + " " + check.getCost());
        }
// if the booking type is shop ftrial,ppc or session get the shop image
        if (("" + check.getBookingType()).equals("1") || check.getBookingType().equals("4") || check.getBookingType().equals("5"))

            Picasso.with(activity).load(BASE_URL + check.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.squareimage);
// if the booking type is coach ftrial,ppc or session get the coach image
        else if (("" + check.getBookingType()).equals("2") || check.getBookingType().equals("6") || check.getBookingType().equals("7")) {
            Picasso.with(activity).load(BASE_URL + check.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.squareimage);
        } else if (("" + check.getBookingType()).equals("3")) {
            holder.squareimage.setImageResource(R.drawable.event_image);
        }


    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public interface onItemClickListener {
        void onItemClick(int item, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView squareimage;
        TextView shopname, shopdesc, tag1, tag2, startingcost, shopdist, period_txt;
        RatingBar ratingbar;
        CardView booking_history_layout;

        public ViewHolder(View view) {
            super(view);
            squareimage = (ImageView) view.findViewById(R.id.squareimage);
            shopname = (TextView) view.findViewById(R.id.shopname);
            shopdesc = (TextView) view.findViewById(R.id.shopdesc);
            tag1 = (TextView) view.findViewById(R.id.tag1);

            startingcost = (TextView) view.findViewById(R.id.startingcost);
            shopdist = (TextView) view.findViewById(R.id.shopdist);
            ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            period_txt = (TextView) view.findViewById(R.id.period_txt);
            booking_history_layout = (CardView) view.findViewById(R.id.shop_view_clayout);
        }
    }
}



