package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Coach;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 28-08-2017.
 */

public class CoachAdapter extends RecyclerView.Adapter<CoachAdapter.ViewHolder> {

    Activity activity;
    List<Coach> coachList;
    CoachAdapter.onItemClickListener listener;
    private Animation animationUp, animationDown;

    public CoachAdapter(Activity activity, List<Coach> coachList,CoachAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.coachList = coachList;
        this.listener =listener;

    }

    public CoachAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coach_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        CoachAdapter.ViewHolder vh = new CoachAdapter.ViewHolder(v);
        animationDown = AnimationUtils.loadAnimation(activity, R.anim.slide_in_left);
        return vh;
    }


    @Override
    public void onBindViewHolder(final CoachAdapter.ViewHolder holder, final int position) {
        final Coach check1 = coachList.get(position);


        try {
           /* if (!check1.getSubservices().equals("") && !check1.getTagtwo().equals("")) {
                holder.tag1.setText(check1.getTagone() + " , " + check1.getTagtwo());
            } else if (!check1.getTagone().equals("") && check1.getTagtwo().equals("")) {
                holder.tag1.setText(check1.getTagone());
            } else if (check1.getTagone().equals("") && !check1.getTagtwo().equals("")) {
                holder.tag1.setText(check1.getTagtwo());
            }*/
            if (check1.getCategory() != null) {
                holder.tag1.setText(check1.getCategory());
            }
            holder.title.setText("" + check1.getTitle());
            holder.title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
            holder.desc.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            holder.tag1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            //holder.rating.setText(check1.getAvgratings());
            holder.startingcost.setText(activity.getResources().getString(R.string.Rs) + " " + check1.getCost() + "");


            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(1);
            formatter.setMaximumFractionDigits(1);
            holder.coachdist.setText("" + (formatter.format(check1.getResults()[0] / 1000)) + " km");
            holder.ratingbar.setRating(Float.valueOf(check1.getAvgratings()));
            // holder.shopdist.setText(new DecimalFormat("##.##").format(check1.getdistanceonthego()) + " Km");


            holder.title.setText("" + check1.getTitle());
            holder.desc.setText("" + check1.getSmallAddress());

            Picasso.with(activity.getApplicationContext()).load(BASE_URL + check1.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.image);

            holder.coach_row_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(R.id.card_view, position);
                }
            });


            if (check1.getIsFreeTrailAvailable().equals("1")) {
                holder.free_trial_txt.setVisibility(View.VISIBLE);
                //    holder.free_trial_txt.setBackgroundColor(activity.getResources().getColor(R.color.light_red));
            } else {
                holder.free_trial_txt.setVisibility(View.GONE);
                //     holder.free_trial_txt.setBackgroundColor(activity.getResources().getColor(R.color.grey));
            }

        } catch (Exception e) {
            holder.coachdist.setText("  ");
        }


        //show shop images in recycler view based on shop position click
        holder.coach_images_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.clicked = true;
                holder.coach_detail_layout.setVisibility(View.INVISIBLE);
                holder.coach_image_layout.setVisibility(View.VISIBLE);
                //  activity.overridePendingTransition();
                holder.coach_image_layout.startAnimation(animationDown);

                LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
                holder.coach_view_recycler.setLayoutManager(manager);
                //stop moving when scroll on left or right side
                holder.coach_view_recycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        int action = e.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_MOVE:
                                rv.getParent().requestDisallowInterceptTouchEvent(true);
                                break;
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
                CoachImageAdapter adapter = new CoachImageAdapter(activity, check1, new CoachImageAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, int position) {

                    }
                });
                holder.coach_view_recycler.setAdapter(adapter);

            }
        });
        //is it is not clicked then sow the detail layout else hide it and show the shop image layout
        if (!check1.clicked) {
            holder.coach_detail_layout.setVisibility(View.VISIBLE);
            holder.coach_image_layout.setVisibility(View.INVISIBLE);
            check1.clicked = false;
        } else {
            holder.coach_detail_layout.setVisibility(View.INVISIBLE);
            holder.coach_image_layout.setVisibility(View.VISIBLE);
            //  activity.overridePendingTransition();
            holder.coach_image_layout.startAnimation(animationDown);

            LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            holder.coach_view_recycler.setLayoutManager(manager);

            CoachImageAdapter adapter = new CoachImageAdapter(activity, check1, new CoachImageAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {

                }
            });
            holder.coach_view_recycler.setAdapter(adapter);
        }

    }


    @Override
    public int getItemCount() {
        return coachList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, desc, coachdist, startingcost, tag1, tag2, free_trial_txt;
        ImageView image, coach_images_img;
        RatingBar ratingbar;
        CardView coach_row_layout;
        RecyclerView coach_view_recycler;
        LinearLayout coach_detail_layout, coach_image_layout;

        public ViewHolder(View view) {
            super(view);

            ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
            image = (ImageView) view.findViewById(R.id.squareimage);
            coach_images_img = (ImageView) view.findViewById(R.id.coach_images_img);
            title = (TextView) view.findViewById(R.id.coachname);
            desc = (TextView) view.findViewById(R.id.coachdesc);
            coachdist = (TextView) view.findViewById(R.id.coachdist);
            startingcost = (TextView) view.findViewById(R.id.startingcost);
            tag1 = (TextView) view.findViewById(R.id.tag1);

            free_trial_txt = (TextView) view.findViewById(R.id.free_trial_txt);
            coach_row_layout = (CardView) view.findViewById(R.id.coach_view_clayout);
            coach_detail_layout = (LinearLayout) view.findViewById(R.id.coach_detail_layout);
            coach_image_layout = (LinearLayout) view.findViewById(R.id.coach_image_layout);
            coach_view_recycler = (RecyclerView) view.findViewById(R.id.coach_view_recycler);
        }
    }

}



