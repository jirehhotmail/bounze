package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.net.ParseException;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 19-08-2017.
 */

public class MyReviewsAdapter extends RecyclerView.Adapter<MyReviewsAdapter.ViewHolder> {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Activity activity;
    List<Review_> reviewList;
    MyReviewsAdapter.onItemClickListener listener;
    Date currentDate;

    public MyReviewsAdapter(Activity activity, List<Review_> reviewList, Date currentDate, onItemClickListener listener) {
        this.activity = activity;
        this.reviewList = reviewList;
        this.listener = listener;
        this.currentDate = currentDate;

    }

    public MyReviewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_reviews_row, parent, false);


        // set the view's size, margins, paddings and layout parameters
        MyReviewsAdapter.ViewHolder vh = new MyReviewsAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //set the values to the view from the arraylist
        final Review_ review = reviewList.get(position);
        holder.category.setText(review.getCategory());
        holder.set_name.setText(review.getCategoryName());
        holder.created_date.setText(review.getCreatedDate());
        holder.ratingBar.setRating(Float.valueOf(review.getRating()));
        holder.reviews.setText(review.getReview());
        holder.rating_status.setText(review.getRatingStatus());
        //set on click listener for edit and delete button
        holder.edit_review_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //On click edit button show the right button which is for submit
                //set enable the edit

                //convert the reviewed date to date format and add two days to get end date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date d = null;
                try {
                    d = sdf.parse(review.getCreatedDate());
                } catch (ParseException ex) {

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);

                calendar.add(Calendar.DATE, 2);

                Date endDate = calendar.getTime();

                //if the end date is after current date you can't edit otherwise you can edit
                if (currentDate.after(endDate)) {
                    holder.edit_review_img.setEnabled(false);
                    Toast.makeText(activity, "You can't review after two days", Toast.LENGTH_SHORT).show();
                } else {
                    holder.edit_review_img.setEnabled(true);
                    holder.ok_img.setVisibility(View.VISIBLE);
                    holder.reviews.setClickable(true);
                }



            }
        });
        holder.ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.delete_review_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //service call to delete
                ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                Call<Review> call = stationClient.deleteMyReviews(Prefs.getString(AppConstants.UserId, ""),
                        "delete", String.valueOf(review.getId()));
                call.enqueue(new Callback<Review>() {
                    @Override
                    public void onResponse(final Call<Review> call, Response<Review> response) {
                        int response_code = response.code();

                        if (response_code == 200) {
                            Toast.makeText(activity, "Successully deleted", Toast.LENGTH_SHORT).show();
                            reviewList.remove(position);
                            notifyItemRemoved(position);
                            //this line below gives you the animation and also updates the
                            //list items after the deleted item
                            notifyItemRangeChanged(position, getItemCount());

                        }

                    }

                    @Override
                    public void onFailure(Call<Review> call, Throwable t) {
                        Log.d("Registration_eqnn", t + "");
                        Log.d("Registration_eqnn", call + "");

                    }
                });
            }
        });

        holder.ok_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //service call to edit
                holder.ok_img.setVisibility(View.GONE);

                ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                Call<Review> call = stationClient.editMyReviews(Prefs.getString(AppConstants.UserId, ""),
                        "edit", String.valueOf(review.getId()), holder.reviews.getText().toString(), String.valueOf(holder.ratingBar.getRating()));
                call.enqueue(new Callback<Review>() {
                    @Override
                    public void onResponse(final Call<Review> call, Response<Review> response) {
                        int response_code = response.code();
                        Review result = response.body();
                        if (response_code == 200) {
                            //on successfull edit call the adpater to refresh
                            listener.onItemClick(R.id.ok_img, position);
                            Toast.makeText(activity, "Successully edited", Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onFailure(Call<Review> call, Throwable t) {
                        Log.d("Registration_eqnn", t + "");
                        Log.d("Registration_eqnn", call + "");

                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(int view, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //declare the values and initialize
        TextView category, set_name, created_date, rating_status;
        EditText reviews;
        RatingBar ratingBar;
        ImageView edit_review_img, delete_review_img, ok_img;

        public ViewHolder(View view) {
            super(view);
            category = (TextView) view.findViewById(R.id.category);
            set_name = (TextView) view.findViewById(R.id.set_name);
            created_date = (TextView) view.findViewById(R.id.created_date);
            reviews = (EditText) view.findViewById(R.id.reviews);
            rating_status = (TextView) view.findViewById(R.id.rating_status);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            edit_review_img = (ImageView) view.findViewById(R.id.edit_review_img);
            delete_review_img = (ImageView) view.findViewById(R.id.delete_review_img);
            ok_img = (ImageView) view.findViewById(R.id.ok_img);
        }
    }
}



