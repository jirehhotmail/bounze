package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Review_;

import java.util.List;

/**
 * Created by jireh_16 on 28-12-2017.
 */

public class AllreviewsAdapter extends RecyclerView.Adapter<AllreviewsAdapter.ReviewHolder> {

    Activity activity;

    List<Review_> reviews;

    public AllreviewsAdapter(Activity activity, List<Review_> reviews) {
        this.activity = activity;
        this.reviews = reviews;

    }

    //INITIALIE VH
    @Override
    public AllreviewsAdapter.ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_reviews, parent, false);
        AllreviewsAdapter.ReviewHolder holder = new AllreviewsAdapter.ReviewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(AllreviewsAdapter.ReviewHolder holder, int position) {
        Review_ review = reviews.get(position);
        //if ratings is not null then set the ratings
        try {
            holder.ratingBar.setRating(Float.valueOf(review.getRating()));
            holder.person_name.setText(review.getUserName());
            holder.reviews_text.setText(review.getReview());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public class ReviewHolder extends RecyclerView.ViewHolder {
        CardView all_review_layout;
        RatingBar ratingBar;
        TextView person_name, reviews_text;

        public ReviewHolder(View view) {
            super(view);
            all_review_layout = (CardView) view.findViewById(R.id.all_review_layout);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            person_name = (TextView) view.findViewById(R.id.person_name);
            reviews_text = (TextView) view.findViewById(R.id.reviews_text);
        }
    }
}

