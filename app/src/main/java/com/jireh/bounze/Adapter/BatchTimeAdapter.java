package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.BatchTime;

import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class BatchTimeAdapter extends RecyclerView.Adapter<BatchTimeAdapter.ViewHolder> {
    int row_index = -1;
    Activity activity;
    List<BatchTime> batchList;
    BatchTimeAdapter.onItemBatchClickListener listener;


    public BatchTimeAdapter(Activity activity, List<BatchTime> batchList, onItemBatchClickListener listener) {
        this.activity = activity;
        this.batchList = batchList;
        this.listener = listener;
    }

    public BatchTimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_time, parent, false);
        // set the view's size, margins, paddings and layout parameters
        BatchTimeAdapter.ViewHolder vh = new BatchTimeAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final BatchTimeAdapter.ViewHolder holder, final int position) {
        BatchTime batchtime = batchList.get(position);

        holder.batch_name.setText(batchtime.getBatchName());
        holder.batch_time_txt.setText(batchtime.getStartTime() + " - " + batchtime.getEndTime());

        //change the background color when selected
        //set batch time listener for the time selected
        holder.batch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                listener.onItemBatchClick(holder.batch_layout, position, activity);
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {
            holder.batch_layout.setCardBackgroundColor(activity.getResources().getColor(R.color.light_dark_blue));
            holder.batch_name.setTextColor(Color.parseColor("#ffffff"));
            holder.batch_time_txt.setTextColor(Color.parseColor("#ffffff"));
        } else {
           holder.batch_layout.setCardBackgroundColor(activity.getResources().getColor(R.color.white));
            holder.batch_name.setTextColor(Color.parseColor("#000000"));
            holder.batch_time_txt.setTextColor(Color.parseColor("#000000"));
        }


    }


    @Override
    public int getItemCount() {
        return batchList.size();
    }

    public interface onItemBatchClickListener {
        public void onItemBatchClick(CardView batch_layout, int position, Activity activity);


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView batch_time_txt, batch_name;
        CardView batch_layout;

        public ViewHolder(View view) {
            super(view);

            batch_name = (TextView) view.findViewById(R.id.batch_name);
            batch_time_txt = (TextView) view.findViewById(R.id.batch_time_txt);
            batch_layout = (CardView) view.findViewById(R.id.batch_layout);


        }
    }
}



