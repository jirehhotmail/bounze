package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.CoachBatchTimeDetail;
import com.jireh.bounze.data.CoachSerializable;
import com.jireh.bounze.data.SessionsDetail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Muthamizhan C on 03-10-2017.
 */

public class ExpandableCoachPkgAdapter extends BaseExpandableListAdapter {

    public Activity _context;
    // child data in format of header title, child title
    RadioButton sessionView;
    int sessionPosition;
    ExpandableCoachPkgAdapter.onItemClickListener listener;
    ExpandableCoachPkgAdapter.onItemBatchClickListener batchClicklistener;
    private List<String> _listDataHeader; // header titles
    private CoachSerializable shopsub_service;
    private HashMap<String, List<String>> _listDataChild;

    public ExpandableCoachPkgAdapter(Activity context, List<String> listDataHeader,
                                     HashMap<String, List<String>> listChildData, CoachSerializable shopsub_service, onItemClickListener listener, onItemBatchClickListener onItemBatchClickListener) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.shopsub_service = shopsub_service;
        this.listener = listener;
        this.batchClicklistener = onItemBatchClickListener;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View itemView, ViewGroup parent) {

        //  final String childText = (String) getChild(groupPosition, childPosition);

        if (itemView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = infalInflater.inflate(R.layout.item_child_layout, null);
        }


        String str = shopsub_service.getCategoryDetails().get(groupPosition).getAvailability();
        List<String> availabilityList = Arrays.asList(str.split(","));
        TextView description_text = (TextView) itemView.findViewById(R.id.description_text);


        Button tv_mon = (Button) itemView.findViewById(R.id.tv_mon);


        ImageView male_image = (ImageView) itemView.findViewById(R.id.male_image);
        ImageView female_image = (ImageView) itemView.findViewById(R.id.female_image);
        ImageView kids_image = (ImageView) itemView.findViewById(R.id.kid_image);
        if (availabilityList.contains("M")) {
            tv_mon.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_mon.setTextColor(_context.getResources().getColor(R.color.black));

            tv_mon.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }

        Button tv_tues = (Button) itemView.findViewById(R.id.tv_tues);
        if (availabilityList.contains("T")) {
            tv_tues.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_tues.setTextColor(_context.getResources().getColor(R.color.black));

            tv_tues.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }


        Button tv_wed = (Button) itemView.findViewById(R.id.tv_wed);
        if (availabilityList.contains("W")) {
            tv_wed.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_wed.setTextColor(_context.getResources().getColor(R.color.black));

            tv_wed.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }


        Button tv_thurs = (Button) itemView.findViewById(R.id.tv_thurs);
        if (availabilityList.contains("Th")) {
            tv_thurs.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_thurs.setTextColor(_context.getResources().getColor(R.color.black));

            tv_thurs.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }


        Button tv_fri = (Button) itemView.findViewById(R.id.tv_fri);
        if (availabilityList.contains("F")) {
            tv_fri.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_fri.setTextColor(_context.getResources().getColor(R.color.black));

            tv_fri.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }


        Button tv_satur = (Button) itemView.findViewById(R.id.tv_satur);
        if (availabilityList.contains("Sa")) {
            tv_satur.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_satur.setTextColor(_context.getResources().getColor(R.color.black));

            tv_satur.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }


        Button tv_sun = (Button) itemView.findViewById(R.id.tv_sun);
        if (availabilityList.contains("S")) {
            tv_sun.setTextColor(_context.getResources().getColor(R.color.white));
            //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
        } else {
            tv_sun.setTextColor(_context.getResources().getColor(R.color.black));

            tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.day_oval_unselect));
            //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
        }

        //if gender have values show it else don't show
        if (shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Male")) {

            male_image.setVisibility(View.VISIBLE);
        }

        if (shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Female")) {

            female_image.setVisibility(View.VISIBLE);


        }
        if (shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Kids")) {

            kids_image.setVisibility(View.VISIBLE);


        }

        if (shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Male") &&
                shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Female")
                && shopsub_service.getCategoryDetails().get(groupPosition).getGender().contains("Kids")) {
            male_image.setVisibility(View.VISIBLE);
            female_image.setVisibility(View.VISIBLE);
            kids_image.setVisibility(View.VISIBLE);
        }

        RecyclerView time_duration_recycler = (RecyclerView) itemView.findViewById(R.id.time_duration_recycler);
        RecyclerView session_recycler = (RecyclerView) itemView.findViewById(R.id.session_recycler);

        LinearLayoutManager time_manager = new LinearLayoutManager(_context, LinearLayoutManager.HORIZONTAL, false);
        time_duration_recycler.setLayoutManager(time_manager);

        LinearLayoutManager session_manager = new LinearLayoutManager(_context);
        session_recycler.setLayoutManager(session_manager);

        try {
            //get the Batch details
            final List<CoachBatchTimeDetail> batchTimes = shopsub_service.getCategoryDetails().get(groupPosition).getUnique_ch_bt_tm_dtl();
            //if batchTimes has values
            if (batchTimes != null) {
                CoachBatchTimeAdapter adapter = new CoachBatchTimeAdapter(_context, batchTimes, new CoachBatchTimeAdapter.onItemBatchClickListener() {
                    @Override
                    public void onItemBatchClick(CardView batch_layout, int position, Activity activity) {
                        batchClicklistener.onItemBatchClick(batch_layout, groupPosition, position, activity);
                    }


                });


                time_duration_recycler.setAdapter(adapter);
            }


            //set Session Details
            final List<SessionsDetail> sessionDetails = shopsub_service.getCategoryDetails().get(groupPosition).getSessionsDetails();
            //if sessionTime has values then set the adapter
            if (sessionDetails != null) {
                CoachSessionAdapter sessionAdapter = new CoachSessionAdapter(_context, sessionDetails, new CoachSessionAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(RadioButton view, int position, Activity activity) {
                       /* //get session details using the position clicked
                        Sessions sessions = sessionDetails.get(position);
                        //send the session details to booking page and book
                        Intent in = new Intent(_context, BookAShop.class);
                        in.putExtra("SesssionDetails", sessions);
                        in.putExtra("BatchDetails", (Serializable) batchTimes);
                        in.putExtra("Flag", "Session");
                        in.putExtra("ShopSubServices", shopsub_service);
                        _context.startActivity(in);*/
                        sessionView = view;
                        sessionPosition = position;
                        listener.onItemClick(groupPosition, view, position, activity);
                    }
                });
                session_recycler.setAdapter(sessionAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemView;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        //if the header title is empty  hide the layout
        if (headerTitle.equals("")) {

//By default the group is hidden
            convertView = new FrameLayout(_context);
        } else {

            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_parent_layout, null);


            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.package_title_txt);
            ImageView status_txt = (ImageView) convertView.findViewById(R.id.status_txt);

            lblListHeader.setText(headerTitle);
            //if expanded show the arrow up else down
            if (isExpanded) {
                status_txt.setImageResource(R.drawable.arrow_up);
            } else {
                status_txt.setImageResource(R.drawable.arrow_down);
            }
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public interface onItemClickListener {
        public void onItemClick(int groupPosition, RadioButton view, int position, Activity activity);
    }

    public interface onItemBatchClickListener {
        public void onItemBatchClick(CardView batch_layout, int group_position, int position, Activity activity);
    }
}
