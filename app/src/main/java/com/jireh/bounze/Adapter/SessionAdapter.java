package com.jireh.bounze.Adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jireh.bounze.R;
import com.jireh.bounze.data.Sessions;

import java.util.List;

/**
 * Created by Muthamizhan C on 25-09-2017.
 */

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.ViewHolder> {


    Activity activity;
    List<Sessions> sessionList;
    SessionAdapter.onItemClickListener listener;
    private int lastSelectedPosition = -1;

    public SessionAdapter(Activity activity, List<Sessions> sessionList, SessionAdapter.onItemClickListener listener) {
        this.activity = activity;
        this.sessionList = sessionList;
        this.listener = listener;
    }

    public SessionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.session_time, parent, false);
        // set the view's size, margins, paddings and layout parameters
        SessionAdapter.ViewHolder vh = new SessionAdapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final SessionAdapter.ViewHolder holder, final int position) {
        Sessions sessionTime = sessionList.get(position);


        holder.session_time_txt.setText(sessionTime.getNoOfDays() + " " + sessionTime.getDaysUnit());
        //if the session is 0 then hide this text
        if (sessionTime.getSessions().equals("0")) {
            holder.no_of_sessions_txt.setVisibility(View.INVISIBLE);
        } else {
            holder.no_of_sessions_txt.setVisibility(View.VISIBLE);
            holder.no_of_sessions_txt.setText(sessionTime.getSessions() + " sessions");
        }


        //if session have offer amt or zero then strike the cost and put the offer cost else show the normal cost
        if (!sessionTime.getSessionsOfferCost().equals("0")) {
            holder.session_cost_txt.setTextColor(activity.getResources().getColor(R.color.red));
            holder.session_cost_txt.setText(activity.getResources().getString(R.string.Rs) + sessionTime.getSessionsCost());
            holder.session_cost_txt.setPaintFlags(holder.session_cost_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.session_offer_txt.setText(activity.getResources().getString(R.string.Rs) + sessionTime.getSessionsOfferCost());
            holder.session_offer_txt.setVisibility(View.VISIBLE);

        } else {
            holder.session_cost_txt.setText(activity.getResources().getString(R.string.Rs) + sessionTime.getSessionsCost());
            holder.session_offer_txt.setVisibility(View.GONE);
        }

        //set on check listener for the session to check which is checked and set checked false to others
        System.out.println("checking::radio::" + (lastSelectedPosition == position));

        holder.session_radio.setChecked(lastSelectedPosition == position);
        //set on check change listener for session radion
        holder.session_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("checking::radio::holder" + isChecked);
                if (isChecked) {

                    //pass the session radio holder to find which is clicked
                    listener.onItemClick(holder.session_radio, position, activity);
                    holder.free_trial_radio.setChecked(false);
                    holder.pay_per_use_radio.setChecked(false);

                }


            }
        });


    }


    @Override
    public int getItemCount() {
        return sessionList.size();
    }

    public interface onItemClickListener {
        public void onItemClick(RadioButton view, int position, Activity activity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button session_book;
        RadioButton session_radio, free_trial_radio, pay_per_use_radio;
        TextView session_time_txt, session_cost_txt, session_offer_txt, no_of_sessions_txt;

        public ViewHolder(View view) {
            super(view);
            session_book = (Button) view.findViewById(R.id.session_book);
            session_radio = (RadioButton) view.findViewById(R.id.session_radio);
            session_time_txt = (TextView) view.findViewById(R.id.session_time_txt);
            session_cost_txt = (TextView) view.findViewById(R.id.session_cost_txt);
            session_offer_txt = (TextView) view.findViewById(R.id.session_offer_txt);
            no_of_sessions_txt = (TextView) view.findViewById(R.id.no_of_sessions_txt);
            free_trial_radio = (RadioButton) activity.findViewById(R.id.free_trial_radio);
            pay_per_use_radio = (RadioButton) activity.findViewById(R.id.pay_per_use_radio);

            session_radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();


                }
            });
        }
    }
}



