package com.jireh.bounze.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jireh.bounze.data.Amenities;
import com.jireh.bounze.data.BatchTime;
import com.jireh.bounze.data.Category;
import com.jireh.bounze.data.CategoryDetail;
import com.jireh.bounze.data.City;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachBatchTimeDetail;
import com.jireh.bounze.data.CoachProgram;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.FilterModel;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.ServiceOnAShop;
import com.jireh.bounze.data.Sessions;
import com.jireh.bounze.data.SessionsDetail;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.ShopServiceDetails;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by android on 2/11/2017.
 */
public class DBFunctions {
    DBTables mDbHelper;
    SQLiteDatabase db;

    public DBFunctions(Context c) {
        mDbHelper = new DBTables(c);
        db = mDbHelper.getWritableDatabase();
    }

    public void insertintoServiceListDB(List<Service> tserlist) {
        db.delete(DataStruct.ServiceListDB.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < tserlist.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataStruct.ServiceListDB.ldb_status, tserlist.get(i).getStatus());
            values.put(DataStruct.ServiceListDB.ldb_iconid, tserlist.get(i).getIconid());
            values.put(DataStruct.ServiceListDB.ldb_message, tserlist.get(i).getMessage());
            values.put(DataStruct.ServiceListDB.ldb_sid, tserlist.get(i).getSid());
            values.put(DataStruct.ServiceListDB.ldb_title, tserlist.get(i).getTitle());
            values.put(DataStruct.ServiceListDB.ldb_description, tserlist.get(i).getDescription());
            values.put(DataStruct.ServiceListDB.ldb_image_url, tserlist.get(i).getImageUrl());
            values.put(DataStruct.ServiceListDB.ldb_active_status, tserlist.get(i).getActiveStatus());
            values.put(DataStruct.ServiceListDB.ldb_priority, tserlist.get(i).getPriority());
            values.put(DataStruct.ServiceListDB.ldb_updated_date, tserlist.get(i).getUpdatedDate());
            values.put(DataStruct.ServiceListDB.ldb_created_date, tserlist.get(i).getCreatedDate());


            long newRowId = db.insert(DataStruct.ServiceListDB.ldb_TABLE_NAME, null, values);

        }// end of for
    }//e end of function

    public void insertintoAmenitiesListDB(List<ResultModel> amenitiesList) {
        db.delete(DataStruct.AmenitiesListDB.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < amenitiesList.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataStruct.AmenitiesListDB.ldb_id, amenitiesList.get(i).getId());
            values.put(DataStruct.AmenitiesListDB.ldb_amenity_name, amenitiesList.get(i).getAmenities());


            long newRowId = db.insert(DataStruct.AmenitiesListDB.ldb_TABLE_NAME, null, values);

        }// end of for
    }//e end of function

    public void insertintoLocationsListDB(List<ResultModel> locationsList) {
        db.delete(DataStruct.LocationsList.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < locationsList.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataStruct.LocationsList.ldb_id, locationsList.get(i).getId());
            values.put(DataStruct.LocationsList.ldb_location, locationsList.get(i).getLocation());


            long newRowId = db.insert(DataStruct.LocationsList.ldb_TABLE_NAME, null, values);

        }// end of for
    }//e end of function

    public List<Service> getServiceListDB() {
        List<Service> tserlist = new ArrayList<Service>();

        String[] projection = DataStruct.ServiceListDB.column;


        Cursor c = db.query(DataStruct.ServiceListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                Service t = new Service();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status))));
                t.setIconid(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_iconid))));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_title)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_description)));
                t.setImageUrl("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_image_url)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_priority)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_created_date)));

                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<ResultModel> getAmenitiesListDB() {
        List<ResultModel> amenitiesList = new ArrayList<ResultModel>();

        String[] projection = DataStruct.AmenitiesListDB.column;
        Cursor c = db.query(DataStruct.AmenitiesListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ResultModel t = new ResultModel();
                t.setId(c.getString(c.getColumnIndex(DataStruct.AmenitiesListDB.ldb_id)));
                t.setAmenities(c.getString(c.getColumnIndex(DataStruct.AmenitiesListDB.ldb_amenity_name)));

                amenitiesList.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return amenitiesList;
    }

    //shop filter insert , Read and delete
    public void insertintoFilterSDb(String category, String value, String clickedPosition) {
        ContentValues values = new ContentValues();
        values.put(DataStruct.FilterShopDB.ldb_category, category);
        values.put(DataStruct.FilterShopDB.ldb_value, value);
        values.put(DataStruct.FilterShopDB.ldb_position, clickedPosition);

        long newRowId = db.insert(DataStruct.FilterShopDB.ldb_TABLE_NAME, null, values);


    }

    public ArrayList<FilterModel> getFilterShopDetails(String category) {
        ArrayList<FilterModel> filterModels = new ArrayList<FilterModel>();

        String[] projection = DataStruct.FilterShopDB.column;

        String selection = DataStruct.FilterShopDB.ldb_category + " = ?";
        String[] selectArgs = {category};
        Cursor c = db.query(DataStruct.FilterShopDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                FilterModel t = new FilterModel();
                t.setCategory(c.getString(c.getColumnIndex(DataStruct.FilterShopDB.ldb_category)));
                t.setValue(c.getString(c.getColumnIndex(DataStruct.FilterShopDB.ldb_value)));
                t.setCheckedPosition(c.getInt(c.getColumnIndex(DataStruct.FilterShopDB.ldb_position)));

                filterModels.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return filterModels;
    }

    public void deleteFilterSDB(String category, String value, String clickedPosition) {

        String whereClause = "Category = ? and Value = ? and Position = ?";
        String whereArgs[] = {category, value, clickedPosition};
        db.delete(DataStruct.FilterShopDB.ldb_TABLE_NAME, whereClause, whereArgs);

    }

    public void deleteFilterByCategorySDB(String category) {
        String whereClause = "Category = ? ";
        String whereArgs[] = {category};
        db.delete(DataStruct.FilterShopDB.ldb_TABLE_NAME, whereClause, whereArgs);
    }

    public void deleteFilterSDBAllData() {
        db.delete(DataStruct.FilterShopDB.ldb_TABLE_NAME, null, null);

    }

    public void insertintoFilterCDb(String category, String value, String clickedPosition) {
        ContentValues values = new ContentValues();
        values.put(DataStruct.FilterCoachDB.ldb_category, category);
        values.put(DataStruct.FilterCoachDB.ldb_value, value);
        values.put(DataStruct.FilterCoachDB.ldb_position, clickedPosition);

        long newRowId = db.insert(DataStruct.FilterCoachDB.ldb_TABLE_NAME, null, values);


    }

    public ArrayList<FilterModel> getFilterCoachDetails(String category) {
        ArrayList<FilterModel> filterModels = new ArrayList<FilterModel>();

        String[] projection = DataStruct.FilterCoachDB.column;

        String selection = DataStruct.FilterCoachDB.ldb_category + " = ?";
        String[] selectArgs = {category};
        Cursor c = db.query(DataStruct.FilterCoachDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                FilterModel t = new FilterModel();
                t.setCategory(c.getString(c.getColumnIndex(DataStruct.FilterCoachDB.ldb_category)));
                t.setValue(c.getString(c.getColumnIndex(DataStruct.FilterCoachDB.ldb_value)));
                t.setCheckedPosition(c.getInt(c.getColumnIndex(DataStruct.FilterCoachDB.ldb_position)));

                filterModels.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return filterModels;
    }

    public void deleteFilterCDB(String category, String value, String clickedPosition) {

        String whereClause = "Category = ? and Value = ? and Position = ?";
        String whereArgs[] = {category, value, clickedPosition};
        db.delete(DataStruct.FilterCoachDB.ldb_TABLE_NAME, whereClause, whereArgs);

    }

    public void deleteFilterByCategoryCDB(String category) {
        String whereClause = "Category = ? ";
        String whereArgs[] = {category};
        db.delete(DataStruct.FilterCoachDB.ldb_TABLE_NAME, whereClause, whereArgs);
    }

    public void deleteFilterCDBAllData() {
        db.delete(DataStruct.FilterCoachDB.ldb_TABLE_NAME, null, null);

    }

    public void insertintoFilterPDb(String category, String value, String clickedPosition) {
        ContentValues values = new ContentValues();
        values.put(DataStruct.FilterPassDB.ldb_category, category);
        values.put(DataStruct.FilterPassDB.ldb_value, value);
        values.put(DataStruct.FilterPassDB.ldb_position, clickedPosition);

        long newRowId = db.insert(DataStruct.FilterPassDB.ldb_TABLE_NAME, null, values);


    }

    public ArrayList<FilterModel> getFilterPassDetails(String category) {
        ArrayList<FilterModel> filterModels = new ArrayList<FilterModel>();

        String[] projection = DataStruct.FilterPassDB.column;

        String selection = DataStruct.FilterPassDB.ldb_category + " = ?";
        String[] selectArgs = {category};
        Cursor c = db.query(DataStruct.FilterPassDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                FilterModel t = new FilterModel();
                t.setCategory(c.getString(c.getColumnIndex(DataStruct.FilterPassDB.ldb_category)));
                t.setValue(c.getString(c.getColumnIndex(DataStruct.FilterPassDB.ldb_value)));
                t.setCheckedPosition(c.getInt(c.getColumnIndex(DataStruct.FilterPassDB.ldb_position)));

                filterModels.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return filterModels;
    }

    public void deleteFilterPDB(String category, String value, String clickedPosition) {

        String whereClause = "Category = ? and Value = ? and Position = ?";
        String whereArgs[] = {category, value, clickedPosition};
        db.delete(DataStruct.FilterPassDB.ldb_TABLE_NAME, whereClause, whereArgs);

    }

    public void deleteFilterByCategoryPDB(String category) {
        String whereClause = "Category = ? ";
        String whereArgs[] = {category};
        db.delete(DataStruct.FilterPassDB.ldb_TABLE_NAME, whereClause, whereArgs);
    }

    public void deleteFilterPDBAllData() {
        db.delete(DataStruct.FilterPassDB.ldb_TABLE_NAME, null, null);

    }

    public List<ResultModel> getLocationsListDB() {
        List<ResultModel> locationsList = new ArrayList<ResultModel>();

        String[] projection = DataStruct.LocationsList.column;
        Cursor c = db.query(DataStruct.LocationsList.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ResultModel t = new ResultModel();
                t.setId(c.getString(c.getColumnIndex(DataStruct.LocationsList.ldb_id)));
                t.setLocation(c.getString(c.getColumnIndex(DataStruct.LocationsList.ldb_location)));

                locationsList.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return locationsList;
    }

    public void insertintoServiceSubListDB(List<Service> tserlist) {
        db.delete(DataStruct.ServiceSubListDB.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < tserlist.size(); i++) {
            ContentValues values = new ContentValues();

            values.put(DataStruct.ServiceSubListDB.ldb_status, tserlist.get(i).getStatus());
            values.put(DataStruct.ServiceSubListDB.ldb_iconid, tserlist.get(i).getIconid());
            values.put(DataStruct.ServiceSubListDB.ldb_message, tserlist.get(i).getMessage());
            values.put(DataStruct.ServiceSubListDB.ldb_sid, tserlist.get(i).getSid());
            values.put(DataStruct.ServiceSubListDB.ldb_title, tserlist.get(i).getTitle());
            values.put(DataStruct.ServiceSubListDB.ldb_description, tserlist.get(i).getDescription());
            values.put(DataStruct.ServiceSubListDB.ldb_image_url, tserlist.get(i).getImageUrl());
            values.put(DataStruct.ServiceSubListDB.ldb_active_status, tserlist.get(i).getActiveStatus());
            values.put(DataStruct.ServiceSubListDB.ldb_priority, tserlist.get(i).getPriority());
            values.put(DataStruct.ServiceSubListDB.ldb_updated_date, tserlist.get(i).getUpdatedDate());
            values.put(DataStruct.ServiceSubListDB.ldb_created_date, tserlist.get(i).getCreatedDate());


            long newRowId = db.insert(DataStruct.ServiceSubListDB.ldb_TABLE_NAME, null, values);

        }// end of for
    }//e end of function

    public List<Service> getServiceSubListDB() {
        List<Service> tserlist = new ArrayList<Service>();

        String[] projection = DataStruct.ServiceSubListDB.column;
        String selection = DataStruct.ServiceSubListDB.ldb_title ;

        Cursor c = db.query(DataStruct.ServiceSubListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                selection                                   // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                Service t = new Service();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_status))));
                t.setIconid(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_iconid))));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_title)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_description)));
                t.setImageUrl("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_image_url)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_priority)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_created_date)));

                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }



    public String getServiceIdBySubListId(String subserviceId) {
        Service t = new Service();

        String[] projection = DataStruct.ServiceSubListDB.column;
        String selection = DataStruct.ServiceSubListDB.ldb_iconid + " = ?";
        String[] selectionArgs = {subserviceId};

        Cursor c = db.query(DataStruct.ServiceSubListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                   // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {



                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ServiceSubListDB.ldb_sid)));

                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/

            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return t.getSid();
    }

    public void insertintoShopListDB(List<Shop> tshoplist) {
        db.delete(DataStruct.ShopListDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.AmenitiesDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.ShoponServiceListDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.ShopServiceDetailsDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.BTimeDetailsDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.SessDetailsDB.ldb_TABLE_NAME, null, null);

        for (int shopidx = 0; shopidx < tshoplist.size(); shopidx++) {
            ContentValues values = new ContentValues();
            values.put(DataStruct.ShopListDB.ldb_status, tshoplist.get(shopidx).getStatus());
            values.put(DataStruct.ShopListDB.ldb_message, tshoplist.get(shopidx).getMessage());
            values.put(DataStruct.ShopListDB.ldb_sid, tshoplist.get(shopidx).getSid());
            values.put(DataStruct.ShopListDB.ldb_title, tshoplist.get(shopidx).getTitle());
            values.put(DataStruct.ShopListDB.ldb_avgratings, tshoplist.get(shopidx).getAvgratings());
            values.put(DataStruct.ShopListDB.ldb_description, tshoplist.get(shopidx).getDescription());
            values.put(DataStruct.ShopListDB.ldb_services, tshoplist.get(shopidx).getServices());
            values.put(DataStruct.ShopListDB.ldb_subservices, tshoplist.get(shopidx).getSubservices());
            values.put(DataStruct.ShopListDB.ldb_service_id, tshoplist.get(shopidx).getServiceId());
            values.put(DataStruct.ShopListDB.ldb_sservice_id, tshoplist.get(shopidx).getSubserviceId());
            values.put(DataStruct.ShopListDB.ldb_image_url_square, tshoplist.get(shopidx).getImageUrlSquare());
            values.put(DataStruct.ShopListDB.ldb_image_url_rectangle, tshoplist.get(shopidx).getImageUrlRectangle());
           /* values.put(DataStruct.ShopListDB.ldb_active_status, tshoplist.get(shopidx).getActiveStatus());
            values.put(DataStruct.ShopListDB.ldb_priority, tshoplist.get(shopidx).getPriority());*/
            values.put(DataStruct.ShopListDB.ldb_latitude, tshoplist.get(shopidx).getLatitude());
            values.put(DataStruct.ShopListDB.ldb_longitude, tshoplist.get(shopidx).getLongitude());
            values.put(DataStruct.ShopListDB.ldb_city, tshoplist.get(shopidx).getCity());
            values.put(DataStruct.ShopListDB.ldb_full_address, tshoplist.get(shopidx).getFullAddress());
            values.put(DataStruct.ShopListDB.ldb_small_address, tshoplist.get(shopidx).getSmallAddress());
            values.put(DataStruct.ShopListDB.ldb_contact_number, tshoplist.get(shopidx).getContactNumber());
            values.put(DataStruct.ShopListDB.ldb_shop_timings, tshoplist.get(shopidx).getShopTimings());
            values.put(DataStruct.ShopListDB.ldb_shop_avlbty, tshoplist.get(shopidx).getAvailability());
            values.put(DataStruct.ShopListDB.ldb_admin_lock_status, tshoplist.get(shopidx).getAdminLockStatus());
            values.put(DataStruct.ShopListDB.ldb_cost, tshoplist.get(shopidx).getCost());
            values.put(DataStruct.ShopListDB.ldb_email_id, tshoplist.get(shopidx).getEmailId());
            values.put(DataStruct.ShopListDB.ldb_isRegnAvailable, tshoplist.get(shopidx).getIsRegnAvailable());
            // values.put(DataStruct.ShopListDB.ldb_isPassAvailable, tshoplist.get(shopidx).getIsPassAvailable());
            values.put(DataStruct.ShopListDB.ldb_isFreeTrailAvailable, tshoplist.get(shopidx).getIsFreeTrailAvailable());
            values.put(DataStruct.ShopListDB.ldb_no_of_free_trial, tshoplist.get(shopidx).getNo_of_freetrial());
            values.put(DataStruct.ShopListDB.ldb_isOfferAvailable, tshoplist.get(shopidx).getIsOfferAvailable());
            values.put(DataStruct.ShopListDB.ldb_password, tshoplist.get(shopidx).getPassword());
            values.put(DataStruct.ShopListDB.ldb_book_online, tshoplist.get(shopidx).getBookOnline());
            values.put(DataStruct.ShopListDB.ldb_updated_date, tshoplist.get(shopidx).getUpdatedDate());
            values.put(DataStruct.ShopListDB.ldb_created_date, tshoplist.get(shopidx).getCreatedDate());
            //if amenitites  array have values put in amenities table

            if (tshoplist.get(shopidx).getAmenities().size() > 0) {

                for (int idx = 0; idx < tshoplist.get(shopidx).getAmenities().size(); idx++) {
                    ContentValues amenities = new ContentValues();
                    amenities.put(DataStruct.AmenitiesDB.ldb_shop_id, tshoplist.get(shopidx).getSid());
                    amenities.put(DataStruct.AmenitiesDB.ldb_amenity_id, tshoplist.get(shopidx).getAmenities().get(idx).getAid());
                    amenities.put(DataStruct.AmenitiesDB.ldb_aicon, tshoplist.get(shopidx).getAmenities().get(idx).getIcon());
                    amenities.put(DataStruct.AmenitiesDB.ldb_amenity_name, tshoplist.get(shopidx).getAmenities().get(idx).getAmenities());
                    db.insert(DataStruct.AmenitiesDB.ldb_TABLE_NAME, null, amenities);

                }
            }
            //if the service on shop have the array then add it in seperate table
            if (tshoplist.get(shopidx).getServiceonshop().size() > 0) {
                for (int sshopidx = 0; sshopidx < tshoplist.get(shopidx).getServiceonshop().size(); sshopidx++) {
                    ContentValues serviceonShop = new ContentValues();
                    serviceonShop.put(DataStruct.ShoponServiceListDB.ldb_shop_id, tshoplist.get(shopidx).getSid());
                    serviceonShop.put(DataStruct.ShoponServiceListDB.ldb_sservice_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getSubserviceId());
                    serviceonShop.put(DataStruct.ShoponServiceListDB.ldb_servie_name, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getServiceName());
                    serviceonShop.put(DataStruct.ShoponServiceListDB.ldb_subservice_name, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getSubserviceName());
                    serviceonShop.put(DataStruct.ShoponServiceListDB.ldb_pay_per_use, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getPayPerUse());

                    db.insert(DataStruct.ShoponServiceListDB.ldb_TABLE_NAME, null, serviceonShop);

                    //if the service details array have values then add it in the service details table
                    if (tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().size() > 0) {

                        for (int serviceidx = 0; serviceidx < tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().size(); serviceidx++) {
                            ContentValues servicDetails = new ContentValues();
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_shop_id, tshoplist.get(shopidx).getSid());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_shop_sservice_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getSubserviceId());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_package_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getPackage());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_package_name, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getPackageName());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_description, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getDescription());
                            //servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_per_session_cost, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getPerSessionCost());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_availability, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getAvailability());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_age_types, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getAgeTypes());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_age_years, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getAgeYears());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_gender, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getGender());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_anythings, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getAnythings());
/*                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_no_of_sessions, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getNoOfSessions());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_session_hour, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessionHour());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_cost, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getCost());
                            servicDetails.put(DataStruct.ShopServiceDetailsDB.ldb_offer_cost, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getOfferCost());*/

                            db.insert(DataStruct.ShopServiceDetailsDB.ldb_TABLE_NAME, null, servicDetails);

                            //if a service have the batch details then add it by inserting the subserice id and package id.
                            if (tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().size() > 0) {
                                for (int bidx = 0; bidx < tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().size(); bidx++) {

                                    if (tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatch_time().size() > 0) {
                                        for (int btidx = 0; btidx < tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatch_time().size(); btidx++) {
                                            ContentValues batchTDetails = new ContentValues();
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_shop_id, tshoplist.get(shopidx).getSid());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_sub_service_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getSubserviceId());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_package_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getPackage());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_batch_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatchId());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_batch_name, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatchName());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_batch_time_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatch_time().get(btidx).getBatchTimeId());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_start_time, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatch_time().get(btidx).getStartTime());
                                            batchTDetails.put(DataStruct.BTimeDetailsDB.ldb_end_time, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getBatch_details().get(bidx).getBatch_time().get(btidx).getEndTime());
                                            db.insert(DataStruct.BTimeDetailsDB.ldb_TABLE_NAME, null, batchTDetails);


                                        }
                                    }
                                }

                            }

                            //if the service have the sesssion details then add it using shop id, sub service id and packageid,

                            if (tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().size() > 0) {
                                for (int ses_idx = 0; ses_idx < tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().size(); ses_idx++) {
                                    ContentValues sessionDetails = new ContentValues();
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_shop_id, tshoplist.get(shopidx).getSid());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_sservice_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getSubserviceId());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_package_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getPackage());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_session_id, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getSessionId());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_no_of_days, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getNoOfDays());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_day_units, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getDaysUnit());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_sessions, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getSessions());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_sessions_cost, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getSessionsCost());
                                    sessionDetails.put(DataStruct.SessDetailsDB.ldb_sessions_offcost, tshoplist.get(shopidx).getServiceonshop().get(sshopidx).getService_details().get(serviceidx).getSessions_details().get(ses_idx).getSessionsOfferCost());

                                    db.insert(DataStruct.SessDetailsDB.ldb_TABLE_NAME, null, sessionDetails);
                                }

                            }

                        }

                    }
                }

            }


            long newRowId = db.insert(DataStruct.ShopListDB.ldb_TABLE_NAME, null, values);
        }// end of for
    }//e end of function

    public List<Shop> getShopListDB() {
        List<Shop> tserlist = new ArrayList<Shop>();

        String[] projection = DataStruct.ShopListDB.column;


        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));

                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));

                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Shop> getShopListBySubServiceId(String sub_serviceId) {
        List<Shop> tserlist = new ArrayList<Shop>();
        //service_id LIKE ("%,3,%") OR service_id IN (3)  OR service_id LIKE ("3,%")  OR service_id LIKE ("%,3")
        String[] projection = DataStruct.ShopListDB.column;
        String selection = DataStruct.ShopListDB.ldb_sservice_id + " LIKE ('%," + sub_serviceId
                + ",%') OR " + DataStruct.ShopListDB.ldb_sservice_id + " IN (" + sub_serviceId + ")  OR " +
                DataStruct.ShopListDB.ldb_sservice_id + " LIKE ('" + sub_serviceId + ",%') OR " + DataStruct.ShopListDB.ldb_sservice_id
                + " LIKE ('%," + sub_serviceId + "')";
        System.out.println("selection qurey::" + selection);
        //  select * from shoplist where  service_id =14 or service_id like '%,14%'  or service_id like '%14,%'
        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));

                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }
    public List<Shop> getShopListByServiceId(String serviceId) {
        List<Shop> tserlist = new ArrayList<Shop>();
        //service_id LIKE ("%,3,%") OR service_id IN (3)  OR service_id LIKE ("3,%")  OR service_id LIKE ("%,3")
        String[] projection = DataStruct.ShopListDB.column;
        String selection = DataStruct.ShopListDB.ldb_service_id + " LIKE ('%," + serviceId
                + ",%') OR " + DataStruct.ShopListDB.ldb_service_id + " IN (" + serviceId + ")  OR " +
                DataStruct.ShopListDB.ldb_service_id + " LIKE ('" + serviceId + ",%') OR " + DataStruct.ShopListDB.ldb_service_id
                + " LIKE ('%," + serviceId + "')";
        System.out.println("selection qurey::" + selection);
        //  select * from shoplist where  service_id =14 or service_id like '%,14%'  or service_id like '%14,%'
        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));

                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Shop> getShopListDBByListId(String shopIds, String ratings) {

        List<Shop> tserlist = new ArrayList<Shop>();

        String[] projection = DataStruct.ShopListDB.column;
        String selection;
        //if the user not select ratings default desc else based on selection
        if (ratings.equals("")) {
            selection = DataStruct.ShopListDB.ldb_sid + " in(" + shopIds + ") order by avgratings DESC";
        } else {
            selection = DataStruct.ShopListDB.ldb_sid + " in(" + shopIds + ") order by avgratings " + ratings;
        }

        System.out.println("shop ids::" + shopIds + "selection" + selection);

        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));
                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public Shop getShopListDetailbyId(String shopId) {

        Shop t = new Shop();
        String[] projection = DataStruct.ShopListDB.column;
        String selection = DataStruct.ShopListDB.ldb_sid + " = ?";
        String[] selectionArgs = {shopId};


        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));
                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/

            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return t;
    }

    public List<Shop> getShopListDetailbyTitle(String title) {
        List<Shop> tserlist = new ArrayList<Shop>();

        String[] projection = DataStruct.ShopListDB.column;
        String selection = DataStruct.ShopListDB.ldb_title + " LIKE '%" + title + "%' or " +
                DataStruct.ShopListDB.ldb_subservices + " LIKE '%" + title + "%' or " + DataStruct.ShopListDB.ldb_small_address + " LIKE '%" + title + "%'";


        Cursor c = db.query(DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                //to replace the special characters with hiphen
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));
                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
              /*  t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));*/
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setShopTimings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_timings)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_shop_avlbty)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setIsRegnAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isRegnAvailable)));

                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isFreeTrailAvailable)));
                t.setNo_of_freetrial("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_no_of_free_trial)));
                t.setIsOfferAvailable("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_isOfferAvailable)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                /*String itemvaln = c.getString(c.getColumnIndex(DataStruct.ServiceListDB.ldb_status));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );
                String itemvalv = c.getString(c.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE));  //c.getLong( c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry._ID) );*/
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Shop> getShopListwithfilterDB(String[] servid, String[] ratingval, long minval, long maxval) {
        List<Shop> tserlist = new ArrayList<Shop>();

        String[] projection = DataStruct.ShopListDB.column;
        //  String selection = DataStruct.ShopListDB.ldb_active_status + " = ?";
        String[] selectionArgs = {"1"};

        /*Cursor c = db.query( DataStruct.ShopListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
*/
        String where1 = "";
        if (ratingval.length > 0) {
            where1 = "";
            for (int i = 0; i < ratingval.length; i++) {
                if ((i != 0))
                    where1 += " or ";
                where1 += "" + DataStruct.ShopListDB.ldb_avgratings + " = '" + ratingval[i] + "'";
            }
            where1 += "";
        }
        String whereval = "";
        if (ratingval.length > 0)
            whereval = "where " + where1;
        // String where2 ="";
       /* if(servid.length >0)
        {
            where2 = "(";
            for (int i = 0; i < servid.length; i++) {
                String services =
                where2 += "(" + DataStruct.ShopListDB.ldb_avgratings + " = " +servid[i]+")";
            }
            where2 += ")";
        }
        */
        String qry = "SELECT * FROM " + DataStruct.ShopListDB.ldb_TABLE_NAME + "  " + whereval + ";";
        Cursor c = db.rawQuery(qry, new String[]{});
        Log.e("qry FromDbOutput", "" + qry);
        Log.e("FromDbOutput", "" + c.getCount());
        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Shop t = new Shop();
                t.setStatus(c.getInt(c.getColumnIndex(DataStruct.ShopListDB.ldb_status)));
                t.setDistanceonthego(null);
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_message)));
                t.setSid("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_avgratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_description)));
                t.setServiceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_service_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_sservice_id)));
                t.setSubservices("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_subservices)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_image_url_rectangle)));
                //       t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_active_status)));
                //        t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_priority)));
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_contact_number)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_cost)));
                t.setEmailId("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_email_id)));
                t.setPassword("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_password)));
                t.setBookOnline(""+c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_book_online)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.ShopListDB.ldb_created_date)));


                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<ServiceOnAShop> getAllShoponSList() {
        List<ServiceOnAShop> tserlist = new ArrayList<ServiceOnAShop>();
        String[] projection = DataStruct.ShoponServiceListDB.column;


        Cursor c = db.query(true, DataStruct.ShoponServiceListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                DataStruct.ShoponServiceListDB.ldb_servie_name,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null, "10000"                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ServiceOnAShop t = new ServiceOnAShop();

                t.setServiceName("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_servie_name)));

                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<ServiceOnAShop> getShoponServiceListDB(String shopid, String sserviceid) {
        List<ServiceOnAShop> tserlist = new ArrayList<ServiceOnAShop>();
        String[] projection = DataStruct.ShoponServiceListDB.column;
        String selection = DataStruct.ShoponServiceListDB.ldb_shop_id + " = ? AND " + DataStruct.ShoponServiceListDB.ldb_sservice_id + " =?";
        String[] selectionArgs = {shopid, sserviceid};


        Cursor c = db.query(DataStruct.ShoponServiceListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ServiceOnAShop t = new ServiceOnAShop();
                t.setShopId("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_shop_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_sservice_id)));
                t.setServiceName("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_servie_name)));
                t.setSubserviceName("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_subservice_name)));
                t.setPayPerUse("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_pay_per_use)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<ServiceOnAShop> getShoponServiceListByShopid(String shopId) {
        List<ServiceOnAShop> tserlist = new ArrayList<ServiceOnAShop>();
        String[] projection = DataStruct.ShoponServiceListDB.column;

        String selection = DataStruct.ShoponServiceListDB.ldb_shop_id + " = ?";
        String[] selectionArgs = {shopId};
        Cursor c = db.query(DataStruct.ShoponServiceListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ServiceOnAShop t = new ServiceOnAShop();
                t.setShopId("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_shop_id)));
                t.setSubserviceId("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_sservice_id)));
                t.setServiceName("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_servie_name)));
                t.setSubserviceName("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_subservice_name)));
                t.setPayPerUse("" + c.getString(c.getColumnIndex(DataStruct.ShoponServiceListDB.ldb_pay_per_use)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Amenities> getShopAmenitiesDBBy_shopid(String shopid) {
        List<Amenities> tserlist = new ArrayList<Amenities>();
        String[] projection = DataStruct.AmenitiesDB.column;
        String selection = DataStruct.AmenitiesDB.ldb_shop_id + " = ?";
        String[] selectionArgs = {shopid};
        Cursor c = db.query(DataStruct.AmenitiesDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                Amenities t = new Amenities();
                t.setShop_id("" + c.getString(c.getColumnIndex(DataStruct.AmenitiesDB.ldb_shop_id)));
                t.setAid("" + c.getString(c.getColumnIndex(DataStruct.AmenitiesDB.ldb_amenity_id)));
                t.setAmenities("" + c.getString(c.getColumnIndex(DataStruct.AmenitiesDB.ldb_amenity_name)));
                t.setIcon("" + c.getString(c.getColumnIndex(DataStruct.AmenitiesDB.ldb_aicon)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<BatchTime> getShopBatchTDetailsBy_shopid(String shopid, String sserviceid, String packageid) {
        List<BatchTime> tserlist = new ArrayList<BatchTime>();
        String[] projection = DataStruct.BTimeDetailsDB.column;
        String selection = DataStruct.BTimeDetailsDB.ldb_shop_id + " = ? AND " + DataStruct.BTimeDetailsDB.ldb_sub_service_id + " =? AND " + DataStruct.BTimeDetailsDB.ldb_package_id + " =?";
        String[] selectionArgs = {shopid, sserviceid, packageid};
        Cursor c = db.query(DataStruct.BTimeDetailsDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                DataStruct.BTimeDetailsDB.ldb_batch_time_id                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                BatchTime t = new BatchTime();
                t.setBatchTimeId("" + c.getString(c.getColumnIndex(DataStruct.BTimeDetailsDB.ldb_batch_time_id)));
                t.setStartTime("" + c.getString(c.getColumnIndex(DataStruct.BTimeDetailsDB.ldb_start_time)));
                t.setEndTime("" + c.getString(c.getColumnIndex(DataStruct.BTimeDetailsDB.ldb_end_time)));
                t.setBatchName(" " + c.getString(c.getColumnIndex(DataStruct.BTimeDetailsDB.ldb_batch_name)));

                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Sessions> getShopSessionDetailsByShopid(String shopid, String sserviceid, String packageid) {
        List<Sessions> tserlist = new ArrayList<Sessions>();
        String[] projection = DataStruct.SessDetailsDB.column;
        String selection = DataStruct.SessDetailsDB.ldb_shop_id + " = ? AND " + DataStruct.SessDetailsDB.ldb_sservice_id + " =? AND " + DataStruct.SessDetailsDB.ldb_package_id + " =?";
        String[] selectionArgs = {shopid, sserviceid, packageid};
        Cursor c = db.query(DataStruct.SessDetailsDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                Sessions t = new Sessions();
                t.setShop_id("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_shop_id)));
                t.setSub_service_id("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_sservice_id)));
                t.setSessionId("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_session_id)));
                t.setNoOfDays("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_no_of_days)));
                t.setDaysUnit("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_day_units)));
                t.setSessions("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_sessions)));
                t.setSessionsCost("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_sessions_cost)));
                t.setSessionsOfferCost("" + c.getString(c.getColumnIndex(DataStruct.SessDetailsDB.ldb_sessions_offcost)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<ShopServiceDetails> getShopDetailsonServiceListDB_ById(String shopId, String subserviceId) {
        List<ShopServiceDetails> tserlist = new ArrayList<ShopServiceDetails>();
        String[] projection = DataStruct.ShopServiceDetailsDB.column;
        String[] selectionArgs = {shopId, subserviceId};
        Cursor c = db.query(DataStruct.ShopServiceDetailsDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                DataStruct.ShopServiceDetailsDB.ldb_shop_id + " = ? AND " + DataStruct.ShopServiceDetailsDB.ldb_shop_sservice_id + " = ?",                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                ShopServiceDetails t = new ShopServiceDetails();
                t.setShop_id("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_shop_id)));
                t.setSub_service_id("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_shop_sservice_id)));
                t.setPackage("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_package_id)));
                t.setPackageName("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_package_name)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_description)));
                //    t.setPerSessionCost("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_per_session_cost)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_availability)));
                t.setAgeTypes("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_age_types)));
                t.setAgeYears("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_age_years)));
                t.setGender("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_gender)));
                t.setAnythings("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_anythings)));
               /* t.setNoOfSessions("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_no_of_sessions)));
                t.setSessionHour("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_session_hour)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_cost)));
                t.setOfferCost("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_offer_cost)));*/
                //  t.setBatchDetails("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_batch_details)));
                // t.setSessionsDetails("" + c.getString(c.getColumnIndex(DataStruct.ShopServiceDetailsDB.ldb_sessions_details)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public void insertintoCoachListDB(List<Coach> tserlist) {
        db.delete(DataStruct.CoachListDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.CoachProgramListDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.CoachCatDetailsDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.CoachBatchTimeDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.CoachSessionTimeDB.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < tserlist.size(); i++) {
            ContentValues values = new ContentValues();


            values.put(DataStruct.CoachListDB.ldb_status, tserlist.get(i).getStatus());
            values.put(DataStruct.CoachListDB.ldb_coach_id, tserlist.get(i).getCid());
            values.put(DataStruct.CoachListDB.ldb_title, tserlist.get(i).getTitle());
            values.put(DataStruct.CoachListDB.ldb_ratings, tserlist.get(i).getAvgratings());
            values.put(DataStruct.CoachListDB.ldb_description, tserlist.get(i).getDescription());
            values.put(DataStruct.CoachListDB.ldb_cat_id, tserlist.get(i).getCategoryId());
            values.put(DataStruct.CoachListDB.ldb_category, tserlist.get(i).getCategory());

            values.put(DataStruct.CoachListDB.ldb_image_url_square, tserlist.get(i).getImageUrlSquare());
            values.put(DataStruct.CoachListDB.ldb_image_url_rectangle, tserlist.get(i).getImageUrlRectangle());
            values.put(DataStruct.CoachListDB.ldb_active_status, tserlist.get(i).getActiveStatus());
            values.put(DataStruct.CoachListDB.ldb_priority, tserlist.get(i).getPriority());
            values.put(DataStruct.CoachListDB.ldb_latitude, tserlist.get(i).getLatitude());
            values.put(DataStruct.CoachListDB.ldb_longitude, tserlist.get(i).getLongitude());
            values.put(DataStruct.CoachListDB.ldb_city, tserlist.get(i).getCity());
            values.put(DataStruct.CoachListDB.ldb_full_address, tserlist.get(i).getFullAddress());
            values.put(DataStruct.CoachListDB.ldb_small_address, tserlist.get(i).getSmallAddress());
            values.put(DataStruct.CoachListDB.ldb_contact_number, tserlist.get(i).getContactNumber());
            values.put(DataStruct.CoachListDB.ldb_admin_lock_status, tserlist.get(i).getAdminLockStatus());
            values.put(DataStruct.CoachListDB.ldb_cost, tserlist.get(i).getCost());
            values.put(DataStruct.CoachListDB.ldb_isFreeTrailAvailable, tserlist.get(i).getIsFreeTrailAvailable());
            values.put(DataStruct.CoachListDB.ldb_no_of_free_trial, tserlist.get(i).getNoOfFreetrial());
            values.put(DataStruct.CoachListDB.ldb_updated_date, tserlist.get(i).getUpdatedDate());
            values.put(DataStruct.CoachListDB.ldb_created_date, tserlist.get(i).getCreatedDate());
            long newRowId = db.insert(DataStruct.CoachListDB.ldb_TABLE_NAME, null, values);
            //get the coach program list and store it in the program table


            for (int idx = 0; idx < tserlist.get(i).getCoachProgram().size(); idx++) {
                ContentValues coach_pgm_values = new ContentValues();

                coach_pgm_values.put(DataStruct.CoachProgramListDB.ldb_coach_id, tserlist.get(i).getCoachProgram().get(idx).getCoachId());
                coach_pgm_values.put(DataStruct.CoachProgramListDB.ldb_category_id, tserlist.get(i).getCoachProgram().get(idx).getCategoryId());
                coach_pgm_values.put(DataStruct.CoachProgramListDB.ldb_category_name, tserlist.get(i).getCoachProgram().get(idx).getCategoryName());
                coach_pgm_values.put(DataStruct.CoachProgramListDB.ldb_pay_per_use, tserlist.get(i).getCoachProgram().get(idx).getPayPerUse());
                long rowid = db.insert(DataStruct.CoachProgramListDB.ldb_TABLE_NAME, null, coach_pgm_values);

                // get the coach category list and dump into coach category details table
                for (int c_idx = 0; c_idx < tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().size(); c_idx++) {


                    ContentValues coach_cat_det = new ContentValues();
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_coach_id, tserlist.get(i).getCoachProgram().get(idx).getCoachId());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_coach_cat_id, tserlist.get(i).getCoachProgram().get(idx).getCategoryId());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_coach_pack, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachPackage());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_coach_packname, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachPackageName());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_avl, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getAvailability());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_gender, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getGender());
                    coach_cat_det.put(DataStruct.CoachCatDetailsDB.ldb_anythings, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getAnythings());
                    long r = db.insert(DataStruct.CoachCatDetailsDB.ldb_TABLE_NAME, null, coach_cat_det);


                    //get the coach batch time details and dump into coach batch time table using coach pack id ,coach batch id and batch name
                    for (int cb_idx = 0; cb_idx < tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().size(); cb_idx++) {


                        for (int cbt_idx = 0; cbt_idx < tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchTime().size(); cbt_idx++) {
                            ContentValues coach_batch_det = new ContentValues();
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_coach_id, tserlist.get(i).getCoachProgram().get(idx).getCoachId());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_coach_pack, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachPackage());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_coach_batch_id, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchId());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_coach_batch_name, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchName());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_coach_batch_time_id, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchTime().get(cbt_idx).getCoachBatchTimeId());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_start_time, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchTime().get(cbt_idx).getStartTime());
                            coach_batch_det.put(DataStruct.CoachBatchTimeDB.ldb_end_time, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachBatchDetails().get(cb_idx).getCoachBatchTime().get(cbt_idx).getEndTime());
                            long ro = db.insert(DataStruct.CoachBatchTimeDB.ldb_TABLE_NAME, null, coach_batch_det);
                        }


                    }
                    //get the coach session time details and dump into coach session time table using coach pack id ;
                    for (int cs_idx = 0; cs_idx < tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().size(); cs_idx++) {


                        ContentValues coach_session_det = new ContentValues();
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_coach_id, tserlist.get(i).getCoachProgram().get(idx).getCoachId());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_coach_pack, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getCoachPackage());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_session_id, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getSessionId());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_no_of_days, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getNoOfDays());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_days_unit, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getDaysUnit());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_sessions, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getSessions());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_sessions_cost, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getSessionsCost());
                        coach_session_det.put(DataStruct.CoachSessionTimeDB.ldb_sessions_offer_cost, tserlist.get(i).getCoachProgram().get(idx).getCategoryDetails().get(c_idx).getSessionsDetails().get(cs_idx).getSessionsOfferCost());

                        long ro = db.insert(DataStruct.CoachSessionTimeDB.ldb_TABLE_NAME, null, coach_session_det);


                    }//
                }


            }// end of for


        }// end of for
    }//e end of function

    public void insertintoCoachCatListDB(List<Category> tserlist) {
        db.delete(DataStruct.CoachCatAllListDB.ldb_TABLE_NAME, null, null);
        for (int i = 0; i < tserlist.size(); i++) {
            ContentValues values = new ContentValues();

            values.put(DataStruct.CoachCatAllListDB.ldb_status, tserlist.get(i).getStatus());
            values.put(DataStruct.CoachCatAllListDB.ldb_message, tserlist.get(i).getMessage());
            values.put(DataStruct.CoachCatAllListDB.ldb_cid, tserlist.get(i).getCid());
            values.put(DataStruct.CoachCatAllListDB.ldb_title, tserlist.get(i).getTitle());
            values.put(DataStruct.CoachCatAllListDB.ldb_description, tserlist.get(i).getDescription());
            values.put(DataStruct.CoachCatAllListDB.ldb_image_url, tserlist.get(i).getImageUrl());
            values.put(DataStruct.CoachCatAllListDB.ldb_active_status, tserlist.get(i).getActiveStatus());
            values.put(DataStruct.CoachCatAllListDB.ldb_priority, tserlist.get(i).getPriority());
            values.put(DataStruct.CoachCatAllListDB.ldb_updated_date, tserlist.get(i).getUpdatedDate());
            values.put(DataStruct.CoachCatAllListDB.ldb_created_date, tserlist.get(i).getCreatedDate());


            long newRowId = db.insert(DataStruct.CoachCatAllListDB.ldb_TABLE_NAME, null, values);
        }// end of for
    }//e end of function

    public List<Category> getCoachCatAllListDB() {
        List<Category> tserlist = new ArrayList<Category>();
        String selection = DataStruct.ServiceSubListDB.ldb_title ;
        String[] projection = DataStruct.CoachCatAllListDB.column;


        Cursor c = db.query(DataStruct.CoachCatAllListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                selection                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Category t = new Category();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_status))));
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_message)));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_cid)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_title)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_description)));
                t.setImageUrl("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_image_url)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_priority)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachCatAllListDB.ldb_created_date)));


                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<CoachProgram> getCoachProgramListDB() {
        List<CoachProgram> tserlist = new ArrayList<CoachProgram>();

        String[] projection = DataStruct.CoachProgramListDB.column;


        Cursor c = db.query(DataStruct.CoachProgramListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                CoachProgram t = new CoachProgram();
                t.setCoachId("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_coach_id)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_category_id)));
                t.setCategoryName("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_category_name)));
                t.setPayPerUse("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_pay_per_use)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<CoachProgram> getCoachProgramListByCoachId(String coachId) {
        List<CoachProgram> tserlist = new ArrayList<CoachProgram>();

        String[] projection = DataStruct.CoachProgramListDB.column;
        String selection = DataStruct.CoachProgramListDB.ldb_coach_id + " = ?";
        String[] selectionArgs = {coachId};

        Cursor c = db.query(DataStruct.CoachProgramListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                CoachProgram t = new CoachProgram();
                t.setCoachId("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_coach_id)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_category_id)));
                t.setCategoryName("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_category_name)));
                t.setPayPerUse("" + c.getString(c.getColumnIndex(DataStruct.CoachProgramListDB.ldb_pay_per_use)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<CategoryDetail> getCoachCategoryDetailsByCoachIdCatId(String coachId, String catId) {
        List<CategoryDetail> tserlist = new ArrayList<CategoryDetail>();

        String[] projection = DataStruct.CoachCatDetailsDB.column;
        String selection = DataStruct.CoachCatDetailsDB.ldb_coach_id + " = ? AND " + DataStruct.CoachCatDetailsDB.ldb_coach_cat_id + " = ?";
        String[] selectionArgs = {coachId, catId};

        Cursor c = db.query(DataStruct.CoachCatDetailsDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                CategoryDetail t = new CategoryDetail();
                t.setUnique_coach_id("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_coach_id)));
                t.setUnique_coach_cat_id("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_coach_cat_id)));
                t.setCoachPackage("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_coach_pack)));
                t.setCoachPackageName("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_coach_packname)));
                t.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_avl)));
                t.setGender("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_gender)));
                t.setAnythings("" + c.getString(c.getColumnIndex(DataStruct.CoachCatDetailsDB.ldb_anythings)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<CoachBatchTimeDetail> getCoachBatchTimeDetailsById(String coachId, String packageid) {
        List<CoachBatchTimeDetail> tserlist = new ArrayList<CoachBatchTimeDetail>();
        String[] projection = DataStruct.CoachBatchTimeDB.column;
        String selection = DataStruct.CoachBatchTimeDB.ldb_coach_id + " = ? AND " + DataStruct.CoachBatchTimeDB.ldb_coach_pack + " = ?";
        String[] selectionArgs = {coachId, packageid};
        Cursor c = db.query(DataStruct.CoachBatchTimeDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                DataStruct.CoachBatchTimeDB.ldb_coach_batch_id                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                CoachBatchTimeDetail t = new CoachBatchTimeDetail();
                t.setCoachBatchId("" + c.getString(c.getColumnIndex(DataStruct.CoachBatchTimeDB.ldb_coach_batch_id)));
                t.setCoachBatchName("" + c.getString(c.getColumnIndex(DataStruct.CoachBatchTimeDB.ldb_coach_batch_name)));
                t.setCoachBatchTimeId("" + c.getString(c.getColumnIndex(DataStruct.CoachBatchTimeDB.ldb_coach_batch_time_id)));
                t.setStartTime(" " + c.getString(c.getColumnIndex(DataStruct.CoachBatchTimeDB.ldb_start_time)));
                t.setEndTime(" " + c.getString(c.getColumnIndex(DataStruct.CoachBatchTimeDB.ldb_end_time)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<SessionsDetail> getCoachSessionTimeDetailsById(String coachId, String packageid) {
        List<SessionsDetail> tserlist = new ArrayList<SessionsDetail>();
        String[] projection = DataStruct.CoachSessionTimeDB.column;
        String selection = DataStruct.CoachSessionTimeDB.ldb_coach_id + " = ? AND " + DataStruct.CoachSessionTimeDB.ldb_coach_pack + " =?";
        String[] selectionArgs = {coachId, packageid};
        Cursor c = db.query(DataStruct.CoachSessionTimeDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                SessionsDetail t = new SessionsDetail();

                t.setSessionId("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_session_id)));
                t.setNoOfDays("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_no_of_days)));
                t.setDaysUnit("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_days_unit)));
                t.setSessions("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_sessions)));
                t.setSessionsCost("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_sessions_cost)));
                t.setSessionsOfferCost("" + c.getString(c.getColumnIndex(DataStruct.CoachSessionTimeDB.ldb_sessions_offer_cost)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public List<Coach> getCoachListDB() {
        List<Coach> tserlist = new ArrayList<Coach>();

        String[] projection = DataStruct.CoachListDB.column;


        Cursor c = db.query(DataStruct.CoachListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Coach t = new Coach();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_status))));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_coach_id)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_ratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_description)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cat_id)));
                t.setCategory("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_category)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_rectangle)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_priority)));
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_contact_number)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cost)));
                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_isFreeTrailAvailable)));
                t.setNoOfFreetrial("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_no_of_free_trial)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_created_date)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }


    public List<Coach> getCoachListByTitleDB(String coachTitle) {
        List<Coach> tserlist = new ArrayList<Coach>();

        String[] projection = DataStruct.CoachListDB.column;
        String selection = DataStruct.CoachListDB.ldb_title + " LIKE '%" + coachTitle + "%' ";


        Cursor c = db.query(DataStruct.CoachListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Coach t = new Coach();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_status))));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_coach_id)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_ratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_description)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cat_id)));
                t.setCategory("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_category)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_rectangle)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_priority)));
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_contact_number)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cost)));
                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_isFreeTrailAvailable)));
                t.setNoOfFreetrial("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_no_of_free_trial)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_created_date)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }
    public List<Coach> getCoachListByIdDB(String coachId,String ratings) {
        List<Coach> tserlist = new ArrayList<Coach>();

        String[] projection = DataStruct.CoachListDB.column;

        String selection;
        //if the user not select ratings default desc else based on selection
        if (ratings.equals("")) {
            selection = DataStruct.CoachListDB.ldb_coach_id + " in(" + coachId + ") order by avgratings DESC";
        } else {
            selection = DataStruct.CoachListDB.ldb_coach_id + " in(" + coachId + ") order by avgratings " + ratings;
        }

        Cursor c = db.query(DataStruct.CoachListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                Coach t = new Coach();
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_status))));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_coach_id)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_ratings)));
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_description)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cat_id)));
                t.setCategory("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_category)));
                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_rectangle)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_priority)));
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_contact_number)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cost)));
                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_isFreeTrailAvailable)));
                t.setNoOfFreetrial("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_no_of_free_trial)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_created_date)));
                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }
    public Coach getCoachDetailsbyId(String coachId) {


        Coach t = new Coach();
        String[] projection = DataStruct.CoachListDB.column;
        String selection = DataStruct.CoachListDB.ldb_coach_id + " = ?";
        String[] selectionArgs = {coachId};

        Cursor c = db.query(DataStruct.CoachListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {


                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_status))));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_coach_id)));
                t.setTitle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_title)));
                t.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_ratings)));
                ;
                t.setDescription("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_description)));
                t.setCategoryId("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cat_id)));
                t.setCategory("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_category)));

                t.setImageUrlSquare("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_square)));
                t.setImageUrlRectangle("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_image_url_rectangle)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_active_status)));
                t.setPriority("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_priority)));
                t.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_latitude)));
                t.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_longitude)));
                t.setCity("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_city)));
                t.setFullAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_full_address)));
                t.setSmallAddress("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_small_address)));
                t.setContactNumber("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_contact_number)));
                t.setAdminLockStatus("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_admin_lock_status)));
                t.setCost("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_cost)));
                t.setIsFreeTrailAvailable("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_isFreeTrailAvailable)));
                t.setNoOfFreetrial("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_no_of_free_trial)));
                t.setUpdatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_updated_date)));
                t.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.CoachListDB.ldb_created_date)));


            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return t;
    }

    public void insertintoCityListDB(List<City> tshoponserlist) {
        db.delete(DataStruct.CityListDB.ldb_TABLE_NAME, null, null);
        Log.v("List<City>", "length =" + tshoponserlist.size());
        for (int i = 0; i < tshoponserlist.size(); i++) {
            ContentValues values = new ContentValues();

            values.put(DataStruct.CityListDB.ldb_cid, tshoponserlist.get(i).getCid());
            values.put(DataStruct.CityListDB.ldb_status, tshoponserlist.get(i).getStatus());
            values.put(DataStruct.CityListDB.ldb_message, tshoponserlist.get(i).getMessage());
            values.put(DataStruct.CityListDB.ldb_name, tshoponserlist.get(i).getName());
            values.put(DataStruct.CityListDB.ldb_active_status, tshoponserlist.get(i).getActiveStatus());
            Log.v("List<City>", "ldb_cid =" + tshoponserlist.get(i).getCid() + " ldb_name = " + tshoponserlist.get(i).getName());
            long newRowId = db.insert(DataStruct.CityListDB.ldb_TABLE_NAME, null, values);
        }// end of for
    }//e end of function

    public List<City> getCityListDB() {
        List<City> tserlist = new ArrayList<City>();

        String[] projection = DataStruct.CityListDB.column;
        String selection = DataStruct.CityListDB.ldb_active_status + " = ?";
        String[] selectionArgs = {"1"};

        Cursor c = db.query(DataStruct.CityListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            Log.v("city list count", "c.getCount() " + c.getCount());
            do {

                City t = new City();
                t.setMessage("" + c.getString(c.getColumnIndex(DataStruct.CityListDB.ldb_message)));
                t.setStatus(Integer.parseInt("" + c.getString(c.getColumnIndex(DataStruct.CityListDB.ldb_status))));
                t.setCid("" + c.getString(c.getColumnIndex(DataStruct.CityListDB.ldb_cid)));
                t.setName("" + c.getString(c.getColumnIndex(DataStruct.CityListDB.ldb_name)));
                t.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.CityListDB.ldb_active_status)));


                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }

    public void updateShopListDB(List<Shop> shoprating) {
        for (int idx = 0; idx < shoprating.size(); idx++) {
            ContentValues cv = new ContentValues();
            cv.put(DataStruct.ShopListDB.ldb_avgratings, shoprating.get(idx).getAvgratings()); //These Fields should be your String values of actual column names
            db.update(DataStruct.ShopListDB.ldb_TABLE_NAME, cv, DataStruct.ShopListDB.ldb_sid + " = " + shoprating.get(idx).getSid(), null);
        }
    }

    public void updateCoachListDB(List<Coach> coachRatings) {
        for (int idx = 0; idx < coachRatings.size(); idx++) {
            ContentValues cv = new ContentValues();
            cv.put(DataStruct.CoachListDB.ldb_ratings, coachRatings.get(idx).getAvgratings()); //These Fields should be your String values of actual column names
            db.update(DataStruct.CoachListDB.ldb_TABLE_NAME, cv, DataStruct.CoachListDB.ldb_coach_id + " = " + coachRatings.get(idx).getCid(), null);
        }
    }

    public void insertintoEventListDB(List<Event> eventlist) {
        db.delete(DataStruct.EventListDB.ldb_TABLE_NAME, null, null);
        db.delete(DataStruct.EventBTimeDB.ldb_TABLE_NAME, null, null);


        for (int eventidx = 0; eventidx < eventlist.size(); eventidx++) {
            ContentValues values = new ContentValues();
            values.put(DataStruct.EventListDB.ldb_status, eventlist.get(eventidx).getStatus());
            values.put(DataStruct.EventListDB.ldb_eid, eventlist.get(eventidx).getEid());
            values.put(DataStruct.EventListDB.ldb_name, eventlist.get(eventidx).getName());
            values.put(DataStruct.EventListDB.ldb_cp, eventlist.get(eventidx).getContactPerson());
            values.put(DataStruct.EventListDB.ldb_cm, eventlist.get(eventidx).getContactMobile());
            values.put(DataStruct.EventListDB.ldb_cemail, eventlist.get(eventidx).getContactEmail());
            values.put(DataStruct.EventListDB.ldb_loc, eventlist.get(eventidx).getLocation());
            values.put(DataStruct.EventListDB.ldb_city, eventlist.get(eventidx).getCity());
            values.put(DataStruct.EventListDB.ldb_address, eventlist.get(eventidx).getAddress());
            values.put(DataStruct.EventListDB.ldb_latitude, eventlist.get(eventidx).getLatitude());
            values.put(DataStruct.EventListDB.ldb_longitude, eventlist.get(eventidx).getLongitude());
            values.put(DataStruct.EventListDB.ldb_image, eventlist.get(eventidx).getImage());
            values.put(DataStruct.EventListDB.ldb_eventType, eventlist.get(eventidx).getEventType());
            values.put(DataStruct.EventListDB.ldb_cost, eventlist.get(eventidx).getCost());
            values.put(DataStruct.EventListDB.ldb_desc, eventlist.get(eventidx).getDescription());
            values.put(DataStruct.EventListDB.ldb_aval, eventlist.get(eventidx).getAvailability());
            values.put(DataStruct.EventListDB.ldb_ageTypes, eventlist.get(eventidx).getAgeTypes());
            values.put(DataStruct.EventListDB.ldb_ageYears, eventlist.get(eventidx).getAgeYears());
            values.put(DataStruct.EventListDB.ldb_gender, eventlist.get(eventidx).getGender());
            values.put(DataStruct.EventListDB.ldb_any, eventlist.get(eventidx).getAnythings());
            values.put(DataStruct.EventListDB.ldb_rat, eventlist.get(eventidx).getAvgratings());
            // values.put(DataStruct.EventListDB.ldb_isPassAvailable, eventlist.get(eventidx).getIsPassAvailable());
            values.put(DataStruct.EventListDB.ldb_activeStatus, eventlist.get(eventidx).getActiveStatus());
            values.put(DataStruct.EventListDB.ldb_createdDate, eventlist.get(eventidx).getCreatedDate());
            long newRowId = db.insert(DataStruct.EventListDB.ldb_TABLE_NAME, null, values);


            //insert all the batch details in to event batch time table
            if (eventlist.get(eventidx).getBatchDetails().size() > 0) {
                for (int bidx = 0; bidx < eventlist.get(eventidx).getBatchDetails().size(); bidx++) {

                    if (eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().size() > 0) {
                        for (int btidx = 0; btidx < eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().size(); btidx++) {
                            ContentValues batchTDetails = new ContentValues();
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_id, eventlist.get(eventidx).getEid());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_batch_id, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatchId());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_batch_name, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatchName());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_batch_time_id, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().get(btidx).getBatchTimeId());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_start_date, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().get(btidx).getStartDate());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_end_date, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().get(btidx).getStartDate());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_start_time, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().get(btidx).getStartTime());
                            batchTDetails.put(DataStruct.EventBTimeDB.ldb_end_time, eventlist.get(eventidx).getBatchDetails().get(bidx).getBatch_time().get(btidx).getEndTime());
                            db.insert(DataStruct.EventBTimeDB.ldb_TABLE_NAME, null, batchTDetails);


                        }
                    }
                }

            }


        }

    }


    public List<Event> getEventListDB() {

        List<Event> tserlist = new ArrayList<Event>();

        String[] projection = DataStruct.EventListDB.column;


        Cursor c = db.query(DataStruct.EventListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {
                Event event = new Event();
                event.setStatus(c.getInt(c.getColumnIndex(DataStruct.EventListDB.ldb_status)));
                event.setEid("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_eid)));
                event.setName("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_name)));
                event.setContactPerson("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cp)));
                event.setContactMobile("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cm)));
                event.setContactEmail("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cemail)));
                event.setLocation("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_loc)));
                event.setCity("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_city)));
                event.setAddress("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_address)));
                event.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_latitude)));
                event.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_longitude)));
              /*  event.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_active_status)));
                event.setPriority("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_priority)));*/
                event.setImage("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_image)));
                event.setEventType("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_eventType)));
                event.setCost("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cost)));
                event.setDescription("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_desc)));
                event.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_aval)));
                event.setAgeTypes("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_ageTypes)));
                event.setAgeYears("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_ageYears)));
                event.setGender("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_gender)));
                event.setAnythings("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_any)));
                event.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_rat)));
                event.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_activeStatus)));
                event.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_createdDate)));


                tserlist.add(event);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }
    public Event getEventListById(String eventId) {

        Event event = new Event();
        String selection = DataStruct.EventListDB.ldb_eid + " = ?";
        String[] projection = DataStruct.EventListDB.column;
        String[] selectionArgs = {eventId};

        Cursor c = db.query(DataStruct.EventListDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();

            do {

                event.setStatus(c.getInt(c.getColumnIndex(DataStruct.EventListDB.ldb_status)));
                event.setEid("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_eid)));
                event.setName("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_name)));
                event.setContactPerson("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cp)));
                event.setContactMobile("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cm)));
                event.setContactEmail("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cemail)));
                event.setLocation("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_loc)));
                event.setCity("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_city)));
                event.setAddress("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_address)));
                event.setLatitude("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_latitude)));
                event.setLongitude("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_longitude)));
              /*  event.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_active_status)));
                event.setPriority("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_priority)));*/
                event.setImage("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_image)));
                event.setEventType("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_eventType)));
                event.setCost("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_cost)));
                event.setDescription("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_desc)));
                event.setAvailability("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_aval)));
                event.setAgeTypes("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_ageTypes)));
                event.setAgeYears("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_ageYears)));
                event.setGender("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_gender)));
                event.setAnythings("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_any)));
                event.setAvgratings("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_rat)));
                event.setActiveStatus("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_activeStatus)));
                event.setCreatedDate("" + c.getString(c.getColumnIndex(DataStruct.EventListDB.ldb_createdDate)));



            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return event;
    }
    public List<BatchTime> getEventTypeById(String eventId) {
        List<BatchTime> tserlist = new ArrayList<BatchTime>();
        String[] projection = DataStruct.EventBTimeDB.column;
        String selection = DataStruct.EventBTimeDB.ldb_id + " = ?";
        String[] selectionArgs = {eventId};
        Cursor c = db.query(DataStruct.EventBTimeDB.ldb_TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                DataStruct.EventBTimeDB.ldb_batch_time_id                                 // The sort order
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            do {
                BatchTime t = new BatchTime();
                t.setBatchTimeId("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_batch_time_id)));
                t.setBatchName("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_batch_name)));
                t.setStartDate("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_start_date)));
                t.setEndDate("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_end_date)));
                t.setStartTime("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_start_time)));
                t.setEndTime("" + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_end_time)));
                t.setBatchName(" " + c.getString(c.getColumnIndex(DataStruct.EventBTimeDB.ldb_batch_name)));

                tserlist.add(t);
            } while (c.moveToNext());
            c.close();
        } else {
            return null;
        }
        return tserlist;
    }
}
