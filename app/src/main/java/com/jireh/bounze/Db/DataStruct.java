package com.jireh.bounze.Db;

import android.provider.BaseColumns;

/**
 * Created by android on 2/11/2017.
 */

public class DataStruct {


    public DataStruct() {

    }


    public static class ServiceListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "servicelist";
        public static final String ldb_status = "status";
        public static final String ldb_iconid = "iconid";
        public static final String ldb_message = "message";
        public static final String ldb_sid = "sid";
        public static final String ldb_title = "title";
        public static final String ldb_description = "description";
        public static final String ldb_image_url = "image_url";
        public static final String ldb_active_status = "active_status";
        public static final String ldb_priority = "priority";
        public static final String ldb_updated_date = "updated_date";
        public static final String ldb_created_date = "created_date";

        public static final String[] column = {ldb_status, ldb_iconid, ldb_message, ldb_sid, ldb_title, ldb_description, ldb_image_url, ldb_active_status, ldb_priority, ldb_updated_date, ldb_created_date};
    }

    public static class ServiceSubListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "service_sublist";
        public static final String ldb_status = "status";
        public static final String ldb_iconid = "iconid";
        public static final String ldb_message = "message";
        public static final String ldb_sid = "sid";
        public static final String ldb_title = "title";
        public static final String ldb_description = "description";
        public static final String ldb_image_url = "image_url";
        public static final String ldb_active_status = "active_status";
        public static final String ldb_priority = "priority";
        public static final String ldb_updated_date = "updated_date";
        public static final String ldb_created_date = "created_date";
        public static final String[] column = {ldb_status, ldb_iconid, ldb_message, ldb_sid, ldb_title, ldb_description, ldb_image_url, ldb_active_status, ldb_priority, ldb_updated_date, ldb_created_date};
    }

    public static class ShopListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "shoplist";
        public static final String ldb_status = "status";
        public static final String ldb_message = "message";
        public static final String ldb_sid = "sid";
        public static final String ldb_title = "title";
        public static final String ldb_avgratings = "avgratings";
        public static final String ldb_description = "description";
        public static final String ldb_services = "services";
        public static final String ldb_subservices = "subservices";
        public static final String ldb_service_id = "service_id";
        public static final String ldb_sservice_id = "subservice_id";

        public static final String ldb_image_url_square = "image_url_square";
        public static final String ldb_image_url_rectangle = "image_url_rectangle";
        public static final String ldb_latitude = "latitude";
        public static final String ldb_longitude = "longitude";
        public static final String ldb_city = "city";
        public static final String ldb_full_address = "full_address";
        public static final String ldb_small_address = "small_address";
        public static final String ldb_contact_number = "contact_number";
        public static final String ldb_shop_timings = "shop_timings";
        public static final String ldb_shop_avlbty = "shop_avlbty";
        public static final String ldb_admin_lock_status = "admin_lock_status";
        public static final String ldb_cost = "cost";
        public static final String ldb_email_id = "email_id";
        public static final String ldb_isRegnAvailable = "isRegnAvailable";

        public static final String ldb_isFreeTrailAvailable = "isFreeTrailAvailable";
        public static final String ldb_no_of_free_trial = "no_of_free_trial";
        public static final String ldb_isOfferAvailable = "isOfferAvailable";
        public static final String ldb_password = "password";
        public static final String ldb_book_online = "book_online";
        public static final String ldb_updated_date = "updated_date";
        public static final String ldb_created_date = "created_date";

        public static final String[] column = {ldb_status, ldb_message, ldb_sid, ldb_title, ldb_avgratings, ldb_description, ldb_services, ldb_subservices
                , ldb_service_id, ldb_sservice_id, ldb_image_url_square, ldb_image_url_rectangle, ldb_latitude, ldb_longitude, ldb_city, ldb_full_address
                , ldb_small_address, ldb_contact_number, ldb_shop_timings, ldb_shop_avlbty, ldb_admin_lock_status, ldb_cost, ldb_email_id, ldb_isRegnAvailable,
                ldb_isFreeTrailAvailable, ldb_no_of_free_trial, ldb_isOfferAvailable, ldb_password,ldb_book_online,ldb_updated_date, ldb_created_date};

    }

    public static class ShopServiceDetailsDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "shop_service_details_db";
        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_shop_sservice_id = "sserivce_id";
        public static final String ldb_package_id = "package_id";
        public static final String ldb_package_name = "package_name";
        public static final String ldb_description = "description";
        //public static final String ldb_per_session_cost = "per_session_cost";
        public static final String ldb_availability = "availability";
        public static final String ldb_age_types = "age_types";
        public static final String ldb_age_years = "age_years";
        public static final String ldb_gender = "gender";
        public static final String ldb_anythings = "anythings";
     /*   public static final String ldb_no_of_sessions = "no_of_sessions";
        public static final String ldb_session_hour = "session_hour";
        public static final String ldb_cost = "cost";
        public static final String ldb_offer_cost = "offer_cost";*/


        public static final String[] column = {ldb_shop_id, ldb_shop_sservice_id, ldb_package_id, ldb_package_name, ldb_description,
                ldb_availability, ldb_age_types, ldb_age_years, ldb_gender, ldb_anythings

        };
    }

    public static class ShoponServiceListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "shoponservicelist";

        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_sservice_id = "sub_service_id";
        public static final String ldb_servie_name = "service_name";
        public static final String ldb_subservice_name = "subservice_name";
        public static final String ldb_pay_per_use = "pay_per_use";
        public static final String[] column = {ldb_shop_id, ldb_sservice_id, ldb_servie_name, ldb_subservice_name, ldb_pay_per_use};
    }


   /* public static class BDetailsDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "batch_details";

        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_service_id = "service_id";
        public static final String ldb_batch_id = "batch_id";
        public static final String ldb_batch_name = "batch_name";

        public static final String[] column = {ldb_shop_id, ldb_service_id, ldb_batch_id, ldb_batch_name};
    }*/


    public static class BTimeDetailsDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "batch_time_details";

        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_sub_service_id = "sservice_id";
        public static final String ldb_package_id = "package_id";
        public static final String ldb_batch_id = "batch_id";
        public static final String ldb_batch_name = "batch_name";
        public static final String ldb_batch_time_id = "batch_tid";
        public static final String ldb_start_time = "start_time";
        public static final String ldb_end_time = "end_time";

        public static final String[] column = {ldb_shop_id, ldb_sub_service_id, ldb_package_id,
                ldb_batch_id, ldb_batch_name, ldb_batch_time_id,
                ldb_start_time, ldb_end_time};
    }


    public static class SessDetailsDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "session_details";
        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_sservice_id = "sub_service_id";
        public static final String ldb_package_id = "package_id";
        public static final String ldb_session_id = "session_id";
        public static final String ldb_no_of_days = "no_of_days";
        public static final String ldb_day_units = "days_unit";
        public static final String ldb_sessions = "sessions";
        public static final String ldb_sessions_cost = "sesssions_cost";
        public static final String ldb_sessions_offcost = "sess_off_cost";


        public static final String[] column = {ldb_shop_id, ldb_sservice_id, ldb_package_id,
                ldb_session_id, ldb_no_of_days, ldb_day_units, ldb_sessions, ldb_sessions_cost, ldb_sessions_offcost};
    }


    public static class CoachListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "coachlist";

        public static final String ldb_status = "status";
        public static final String ldb_coach_id = "coach_id";
        public static final String ldb_title = "title";
        public static final String ldb_ratings = "avgratings";
        public static final String ldb_description = "description";
        public static final String ldb_cat_id = "category_id";
        public static final String ldb_category = "category";
        public static final String ldb_image_url_square = "image_url_square";
        public static final String ldb_image_url_rectangle = "image_url_rectangle";
        public static final String ldb_active_status = "active_status";
        public static final String ldb_priority = "priority";
        public static final String ldb_latitude = "latitude";
        public static final String ldb_longitude = "longitude";
        public static final String ldb_city = "city";
        public static final String ldb_full_address = "full_address";
        public static final String ldb_small_address = "small_address";
        public static final String ldb_contact_number = "contact_number";
        public static final String ldb_admin_lock_status = "admin_lock_status";
        public static final String ldb_cost = "cost";
        public static final String ldb_isFreeTrailAvailable = "isFreeTrailAvailable";
        public static final String ldb_no_of_free_trial = "no_of_free_trial";
        public static final String ldb_updated_date = "updated_date";
        public static final String ldb_created_date = "created_date";
        public static final String[] column = {ldb_status, ldb_coach_id, ldb_title, ldb_ratings, ldb_description, ldb_cat_id, ldb_category,
                ldb_image_url_square, ldb_image_url_rectangle, ldb_active_status, ldb_priority, ldb_latitude, ldb_longitude,
                ldb_city, ldb_full_address, ldb_small_address, ldb_contact_number, ldb_admin_lock_status, ldb_cost, ldb_isFreeTrailAvailable, ldb_no_of_free_trial, ldb_updated_date, ldb_created_date};
    }

    public static class CoachCatDetailsDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "coachcat_details";
        public static final String ldb_coach_id = "coach_id";
        public static final String ldb_coach_cat_id = "coach_cat_id";
        public static final String ldb_coach_pack = "coach_package";
        public static final String ldb_coach_packname = "coach_package_name";
        public static final String ldb_avl = "available";
        public static final String ldb_gender = "gender";
        public static final String ldb_anythings = "anythings";
        public static final String[] column = {ldb_coach_id, ldb_coach_cat_id, ldb_coach_pack, ldb_coach_packname, ldb_avl, ldb_gender, ldb_anythings};
    }

    public static class CoachCatAllListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "coachcatalllist";
        public static final String ldb_status = "status";
        public static final String ldb_message = "message";
        public static final String ldb_cid = "cid";
        public static final String ldb_title = "title";
        public static final String ldb_description = "description";
        public static final String ldb_image_url = "image_url";
        public static final String ldb_active_status = "active_status";
        public static final String ldb_priority = "priority";
        public static final String ldb_updated_date = "updated_date";
        public static final String ldb_created_date = "created_date";
        public static final String[] column = {ldb_status, ldb_message, ldb_cid, ldb_title, ldb_description, ldb_image_url, ldb_active_status, ldb_priority, ldb_updated_date, ldb_created_date};
    }

    public static class CoachProgramListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "coachprogramlist";
        public static final String ldb_coach_id = "coach_id";
        public static final String ldb_category_id = "category_id";
        public static final String ldb_category_name = "category_name";
        public static final String ldb_pay_per_use = "pay_per_use";


        public static final String[] column = {ldb_coach_id, ldb_category_id, ldb_category_name, ldb_pay_per_use};

    }

    public static class CityListDB implements BaseColumns {


        public static final String ldb_TABLE_NAME = "citylist";
        public static final String ldb_status = "status";
        public static final String ldb_message = "message";
        public static final String ldb_cid = "cid";
        public static final String ldb_name = "name";
        public static final String ldb_active_status = "active_status";

        public static final String[] column = {ldb_status, ldb_message, ldb_cid, ldb_name, ldb_active_status};
    }

    public static class CoachBatchTimeDB implements BaseColumns {

        public static final String ldb_TABLE_NAME = "coach_batch_time";
        public static final String ldb_coach_id = "coach_id";
        public static final String ldb_coach_pack = "coach_package";
        public static final String ldb_coach_batch_id = "coach_batch_id";
        public static final String ldb_coach_batch_name = "coach_batch_name";
        public static final String ldb_coach_batch_time_id = "c_batch_timeid";
        public static final String ldb_start_time = "start_time";
        public static final String ldb_end_time = "end_time";
        public static final String[] column = {ldb_coach_id, ldb_coach_pack, ldb_coach_batch_id, ldb_coach_batch_name,
                ldb_coach_batch_time_id, ldb_start_time, ldb_end_time};


    }

    public static class CoachSessionTimeDB implements BaseColumns {


        public static final String ldb_TABLE_NAME = "coach_session_time";
        public static final String ldb_coach_id = "coach_id";
        public static final String ldb_coach_pack = "coach_package";
        public static final String ldb_session_id = "session_id";
        public static final String ldb_no_of_days = "no_of_days";
        public static final String ldb_days_unit = "days_unit";
        public static final String ldb_sessions = "sessions";
        public static final String ldb_sessions_cost = "sessions_cost";
        public static final String ldb_sessions_offer_cost = "sessions_offer_cost";

        public static final String[] column = {ldb_coach_id, ldb_coach_pack, ldb_session_id, ldb_no_of_days,
                ldb_days_unit, ldb_sessions, ldb_sessions_cost, ldb_sessions_offer_cost};
    }

    public static class PassListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "passlist";
        public static final String ldb_pass_id = "pass_id";

        public static final String ldb_type = "type";
        public static final String ldb_pass_image = "pass_image";
        public static final String ldb_service_logo1 = "service_logo1";
        public static final String ldb_service_logo2 = "service_logo2";
        public static final String ldb_service_logo3 = "service_logo3";
        public static final String ldb_service_logo4 = "service_logo4";
        public static final String ldb_service_logo5 = "service_logo5";
        public static final String ldb_ratings = "ratings";
        public static final String ldb_sessions = "sessions";
        public static final String ldb_price = "price";
        public static final String ldb_areas = "areas";
        public static final String ldb_total_bought = "total_bought";
        public static final String ldb_remove_status = "remove_status";
        public static final String ldb_active_status = "active_status";

        public static final String[] column = {ldb_pass_id, ldb_type, ldb_pass_image, ldb_service_logo1, ldb_service_logo2,
                ldb_service_logo3, ldb_service_logo4, ldb_service_logo5, ldb_ratings, ldb_sessions, ldb_price, ldb_areas, ldb_total_bought, ldb_remove_status, ldb_active_status};
    }


    public static class AmenitiesDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "amenities";
        public static final String ldb_shop_id = "shop_id";
        public static final String ldb_amenity_id = "amenity_id";
        public static final String ldb_amenity_name = "amenity_name";
        public static final String ldb_aicon = "aicon";


        public static final String[] column = {ldb_shop_id, ldb_amenity_id, ldb_amenity_name, ldb_aicon};
    }

    public static class AmenitiesListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "amenities_list";
        public static final String ldb_id = "id";

        public static final String ldb_amenity_name = "amenity_name";


        public static final String[] column = {ldb_id, ldb_amenity_name};
    }

    public static class LocationsList implements BaseColumns {
        public static final String ldb_TABLE_NAME = "locations_list";
        public static final String ldb_id = "id";
        public static final String ldb_location = "location";


        public static final String[] column = {ldb_id, ldb_location};
    }

    public static class FilterShopDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "filter_sdb";
        public static final String ldb_id = "id";
        public static final String ldb_category = "Category";
        public static final String ldb_value = "Value";
        public static final String ldb_position = "Position";

        public static final String[] column = {ldb_id, ldb_category, ldb_value, ldb_position};
    }

    public static class FilterCoachDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "filter_cdb";
        public static final String ldb_id = "id";
        public static final String ldb_category = "Category";
        public static final String ldb_value = "Value";
        public static final String ldb_position = "Position";

        public static final String[] column = {ldb_id, ldb_category, ldb_value, ldb_position};
    }

    public static class FilterPassDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "filter_pdb";
        public static final String ldb_id = "id";
        public static final String ldb_category = "Category";
        public static final String ldb_value = "Value";
        public static final String ldb_position = "Position";

        public static final String[] column = {ldb_id, ldb_category, ldb_value, ldb_position};
    }

    public static class EventListDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "EventList";

        public static final String ldb_status = "status";
        public static final String ldb_eid = "eid";
        public static final String ldb_name = "name";
        public static final String ldb_cp = "contactPerson";
        public static final String ldb_cm = "contactMobile";
        public static final String ldb_cemail = "contactEmail";
        public static final String ldb_loc = "location";
        public static final String ldb_city = "city";
        public static final String ldb_address = "address";
        public static final String ldb_latitude = "latitude";
        public static final String ldb_longitude = "longitude";
        public static final String ldb_image = "image";
        public static final String ldb_eventType = "eventType";
        public static final String ldb_cost = "cost";
        public static final String ldb_desc = "description";
        public static final String ldb_aval = "availability";
        public static final String ldb_ageTypes = "ageTypes";
        public static final String ldb_ageYears = "ageYears";
        public static final String ldb_gender = "gender";
        public static final String ldb_any = "anythings";
        public static final String ldb_rat = "avgratings";
        public static final String ldb_activeStatus = "activeStatus";
        public static final String ldb_createdDate = "createdDate";
        public static final String[] column = {ldb_status, ldb_eid, ldb_name, ldb_cp, ldb_cm, ldb_cemail, ldb_loc, ldb_city, ldb_address,
                ldb_latitude, ldb_longitude, ldb_image, ldb_eventType, ldb_cost, ldb_desc, ldb_aval, ldb_ageTypes, ldb_ageYears,
                ldb_gender, ldb_any, ldb_rat, ldb_activeStatus, ldb_createdDate};


    }

    public static class EventBTimeDB implements BaseColumns {
        public static final String ldb_TABLE_NAME = "EventBatchTime";
        public static final String ldb_id = "event_id";
        public static final String ldb_batch_id = "batch_id";
        public static final String ldb_batch_name = "batch_name";
        public static final String ldb_batch_time_id = "batch_tid";
        public static final String ldb_start_date = "start_date";
        public static final String ldb_end_date = "end_date";
        public static final String ldb_start_time = "start_time";
        public static final String ldb_end_time = "end_time";

        public static final String[] column = {ldb_id, ldb_batch_id, ldb_batch_name, ldb_batch_time_id, ldb_start_date,
                ldb_end_date, ldb_start_time, ldb_end_time};
    }
}

