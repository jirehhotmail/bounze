package com.jireh.bounze.Db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by android on 2/11/2017.
 */

public class DBTables extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 39;
    public static final String DATABASE_NAME = "bounzedb.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static String CREATE_ServiceListDB = "";
    private static String CREATE_ServiceSubListDB = "";
    private static String CREATE_ShopListDB = "";

    private static String CREATE_CoachListDB = "";
    private static String CREATE_CoachCatListDB = "";
    private static String CREATE_CoachCatAllListDB = "";
    private static String CREATE_CoachProgramListDB = "";
    private static String CREATE_CoachBatchTimeDB = "";
    private static String CREATE_CoachSessionTimeDB = "";


    private static String CREATE_CityListDB = "";
    private static String CREATE_PassListDB = "";
    private static String CREATE_AmenitiesDB = "";
    private static String CREATE_ShoponServiceListDB = "";
    private static String CREATE_ShopServiceDetailsDB = "";

    private static String CREATE_BTimeDetailsDB = "";
    private static String CREATE_SessDetailsDB = "";
    private static String CREATE_AmenitiesListDB = "";
    private static String CREATE_LocationListDB = "";
    private static String CREATE_FilterShopDB = "";
    private static String CREATE_FilterCoachDB = "";
    private static String CREATE_FilterPassDB = "";
    private static String CREATE_EventListDB = "";
    private static String CREATE_EventBTimeDB = "";

    private static String DROP_ServiceListDB = "DROP TABLE IF EXISTS " + DataStruct.ServiceListDB.ldb_TABLE_NAME;
    private static String DROP_ServiceSubListDB = "DROP TABLE IF EXISTS " + DataStruct.ServiceSubListDB.ldb_TABLE_NAME;
    private static String DROP_ShopListDB = "DROP TABLE IF EXISTS " + DataStruct.ShopListDB.ldb_TABLE_NAME;
    private static String DROP_ShoponServiceListDB = "DROP TABLE IF EXISTS " + DataStruct.ShoponServiceListDB.ldb_TABLE_NAME;
    private static String DROP_CoachListDB = "DROP TABLE IF EXISTS " + DataStruct.CoachListDB.ldb_TABLE_NAME;
    private static String DROP_CoachCatListDB = "DROP TABLE IF EXISTS " + DataStruct.CoachCatDetailsDB.ldb_TABLE_NAME;
    private static String DROP_CoachProgramListDB = "DROP TABLE IF EXISTS " + DataStruct.CoachProgramListDB.ldb_TABLE_NAME;
    private static String DROP_CityListDB = "DROP TABLE IF EXISTS " + DataStruct.CityListDB.ldb_TABLE_NAME;
    private static String DROP_PassListDB = "DROP TABLE IF EXISTS " + DataStruct.PassListDB.ldb_TABLE_NAME;
    private static String DROP_AmenitiesDB = "DROP TABLE IF EXISTS " + DataStruct.AmenitiesDB.ldb_TABLE_NAME;
    private static String DROP_ShopServiceDetailsDB = "DROP TABLE IF EXISTS " + DataStruct.ShopServiceDetailsDB.ldb_TABLE_NAME;
    private static String DROP_CoachCatAllListDB = "DROP TABLE IF EXISTS " + DataStruct.CoachCatAllListDB.ldb_TABLE_NAME;
    private static String DROP_BTimeDetailsDB = "DROP TABLE IF EXISTS " + DataStruct.BTimeDetailsDB.ldb_TABLE_NAME;
    private static String DROP_SessDetailsDB = "DROP TABLE IF EXISTS " + DataStruct.SessDetailsDB.ldb_TABLE_NAME;

    private static String DROP_AmenitiesListDB = "DROP TABLE IF EXISTS " + DataStruct.AmenitiesListDB.ldb_TABLE_NAME;
    private static String DROP_LocationListDB = "DROP TABLE IF EXISTS " + DataStruct.LocationsList.ldb_TABLE_NAME;

    private static String DROP_CoachBatchTimeDB = "DROP TABLE IF EXISTS " + DataStruct.CoachBatchTimeDB.ldb_TABLE_NAME;

    private static String DROP_CoachSessionTimeDB = "DROP TABLE IF EXISTS " + DataStruct.CoachSessionTimeDB.ldb_TABLE_NAME;
    private static String DROP_FilterShopDB = "DROP TABLE IF EXISTS " + DataStruct.FilterShopDB.ldb_TABLE_NAME;
    private static String DROP_FilterCoachDB = "DROP TABLE IF EXISTS " + DataStruct.FilterCoachDB.ldb_TABLE_NAME;
    private static String DROP_FilterPassDB = "DROP TABLE IF EXISTS " + DataStruct.FilterPassDB.ldb_TABLE_NAME;
    private static String DROP_EventListDB = "DROP TABLE IF EXISTS " + DataStruct.EventListDB.ldb_TABLE_NAME;
    private static String DROP_EventBTimeDB = "DROP TABLE IF EXISTS " + DataStruct.EventBTimeDB.ldb_TABLE_NAME;

    public DBTables(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        CREATE_ServiceListDB = "CREATE TABLE " + DataStruct.ServiceListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.ServiceListDB.column.length; i++) {
            CREATE_ServiceListDB += DataStruct.ServiceListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.ServiceListDB.column.length - 1)
                CREATE_ServiceListDB += COMMA_SEP;
            else
                CREATE_ServiceListDB += ")";
        }
        CREATE_ServiceSubListDB = "CREATE TABLE " + DataStruct.ServiceSubListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.ServiceSubListDB.column.length; i++) {
            CREATE_ServiceSubListDB += DataStruct.ServiceSubListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.ServiceSubListDB.column.length - 1)
                CREATE_ServiceSubListDB += COMMA_SEP;
            else
                CREATE_ServiceSubListDB += ")";
        }
        CREATE_ShopListDB = "CREATE TABLE " + DataStruct.ShopListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.ShopListDB.column.length; i++) {
            CREATE_ShopListDB += DataStruct.ShopListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.ShopListDB.column.length - 1)
                CREATE_ShopListDB += COMMA_SEP;
            else
                CREATE_ShopListDB += ")";
        }

        CREATE_ShoponServiceListDB = "CREATE TABLE " + DataStruct.ShoponServiceListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.ShoponServiceListDB.column.length; i++) {
            CREATE_ShoponServiceListDB += DataStruct.ShoponServiceListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.ShoponServiceListDB.column.length - 1)
                CREATE_ShoponServiceListDB += COMMA_SEP;
            else
                CREATE_ShoponServiceListDB += ")";
        }
        CREATE_CoachListDB = "CREATE TABLE " + DataStruct.CoachListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.CoachListDB.column.length; i++) {
            CREATE_CoachListDB += DataStruct.CoachListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.CoachListDB.column.length - 1)
                CREATE_CoachListDB += COMMA_SEP;
            else
                CREATE_CoachListDB += ")";
        }
        CREATE_CoachCatListDB = "CREATE TABLE " + DataStruct.CoachCatDetailsDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.CoachCatDetailsDB.column.length; i++) {
            CREATE_CoachCatListDB += DataStruct.CoachCatDetailsDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.CoachCatDetailsDB.column.length - 1)
                CREATE_CoachCatListDB += COMMA_SEP;
            else
                CREATE_CoachCatListDB += ")";
        }

        CREATE_CoachCatAllListDB = "CREATE TABLE " + DataStruct.CoachCatAllListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.CoachCatAllListDB.column.length; i++) {
            CREATE_CoachCatAllListDB += DataStruct.CoachCatAllListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.CoachCatAllListDB.column.length - 1)
                CREATE_CoachCatAllListDB += COMMA_SEP;
            else
                CREATE_CoachCatAllListDB += ")";
        }


        CREATE_CoachProgramListDB = "CREATE TABLE " + DataStruct.CoachProgramListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.CoachProgramListDB.column.length; i++) {
            CREATE_CoachProgramListDB += DataStruct.CoachProgramListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.CoachProgramListDB.column.length - 1)
                CREATE_CoachProgramListDB += COMMA_SEP;
            else
                CREATE_CoachProgramListDB += ")";
        }
        CREATE_CityListDB = "CREATE TABLE " + DataStruct.CityListDB.ldb_TABLE_NAME + " (";
        for (int i = 0; i < DataStruct.CityListDB.column.length; i++) {
            CREATE_CityListDB += DataStruct.CityListDB.column[i] + TEXT_TYPE;
            if (i != DataStruct.CityListDB.column.length - 1)
                CREATE_CityListDB += COMMA_SEP;
            else
                CREATE_CityListDB += ")";
        }

        CREATE_PassListDB = "CREATE TABLE " + DataStruct.PassListDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.PassListDB.column.length; idx++) {
            CREATE_PassListDB += DataStruct.PassListDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.PassListDB.column.length - 1)
                CREATE_PassListDB += COMMA_SEP;
            else
                CREATE_PassListDB += ")";
        }


        CREATE_AmenitiesDB = "CREATE TABLE " + DataStruct.AmenitiesDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.AmenitiesDB.column.length; idx++) {
            CREATE_AmenitiesDB += DataStruct.AmenitiesDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.AmenitiesDB.column.length - 1)
                CREATE_AmenitiesDB += COMMA_SEP;
            else
                CREATE_AmenitiesDB += ")";
        }


        CREATE_ShopServiceDetailsDB = "CREATE TABLE " + DataStruct.ShopServiceDetailsDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.ShopServiceDetailsDB.column.length; idx++) {
            CREATE_ShopServiceDetailsDB += DataStruct.ShopServiceDetailsDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.ShopServiceDetailsDB.column.length - 1)
                CREATE_ShopServiceDetailsDB += COMMA_SEP;
            else
                CREATE_ShopServiceDetailsDB += ")";
        }



        CREATE_BTimeDetailsDB = "CREATE TABLE " + DataStruct.BTimeDetailsDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.BTimeDetailsDB.column.length; idx++) {
            CREATE_BTimeDetailsDB += DataStruct.BTimeDetailsDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.BTimeDetailsDB.column.length - 1)
                CREATE_BTimeDetailsDB += COMMA_SEP;
            else
                CREATE_BTimeDetailsDB += ")";
        }
        CREATE_SessDetailsDB = "CREATE TABLE " + DataStruct.SessDetailsDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.SessDetailsDB.column.length; idx++) {
            CREATE_SessDetailsDB += DataStruct.SessDetailsDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.SessDetailsDB.column.length - 1)
                CREATE_SessDetailsDB += COMMA_SEP;
            else
                CREATE_SessDetailsDB += ")";
        }

        CREATE_AmenitiesListDB = "CREATE TABLE " + DataStruct.AmenitiesListDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.AmenitiesListDB.column.length; idx++) {
            CREATE_AmenitiesListDB += DataStruct.AmenitiesListDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.AmenitiesListDB.column.length - 1)
                CREATE_AmenitiesListDB += COMMA_SEP;
            else
                CREATE_AmenitiesListDB += ")";
        }

        CREATE_LocationListDB = "CREATE TABLE " + DataStruct.LocationsList.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.LocationsList.column.length; idx++) {
            CREATE_LocationListDB += DataStruct.LocationsList.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.LocationsList.column.length - 1)
                CREATE_LocationListDB += COMMA_SEP;
            else
                CREATE_LocationListDB += ")";
        }

        CREATE_CoachBatchTimeDB = "CREATE TABLE " + DataStruct.CoachBatchTimeDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.CoachBatchTimeDB.column.length; idx++) {
            CREATE_CoachBatchTimeDB += DataStruct.CoachBatchTimeDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.CoachBatchTimeDB.column.length - 1)
                CREATE_CoachBatchTimeDB += COMMA_SEP;
            else
                CREATE_CoachBatchTimeDB += ")";
        }

        CREATE_CoachSessionTimeDB = "CREATE TABLE " + DataStruct.CoachSessionTimeDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.CoachSessionTimeDB.column.length; idx++) {
            CREATE_CoachSessionTimeDB += DataStruct.CoachSessionTimeDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.CoachSessionTimeDB.column.length - 1)
                CREATE_CoachSessionTimeDB += COMMA_SEP;
            else
                CREATE_CoachSessionTimeDB += ")";
        }

        CREATE_FilterShopDB= "CREATE TABLE " + DataStruct.FilterShopDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.FilterShopDB.column.length; idx++) {
            CREATE_FilterShopDB += DataStruct.FilterShopDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.FilterShopDB.column.length - 1)
                CREATE_FilterShopDB += COMMA_SEP;
            else
                CREATE_FilterShopDB += ")";
        }
        CREATE_FilterCoachDB= "CREATE TABLE " + DataStruct.FilterCoachDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.FilterCoachDB.column.length; idx++) {
            CREATE_FilterCoachDB += DataStruct.FilterCoachDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.FilterCoachDB.column.length - 1)
                CREATE_FilterCoachDB += COMMA_SEP;
            else
                CREATE_FilterCoachDB += ")";
        }
        CREATE_FilterPassDB= "CREATE TABLE " + DataStruct.FilterPassDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.FilterPassDB.column.length; idx++) {
            CREATE_FilterPassDB += DataStruct.FilterPassDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.FilterPassDB.column.length - 1)
                CREATE_FilterPassDB += COMMA_SEP;
            else
                CREATE_FilterPassDB += ")";
        }
        CREATE_EventListDB= "CREATE TABLE " + DataStruct.EventListDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.EventListDB.column.length; idx++) {
            CREATE_EventListDB += DataStruct.EventListDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.EventListDB.column.length - 1)
                CREATE_EventListDB += COMMA_SEP;
            else
                CREATE_EventListDB += ")";
        }

        CREATE_EventBTimeDB= "CREATE TABLE " + DataStruct.EventBTimeDB.ldb_TABLE_NAME + " (";
        for (int idx = 0; idx < DataStruct.EventBTimeDB.column.length; idx++) {
            CREATE_EventBTimeDB += DataStruct.EventBTimeDB.column[idx] + TEXT_TYPE;
            if (idx != DataStruct.EventBTimeDB.column.length - 1)
                CREATE_EventBTimeDB += COMMA_SEP;
            else
                CREATE_EventBTimeDB += ")";
        }

        db.execSQL(DROP_ServiceListDB);
        db.execSQL(DROP_ServiceSubListDB);
        db.execSQL(DROP_ShopListDB);
        db.execSQL(DROP_ShoponServiceListDB);
        db.execSQL(DROP_CoachListDB);
        db.execSQL(DROP_CoachCatListDB);
        db.execSQL(DROP_CoachProgramListDB);
        db.execSQL(DROP_CityListDB);
        db.execSQL(DROP_PassListDB);
        db.execSQL(DROP_AmenitiesDB);
        db.execSQL(DROP_ShopServiceDetailsDB);
        db.execSQL(DROP_CoachCatAllListDB);
        db.execSQL(DROP_BTimeDetailsDB);
        db.execSQL(DROP_SessDetailsDB);
        db.execSQL(DROP_AmenitiesListDB);
        db.execSQL(DROP_LocationListDB);
        db.execSQL(DROP_CoachBatchTimeDB);
        db.execSQL(DROP_CoachSessionTimeDB);
        db.execSQL(DROP_FilterShopDB);
        db.execSQL(DROP_FilterCoachDB);
        db.execSQL(DROP_FilterPassDB);
        db.execSQL(DROP_EventListDB);
        db.execSQL(DROP_EventBTimeDB);

        db.execSQL(CREATE_ServiceListDB);
        db.execSQL(CREATE_ServiceSubListDB);
        db.execSQL(CREATE_ShopListDB);
        db.execSQL(CREATE_ShoponServiceListDB);
        db.execSQL(CREATE_CoachListDB);
        db.execSQL(CREATE_CoachCatListDB);
        db.execSQL(CREATE_CoachProgramListDB);
        db.execSQL(CREATE_CityListDB);
        db.execSQL(CREATE_PassListDB);
        db.execSQL(CREATE_AmenitiesDB);
        db.execSQL(CREATE_ShopServiceDetailsDB);
        db.execSQL(CREATE_CoachCatAllListDB);
        db.execSQL(CREATE_BTimeDetailsDB);
        db.execSQL(CREATE_SessDetailsDB);
        db.execSQL(CREATE_AmenitiesListDB);
        db.execSQL(CREATE_LocationListDB);
        db.execSQL(CREATE_CoachBatchTimeDB);
        db.execSQL(CREATE_CoachSessionTimeDB);
        db.execSQL(CREATE_FilterShopDB);
        db.execSQL(CREATE_FilterCoachDB);
        db.execSQL(CREATE_FilterPassDB);
        db.execSQL(CREATE_EventListDB);
        db.execSQL(CREATE_EventBTimeDB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_ServiceListDB);
        db.execSQL(DROP_ServiceSubListDB);
        db.execSQL(DROP_ShopListDB);
        db.execSQL(DROP_ShoponServiceListDB);
        db.execSQL(DROP_CoachListDB);
        db.execSQL(DROP_CoachCatListDB);
        db.execSQL(DROP_CoachProgramListDB);
        db.execSQL(DROP_CityListDB);
        db.execSQL(DROP_PassListDB);
        db.execSQL(DROP_AmenitiesDB);
        db.execSQL(DROP_ShopServiceDetailsDB);
        db.execSQL(DROP_CoachCatAllListDB);
        db.execSQL(DROP_BTimeDetailsDB);
        db.execSQL(DROP_SessDetailsDB);
        db.execSQL(DROP_AmenitiesListDB);
        db.execSQL(DROP_LocationListDB);
        db.execSQL(DROP_CoachBatchTimeDB);
        db.execSQL(DROP_CoachSessionTimeDB);
        db.execSQL(DROP_FilterShopDB);
        db.execSQL(DROP_FilterCoachDB);
        db.execSQL(DROP_FilterPassDB);
        db.execSQL(DROP_EventListDB);
        db.execSQL(DROP_EventBTimeDB);
        onCreate(db);
    }


}
