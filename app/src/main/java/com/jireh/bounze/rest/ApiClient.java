package com.jireh.bounze.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jireh.bounze.Util.UnsafeOkHttpClient;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Triton-PC on 02-11-2016.
 */

public class ApiClient {

    //static final String new_BASE_URL = "http://www.callistaindia.com/demo/bounze/api/";
    // static final String new_BASE_URL = "http://bounze.in/api/";
    //static final String new_BASE_URL = "https://192.168.1.111/bounze/api/";

    static final String new_BASE_URL = "http://ec2-13-126-230-103.ap-south-1.compute.amazonaws.com/api/";
    private static final Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient().newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

   /* private static OkHttpClient httpClient = new OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();*/
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(new_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));


    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }


}
