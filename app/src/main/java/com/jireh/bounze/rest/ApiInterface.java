package com.jireh.bounze.rest;

import com.jireh.bounze.data.Analytics;
import com.jireh.bounze.data.BlogPojo;
import com.jireh.bounze.data.BookingProcess;
import com.jireh.bounze.data.ChProgram;
import com.jireh.bounze.data.ChangePassword;
import com.jireh.bounze.data.CityList;
import com.jireh.bounze.data.CoachList;
import com.jireh.bounze.data.Coachcategory;
import com.jireh.bounze.data.Content;
import com.jireh.bounze.data.Events;
import com.jireh.bounze.data.FilterService;
import com.jireh.bounze.data.Forgotpwd;
import com.jireh.bounze.data.History;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.PassBookingPojo;
import com.jireh.bounze.data.PassCompleteHistoryPojo;
import com.jireh.bounze.data.PassPojo;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Reward;
import com.jireh.bounze.data.RewardDetails;
import com.jireh.bounze.data.Search;
import com.jireh.bounze.data.Shoplist;
import com.jireh.bounze.data.UserProfileService;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


/**
 * Created by Triton-PC on 02-11-2016.
 */

public interface ApiInterface {
//    http://olpa.in/url/storyc.php?cate=30
//    http://bounze.in/api/

    @GET("servicelist.php")
    Call<Content> getservicelist();

    @GET("service_sublist.php")
    Call<Content> getsubservicelist();

    @GET("service_sublist_home.php")
    Call<Content> getsubservicelistHome();

    @GET("passlist.php")
    Call<PassPojo> getPassPojo();

    @GET("shoplist.php")
    Call<Shoplist> getshoplist();

    @GET("shoprating.php")
    Call<Shoplist> getShopRatings();

    @GET("coachrating.php")
    Call<CoachList> getCoachRatings();

    @GET("homesliderlist.php")
    Call<Search> getHomeSliderList();

    @GET("coachlist.php")
    Call<CoachList> getCoachList();

    @GET("version_control.php")
    Call<Search> getCurrentVersion();

    @GET("amenitieslist.php")
    Call<Search> getAmenitiesList();


    @GET("shop_maxcost.php")
    Call<Search> getMaxcost();

    @GET("locationlist.php")
    Call<Search> getLocationsList();


    @GET("eventlist.php")
    Call<Events> getEventList();

    @GET("bloglist.php")
    Call<BlogPojo> getblogList();


    @GET("currenttime.php")
    Call<LogIn> getCurrentTime();


    @GET("citylist.php")
    Call<CityList> getCitylist();

    @GET("coach_categorylist.php")
    Call<Coachcategory> getCoachCatList();

    @GET("coach_programlist.php")
    Call<ChProgram> getCoachProList();

    @FormUrlEncoded
    @POST("analytics.php")
    Call<Analytics> analyticsdate(@Field("userid") String userid, @Field("description") String description, @Field("serviceid") String serviceid,
                                  @Field("shopid") String shopid, @Field("viewedport") String viewedport,
                                  @Field("viewtime") String viewtime, @Field("clicked") String clicked,
                                  @Field("lat") String lat, @Field("lon") String lon);

    @FormUrlEncoded
    @POST("myreviews.php")
    Call<Review> getMyReviewDetails(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("shopreviews.php")
    Call<Review> getShopReviews(@Field("shop_id") String shop_id);

    @FormUrlEncoded
    @POST("coachreviews.php")
    Call<Review> getCoachReviews(@Field("coach_id") String coach_id);

    @FormUrlEncoded
    @POST("eventreviews.php")
    Call<Review> getEventReviews(@Field("event_id") String event_id);

    @FormUrlEncoded
    @POST("passreviews.php")
    Call<Review> getPassReviews(@Field("pass_id") String pass_id);

    @FormUrlEncoded
    @POST("promocode.php")
    Call<LogIn> getPromocodePrice(@Field("user_id") String user_id, @Field("promocode") String pass_id);



    @FormUrlEncoded
    @POST("blogreviews.php")
    Call<Review> getBlogReviews(@Field("blog_id") String blog_id);

    @FormUrlEncoded
    @POST("myreviews.php")
    Call<Review> deleteMyReviews(@Field("user_id") String user_id, @Field("action") String action,
                                 @Field("review_id") String review_id);

    @FormUrlEncoded
    @POST("myreviews.php")
    Call<Review> editMyReviews(@Field("user_id") String user_id, @Field("action") String action,
                               @Field("review_id") String review_id,
                               @Field("review") String review,
                               @Field("rating") String rating);


    @FormUrlEncoded
    @POST("register.php")
    Call<LogIn> createRegistration(@Field("user_name") String userName, @Field("email_id") String mailId,
                                   @Field("password") String password, @Field("login_type") String loginType,
                                   @Field("mobile_number") String mobileNumber);

    @FormUrlEncoded
    @POST("filter.php")
    Call<FilterService> filterShop(@Field("freetrial") String freetrial, @Field("freereg") String freereg, @Field("filter_bookmark") String filter_bookmark,
                                   @Field("filter_service") String filter_service, @Field("ratings") String ratings,
                                   @Field("filter_amt_min") String filter_amt_min, @Field("filter_amt_max") String filter_amt_max, @Field("filter_amenities") String filter_amenities, @Field("filter_location") String filter_location, @Field("user_id") String user_id);



    @FormUrlEncoded
    @POST("coach_filter.php")
    Call<FilterService> filterCoach(@Field("freetrial") String freetrial,@Field("filter_bookmark") String filter_bookmark,
                                   @Field("filter_service") String filter_service, @Field("ratings") String ratings,
                                   @Field("filter_amt_min") String filter_amt_min, @Field("filter_amt_max") String filter_amt_max,  @Field("filter_location") String filter_location, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("pass_filter.php")
    Call<FilterService> filterPass(@Field("freetrial") String freetrial,@Field("filter_bookmark") String filter_bookmark,
                                    @Field("filter_service") String filter_service, @Field("ratings") String ratings,
                                    @Field("filter_amt_min") String filter_amt_min, @Field("filter_amt_max") String filter_amt_max,  @Field("filter_location") String filter_location, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("ratings.php")
    Call<ResponsePojo> createRating(@Field("userid") String user_id, @Field("pass_id") String pass_id,
                                    @Field("review") String review,  @Field("rating") String rating);

    @FormUrlEncoded
    @POST("pass_rating.php")
    Call<ResponsePojo> createBookmark(@Field("user_id") String user_id, @Field("pass_id") String pass_id,
                                      @Field("bookmark") String bookmark);

    @FormUrlEncoded
    @POST("ratings.php")
    Call<ResponsePojo> createBlogRating(@Field("userid") String user_id, @Field("blog_id") String blog_id,
                                        @Field("review") String review,  @Field("rating") String rating);

    @FormUrlEncoded
    @POST("blog_rating.php")
    Call<ResponsePojo> createBlogBookmark(@Field("user_id") String user_id, @Field("blog_id") String blog_id,
                                      @Field("bookmark") String bookmark);

    @FormUrlEncoded
    @POST("getprofile.php")
    Call<UserProfileService> getProfile(@Field("userid") String user_id);

    @FormUrlEncoded
    @POST("userreward.php")
    Call<Reward> getRewardDetails(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("add_referrence.php")
    Call<ResponsePojo> addRewardReference(@Field("userid") String user_id,@Field("referrencecode") String referrencecode);



    @FormUrlEncoded
    @POST("get_health_track.php")
    Call<UserProfileService> getHealthTrack(@Field("userid") String user_id);


    @FormUrlEncoded
    @POST("shop_bookmark.php")
    Call<LogIn> create_ShopBookmark(@Field("user_id") String user_id, @Field("shop_id") String shop_id,
                                    @Field("bookmark") int bookmark);

    @FormUrlEncoded
    @POST("coach_bookmark.php")
    Call<LogIn> create_CoachBookmark(@Field("user_id") String user_id, @Field("coach_id") String coach_id,
                                     @Field("bookmark") int bookmark);

    @FormUrlEncoded
    @POST("event_bookmark.php")
    Call<LogIn> create_EventBookmark(@Field("user_id") String user_id, @Field("event_id") String event_id,
                                     @Field("bookmark") String bookmark);

    @FormUrlEncoded
    @POST("freetrial_check_exist.php")
    Call<LogIn> getFreeTrialStatus(@Field("user_id") String user_id, @Field("shopid") String shopid, @Field("booking_type") String booking_type);

    @Multipart
    @POST("updateprofile.php")
    Call<UserProfileService> updateProfile(@Part("userid") RequestBody userid,
                                           @Part("first_name") RequestBody first_name,
                                           @Part("picture\"; filename=\"mypic.jpg\" ") RequestBody file,
                                           @Part("email_id") RequestBody email_id,
                                           @Part("mobile_number") RequestBody mobile_number,
                                           @Part("gender") RequestBody gender,
                                           @Part("blood_group") RequestBody blood_group,
                                           @Part("date_of_birth") RequestBody date_of_birth,
                                           @Part("height") RequestBody height,
                                           @Part("weight") RequestBody weight);
    @Multipart
    @POST("updateprofile.php")
    Call<UserProfileService> updateWallet(@Part("userid") RequestBody userid,
                                           @Part("wallet") RequestBody wallet);

    @Multipart
    @POST("updateprofile.php")
    Call<UserProfileService> updateProfileWithTokenId(@Part("userid") RequestBody userid,
                                                      @Part("tokenid") RequestBody tokenid
    );


    @Multipart
    @POST("updateprofile.php")
    Call<UserProfileService> updateProfilePicture(@Part("userid") RequestBody userid,

                                                  @Part("picture\"; filename=\"mypic.jpg\" ") RequestBody file
    );

    @Multipart
    @POST("add_health_track.php")
    Call<UserProfileService> addHealthTrack(@Part("userid") RequestBody userid,
                                            @Part("height") RequestBody height,
                                            @Part("weight") RequestBody weight,
                                            @Part("picture\"; filename=\"health.jpg\" ") RequestBody file);

    @FormUrlEncoded
    @POST("pass_booking.php")
    Call<PassBookingPojo> bookPass(@Field("user_id") String user_id, @Field("pass_id") String pass_id,
                                   @Field("booking_status") String booking_status, @Field("cost") String cost,
                                   @Field("payment_mode") String payment_mode, @Field("payment_status") String payment_status);

    @FormUrlEncoded
    @POST("pass_booking_history.php")
    Call<Coachcategory> getSinglePassHistory(@Field("booking_id") String booking_id, @Field("shop_id") String shop_id,
                                             @Field("service_id") String service_id);

    /**
     * This method is used for getting all the pass booking data included Current and Past both
     * and getting called from My Profile screen -> Pass option.
     *
     * @param user_id
     * @return
     */
    @GET("pass_user_history.php")
    Call<PassCompleteHistoryPojo> getAllPassHistory(@Query("user_id") String user_id);

    @FormUrlEncoded
    @POST("booking_history.php")
    Call<History> historyget(@Field("userid") String userid);


    @FormUrlEncoded
    @POST("ratings.php")
    Call<Review> ratings(@Field("userid") String userid, @Field("shopid") String shopid,
                         @Field("event_id") String event_id,
                         @Field("coach_id") String coach_id,
                         @Field("bookingid") String bookingid, @Field("review") String review, @Field("rating") String rating);


    @FormUrlEncoded
    @POST("booking_insert.php")
    Call<BookingProcess> booking(@Field("program_id") String program_id, @Field("shopid") String shopid,
                                 @Field("serviceid") String serviceid, @Field("booking_type") String booking_type,
                                 @Field("no_of_days") String no_of_days, @Field("no_of_sessions") String no_of_sessions,
                                 @Field("start_date") String start_date, @Field("end_date") String end_date,
                                 @Field("session_s_time") String session_s_time, @Field("session_hours") String session_hours,
                                 @Field("is_batch") String is_batch,
                                 @Field("list_of_days") String list_of_days, @Field("approval_status") String approval_status,
                                 @Field("cost") String cost, @Field("Paymentmode") String Paymentmode,
                                 @Field("payment_status") String payment_status,
                                 @Field("user_id") String user_id,
                                 @Field("rewardstatus") String rewardstatus,
                                 @Field("promocode") String promocode,
                                 @Field("promocodeval") String promocodeval,
                                 @Field("tax") String tax);

    @FormUrlEncoded
    @POST("booking_payment_update.php")
    Call<BookingProcess> updatepayment(@Field("uniqueid") String uniqueid);

    @FormUrlEncoded
    @POST("login.php")
    Call<LogIn> createLogIn(@Field("email_id") String email, @Field("password") String password, @Field("type") String type, @Field("fbid") String fid);

    @FormUrlEncoded
    @POST("otp_login.php")
    Call<LogIn> createOtpLogin(@Field("userid") String userid, @Field("otp") String otp);



    @FormUrlEncoded
    @POST("login.php")
    Call<LogIn> createfbLogIn(@Field("type") String type, @Field("fbid") String fbid, @Field("name") String name, @Field("email_id") String email_id,
                              @Field("picture") String picture, @Field("gender") String gender, @Field("birthday") String birthday);

    @FormUrlEncoded
    @POST("forgotpwd.php")
    Call<Forgotpwd> forgotpassword(@Field("fuser") String email);


    @FormUrlEncoded
    @POST("resetpassword.php")
    Call<ChangePassword> changepassword(@Field("user_id") int user_id, @Field("current_password") String current_password, @Field("new_password") String new_password);

}
