package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.pixplicity.easyprefs.library.Prefs;
import com.jireh.bounze.Adapter.FilterAdapter;
import com.jireh.bounze.Adapter.FilterByShopAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.FilterModel;
import com.jireh.bounze.data.FilterService;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 06-10-2017.
 */

public class ShopFilter extends Activity {
    private static final String TAG = "ShopFilter";
    RecyclerView filter_recyclerview, filter_details_recycler;
    Toolbar toolbar;
    ImageView back_img;
    TextView title;
    DBFunctions dbFunctions;
    Button cancel_btn, apply_btn;
    ImageView reset_img;
    ArrayList<FilterModel> shopFree = new ArrayList<FilterModel>();
    ArrayList<FilterModel> shopSubService = new ArrayList<FilterModel>();
    ArrayList<FilterModel> shopAmenities = new ArrayList<FilterModel>();
    ArrayList<FilterModel> shopLocations = new ArrayList<FilterModel>();
    ArrayList<FilterModel> shopRatings = new ArrayList<FilterModel>();
    EditText subservie_edit_text;
    CrystalRangeSeekbar rangeSeekbar;
    FilterByShopAdapter filterByAdapter;
    RelativeLayout seekbar_layout;
    String minPrice = "";
    boolean reset_price, reset_pressed;
    String groupPosition = "";
    int position = 0;
    ArrayList<String> filtertitle = new ArrayList<>();
    Integer maxPrice,tempMaxPrice;
    ArrayList<Integer> maxprice = new ArrayList<>();
    DBFunctions dbobject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_layout);
        initVariable();
        //get the max price of the shop
        dbobject = new DBFunctions(getApplicationContext());
        List<Shop> shopDetails = dbobject.getShopListDB();
        for (int idx = 0; idx < shopDetails.size(); idx++) {
            maxprice.add(Integer.parseInt(shopDetails.get(idx).getCost()));
        }
        maxPrice =  Collections.max(maxprice);
        //store the max price is temp to reset the price
        tempMaxPrice = maxPrice;
        setOnClickListener();
        //set the title
        title.setText("ShopFilter");
        //set the adapter for headings

        filtertitle.add("Shop");
        filtertitle.add("Sub Services");
        filtertitle.add("Ratings");
        filtertitle.add("Price");
        filtertitle.add("Amenities");
        filtertitle.add("Areas");
        //set the filter details layout and onclick the heading send position
        FilterAdapter adapter = new FilterAdapter(ShopFilter.this, filtertitle, new FilterAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                //call the method to get the filter details based on position
                setFilterDetails(position);


            }
        });
        filter_recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //initially set the group position to 0 and call the filterby  adapter
        setFilterDetails(position);


    }


    private void setFilterDetails(int pos) {

        //set visibility for the reset_btn
        reset_img.setVisibility(View.VISIBLE);

        //if the reset button is pressed then get the group position and reset the page else get the normal positon
        //and the call adapter accordingly
        if (reset_pressed == true) {
            position = pos;
            reset_pressed = false;
        } else {
            position = pos;
        }

        switch (position) {
            //case 0 for shop free
            case 0:
                //invisible rangeseek bar,actv, visible rv
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.GONE);


                //if user selected free trial then add two values in filter model and send to adapter to display
                ArrayList<String> allFree = new ArrayList<>();
                if (shopFree.size() > 0) {
                    shopFree.clear();

                }
                allFree.add("Free Trial");
                allFree.add("Free Registration");
                allFree.add("Bookmark");
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedItems = dbFunctions.getFilterShopDetails("0");

                for (int idx = 0; idx < allFree.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(allFree.get(idx));
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedItems != null) {
                        for (int check = 0; check < checkedItems.size(); check++) {
                            if (idx == checkedItems.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    }
                    shopFree.add(model);
                }

                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "0";
                filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopFree, new FilterByShopAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {


                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);

                break;

            case 1:
                //case 1 for shop sub services
                //visible autocomplete text view,rv gone for  rangebar
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.VISIBLE);
                subservie_edit_text.setHint(getResources().getString(R.string.sservice_hint));

                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedServiceItems = dbFunctions.getFilterShopDetails("1");

                //get all sub service list name and add in  filter model and send to adapter to display
                List<Service> shopServiceList = dbFunctions.getServiceSubListDB();


                if (shopSubService.size() > 0) {
                    shopSubService.clear();

                }

                for (int idx = 0; idx < shopServiceList.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(shopServiceList.get(idx).getTitle());
                    model.setSubServiceId(shopServiceList.get(idx).getIconid());
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedServiceItems != null) {
                        for (int check = 0; check < checkedServiceItems.size(); check++) {
                            if (idx == checkedServiceItems.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    } else {
                        model.setCheckIt(false);
                    }
                    shopSubService.add(model);

                }


                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "1";
                filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopSubService, new FilterByShopAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {
                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                //search based on the sub services and set the adatper
                addTextListener();
                break;
            case 2:
                //case 2 for ratings filter
                //invisible rangeseek bar,actv, visible rv
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.GONE);
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedRatings = dbFunctions.getFilterShopDetails("2");

                //if user selected ratings then add two values  in filter model and send to adapter to display
                ArrayList<String> ratings = new ArrayList<>();
                if (shopRatings.size() > 0) {
                    shopRatings.clear();

                }
                ratings.add("Low - High");
                ratings.add("High - Low");
                for (int idx = 0; idx < ratings.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(ratings.get(idx));
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedRatings != null) {
                        for (int check = 0; check < checkedRatings.size(); check++) {
                            if (idx == checkedRatings.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    }
                    shopRatings.add(model);
                }
                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "2";
                filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopRatings, new FilterByShopAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {
                        filterByAdapter.notifyDataSetChanged();
                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                break;
            case 3:
                //case 3 for price
                //visible rangeseek bar and gone for rv and actv
                seekbar_layout.setVisibility(View.VISIBLE);
                filter_details_recycler.setVisibility(View.GONE);
                subservie_edit_text.setVisibility(View.GONE);
                rangeSeekbar.setMinValue(0.0f);
                rangeSeekbar.setMaxValue(Float.valueOf(maxPrice));
                getRangeSeekBar();
                //if reset pressed then reset the range seekbar set min and max value to default
                if (reset_price) {
                    rangeSeekbar.setMinStartValue(0.0f).setMaxStartValue(Float.valueOf(maxPrice)).apply();
                    reset_price = false;
                }
                break;

            case 4:

                //visible autocomplete text view,rv gone for  rangebar
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.VISIBLE);
                subservie_edit_text.setHint(getResources().getString(R.string.amenities));
                setAmenitiesAdapter();
                addAmenitiesTextListener();


                break;
            case 5:

                //visible autocomplete text view,rv gone for  rangebar
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.VISIBLE);
                subservie_edit_text.setHint(getResources().getString(R.string.locations));
                setLocationsAdapter();
                //set text change listener for the location
                addLocationsTextListener();
                break;

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        apply_btn.setEnabled(true);
    }

    private void getRangeSeekBar() {
        // get min and max text view
        final TextView tvMin = (TextView) findViewById(R.id.min_value_txt);
        final TextView tvMax = (TextView) findViewById(R.id.max_value_txt);

// set listener
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(getResources().getString(R.string.Rs) + minValue));
                tvMax.setText(String.valueOf(getResources().getString(R.string.Rs) + maxValue));
            }
        });

// set final value listener
        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
                minPrice = String.valueOf(minValue);
                tempMaxPrice = Integer.valueOf(String.valueOf(maxValue));
            }
        });
    }

    private void setAmenitiesAdapter() {

        //clear if the shop amenities size is greater than 0

        if (shopAmenities.size() > 0) {
            shopAmenities.clear();
        }
        //get the selected values by the position from filter table
        ArrayList<FilterModel> checkedAmenitiesItems = dbFunctions.getFilterShopDetails("4");

        //get all amenities and add in  filter model and send to adapter to display
        List<ResultModel> amenities_list = dbFunctions.getAmenitiesListDB();
        for (int idx = 0; idx < amenities_list.size(); idx++) {
            FilterModel model = new FilterModel();
            model.setAmenities(amenities_list.get(idx).getAmenities());
            model.setId(amenities_list.get(idx).getId());
            model.setPosition(idx);
            //based on the checked position set true else make it false
            //if the checkItems have values then make the loop
            if (checkedAmenitiesItems != null) {
                for (int check = 0; check < checkedAmenitiesItems.size(); check++) {
                    if (idx == checkedAmenitiesItems.get(check).getCheckedPosition()) {
                        model.setCheckIt(true);
                    }
                }
            }
            shopAmenities.add(model);

        }


        //Send the flag to adapter to find whare it is called
        //set the filter by details
        groupPosition = "4";
        filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopAmenities, new FilterByShopAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, String groupPositon, int position) {


            }
        });
        filter_details_recycler.setAdapter(filterByAdapter);
        //search based on the amenities and set the adatper

    }


    private void addTextListener() {


        subservie_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                //clear before adding the filter modellist
                if (shopSubService.size() > 0) {
                    shopSubService.clear();

                }
                query = query.toString().toLowerCase();
                List<Service> shopServiceList = dbFunctions.getServiceSubListDB();
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedServiceItems = dbFunctions.getFilterShopDetails("1");

                for (int idx = 0; idx < shopServiceList.size(); idx++) {
                    final String text = shopServiceList.get(idx).getTitle().toLowerCase();
                    if (text.contains(query)) {
                        FilterModel model = new FilterModel();
                        model.setName(shopServiceList.get(idx).getTitle());
                        model.setSubServiceId(shopServiceList.get(idx).getIconid());
                        model.setPosition(idx);
                        //based on the checked position set true else make it false
                        //if the checkItems have values then make the loop
                        if (checkedServiceItems != null) {
                            for (int check = 0; check < checkedServiceItems.size(); check++) {
                                if (idx == checkedServiceItems.get(check).getCheckedPosition()) {
                                    model.setCheckIt(true);
                                }
                            }
                        }
                        shopSubService.add(model);

                    }
                }


                //set the filter by details
                filterByAdapter = new FilterByShopAdapter(ShopFilter.this, "1", shopSubService, new FilterByShopAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {

                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                filterByAdapter.notifyDataSetChanged();  // data set changed
            }
        });

    }

    private void addAmenitiesTextListener() {


        subservie_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                //if user enter text then filter it else show all the values
                if (query.length() > 0) {

                    if (shopAmenities.size() > 0) {
                        shopAmenities.clear();
                    }
                    //get all amenities and add in  filter model and send to adapter to display
                    List<ResultModel> amenities_list = dbFunctions.getAmenitiesListDB();
                    //get the selected values by the position from filter table
                    ArrayList<FilterModel> checkedAmenitiesItems = dbFunctions.getFilterShopDetails("4");
                    for (int idx = 0; idx < amenities_list.size(); idx++) {

                        final String text = amenities_list.get(idx).getAmenities().toLowerCase();


                        if (text.contains(query)) {

                            FilterModel model = new FilterModel();
                            model.setAmenities(amenities_list.get(idx).getAmenities());
                            model.setId(amenities_list.get(idx).getId());
                            model.setPosition(idx);
                            //based on the checked position set true else make it false
                            //if the checkItems have values then make the loop
                            if (checkedAmenitiesItems != null) {
                                for (int check = 0; check < checkedAmenitiesItems.size(); check++) {
                                    if (idx == checkedAmenitiesItems.get(check).getCheckedPosition()) {
                                        model.setCheckIt(true);
                                    }
                                }
                            }
                            shopAmenities.add(model);
                        }
                    }
                    //set the filter by details
                    groupPosition = "4";
                    filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopAmenities, new FilterByShopAdapter.onItemClickListener() {
                        @Override
                        public void onItemClick(int view, String groupPositon, int position) {

                        }
                    });
                    filter_details_recycler.setAdapter(filterByAdapter);
                    filterByAdapter.notifyDataSetChanged();  // data set changed
                } else {

                    setAmenitiesAdapter();


                }


            }
        });

    }

    private void addLocationsTextListener() {


        subservie_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                if (query.length() > 0) {


                    if (shopLocations.size() > 0) {
                        shopLocations.clear();
                    }
                    //get all amenities and add in  filter model and send to adapter to display
                    List<ResultModel> locationsList = dbFunctions.getLocationsListDB();
                    //get the selected values by the position from filter table
                    ArrayList<FilterModel> checkedLocationsItems = dbFunctions.getFilterShopDetails("5");

                    for (int idx = 0; idx < locationsList.size(); idx++) {

                        final String text = locationsList.get(idx).getLocation().toLowerCase();


                        if (text.contains(query)) {

                            FilterModel model = new FilterModel();
                            model.setLocation(locationsList.get(idx).getLocation());
                            model.setId(locationsList.get(idx).getId());
                            model.setPosition(idx);
                            //based on the checked position set true else make it false
                            //if the checkItems have values then make the loop
                            if (checkedLocationsItems != null) {
                                for (int check = 0; check < checkedLocationsItems.size(); check++) {
                                    if (idx == checkedLocationsItems.get(check).getCheckedPosition()) {
                                        model.setCheckIt(true);
                                    }
                                }
                            }
                            shopLocations.add(model);
                        }
                    }
                    //set the filter by details
                    groupPosition = "5";
                    filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopLocations, new FilterByShopAdapter.onItemClickListener() {
                        @Override
                        public void onItemClick(int view, String groupPositon, int position) {

                        }
                    });
                    filter_details_recycler.setAdapter(filterByAdapter);
                    filterByAdapter.notifyDataSetChanged();  // data set changed

                } else {
                    setLocationsAdapter();
                }


            }
        });

    }

    private void setLocationsAdapter() {
        //clear if the shop subservice size is greater than 0
        if (shopLocations.size() > 0) {
            shopLocations.clear();

        }
        //get the selected values by the position from filter table
        ArrayList<FilterModel> checkedLocationsItems = dbFunctions.getFilterShopDetails("5");

        //get all amenities and add in  filter model and send to adapter to display
        List<ResultModel> locationsList = dbFunctions.getLocationsListDB();
        for (int idx = 0; idx < locationsList.size(); idx++) {
            FilterModel model = new FilterModel();

            model.setId(locationsList.get(idx).getId());
            model.setLocation(locationsList.get(idx).getLocation());
            model.setPosition(idx);
            //based on the checked position set true else make it false
            //if the checkItems have values then make the loop
            if (checkedLocationsItems != null) {
                for (int check = 0; check < checkedLocationsItems.size(); check++) {
                    if (idx == checkedLocationsItems.get(check).getCheckedPosition()) {
                        model.setCheckIt(true);
                    }
                }
            }
            shopLocations.add(model);

        }
        //set the filter by details
        groupPosition = "5";
        filterByAdapter = new FilterByShopAdapter(ShopFilter.this, groupPosition, shopLocations, new FilterByShopAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, String groupPositon, int position) {

            }
        });
        filter_details_recycler.setAdapter(filterByAdapter);
        filterByAdapter.notifyDataSetChanged();  // data set changed
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        reset_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete all the details in the filter table
                //set the range seekbar to min and max values
                reset_price = true;
                reset_pressed = true;
                dbFunctions.deleteFilterSDBAllData();

                //set the filter details layout
                FilterAdapter adapter = new FilterAdapter(ShopFilter.this, filtertitle, new FilterAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, int position) {
                        setFilterDetails(position);


                    }
                });
                filter_recyclerview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                //reset the page which is visible to user.
                setFilterDetails(0);
                //show the reset toast
                Toast.makeText(ShopFilter.this,"Reset Successfully",Toast.LENGTH_SHORT).show();
            }
        });
        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set anable false to apply btn
                apply_btn.setEnabled(false);

                //check the internet connection if it is available process else show toast
                if (isInternetOn()) {

                    //get the values from the db to get filter based on shop
                    String freeTrial = "", freeReg = "", bookmark = "";
                    List<FilterModel> mlShop = dbFunctions.getFilterShopDetails("0");
                    if (mlShop != null) {
                        for (int idx = 0; idx < mlShop.size(); idx++) {
                            if (mlShop.get(idx).getCheckedPosition() == 0) {
                                freeTrial = mlShop.get(idx).getValue();
                            } else if (mlShop.get(idx).getCheckedPosition() == 1) {
                                freeReg = mlShop.get(idx).getValue();
                            } else if (mlShop.get(idx).getCheckedPosition() == 2) {
                                bookmark = mlShop.get(idx).getValue();
                            }

                        }
                    }
                    //get the values from the db to get filter based on serviceid's
                    String subServiceIds = "";
                    List<FilterModel> mlSubService = dbFunctions.getFilterShopDetails("1");
                    if (mlSubService != null) {
                        for (int idx = 0; idx < mlSubService.size(); idx++) {
                            subServiceIds = subServiceIds + mlSubService.get(idx).getValue() + ",";

                        }
                        subServiceIds = subServiceIds.substring(0, subServiceIds.length() - 1);
                    }

                    //get the values from the db to get filter based on ratings
                    String ratings = "";
                    List<FilterModel> mlRatings = dbFunctions.getFilterShopDetails("2");
                    if (mlRatings != null) {
                        for (int idx = 0; idx < mlRatings.size(); idx++) {
                            ratings = mlRatings.get(idx).getValue();

                        }
                    }
                    //get the values from the db to get filter based on amenities
                    String amenities = "";
                    List<FilterModel> mlAmenities = dbFunctions.getFilterShopDetails("4");
                    if (mlAmenities != null) {
                        for (int idx = 0; idx < mlAmenities.size(); idx++) {
                            amenities = amenities + mlAmenities.get(idx).getValue() + ",";

                        }
                        amenities = amenities.substring(0, amenities.length() - 1);
                    }
                    //get the values from the db to get filter based on locations
                    String locations = "";
                    List<FilterModel> mlLocations = dbFunctions.getFilterShopDetails("5");
                    if (mlLocations != null) {
                        for (int idx = 0; idx < mlLocations.size(); idx++) {
                            locations = locations + mlLocations.get(idx).getValue() + ",";

                        }
                        locations = locations.substring(0, locations.length() - 1);
                    }

                    //call the service to get the filtered shops based on the selection
                    serviceCall(freeTrial, freeReg, bookmark, subServiceIds, ratings, minPrice, String.valueOf(tempMaxPrice), amenities, locations);

                } else {
                    Toast.makeText(ShopFilter.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void serviceCall(String freeTrial, String freeReg, String bookmark,
                             String subServiceIds, final String ratings, String price_min, String price_max, String amenities, String locations) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<FilterService> call = stationClient.filterShop(freeTrial, freeReg, bookmark, subServiceIds, ratings, price_min, price_max, amenities, locations, Prefs.getString(AppConstants.UserId, ""));
        System.out.println(" filter result::ft::" + freeTrial + "fr::" + freeReg + "bm::" + bookmark
                + "ssv::" + subServiceIds + "rat::" + ratings + "prmax::" + price_min + "prmin::" + price_max + "am::" + amenities + "loc::" + locations);
        call.enqueue(new Callback<FilterService>() {
            @Override
            public void onResponse(Call<FilterService> call, Response<FilterService> response) {

                FilterService result = response.body();
                if (result.getResult().get(0).getStatus() == 0) {
                    Toast.makeText(ShopFilter.this, "No shop found", Toast.LENGTH_SHORT).show();
                    //set anable true to apply btn
                    apply_btn.setEnabled(true);
                } else {

                    //get the shop details based on the result and send to shopfilterlist class to display it
                    List<Shop> shopList = dbFunctions.getShopListDBByListId(result.getResult().get(0).getShopId(), ratings);

                    Intent in = new Intent(ShopFilter.this, AllFilterResult.class);
                    in.putExtra("FilterFlag","shop");
                    in.putExtra("ShopList", (Serializable) shopList);
                    startActivity(in);
                }

            }

            @Override
            public void onFailure(Call<FilterService> call, Throwable t) {
                Toast.makeText(ShopFilter.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }


    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        reset_img = (ImageView) toolbar.findViewById(R.id.reset_img);
        seekbar_layout = (RelativeLayout) findViewById(R.id.seekbar_layout);
        dbFunctions = new DBFunctions(ShopFilter.this);

        filter_recyclerview = (RecyclerView) findViewById(R.id.filter_recyclerview);
        filter_details_recycler = (RecyclerView) findViewById(R.id.filter_details_recycler);

        LinearLayoutManager manager = new LinearLayoutManager(ShopFilter.this);
        filter_recyclerview.setLayoutManager(manager);

        LinearLayoutManager filter_bymanager = new LinearLayoutManager(ShopFilter.this);
        filter_details_recycler.setLayoutManager(filter_bymanager);

        cancel_btn = (Button) findViewById(R.id.cancel_btn);
        apply_btn = (Button) findViewById(R.id.apply_btn);

        subservie_edit_text = (EditText) findViewById(R.id.subservie_edit_text);
        rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.range_seek_bar);
    }
}
