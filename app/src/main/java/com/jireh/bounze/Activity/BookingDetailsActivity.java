package com.jireh.bounze.Activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.jireh.bounze.Fragment.BookingHistory;
import com.jireh.bounze.Fragment.UpcomingEvents;
import com.jireh.bounze.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 12-09-2017.
 */

public class BookingDetailsActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView title_txt;
    ImageView back_img;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_layout);
        initVariable();
        setOnClickListener();

        //if booking newly then move to upcoming tab automatically
        try {
            String bookFlag = getIntent().getStringExtra("BookFlag");
            if (bookFlag.equals("BookAShop")) {
                pager.setCurrentItem(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if intent in from booka shop the back button is pressed clear all the back stack and then open the main page
                //else finish the page
                try {
                    String bookFlag = getIntent().getStringExtra("BookFlag");
                    if (bookFlag.equals("BookAShop")) {
                        Intent in = new Intent(BookingDetailsActivity.this, PagerLayout.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(in);

                    } else {
                        finish();
                    }

                } catch (Exception e) {
                    finish();
                }

            }
        });
    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        title_txt.setText("Bookings");
        // Initialize the ViewPager and set an adapter
        pager = (ViewPager) findViewById(R.id.pager);
        ProfilePagerAdapter adapter = new ProfilePagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new BookingHistory(), "History");
        adapter.addFrag(new UpcomingEvents(), "Upcoming");

        pager.setAdapter(adapter);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tab_layout);

        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabs.setTypeface(typeFace , 0);
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private class ProfilePagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.CustomTabProvider {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int icons[] = {R.drawable.history, R.drawable.upcoming_event};

        public ProfilePagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
/*

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
*/


        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            LinearLayout materialRippleLayout = (LinearLayout) LayoutInflater.from(BookingDetailsActivity.this).inflate(R.layout.pager_sliding_tab_custom_tab, parent, false);
            ((ImageView) materialRippleLayout.findViewById(R.id.image)).setImageResource(icons[position]);
            TextView title_txt = (TextView) materialRippleLayout.findViewById(R.id.title_txt);
            title_txt.setText(mFragmentTitleList.get(position));
            return materialRippleLayout;
        }

        @Override
        public void tabSelected(View tab) {
            LinearLayout layout = (LinearLayout) tab;
            TextView title = (TextView) layout.findViewById(R.id.title_txt);
            title.setTextColor(getResources().getColor(R.color.black));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.black),
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);

        }

        @Override
        public void tabUnselected(View tab) {
            LinearLayout layout = (LinearLayout) tab;
            TextView title = (TextView) layout.findViewById(R.id.title_txt);
            title.setTextColor(getResources().getColor(R.color.white));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);

        }


    }
}



