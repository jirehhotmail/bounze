package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 29-07-2017.
 */

public class TermsAndConditions extends Activity {
    TextView terms_and_conditions, title_txt,heading1;
    ImageView back_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditons);
        initVariable();


        title_txt.setText("Terms and Conditions");
        heading1.setText(Html.fromHtml(getResources().getString(R.string.terms_conditions)));
        setOnClickListener();

    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //send email on click the email id
        SpannableString ss = new SpannableString(Html.fromHtml(getResources().getString(R.string.para_1)));
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("message/rfc822");
                in.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@bounze.in"});

                startActivity(Intent.createChooser(in, "Send Email"));
            }
        };


        ss.setSpan(span1, 128, 143, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        terms_and_conditions.setText(ss);
        terms_and_conditions.setMovementMethod(LinkMovementMethod.getInstance());


    }

    private void initVariable() {
        terms_and_conditions = (TextView) findViewById(R.id.para_email);
        title_txt = (TextView) findViewById(R.id.title_txt);
        back_img = (ImageView) findViewById(R.id.back_img);
        heading1= (TextView) findViewById(R.id.heading1);
    }
}
