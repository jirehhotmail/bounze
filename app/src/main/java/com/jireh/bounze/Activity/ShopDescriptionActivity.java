package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Adapter.AllreviewsAdapter;
import com.jireh.bounze.Adapter.ShopAmenitiesAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.Fragment.ShopServiceDescription;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.Amenities;
import com.jireh.bounze.data.BatchTime;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.data.ServiceOnAShop;
import com.jireh.bounze.data.Sessions;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.ShopSerializable;
import com.jireh.bounze.data.ShopServiceDetails;
import com.jireh.bounze.data.Shoplist;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 18-09-2017.
 */

public class ShopDescriptionActivity extends AppCompatActivity {
    SliderLayout shop_image_layout;
    Shop shop;
    RecyclerView amenities_recycler;
    LinearLayout amenities_layout;
    CollapsingToolbarLayout toolbar;
    TextView title_txt;
    DBFunctions dbobject;
    ImageView description_img, direction_img, bookmark_img, share_img, rating_img, call_img;
    List<ShopSerializable> shopDetails = new ArrayList<>();
    LinearLayout mTabsLinearLayout;
    int currentPosition = 0;
    PagerSlidingTabStrip tabs;
    ImageView back_img;
    LinearLayout ll_bookmark;
    ProgressBar progress_bar;
    List<Review_> myReview;
    RecyclerView recycler_view_review;
    int clickcount = 0;
    String subseriveId, serviceId;
    private ArrayList<Shop> bookmarkList;
    private SessionManager sessionManager;
    private boolean bookmarkFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_description);
        dbobject = new DBFunctions(getBaseContext());

        initVariable();

        //get data from the url else load from the database
        /*Intent appLinkIntent = getIntent();

        Uri appLinkData = appLinkIntent.getData();

        if (appLinkData != null) {
            System.out.println("Shop id" + appLinkData.getPath());
            String shop_Id = appLinkData.getQueryParameter("shopid");
            System.out.println("Shop id" + shop_Id);
            String shopId = appLinkData.getLastPathSegment();
            System.out.println("Shop id" + shopId);
            shop = dbobject.getShopListDetailbyId(shopId);
        } else {

        }*/
        //get Shop Details
        shop = (Shop) getIntent().getSerializableExtra("ShopDetails");

        //create object for session manager class

        //set the title of the app
        title_txt.setText(shop.getTitle() + " - " + shop.getSmallAddress());
        //get the user reviews for the shop
        setReviews();
        setAmenities();
        setShopSlider();
        setViewPager();
        setOnClickListener();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            startActivity(new Intent(ShopDescriptionActivity.this, PagerLayout.class));
        } else {
            finish();
        }
    }

    private void setReviews() {

        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getShopReviews(shop.getSid());
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    myReview = result.getReviews();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });


    }


    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //by default use the value as one get the service id and subservice id to use in the share link
        try {
            subseriveId = shopDetails.get(0).getServiceDetails().get(0).getSub_service_id();
            serviceId = dbobject.getServiceIdBySubListId(subseriveId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //set the tab listener to change the tab text color
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //get the service id and subservice id to use in the share link
                subseriveId = shopDetails.get(position).getServiceDetails().get(0).getSub_service_id();
                serviceId = dbobject.getServiceIdBySubListId(subseriveId);
                currentPosition = position;
                for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    if (i == position) {
                        tv.setTextColor(getResources().getColor(R.color.black));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        description_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the description dialog
                AlertDialog.Builder mBuild = new AlertDialog.Builder(ShopDescriptionActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.description_dialog, null);
                final TextView desc_txt = (TextView) mView.findViewById(R.id.description_txt);
                TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
                Button close = (Button) mView.findViewById(R.id.close_btn);
                desc_txt.setText(Html.fromHtml(shop.getDescription()));
                tv_title.setText("Description");
                mBuild.setView(mView);
                final AlertDialog dialog = mBuild.create();
                dialog.setCancelable(true);
                dialog.show();
                //close the dialog on click ok btn
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            }
        });
        direction_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the mapview between your current location and the shop location
                GPSTracker gpsTracker = new GPSTracker(ShopDescriptionActivity.this);
                if (!gpsTracker.canGetLocation())
                    gpsTracker.showSettingsAlert();
                else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + gpsTracker.getLatitude() + "," + gpsTracker.getLongitude() + "&daddr=" + shop.getLatitude() + "," + shop.getLongitude()));
                    startActivity(intent);
                }
            }
        });
        //send the bookmark details to server
        bookmark_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetOn()) {
                    //check if already it is bookmarked on click remove else bookmark

                    bookmarkList = sessionManager.getBookmarkShop();
                    bookmarkFlag = true;
                    for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                        if (bookmarkList.get(tempPos).getSid().equals(shop.getSid())) {
                            bookmarkFlag = false;
                            break;
                        }
                    }
                    if (bookmarkFlag) {
                        Shop pojo = new Shop();
                        pojo.setSid(shop.getSid());
                        pojo.setTitle(shop.getTitle());
                        bookmarkList.add(bookmarkList.size(), pojo);
                        sessionManager.setBookmarkShop(bookmarkList);
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));

                        showProgressDialog();
                        //bookmark the page
                        shopBookmark(1);

                    } else {
                        showRemoveBookmarkAlert();
                    }


                } else {
                    Toast.makeText(ShopDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        //show the sharing page
        share_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the shop description if length >40 show it else directly show it
                int desc_length = shop.getDescription().length();
                String desc = shop.getDescription();
                if (desc_length > 40) {
                    desc = shop.getDescription().substring(0, 40);
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shop.getTitle() + "\n" + shop.getSmallAddress().trim() + "\n" + shop.getSubservices().trim() + "\n" + desc + "\n\n" + "https://www.bounze.in/programs.php?sid=" + serviceId + "&shopid=" + shop.getSid() + "&suid=" + subseriveId
                        + "\n\n" + "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n" + "https://play.google.com/store/apps/details?id=com.triton.bounze");
                //shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/shopdescription/" + shop.getSid());
                //  shareIntent.putExtra(Intent.EXTRA_TEXT,"https://www.bounze.in/programs.php?sid=4&shopid="+ shop.getSid()+"&suid=23");
                startActivity(Intent.createChooser(shareIntent, "Share Using"));
            }
        });

        //post the ratings for the shop

        rating_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                clickcount = clickcount + 1;
                if (clickcount % 2 == 1) {
                    //SET visibility visible for the recycler review
                    recycler_view_review.setVisibility(View.VISIBLE);

                    //check for my reviews if available show else show no reviews ound

                    if (myReview != null) {
                        //on single click show the all review and on second click show the rating bar alert
                        AllreviewsAdapter adapter = new AllreviewsAdapter(ShopDescriptionActivity.this, myReview);
                        recycler_view_review.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "No reviews found", Toast.LENGTH_LONG).show();
                    }
                    //first time clicked to do this
                    Toast.makeText(getApplicationContext(), "Click again to write a review", Toast.LENGTH_LONG).show();
                } else {
                    //show the rating alert in android  and close the recyclerview in android
                    recycler_view_review.setVisibility(View.GONE);
                    showRatingAlert();
                    //check how many times clicked and so on
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();
                }


            }
        });
        //call the branch phone no
        call_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //show the alert dialog  for the confirmation
                showConfirmationDialog();
                //get the phone call permission based on the versions of android


            }
        });
    }

    private void showRatingAlert() {

        AlertDialog.Builder mBuild = new AlertDialog.Builder(ShopDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.rate_bar_dailog, null);
        final RatingBar ratebar = (RatingBar) mView.findViewById(R.id.ratingBar);
        final EditText review = (EditText) mView.findViewById(R.id.comments_edt);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        tv_title.setText("Tell us what you think?");
        mBuild.setView(mView);
        ratebar.setRating(Float.valueOf(shop.getAvgratings()));
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                doRating("" + ratebar.getRating(), review);

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void doRating(String rating, EditText review) {
        if (isInternetOn()) {
            showProgressDialog();
            shop_ratings(rating, review);
        } else {
            Toast.makeText(ShopDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void shop_ratings(String rating, EditText review) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), shop.getSid(), "", "", "", review.getText().toString(), rating);
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();

                if (response_code == 200) {
                    //if submitted once show the reiew submitted toast else show the message from server
                    if (result.getStatus().get(0).getStatus().equals("1")) {
                        hideProgressDialog();
                        Toast.makeText(ShopDescriptionActivity.this, "Review submitted successfully", Toast.LENGTH_SHORT).show();
                        //udate ratings from server
                        getShopRatings();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(ShopDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if (response_code == 400) {
                    Toast.makeText(ShopDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(ShopDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "500 error");
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Toast.makeText(ShopDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });

    }

    private void getShopRatings() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Shoplist> call = stationClient.getShopRatings();
        call.enqueue(new Callback<Shoplist>() {
            @Override
            public void onResponse(Call<Shoplist> call, Response<Shoplist> response) {
                int response_code = response.code();
                Shoplist result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateShopListDB(result.getShoprating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");


                } else {

                }
            }

            @Override
            public void onFailure(Call<Shoplist> call, Throwable t) {


            }
        });
    }

    private void showConfirmationDialog() {


        AlertDialog.Builder mBuild = new AlertDialog.Builder(ShopDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Call confirmation");
        tv_msg.setText("Are you sure, you want to call?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(ShopDescriptionActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ShopDescriptionActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                10);
                    }
                } else {

                    if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                }
                try {
                    callIntent.setData(Uri.parse("tel:" + "08026725115"));
                    startActivity(callIntent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    private void showRemoveBookmarkAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(ShopDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Bookmark");
        tv_msg.setText("Are you sure, you want to remove the Bookmark?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //remove the bookmark shop from the shared preference
                Shop pojo = new Shop();
                pojo.setSid(shop.getSid());
                pojo.setTitle(shop.getTitle());
                int removePos = 0;
                for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                    if (shop.getSid().equals(bookmarkList.get(tempPos).getSid())) {
                        removePos = tempPos;
                    }
                }
                bookmarkList.remove(removePos);
                sessionManager.setBookmarkShop(bookmarkList);
                bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));

                shopBookmark(2);
                dialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void shopBookmark(final int bookmark) {
        //bookmark the shop id by using user id
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call;

        call = stationClient.create_ShopBookmark(Prefs.getString(AppConstants.UserId, ""), shop.getSid(), bookmark);
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                final LogIn result = response.body();

                if (result.getStatus().get(0).getStatus() == 1) {

                    Toast.makeText(getBaseContext(), result.getStatus().get(0).getMessage().toString(), Toast.LENGTH_SHORT).show();
                    //make the book mark as true if it is remove make it false
                    if (bookmarkFlag) {
                        bookmarkFlag = true;
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
                    } else {
                        bookmarkFlag = false;
                        bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));


                    }
                    hideProgressDialog();

                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(ShopDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(ShopDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 10: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(ShopDescriptionActivity.this, "permission granted", Toast.LENGTH_SHORT).show();

                } else {

                    ActivityCompat.requestPermissions(ShopDescriptionActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            10);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    private void setViewPager() {
        final ViewPager wrapContentViewPager = (ViewPager) findViewById(R.id.pager_layout);

        ServiceAdapter adapter = new ServiceAdapter(getSupportFragmentManager());
        //get the subservices from shop id
        List<ServiceOnAShop> shopSubServices = dbobject.getShoponServiceListByShopid(shop.getSid());

        //if shop have packages then get the values of the package
        if (shopSubServices != null) {
            if (shopSubServices.size() > 0) {
                for (int idx = 0; idx < shopSubServices.size(); idx++) {

                    ShopSerializable shopDetail = new ShopSerializable();
                    shopDetail.setShopId(shop.getSid());
                    shopDetail.setShopTimings(shop.getShopTimings());
                    shopDetail.setNo_of_freetrial(shop.getNo_of_freetrial());
                    shopDetail.setSubServiceName(shopSubServices.get(idx).getSubserviceName());
                    shopDetail.setPayPerUse(shopSubServices.get(idx).getPayPerUse());
                    shopDetail.setSubServiceId(shopSubServices.get(idx).getSubserviceId());
                    shopDetail.setServiceDetails(dbobject.getShopDetailsonServiceListDB_ById(shop.getSid(), shopSubServices.get(idx).getSubserviceId()));
                    try {
                        List<ShopServiceDetails> serviceDetails = dbobject.getShopDetailsonServiceListDB_ById(shop.getSid(), shopSubServices.get(idx).getSubserviceId());
                        //if service details greater than zero
                        if (serviceDetails.size() > 0) {
                            //using service details loop it
                            for (int servidx = 0; servidx < serviceDetails.size(); servidx++) {
                                //store the package name in a variable name
                                shopDetail.setPackageName(serviceDetails.get(servidx).getPackageName());


                                //if batch details is there add it else don't add
                                List<BatchTime> batchTimes = dbobject.getShopBatchTDetailsBy_shopid(shop.getSid(), shopSubServices.get(idx).getSubserviceId(), serviceDetails.get(servidx).getPackage());

                                for (int bidx = 0; bidx < serviceDetails.size(); bidx++) {
                                    //if the batchTimes not null then insert the batch times only check not null don't check size of it
                                    if (batchTimes != null) {
                                        shopDetail.getServiceDetails().get(servidx).setBatchTimes(batchTimes);
                                    }
                                }

                                //if session details is there add it else don't add
                                List<Sessions> sessions = dbobject.getShopSessionDetailsByShopid(shop.getSid(), shopSubServices.get(idx).getSubserviceId(), serviceDetails.get(servidx).getPackage());
                                for (int sidx = 0; sidx < serviceDetails.size(); sidx++) {
                                    //if the Session Times not null then insert the session times only check not null don't check size of it
                                    if (sessions != null) {
                                        shopDetail.getServiceDetails().get(servidx).setSessions_details(sessions);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    shopDetails.add(shopDetail);

                }
            }
        }


        //if the shop have sub services then display else don't display
        if (shopSubServices != null && shopSubServices.size() > 0) {
            for (int idx = 0; idx < shopSubServices.size(); idx++) {
                adapter.addFrag(new ShopServiceDescription(), shopDetails.get(idx).getSubServiceName());
            }
        }

        wrapContentViewPager.setAdapter(adapter);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Bind the tabs to the ViewPager

        tabs.setViewPager(wrapContentViewPager);


    }

    private void setAmenities() {
        // get amenities from db using shopid

        List<Amenities> amenities = dbobject.getShopAmenitiesDBBy_shopid(shop.getSid());
        //if the shop have amenities then show it else don't show
        if (amenities != null && amenities.size() > 0) {
            amenities_layout.setVisibility(View.VISIBLE);
            ShopAmenitiesAdapter adapter = new ShopAmenitiesAdapter(ShopDescriptionActivity.this, amenities, new ShopAmenitiesAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {

                }
            });
            amenities_recycler.setAdapter(adapter);
        } else {
            amenities_layout.setVisibility(View.GONE);
        }
    }

    private void initVariable() {
        shop_image_layout = (SliderLayout) findViewById(R.id.shop_image_layout);
        amenities_layout = (LinearLayout) findViewById(R.id.review_amenities_layout);

        amenities_recycler = (RecyclerView) findViewById(R.id.amenities_recycler);
        toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        title_txt = (TextView) findViewById(R.id.title_txt);
        //set layout manager for amenities and review
        LinearLayoutManager alayoutManager = new LinearLayoutManager(ShopDescriptionActivity.this, LinearLayout.HORIZONTAL, false);
        amenities_recycler.setLayoutManager(alayoutManager);

        //set the layout for the recycler for review
        recycler_view_review = (RecyclerView) findViewById(R.id.recycler_view_review);

        //set layout manager for amenities and review
        LinearLayoutManager review_manager = new LinearLayoutManager(ShopDescriptionActivity.this, LinearLayout.HORIZONTAL, false);
        recycler_view_review.setLayoutManager(review_manager);


        description_img = (ImageView) findViewById(R.id.description_img);
        direction_img = (ImageView) findViewById(R.id.direction_img);
        bookmark_img = (ImageView) findViewById(R.id.bookmark_img);
        share_img = (ImageView) findViewById(R.id.share_img);
        rating_img = (ImageView) findViewById(R.id.rating_img);
        call_img = (ImageView) findViewById(R.id.call_img);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.service_layout);
        back_img = (ImageView) findViewById(R.id.back_img);
        ll_bookmark = (LinearLayout) findViewById(R.id.ll_bookmark);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabs.setTypeface(typeFace, 0);
        //change tab text color on change
        mTabsLinearLayout = ((LinearLayout) tabs.getChildAt(0));
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);

            if (i == currentPosition) {
                tv.setTextColor(getResources().getColor(R.color.black));
            } else {
                tv.setTextColor(getResources().getColor(R.color.white));
            }
        }
        sessionManager = new SessionManager(ShopDescriptionActivity.this);
        //if it is already bookmarked make the change bg color else don't
        checkBookmarked();
    }

    private void checkBookmarked() {
        try {
            bookmarkList = sessionManager.getBookmarkShop();
            bookmarkFlag = true;
            for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                if (bookmarkList.get(tempPos).getSid().equals(shop.getSid())) {
                    bookmarkFlag = false;
                    break;
                }
            }
            if (bookmarkFlag)       //Not yet bookmarked.
            {
                bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else                   //Already bookmarked.
            {
                bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setShopSlider() {

//seperare teh values of image url base on the comma
        String strings[] = shop.getImageUrlRectangle().split(",");

        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].trim();

        }

        for (int idx = 0; idx < strings.length; idx++) {
            System.out.println("banner image::" + BASE_URL + strings[idx]);
            DefaultSliderView defaultSliderView = new DefaultSliderView(ShopDescriptionActivity.this);
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(BASE_URL + strings[idx])

                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            shop_image_layout.addSlider(defaultSliderView);
        }
        shop_image_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        shop_image_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        shop_image_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        shop_image_layout.setDuration(4000);
    }

    private class ServiceAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ServiceAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
//pass the shop details to fragment using bundle
            System.out.println("get item::" + position);
            Bundle bundle = new Bundle();
            bundle.putSerializable("ShopDetails", shopDetails.get(position));
            bundle.putSerializable("shop", shop);

            mFragmentList.get(position).setArguments(bundle);

            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}