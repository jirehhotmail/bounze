package com.jireh.bounze.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Adapter.AllreviewsAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.Fragment.CoachServiceDescription;
import com.jireh.bounze.Fragment.ShowReviewFragment;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.SendAnalytics;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.Category;
import com.jireh.bounze.data.CategoryDetail;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachBatchTimeDetail;
import com.jireh.bounze.data.CoachList;
import com.jireh.bounze.data.CoachProgram;
import com.jireh.bounze.data.CoachSerializable;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.data.SessionsDetail;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;
import static com.jireh.bounze.Activity.SplashScreenActivity.current_coachcat;
import static com.jireh.bounze.Activity.SplashScreenActivity.current_loc;


/**
 * Created by Muthamizhan C on 28-08-2017.
 */

public class CoachDescriptionActivity extends AppCompatActivity {
    public static String current_shop = "";
    static String temp_serviceid = current_coachcat;
    public List<Coach> coachlist;
    public List<Category> coachcatlist;
    public List<CoachProgram> coachprolist;
    public String current_coach = "";
    ImageView description_img, direction_img, bookmark_img, share_img, rating_img, call_img;
    int temp_servicepos = 0;
    LinearLayout servicelistonshop;
    List<Category> current_s_categorylist = new ArrayList<Category>();
    /* List<CoachProgram> current_coach_program;*/
    ImageView back_img;
    CollapsingToolbarLayout toolbar;
    //   ListView programlist;
    int shopposition = 0;
    int serviceposition = 0;
    DBFunctions dbobject;
    SliderLayout trainer_image_layout;
    boolean isBookMarked = false;
    LinearLayout ll_bookmark;
    ProgressBar progressBar;
    PagerSlidingTabStrip tabs;
    List<CoachSerializable> coachDetails = new ArrayList<>();
    Coach coachDetail;
    TextView title_txt;
    private ArrayList<Coach> bookmarkList;
    private SessionManager sessionManager;
    private boolean bookmarkFlag;
    RecyclerView reviews_recycler;
    List<Review_> myReview;
    int clickcount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_view);
        initVariable();
        //get the coach id
        //get coach details by id
        dbobject = new DBFunctions(getApplicationContext());
        //get data from the url else get from database
      /*  Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
        if (appLinkData != null) {
            String coachId = appLinkData.getLastPathSegment();
            coachDetail = dbobject.getCoachDetailsbyId(coachId);
            current_coach = coachId;
        } else {

        }*/
        current_coach = getIntent().getStringExtra("CoachId");
        coachDetail = dbobject.getCoachDetailsbyId(current_coach);


        // programlist = (ListView) findViewById(R.id.programlist);
        //set the title for the pag
        title_txt.setText(coachDetail.getTitle() + "-" + coachDetail.getSmallAddress());


        coachlist = dbobject.getCoachListDB();
        coachcatlist = dbobject.getCoachCatAllListDB();
        coachprolist = dbobject.getCoachProgramListDB();
        for (int i = 0; i < coachlist.size(); i++) {
            if (coachlist.get(i).getCid().equals(current_coach)) shopposition = i;

        }


        for (int i = 0; i < coachcatlist.size(); i++) {
            if (coachcatlist.get(i).getCid().equals(current_coachcat)) {
                serviceposition = i;
                temp_serviceid = coachcatlist.get(i).getCid();
            }
        }


        for (int i = 0; i < coachprolist.size(); i++) {
            if (coachprolist.get(i).getCoachId().equals(current_coach)) {
                String categoryid = coachprolist.get(i).getCategoryId();

                if (current_s_categorylist.size() != 0) {
                    int exixt = 0;

                    for (int k = 0; k < current_s_categorylist.size(); k++) {
                        if (current_s_categorylist.get(k).getCid().equals(categoryid))
                            exixt = 1;
                    }


                    if (exixt != 1)
                        for (int k = 0; k < coachcatlist.size(); k++) {
                            if (coachcatlist.get(k).getCid().equals(categoryid))
                                if (!coachcatlist.get(k).getPriority().equals("1"))
                                    current_s_categorylist.add(coachcatlist.get(k));
                        }
                } else {
                    for (int k = 0; k < coachcatlist.size(); k++) {
                        if (coachcatlist.get(k).getCid().equals(categoryid))
                            if (!coachcatlist.get(k).getPriority().equals("1"))
                                current_s_categorylist.add(coachcatlist.get(k));
                    }

                }


            }
        }


        if (current_s_categorylist.size() > 0) {
            temp_serviceid = current_s_categorylist.get(0).getCid();
            temp_servicepos = 0;
            //getlistofprograms();
        }
        //get the user reviews for the coach
        setReviews();
        setCoachSlider();
        setOnClickListener();

        try {
            new SendAnalytics(getApplicationContext(), Prefs.getString(AppConstants.UserId, ""), " shop view ", "", current_shop, "2", " ", "1", "" + current_loc.getLatitude(), "" + current_loc.getLongitude());
        } catch (Exception e) {
        }

        setViewPager();
        sessionManager = new SessionManager(CoachDescriptionActivity.this);
        //if it is already bookmarked make the change bg color else don't
        checkBookmarked();
    }

    private void setReviews() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getCoachReviews(coachDetail.getCid());
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    myReview = result.getReviews();

                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");


            }
        });
    }

    private void checkBookmarked() {
        bookmarkList = sessionManager.getBookmarkCoach();
        bookmarkFlag = true;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (bookmarkList.get(tempPos).getCid().equals(current_coach)) {
                bookmarkFlag = false;
                break;
            }
        }
        if (bookmarkFlag)       //Not yet bookmarked.
        {
            bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else                   //Already bookmarked.
        {
            bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
        }

    }


    private void setViewPager() {
        final ViewPager wrapContentViewPager = (ViewPager) findViewById(R.id.pager_layout);


        ServiceAdapter adapter = new ServiceAdapter(getSupportFragmentManager());
        //get the programs of the coach from coach program list
        List<CoachProgram> coachPrograms = dbobject.getCoachProgramListByCoachId(current_coach);


        if (coachPrograms !=null) {

            for (int idx = 0; idx < coachPrograms.size(); idx++) {

                CoachSerializable coachDtl = new CoachSerializable();
                coachDtl.setCoachId(current_coach);
                coachDtl.setCoachSubCategoryId(coachPrograms.get(idx).getCategoryId());
                coachDtl.setSubCategoryName(coachPrograms.get(idx).getCategoryName());
                coachDtl.setPayPerUse(coachPrograms.get(idx).getPayPerUse());
                coachDtl.setNoOfFreeTrial(coachDetail.getNoOfFreetrial());
                coachDtl.setCategoryDetails(dbobject.getCoachCategoryDetailsByCoachIdCatId(current_coach, coachPrograms.get(idx).getCategoryId()));
                //get the category details list by using coach id
                try {
                    List<CategoryDetail> coachUniqueDetails = dbobject.getCoachCategoryDetailsByCoachIdCatId(current_coach, coachPrograms.get(idx).getCategoryId());
                    //if coach details greater than zero
                    if (coachUniqueDetails.size() > 0) {
                        //using coach details loop it
                        for (int chidx = 0; chidx < coachUniqueDetails.size(); chidx++) {
                            //if batch details is there add it else don't add
                            List<CoachBatchTimeDetail> batchTimes = dbobject.getCoachBatchTimeDetailsById(current_coach, coachUniqueDetails.get(chidx).getCoachPackage());
                            if (batchTimes != null) {
                                for (int bidx = 0; bidx < coachUniqueDetails.size(); bidx++) {

                                    coachDtl.getCategoryDetails().get(chidx).setUnique_ch_bt_tm_dtl(batchTimes);
                                }
                            }
                            //if session details is there add it else don't add
                            List<SessionsDetail> sessions = dbobject.getCoachSessionTimeDetailsById(current_coach, coachUniqueDetails.get(chidx).getCoachPackage());
                            if (sessions != null) {
                                for (int sidx = 0; sidx < coachUniqueDetails.size(); sidx++) {
                                    coachDtl.getCategoryDetails().get(chidx).setSessionsDetails(sessions);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                coachDetails.add(coachDtl);

            }
        }


        //if the shop have sub services then display else don't display
        if (coachPrograms != null && coachPrograms.size() > 0) {
            for (int idx = 0; idx < coachPrograms.size(); idx++) {
                adapter.addFrag(new CoachServiceDescription(), coachPrograms.get(idx).getCategoryName());
            }
        }

        wrapContentViewPager.setAdapter(adapter);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Bind the tabs to the ViewPager

        tabs.setViewPager(wrapContentViewPager);


    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        description_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the description dialog
                AlertDialog.Builder mBuild = new AlertDialog.Builder(CoachDescriptionActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.description_dialog, null);
                final TextView desc_txt = (TextView) mView.findViewById(R.id.description_txt);
                TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
                tv_title.setText("Description");
                Button close = (Button) mView.findViewById(R.id.close_btn);
                desc_txt.setText(coachlist.get(shopposition).getDescription());

                mBuild.setView(mView);
                final AlertDialog dialog = mBuild.create();
                dialog.setCancelable(true);
                dialog.show();
                //close the dialog on click ok btn
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            }
        });
        direction_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the small address details

                //show the description dialog
                AlertDialog.Builder mBuild = new AlertDialog.Builder(CoachDescriptionActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.description_dialog, null);
                final TextView address_txt = (TextView) mView.findViewById(R.id.description_txt);
                TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
                tv_title.setText("Address");
                Button close = (Button) mView.findViewById(R.id.close_btn);


                mBuild.setView(mView);
                final AlertDialog dialog = mBuild.create();
                dialog.setCancelable(true);
                dialog.show();
                //close the dialog on click ok btn
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                String strings[] = coachlist.get(shopposition).getSmallAddress().split(",");

                for (int i = 0; i < strings.length; i++) {
                    strings[i] = strings[i].trim();
                    strings[i] += ",\n";
                }
                String address = "";

                for (int i = 0; i < strings.length; i++)
                    address += strings[i];

                address_txt.setText(address);
            }
        });
        //send the bookmark details to server
        bookmark_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetOn()) {
                    //check if already it is bookmarked on click remove else bookmark

                    bookmarkList = sessionManager.getBookmarkCoach();
                    bookmarkFlag = true;
                    for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                        if (bookmarkList.get(tempPos).getCid().equals(current_coach)) {
                            bookmarkFlag = false;
                            break;
                        }
                    }
                    if (bookmarkFlag) {
                        Coach pojo = new Coach();
                        pojo.setCid(current_coach);
                        pojo.setTitle(coachDetail.getTitle());
                        bookmarkList.add(bookmarkList.size(), pojo);
                        sessionManager.setBookmarkCoach(bookmarkList);
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));

                        //showProgressDialog();
                        //bookmark the page
                        coachBookmark(1);

                    } else {
                        showRemoveBookmarkAlert();
                    }


                } else {
                    Toast.makeText(CoachDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        //show the sharing page
        share_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the coach description if length >40 show it else directly show it
                int desc_length = coachDetail.getDescription().length();
                String desc = coachDetail.getDescription();
                if (desc_length > 40) {
                    desc = coachDetail.getDescription().substring(0,40);
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, coachDetail.getTitle() + "\n" + coachDetail.getSmallAddress().trim() + "\n" + coachDetail.getCategory().trim() + "\n" +   desc+"..."
                        +"\n\n"+"https://www.bounze.in/coach_programs.php?cid="+ coachDetail.getCid()+"&coachid="+current_coach
                        +"\n\n"+ "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n"+ "https://play.google.com/store/apps/details?id=com.triton.bounze");
                //shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/coachdescription/" + current_coach);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));
            }
        });

        //post the ratings for the shop

        rating_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount = clickcount + 1;
                if (clickcount % 2 == 1) {
                    //SET visibility visible for the recycler review
                    reviews_recycler.setVisibility(View.VISIBLE);
                    //check for my reviews if available show else show no reviews ound

                    if(myReview !=null)
                    {
                        //on single click show the all review and on second click show the rating bar alert
                        AllreviewsAdapter adapter = new AllreviewsAdapter(CoachDescriptionActivity.this, myReview);
                        reviews_recycler.setAdapter(adapter);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "No reviews found", Toast.LENGTH_LONG).show();
                    }

                    //first time clicked to do this
                    Toast.makeText(getApplicationContext(), "Click again to write a review", Toast.LENGTH_LONG).show();
                } else {
                    //show the rating alert in android  and close the recyclerview in android
                    reviews_recycler.setVisibility(View.GONE);
                    showRatingAlert();
                    //check how many times clicked and so on
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();
                }
            }
        });
        //call the branch phone no
        call_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //show the alert dialog  for the confirmation
                showConfirmationDialog();
                //get the phone call permission based on the versions of android


            }
        });
    }

    private void showRatingAlert() {

        AlertDialog.Builder mBuild = new AlertDialog.Builder(CoachDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.rate_bar_dailog, null);
        final RatingBar ratebar = (RatingBar) mView.findViewById(R.id.ratingBar);
        final EditText review = (EditText) mView.findViewById(R.id.comments_edt);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        tv_title.setText("Tell us what you think?");
        mBuild.setView(mView);
        ratebar.setRating(Float.valueOf(coachDetail.getAvgratings()));
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                doRating("" + ratebar.getRating(), review);

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void doRating(String rating, EditText review) {

        if (isInternetOn()) {

            trainer_ratings(rating, review);
        } else {
            Toast.makeText(CoachDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void trainer_ratings(String rating, EditText review) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), "", "", coachlist.get(shopposition).getCid(), "", review.getText().toString(), rating);
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();

                if (response_code == 200) {
                    //if submitted once show the reiew submitted toast else show the message from server
                    if (result.getStatus().get(0).getStatus().equals("1")) {

                        Toast.makeText(CoachDescriptionActivity.this, "Review submitted successfully", Toast.LENGTH_SHORT).show();

                        //call the server for updation
                        getCoachRatings();
                    } else {

                        Toast.makeText(CoachDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else if (response_code == 400) {
                    Toast.makeText(CoachDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(CoachDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "500 error");
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Toast.makeText(CoachDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void getCoachRatings() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<CoachList> call = stationClient.getCoachRatings();
        call.enqueue(new Callback<CoachList>() {
            @Override
            public void onResponse(Call<CoachList> call, Response<CoachList> response) {
                int response_code = response.code();
                CoachList result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateCoachListDB(result.getCoachrating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");


                } else {

                }
            }

            @Override
            public void onFailure(Call<CoachList> call, Throwable t) {


            }
        });
    }

    private void showConfirmationDialog() {


        AlertDialog.Builder mBuild = new AlertDialog.Builder(CoachDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Call confirmation");
        tv_msg.setText("Are you sure, you want to call?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(CoachDescriptionActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CoachDescriptionActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                10);
                    }
                } else {

                    if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                }
                try {
                    callIntent.setData(Uri.parse("tel:" + "08026725115"));
                    startActivity(callIntent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }







    private void showRemoveBookmarkAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(CoachDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Bookmark");
        tv_msg.setText("Are you sure, you want to remove the Bookmark?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //remove the bookmark coach from the shared preference
                Coach pojo = new Coach();
                pojo.setCid(current_coach);
                pojo.setTitle(coachDetail.getTitle());
                int removePos = 0;
                for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                    if (current_coach.equals(bookmarkList.get(tempPos).getCid())) {
                        removePos = tempPos;
                    }
                }
                bookmarkList.remove(removePos);
                sessionManager.setBookmarkCoach(bookmarkList);
                bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));


                coachBookmark(2);
                dialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void coachBookmark(final int bookmark) {
        //bookmark the shop id by using user id
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call;

        call = stationClient.create_CoachBookmark(Prefs.getString(AppConstants.UserId, ""), coachcatlist.get(shopposition).getCid(), bookmark);
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                final LogIn result = response.body();

                if (result.getStatus().get(0).getStatus() == 1) {

                    Toast.makeText(getBaseContext(), result.getStatus().get(0).getMessage().toString(), Toast.LENGTH_SHORT).show();
                    //make the book mark as true if it is remove make it false
                    if (bookmark == 1) {
                        isBookMarked = true;
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
                    } else {
                        isBookMarked = false;
                        bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    }


                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(CoachDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(CoachDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null)
                return netInfo.isConnected();
            else
                return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void initVariable() {
        trainer_image_layout = (SliderLayout) findViewById(R.id.trainer_image_layout);
        description_img = (ImageView) findViewById(R.id.description_img);
        direction_img = (ImageView) findViewById(R.id.direction_img);
        bookmark_img = (ImageView) findViewById(R.id.bookmark_img);
        share_img = (ImageView) findViewById(R.id.share_img);
        rating_img = (ImageView) findViewById(R.id.rating_img);
        call_img = (ImageView) findViewById(R.id.call_img);
        ll_bookmark = (LinearLayout) findViewById(R.id.ll_bookmark);
        back_img = (ImageView) findViewById(R.id.back_img);
        toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.service_layout);
        reviews_recycler = (RecyclerView) findViewById(R.id.reviews_recycler);
        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabs.setTypeface(typeFace, 0);


        LinearLayoutManager rlayoutManager = new LinearLayoutManager(CoachDescriptionActivity.this, LinearLayout.HORIZONTAL, false);
        reviews_recycler.setLayoutManager(rlayoutManager);
    }

    private void setCoachSlider() {


        for (int idx = 0; idx < 2; idx++) {

            DefaultSliderView defaultSliderView = new DefaultSliderView(CoachDescriptionActivity.this);
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(BASE_URL + coachlist.get(shopposition).getImageUrlRectangle())

                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            trainer_image_layout.addSlider(defaultSliderView);
        }
        trainer_image_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        trainer_image_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        trainer_image_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        trainer_image_layout.setDuration(4000);

    }

  /*  void getlistofprograms() {
        current_coach_program = new ArrayList<CoachProgram>();

        TextView shopname = (TextView) findViewById(R.id.shopname);
        TextView description = (TextView) findViewById(R.id.description);

        shopname.setText("" + coachlist.get(shopposition).getTitle());
        description.setText("" + coachlist.get(shopposition).getDescription());


        for (int i = 0; i < coachprolist.size(); i++) {

            if ((coachprolist.get(i).getCoachId().equals(current_coach)) && (coachprolist.get(i).getCategoryId().equals(temp_serviceid))) {
                current_coach_program.add(coachprolist.get(i));
            }

        }


   *//*     // servicelistonshop = (LinearLayout) findViewById(R.id.servicelistonshop);
        //    servicelistonshop.removeAllViews();
        for (int i = 0; i < current_s_categorylist.size(); i++) {
            LinearLayout l = new LinearLayout(this);
            l.setOrientation(LinearLayout.VERTICAL);
            TextView t = new TextView(this);
            t.setText(current_s_categorylist.get(i).getTitle());
            t.setGravity(Gravity.CENTER);
            LinearLayout u = new LinearLayout(this);
            u.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 5));
            if (i == -1)
                u.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            l.setGravity(Gravity.CENTER);
            l.addView(t);
            l.addView(u);
            l.setPadding(15, 15, 15, 15);
           *//**//* if (i == temp_servicepos) {
                l.setBackgroundResource(R.drawable.timered);
                t.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
            } else {
                l.setBackgroundResource(R.drawable.timewhite);
                t.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            }*//**//*
            final int pos_x = i;
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    temp_serviceid = current_s_categorylist.get(pos_x).getCid();
                    temp_servicepos = pos_x;
                    getlistofprograms();
                }
            });
            servicelistonshop.addView(l);
        }*//*


        // programlist.setAdapter(new ShopListAdapter(getApplicationContext()));


    }*/


    private class ServiceAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ServiceAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
//pass the coach details to fragment using bundle
            System.out.println("get item::" + position);
            Bundle bundle = new Bundle();
            bundle.putSerializable("coachDetails", coachDetails.get(position));
            bundle.putSerializable("coach", coachDetail);

            mFragmentList.get(position).setArguments(bundle);

            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
