package com.jireh.bounze.Activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Fragment.Explore;
import com.jireh.bounze.Fragment.HomePage;
import com.jireh.bounze.Fragment.MyProfile;
import com.jireh.bounze.Fragment.OnTheGo;
import com.jireh.bounze.Fragment.Pass;
import com.jireh.bounze.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 29-07-2017.
 */

public class PagerLayout extends AppCompatActivity {
    public static ViewPager viewPager;
    public static Animation rotate_forward, rotate_backward;
    FloatingActionButton tabThree;
    ViewPagerAdapter adapter;
    Toolbar toolbar;
    int count = 0, select_count = 0;
    int home_click_count = 0;
    boolean doubleBackToExitPressedOnce = false;
    ImageView back_img;
    RelativeLayout no_connection_layout;
    int selectedPosition;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
/*
        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();       */


   /*     if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
        }*/


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        no_connection_layout = (RelativeLayout) findViewById(R.id.no_connection_layout);
        // Use package name which we want to check if it is installed then add wallet 100 points and if user id is there then add 50 points
        boolean isAppInstalled = appInstalledOrNot("in.bounze");

        if (isAppInstalled) {
            //update the wallet points
            System.out.println("App installed");
            //update wallet to add reward points
            // updateWallet();
        } else {
            // Do whatever we want to do if application not installed
            // For example, Redirect to play store
            System.out.println("App is not currently installed.");
        }
        //check the internet connection if it is there show internet else don't show
        if (isInternetOn()) {
            no_connection_layout.setVisibility(View.GONE);
            viewPager.setVisibility(View.VISIBLE);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.tabs);

            tabLayout.setupWithViewPager(viewPager);


            setupTabIcons();

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    //if the tab 2 is clicked then show then don't move the tab and then show the 4 icons
                    //where the user in last tab
                    selectedPosition = tab.getPosition();
                    System.out.println("selected position::" + selectedPosition);
                    //based on the selection of the page change the selected icon
                    switch (selectedPosition) {
                        case 0:

                            //set the tabs to the tab layouts with image and text
                            TextView tabOne = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabOne.setText("Home");
                            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_selected, 0, 0);
                            tabLayout.getTabAt(0).setCustomView(null);
                            tabLayout.getTabAt(0).setCustomView(tabOne);
                            break;
                        case 1:
                            TextView tabTwo = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabTwo.setText("OnTheGo");
                            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.onthego_select, 0, 0);
                            tabLayout.getTabAt(1).setCustomView(null);
                            tabLayout.getTabAt(1).setCustomView(tabTwo);
                            break;

                        case 2:
                            tabThree = (FloatingActionButton) LayoutInflater.from(PagerLayout.this).inflate(R.layout.floating_button_icon, null);
                            tabLayout.getTabAt(2).setCustomView(null);
                            tabLayout.getTabAt(2).setCustomView(tabThree);
                            break;
                        case 3:
                            TextView tabFour = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabFour.setText("Pass");
                            tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.pass_select, 0, 0);
                            tabLayout.getTabAt(3).setCustomView(null);
                            tabLayout.getTabAt(3).setCustomView(tabFour);
                            break;
                        case 4:
                            TextView tabFive = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabFive.setText("Me");
                            tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_profile_select, 0, 0);
                            tabLayout.getTabAt(4).setCustomView(null);
                            tabLayout.getTabAt(4).setCustomView(tabFive);
                            // exportDb();
                            break;


                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    //based on the unselected tab page change the icon as normal icon
                    switch (tab.getPosition()) {
                        case 0:

                            //set the tabs to the tab layouts with image and text
                            TextView tabOne = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabOne.setText("Home");
                            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home, 0, 0);
                            tabLayout.getTabAt(0).setCustomView(null);
                            tabLayout.getTabAt(0).setCustomView(tabOne);
                            break;
                        case 1:
                            TextView tabTwo = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabTwo.setText("OnTheGo");
                            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.onthego_location, 0, 0);
                            tabLayout.getTabAt(1).setCustomView(null);
                            tabLayout.getTabAt(1).setCustomView(tabTwo);
                            break;
                        case 2:
                            tabThree = (FloatingActionButton) LayoutInflater.from(PagerLayout.this).inflate(R.layout.floating_button_icon, null);
                            tabLayout.getTabAt(2).setCustomView(null);
                            tabLayout.getTabAt(2).setCustomView(tabThree);
                            break;
                        case 3:
                            TextView tabFour = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabFour.setText("Pass");
                            tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.id_card, 0, 0);
                            tabLayout.getTabAt(3).setCustomView(null);
                            tabLayout.getTabAt(3).setCustomView(tabFour);
                            break;
                        case 4:
                            TextView tabFive = (TextView) LayoutInflater.from(PagerLayout.this).inflate(R.layout.custom_tab, null);
                            tabFive.setText("Me");
                            tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile, 0, 0);
                            tabLayout.getTabAt(4).setCustomView(null);
                            tabLayout.getTabAt(4).setCustomView(tabFive);
                            //exportDb();
                            break;


                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {


                }
            });

              setOnClickListener();
        } else {

            no_connection_layout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.GONE);


        }


    }

    private void setOnClickListener() {

          viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
              @Override
              public void onPageScrolled(int i, float v, int i1) {

              }

              @Override
              public void onPageSelected(int i) {
                  //set onclick listner for the tab three layout
                  tabThree.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {

                          if ((select_count % 2 == 0)) {
                              ViewCompat.animate(tabThree)
                                      .rotation(135.0F)
                                      .withLayer()
                                      .setDuration(300L)
                                      .setInterpolator(new OvershootInterpolator())
                                      .start();

                              Fragment show_frag = adapter.getItem(selectedPosition);
                              if (show_frag instanceof HomePage) {
                                  ((HomePage) show_frag).showMenu();
                              } else if (show_frag instanceof OnTheGo) {
                                  ((OnTheGo) show_frag).showMenu();
                              } else if (show_frag instanceof Explore) {
                                  ((Explore) show_frag).showMenu();
                              } else if (show_frag instanceof Pass) {
                                  ((Pass) show_frag).showMenu();
                              } else if (show_frag instanceof MyProfile) {
                                  ((MyProfile) show_frag).showMenu();
                              }
                              select_count++;
                          } else {
                              ViewCompat.animate(tabThree)
                                      .rotation(0.0F)
                                      .withLayer()
                                      .setDuration(300L)
                                      .setInterpolator(new OvershootInterpolator())
                                      .start();
                              Fragment current_frag = adapter.getItem(selectedPosition);
                              if (current_frag instanceof HomePage) {
                                  ((HomePage) current_frag).hideMenu();
                              } else if (current_frag instanceof OnTheGo) {
                                  ((OnTheGo) current_frag).hideMenu();
                              } else if (current_frag instanceof Explore) {
                                  ((Explore) current_frag).hideMenu();
                              } else if (current_frag instanceof Pass) {
                                  ((Pass) current_frag).hideMenu();
                              } else if (current_frag instanceof MyProfile) {
                                  ((MyProfile) current_frag).hideMenu();
                              }

                              select_count++;



                          }


                      }
                  });
              }

              @Override
              public void onPageScrollStateChanged(int i) {

              }
          });

    }



    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    private boolean isInternetOn() {

        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null)
                return netInfo.isConnected();
            else
                return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;

    }

    //on double click the back button only close the app
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void exportDb() {


        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/bounzedb.db";
                String backupDBPath = "bounzedb.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Adding custom view to tab
     */
    private void setupTabIcons() {
        //set the tabs to the tab layouts with image and text
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Home");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home, 0, 0);

        tabLayout.getTabAt(0).setCustomView(tabOne);


        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("OnTheGo");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.onthego_location, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        tabThree = (FloatingActionButton) LayoutInflater.from(this).inflate(R.layout.floating_button_icon, null);

        tabLayout.getTabAt(2).setCustomView(tabThree);


        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Pass");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.id_card, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);

        TextView tabFive = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFive.setText("Me");
        tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profile, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabFive);
    }

    /**
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomePage(), "");
        adapter.addFrag(new OnTheGo(), "");
        adapter.addFrag(new Explore(), "");
        adapter.addFrag(new Pass(), "");
        adapter.addFrag(new MyProfile(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
           /*  SparseArray<WeakReference<MyFragment>> mFragments = new SparseArray<WeakReference<MyFragment>>(3);
            myMemberSparseArrayObject.put(position, new WeakReference<MyFragment> (f))*/
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
