package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jireh.bounze.Adapter.EventAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.data.Event;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 07-10-2017.
 */

public class EventActivity extends Activity {
    RecyclerView event_recycler;
    List<Event> eventlist;
    Toolbar toolbar;
    TextView title_txt;
    ImageView back_img;
    //  private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    DBFunctions dbobject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eventlist);
        initvariable();

        dbobject = new DBFunctions(getApplicationContext());

        //set the title
        title_txt.setText("Events");

        //get event values form the local db
        eventlist = dbobject.getEventListDB();


        //set the event adapter
        setEventAdapter(eventlist);


        //set OnclickListener
        setOnClickListener();

    }


    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(1500);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }


    private void setEventAdapter(final List<Event> events) {

        EventAdapter eventAdapter = new EventAdapter(EventActivity.this, events, new EventAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                Intent in = new Intent(EventActivity.this, EventDescriptionActivity.class);
                in.putExtra("EventDetails", (Serializable) events.get(position));
                startActivity(in);
            }
        });
        //set the adapter for recyclerview
        event_recycler.setAdapter(eventAdapter);
    }

    private void initvariable() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) findViewById(R.id.title_txt);
        event_recycler = (RecyclerView) findViewById(R.id.event_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(EventActivity.this);
        event_recycler.setLayoutManager(manager);

        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

    }
}
