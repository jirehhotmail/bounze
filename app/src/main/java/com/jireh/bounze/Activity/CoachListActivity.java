package com.jireh.bounze.Activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.jireh.bounze.Adapter.CoachAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.Util.SendAnalytics;
import com.jireh.bounze.data.Category;
import com.jireh.bounze.data.City;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachProgram;
import com.pixplicity.easyprefs.library.Prefs;
import java.util.ArrayList;
import java.util.List;
import static com.jireh.bounze.Activity.SplashScreenActivity.current_loc;
import static com.jireh.bounze.Activity.SplashScreenActivity.current_shop;

public class CoachListActivity extends AppCompatActivity {
    public List<Coach> coachlist;
    public List<Category> coachcatlist;
    public List<CoachProgram> coachprolist;
    public List<City> citylist;
    RelativeLayout spinner_layout;
    List<Coach> current_coachlist = new ArrayList<Coach>();
    DBFunctions dbobject;
    RecyclerView coach_list_recycler;
    GPSTracker gpsTracker;
    Toolbar toolbar;
    TextView title_txt, no_coach_txt;
    ImageView back_img, search_img, filter_img;
    EditText search_edit_text;
    List<Coach> search_coach = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_list);
        initVariable();

        gpsTracker = new GPSTracker(CoachListActivity.this);

        title_txt.setText(getIntent().getStringExtra("CategoryName"));


        //set visibility for the search and filter image
        search_img.setVisibility(View.VISIBLE);
        filter_img.setVisibility(View.VISIBLE);
        setOnClickListener();
        //get all coach list
        coachlist = dbobject.getCoachListDB();
        //get the all the category details
        coachcatlist = dbobject.getCoachCatAllListDB();
        //get the coach program details
        coachprolist = dbobject.getCoachProgramListDB();

        citylist = dbobject.getCityListDB();
        //get the categoryId selected
        String categoryId = getIntent().getStringExtra("CategoryIdSelected");
        //loop with the coach program list
        for (int i = 0; i < coachprolist.size(); i++) {
            //if coach category id match with selected category id true enter loop
            if (coachprolist.get(i).getCategoryId().equals(categoryId)) {
                //if matches then get the coachid
                String coachid = coachprolist.get(i).getCoachId();//69
                //initially coachlist is empty
                if (current_coachlist.size() != 0) {
                    int exixt = 0;

                    for (int k = 0; k < current_coachlist.size(); k++) {
                        if (current_coachlist.get(k).getCid().equals(coachid))
                            exixt = 1;
                    }
                    if (exixt != 1)
                        for (int k = 0; k < coachlist.size(); k++) {
                            if (coachlist.get(k).getCid().equals(coachid))
                                current_coachlist.add(coachlist.get(k));
                        }
                } else {
                    //enters here loop with the coach list details
                    for (int k = 0; k < coachlist.size(); k++) {
                        //if the coach id matches the
                        if (coachlist.get(k).getCid().equals(coachid))
                            current_coachlist.add(coachlist.get(k));
                    }
                }


            }
        }

        putdate();

        try {
            new SendAnalytics(getApplicationContext(), Prefs.getString(AppConstants.UserId, ""), " list of shops for service ", "", current_shop, "1", " ", "1", "" + current_loc.getLatitude(), "" + current_loc.getLongitude());
        } catch (Exception e) {
        }
    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //on click filter move to filter page

        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CoachListActivity.this, CoachFilter.class);
                startActivity(in);
                finish();
            }
        });

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set the visibility for the edittext search and gone for filter and search img
                search_edit_text.setVisibility(View.VISIBLE);
                search_edit_text.setHint("Lokesh");
                filter_img.setVisibility(View.GONE);
                search_img.setVisibility(View.GONE);
                addTextchangeListener();
            }
        });

    }

    private void addTextchangeListener() {

        search_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                //clear before adding the filter model list
                if (search_coach.size() > 0) {
                    search_coach.clear();
                }

                //if search text is greater than zero filter it else don't filter
                query = query.toString().toLowerCase();
                if (query.length() > 0) {

                    //get the coach list by name
                    final List<Coach> searchCoachList = dbobject.getCoachListByTitleDB(query.toString().toLowerCase().replaceAll(" ", "%"));
                    if (searchCoachList != null) {
                        //if coach available then set no coach to gone and recyclerview visible
                        no_coach_txt.setVisibility(View.GONE);
                        coach_list_recycler.setVisibility(View.VISIBLE);

                        CoachAdapter coachAdapter = new CoachAdapter(CoachListActivity.this, searchCoachList, new CoachAdapter.onItemClickListener() {
                            @Override
                            public void onItemClick(int view, int position) {
                                startActivity(new Intent(CoachListActivity.this, CoachDescriptionActivity.class).putExtra("CoachId", searchCoachList.get(position).getCid()));
                            }
                        });
                        coach_list_recycler.setAdapter(coachAdapter);

                    } else {
                        //if no coach available then set no coach to visible and recyclerview gone
                        no_coach_txt.setVisibility(View.VISIBLE);
                        coach_list_recycler.setVisibility(View.GONE);
                    }
                }


            }
        });
    }


    private void initVariable() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        dbobject = new DBFunctions(getApplicationContext());
        spinner_layout = (RelativeLayout) toolbar.findViewById(R.id.spinner_layout);
        coach_list_recycler = (RecyclerView) findViewById(R.id.coach_list_recycler);

        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        back_img = (ImageView) findViewById(R.id.back_img);
        search_edit_text = (EditText) toolbar.findViewById(R.id.search_edit_text);
        no_coach_txt = (TextView) findViewById(R.id.no_coach_txt);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    void putdate() {


        List<Coach> tcurrent_coachlist = new ArrayList<Coach>();
        for (int idx = 0; idx < current_coachlist.size(); idx++) {
            Coach coach_obj = new Coach();
            Log.e("current_coachlist", current_coachlist.get(idx).getActiveStatus() + " ==1.");
            if (current_coachlist.get(idx).getActiveStatus().equals("1")) {

                tcurrent_coachlist.add(current_coachlist.get(idx));
                try {
                    //check if the latitude and longitude values are not null
                    if (Double.valueOf(gpsTracker.getLatitude()) != null || Double.valueOf(gpsTracker.getLongitude()) != null) {
                        Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(current_coachlist.get(idx).getLatitude()), Double.parseDouble(current_coachlist.get(idx).getLongitude()), current_coachlist.get(idx).getResults());

                        coach_obj.setResults(current_coachlist.get(idx).getResults());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }

                if (coach_obj.getCid() != null) {
                    tcurrent_coachlist.add(coach_obj);
                }


            }
        }

        current_coachlist = tcurrent_coachlist;
        if (current_coachlist.size() == 0) {
            Toast.makeText(CoachListActivity.this, "No coaches found", Toast.LENGTH_LONG).show();
            // lv.setBackgroundResource(R.drawable.noresultfound);
        } else {
            LinearLayoutManager manager = new LinearLayoutManager(this);
            coach_list_recycler.setLayoutManager(manager);
            CoachAdapter adapter = new CoachAdapter(CoachListActivity.this, current_coachlist, new CoachAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {
                    startActivity(new Intent(CoachListActivity.this, CoachDescriptionActivity.class).putExtra("CoachId", current_coachlist.get(position).getCid()));
                }
            });
            coach_list_recycler.setAdapter(adapter);
        }

    }


}
