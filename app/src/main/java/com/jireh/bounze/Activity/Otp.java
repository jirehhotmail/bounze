package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.SendOTP;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Otp extends Activity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String mobile_no = "", name = "", email = "", password = "";
    SendOTP send_otp = new SendOTP();
    TextView mobile_txt, title, resend_code, mobile_no_text;
    ProgressBar progress_bar;
    Toolbar toolbar;
    LinearLayout ll_back;
    private EditText code_one, code_two, code_three, code_four, code_five, code_six;
    private Button Verify_btn;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");

                String numberOnly = message.replaceAll("[^0-9]", "");
                System.out.println("message only ::" + message);
                System.out.println("number only ::" + numberOnly);
                String[] alphabets = numberOnly.split("");
                code_one.setText(alphabets[1]);
                code_two.setText(alphabets[2]);
                code_three.setText(alphabets[3]);
                code_four.setText(alphabets[4]);
                code_five.setText(alphabets[5]);
                code_six.setText(alphabets[6]);

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_layout);
        initVariable();

        //set the title
        title.setText("Authentication process");
        try {
            //get the user details
            mobile_no = getIntent().getExtras().getString("mobile_no");
            name = getIntent().getStringExtra("name");
            email = getIntent().getStringExtra("email");
            password = getIntent().getStringExtra("password");
            //set the mobile no
            mobile_no_text.setText("+91- " + mobile_no);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //ask the user for permission to read, write sms
        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.

            //get a random number and send it
            final int random_no = send_otp.getRandomNumber(100000, 999999);
            send_otp.send_otp_msg91(mobile_no, random_no, Otp.this);
            send_otp.showcounter(R.id.done_btn, this, 60000, 1000, " Seconds.");
            //set on click listener
            setOnClickListener();
        } else {
            //get a random number and send it
            final int random_no = send_otp.getRandomNumber(100000, 999999);
            send_otp.send_otp_msg91(mobile_no, random_no, Otp.this);
            send_otp.showcounter(R.id.done_btn, this, 60000, 1000, " Seconds.");
            //set on click listener
            setOnClickListener();
        }
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).
                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void setOnClickListener() {
        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resend_code.setEnabled(false);
                int random_no = send_otp.getRandomNumber(100000, 999999);
                send_otp.send_otp_msg91(mobile_no, random_no, Otp.this);
                send_otp.showcounter(R.id.done_btn, Otp.this, 60000, 1000, " Seconds.");
            }
        });


        Verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Verify_btn.setEnabled(false);
                if (send_otp.validation_phoneno(R.id.code_one, Otp.this)) {

                    //check the user otp and the random no
                    if (send_otp.checkNumber(code_one.getText().toString() +
                            code_two.getText().toString() +
                            code_three.getText().toString() +
                            code_four.getText().toString() +
                            code_five.getText().toString() +
                            code_six.getText().toString())) {
                        Toast.makeText(Otp.this, "Verified", Toast.LENGTH_LONG).show();
                        //if verified then register
                        checkDetails(name, email, mobile_no, password);

                    } else {
                        Toast.makeText(Otp.this, "Otp doesn't match, please try again", Toast.LENGTH_SHORT).show();
                        Verify_btn.setEnabled(true);
                    }

                }
                Verify_btn.setEnabled(true);
            }
        });
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initVariable() {
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.title_txt);
        mobile_txt = (TextView) findViewById(R.id.send_otp_txt);
        code_one = (EditText) findViewById(R.id.code_one);
        code_two = (EditText) findViewById(R.id.code_two);
        code_three = (EditText) findViewById(R.id.code_three);
        code_four = (EditText) findViewById(R.id.code_four);
        code_five = (EditText) findViewById(R.id.code_five);
        code_six = (EditText) findViewById(R.id.code_six);
        Verify_btn = (Button) findViewById(R.id.done_btn);
        ll_back = (LinearLayout) toolbar.findViewById(R.id.ll_back);
        resend_code = (TextView) findViewById(R.id.resend_code);
        mobile_no_text = (TextView) findViewById(R.id.mobile_no_text);
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void checkDetails(final String name, String email, String mobile_no, String password) {

        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.createRegistration(name, email, password, "3", mobile_no);

        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                final LogIn result = response.body();


                hideProgressDialog();
                if (response_code == 200) {

                    List<Profile> check = result.getStatus();
                    Profile check1 = check.get(0);
                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    if (value.equals("1")) {


                        SharedPreferences pred = getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editorp = pred.edit();
                        editorp.putString("status", value);
                        editorp.commit();
                        Toast.makeText(Otp.this, "Welcome "+name,Toast.LENGTH_LONG).show();
                        Toast.makeText(Otp.this, "Registered Successfully, Check your email for activation link.", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(Otp.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        //update the wallet
                        //updateWallet();
                    } else {
                        Toast.makeText(Otp.this, message, Toast.LENGTH_LONG).show();
                        finish();
                    }

                } else if (response_code == 400) {
                    Toast.makeText(Otp.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    finish();
                    hideProgressDialog();
                } else if (response_code == 500) {
                    Toast.makeText(Otp.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    finish();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(Otp.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();
                finish();
            }
        });
    }

   /* private void updateWallet() {

           //add the registration points to wallet
        String wallet = "50";

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));

        RequestBody wallet_ = RequestBody.create(MediaType.parse("text/plain"), wallet);

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.updateWallet(userId, wallet_);

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                UserProfileService result = response.body();
                if (result.getResult().get(0).getStatus() == 0) {
                  //  Toast.makeText(Otp.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                  //  Toast.makeText(Otp.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                Toast.makeText(Otp.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");


            }
        });

    }*/
}



