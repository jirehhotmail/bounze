package com.jireh.bounze.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.firebase.auth.FirebaseAuth;
import com.pixplicity.easyprefs.library.Prefs;
import com.jireh.bounze.Fragment.Change_Password;
import com.jireh.bounze.Fragment.HealthTracking;
import com.jireh.bounze.Fragment.MyDetails;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 30-08-2017.
 */

public class ProfileDetailsActivity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView back_img;
    TextView title_txt;
    LinearLayout mTabsLinearLayout;
    int currentPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);
        initVariable();


        setOnClickListener();

    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);

        title_txt.setText("Me");

        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        ProfilePagerAdapter adapter = new ProfilePagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new MyDetails(), "Profile");
        adapter.addFrag(new HealthTracking(), "Health Tracking");
        //if user logged in as google or facebook then remove the change password field
        if (!(Prefs.getBoolean(AppConstants.isUserLoggedGoogle, false) || Prefs.getBoolean(AppConstants.isUserLoggedFacebook, false))) {
            adapter.addFrag(new Change_Password(), "Change Password");
        }
        adapter.addFrag(new MyReviews(), "My Reviews");
        pager.setAdapter(adapter);

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tab_layout);
        tabs.setViewPager(pager);
        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabs.setTypeface(typeFace , 0);
        mTabsLinearLayout = ((LinearLayout) tabs.getChildAt(0));
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);

            if (i == currentPosition) {
                tv.setTextColor(getResources().getColor(R.color.black));
            } else {
                tv.setTextColor(getResources().getColor(R.color.white));
            }
        }


        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    if (i == position) {
                        tv.setTextColor(getResources().getColor(R.color.black));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private class ProfilePagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ProfilePagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


