package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.Adapter.OnTheGoAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.CustomEditText;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.data.Shop;

import java.util.List;

/**
 * Created by Muthamizhan C on 18-08-2017.
 */

public class SearchActivity extends Activity {
    RecyclerView search_recycler;
    CustomEditText search_edt;
    Toolbar toolbar;
    TextView title_txt, empty_view;
    ImageView back_img;
    List<Shop> shoplist;
    DBFunctions dbobject;
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);
        initializeVariable();
        setOnClickListener();
        title_txt.setText("Search");
        dbobject = new DBFunctions(getApplicationContext());
        //to hide the keyboard hide when open this activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //getWindow().setBackgroundDrawableResource(R.drawable.search_bg) ;
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.search_bg));
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        search_edt.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                //query the shoplist table based on the shop title and then get the result
                //if the query length greater than 0 then search it
                OnTheGoAdapter searchadapter = null;
                if (query.toString().length() > 0) {
                    search_recycler.setVisibility(View.VISIBLE);
                    empty_view.setVisibility(View.GONE);
                    final List<Shop> shoplist = dbobject.getShopListDetailbyTitle(query.toString().toLowerCase().replaceAll(" ","%"));

                    try {

                        //if the shop exist in search then set the adapter else don't do anything
                        if (shoplist.size() > 0 && shoplist != null) {


                            try {
                                //get the latitude and longitude from the gps tracker and set the distance
                                gpsTracker = new GPSTracker(SearchActivity.this);
                                if (!gpsTracker.canGetLocation()) {
                                    gpsTracker.showSettingsAlert();
                                }
                                else {
                                    for (int idx = 0; idx < shoplist.size(); idx++) {
                                        Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(shoplist.get(idx).getLatitude()), Double.parseDouble(shoplist.get(idx).getLongitude()), shoplist.get(idx).getResults());
                                        shoplist.get(idx).setResults(shoplist.get(idx).getResults());
                                    }
                                }

                            } catch (Exception e) {
                            }
                            searchadapter = new OnTheGoAdapter(SearchActivity.this, shoplist, new OnTheGoAdapter.onItemClickListener() {
                                @Override
                                public void onItemClick(int view, int position) {
                                    //on click get the shop id and go to shop description details page
                                    Intent in = new Intent(SearchActivity.this, ShopDescriptionActivity.class);
                                    in.putExtra("ShopDetails", shoplist.get(position));
                                    startActivity(in);
                                }
                            });
                            search_recycler.setAdapter(searchadapter);


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        search_recycler.setVisibility(View.GONE);
                        empty_view.setVisibility(View.VISIBLE);
                    }
                } else if (query.length() == 0 || shoplist.size() == 0) {
                    search_recycler.setVisibility(View.GONE);
                    empty_view.setVisibility(View.VISIBLE);

                }
            }
        });
    }


    private void initializeVariable() {
        search_recycler = (RecyclerView) findViewById(R.id.search_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        search_recycler.setLayoutManager(layoutManager);
        search_edt = (CustomEditText) findViewById(R.id.search_edit);
        empty_view = (TextView) findViewById(R.id.empty_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
    }


}
