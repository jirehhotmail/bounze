package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.jireh.bounze.BuildConfig;
import com.jireh.bounze.Intents.IntentHelper;
import com.jireh.bounze.Util.UriToIntentMapper;

/**
 * Created by Muthamizhan C on 05-01-2018.
 */

public class LinkDispatcherActivity extends Activity {

    private final UriToIntentMapper mMapper = new UriToIntentMapper(this, new IntentHelper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mMapper.dispatchIntent(getIntent());

        } catch (IllegalArgumentException iae) {
            // Malformed URL
            if (BuildConfig.DEBUG) {
                Log.e("Deep links", "Invalid URI", iae);
            }
        } finally {
            // Always finish the activity so that it doesn't stay in our history
            finish();
        }

    }
}