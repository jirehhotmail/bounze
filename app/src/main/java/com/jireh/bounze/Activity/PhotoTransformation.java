package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.jireh.bounze.R;
import com.jireh.bounze.data.UserProfile;

import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */

public class PhotoTransformation extends Activity {
    ImageView compare_first_image, compare_second_image;
    Toolbar toolbar;
    TextView title;
    ImageView backImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_transformation);
        initVariable();
        //set the title for the page
        title.setText("Photo Transformation");
        setOnClickListener();
        //get all the entries from the serializable object
        List<UserProfile> profile = (List<UserProfile>) getIntent().getSerializableExtra("photoTransformation");
        //get the
        String compare_one = profile.get(profile.size() - 2).getPicture();
        String compare_two = profile.get(profile.size() - 1).getPicture();
        //set the last two images in the imageview
        Picasso.with(PhotoTransformation.this).load(BASE_URL + compare_one).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(compare_first_image);
        Picasso.with(PhotoTransformation.this).load(BASE_URL + compare_two).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(compare_second_image);
    }

    private void setOnClickListener() {
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        backImage = (ImageView) toolbar.findViewById(R.id.back_img);
        compare_first_image = (ImageView) findViewById(R.id.compare_first_image);
        compare_second_image = (ImageView) findViewById(R.id.compare_second_image);
    }
}
