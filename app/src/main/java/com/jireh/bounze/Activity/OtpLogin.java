package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MuthuBala on 1/25/2018.
 */

public class OtpLogin extends Activity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 2;
    private static final String TAG = "OtpLogin";
    Toolbar toolbar;
    TextView title;
    EditText code_one, code_two, code_three, code_four, code_five, code_six;
    ProgressBar progress_bar;
    Button done_btn;
    SharedPreferences pred;
    String otp;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otpLogin")) {
                final String message = intent.getStringExtra("message");

                String numberOnly = message.replaceAll("[^0-9]", "");
                System.out.println("message only ::" + message);
                System.out.println("number only ::" + numberOnly);
                String[] alphabets = numberOnly.split("");
                code_one.setText(alphabets[1]);
                code_two.setText(alphabets[2]);
                code_three.setText(alphabets[3]);
                code_four.setText(alphabets[4]);
                code_five.setText(alphabets[5]);
                code_six.setText(alphabets[6]);

            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_login);

        //initialize variable
        initVariable();


        //set the title
        title.setText("Authentication process");

        if (checkAndRequestPermissions()) {
            setOnClickListener();
        } else {
            setOnClickListener();
        }

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).
                registerReceiver(receiver, new IntentFilter("otpLogin"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void setOnClickListener() {


        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifyOTP();


            }
        });
    }

    private void verifyOTP() {

        otp = code_one.getText().toString() +
                code_two.getText().toString() +
                code_three.getText().toString() +
                code_four.getText().toString() +
                code_five.getText().toString() +
                code_six.getText().toString();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        //if text contains at symbole pass as email id else pass mobile no

        Call<LogIn> call = stationClient.createOtpLogin(Prefs.getString(AppConstants.UserId, ""), otp);

        System.out.println("call " + call);


        call.enqueue(new Callback<LogIn>() {

            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                hideProgressDialog();
                if (response_code == 200) {
                    Log.d(TAG, "Registration_eqnn" + response.code() + "");
                    List<Profile> check = result.getStatus();
                    Profile check1 = check.get(0);

                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    if (value.equals("1")) {
                        Toast.makeText(OtpLogin.this, "" + message, Toast.LENGTH_SHORT).show();

                        //show toast to change the password
                        Toast.makeText(OtpLogin.this, "You can change the password in my profile", Toast.LENGTH_SHORT).show();


                        String id = "" + check1.getId();
                        SessionManager sessionManager = new SessionManager(OtpLogin.this);
                        sessionManager.setUserId(id);
                        //store the values in the from the login details
                        pred = OtpLogin.this.getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pred.edit();
                        editor.putString("name", check1.getUserName());
                        editor.putString("emailid", check1.getEmailid());
                        editor.putString("mobile", check1.getMobileNumber());
                        editor.commit();
                        Prefs.putString(AppConstants.UserId, id);
                        Prefs.putBoolean(AppConstants.isUserLoggedIn, true);
                        //clear the previous activity in stack and open as new activity
                        Intent intent = new Intent(OtpLogin.this, PagerLayout.class);
                        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(OtpLogin.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } else if (response_code == 400) {
                    Toast.makeText(OtpLogin.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(OtpLogin.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "500 error");
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(OtpLogin.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void initVariable() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        code_one = (EditText) findViewById(R.id.code_one);
        code_two = (EditText) findViewById(R.id.code_two);
        code_three = (EditText) findViewById(R.id.code_three);
        code_four = (EditText) findViewById(R.id.code_four);
        code_five = (EditText) findViewById(R.id.code_five);
        code_six = (EditText) findViewById(R.id.code_six);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        done_btn = (Button) findViewById(R.id.done_btn);
    }
}
