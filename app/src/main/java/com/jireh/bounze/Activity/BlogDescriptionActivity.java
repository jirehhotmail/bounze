package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Adapter.AllreviewsAdapter;
import com.jireh.bounze.Fragment.ShowReviewFragment;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.BlogData;
import com.jireh.bounze.data.BookmarkPojo;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.data.Service_details;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * <h2>BlogDescriptionActivity</h2>
 * This activity is used for providing 1 single BlogData based on what we choosed in our previous page.
 *
 * @author Shubham
 */
public class BlogDescriptionActivity extends AppCompatActivity {
   //public static String BASE_URL = "https://bounze.in/demo/api/";
   // public static String BASE_URL ="https://192.168.1.111/bounze/api/";
    public static String BASE_URL =  "http://ec2-13-126-230-103.ap-south-1.compute.amazonaws.com/api/";
    RecyclerView reviews_recycler;
    int currentPosition;
    //  private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    private ImageView iv_image;
    private TextView tv_blog_title, tv_description;
    int clickcount = 0;
    private boolean bookmarkFlag;
    private ArrayList<BookmarkPojo> bookmarkList;
    private String id, date, title, image, description, rating;
    private SessionManager sessionManager;
    List<Review_> myReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_blog_layout);
        sessionManager = new SessionManager(this);

        //get data from the url else load from the database
        Intent appLinkIntent = getIntent();

        String blogId = appLinkIntent.getStringExtra("blogId");
        if (blogId != null) {
           // String blogId = appLinkData.getLastPathSegment();
            System.out.println("blogId" + blogId);
            BlogData[] blogData = sessionManager.getBlogResult();
            for (int idx = 0; idx < blogData.length; idx++) {
                if (blogData[idx].getBlog_id().equals(blogId)) {
                    id = blogData[idx].getBlog_id();
                    date = blogData[idx].getCreated_date();
                    title = blogData[idx].getBlog_title();
                    image = blogData[idx].getImage_url();
                    description = blogData[idx].getDescription();
                    rating = blogData[idx].getRatings();
                }
            }
        } else {
            //get blog details


            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                id = bundle.getString("id");
                date = bundle.getString("date");
                title = bundle.getString("title");
                image = bundle.getString("image");
                description = bundle.getString("description");
                rating = bundle.getString("rating");
            }
        }
        Log.d("value of pass: ", " passarrr: " + title);
        init();
        tv_blog_title.setText(title);
        tv_description.setText("Description: " + description);

        Picasso.with(this).load(BASE_URL + image).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(iv_image);


        //get the reviews
        setReviews();
    }

    private void setReviews() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getBlogReviews(id);
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    myReview = result.getReviews();

                } else {

                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");


            }
        });
    }

    /**
     * <h3>init</h3>
     * This method is used for initialising all of the views and other variables.
     */
    private void init() {
        iv_image = (ImageView) findViewById(R.id.iv_image);
        tv_blog_title = (TextView) findViewById(R.id.tv_blog_title);
        tv_description = (TextView) findViewById(R.id.tv_description);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        reviews_recycler = (RecyclerView) findViewById(R.id.reviews_recycler);

        reviews_recycler.setLayoutManager(new LinearLayoutManager(BlogDescriptionActivity.this));


        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1B76C9")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back_arrow));
        actionBar.setTitle("Blogs");
        LayoutInflater inflator = LayoutInflater.from(this);
        View v = inflator.inflate(R.layout.titleview, null);

//if you need to customize anything else about the text, do it here.
//I'm using a custom TextView with a custom font in my layout xml so all I need to do is set title
        ((TextView) v.findViewById(R.id.title)).setText(this.getTitle());

//assign the view to the actionbar
        actionBar.setCustomView(v);

        checkBookmarked();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        /*case R.id.bookmark:
            bookmarkList = sessionManager.getBookmarkBlog();
            bookmarkFlag = true;
            for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                if (bookmarkList.get(tempPos).getPassId().equals(id)) {
                    bookmarkFlag = false;
                    break;
                }
            }
            if (bookmarkFlag) {
                BookmarkPojo pojo = new BookmarkPojo();
                pojo.setPassId(id);
                pojo.setPassName(title);
                bookmarkList.add(bookmarkList.size(), pojo);
                sessionManager.setBookmarkBlog(bookmarkList);
//                ll_bookmark.setBackgroundColor(Color.parseColor("#64B5F6"));
                doBookmark("1");
            } else {
                showRemoveBookmarkAlert();
            }
            return(true);*/
            case R.id.rating:
                // TODO Auto-generated method stub
                clickcount = clickcount + 1;
                if (clickcount % 2 == 1) {
                    //SET visibility visible for the recycler review
                    reviews_recycler.setVisibility(View.VISIBLE);
                    //check for my reviews if available show else show no reviews ound
                    if (myReview != null) {   //on single click show the all review and on second click show the rating bar alert
                        AllreviewsAdapter adapter = new AllreviewsAdapter(BlogDescriptionActivity.this, myReview);
                        reviews_recycler.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "No reviews found", Toast.LENGTH_LONG).show();
                    }
                    //first time clicked to do this
                    Toast.makeText(getApplicationContext(), "Click again to write a review", Toast.LENGTH_LONG).show();
                } else {
                    //show the rating alert in android  and close the recyclerview in android
                    reviews_recycler.setVisibility(View.GONE);
                    showRatingAlert();
                    //check how many times clicked and so on
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.share:
                //get the event description if length >40 show it else directly show it

                String desc = description;
                if (description.length() > 40) {
                    desc = description.substring(0,40);
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, title + "\n" + desc + "..."
                        +"\n\n"+"https://www.bounze.in/blog.php?blogid="+ id
                        + "\n\n" + "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n" + "https://play.google.com/store/apps/details?id=com.triton.bounze");

                //  shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/blogdescription/" + id);
                startActivity(Intent.createChooser(shareIntent, "Share using "));
                return (true);

        }
        return (super.onOptionsItemSelected(item));
    }

    /**
     * <h3>checkBookmarked</h3>
     * This method is used to check that the current pass is bookmarked or not.
     */
    private void checkBookmarked() {
        bookmarkList = sessionManager.getBookmarkBlog();
        bookmarkFlag = true;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (bookmarkList.get(tempPos).getBlogId().equals(id)) {
                bookmarkFlag = false;
                break;
            }
        }
        /*if (bookmarkFlag)       //Not yet bookmarked.
        {
            ll_bookmark.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else                   //Already bookmarked.
        {
            ll_bookmark.setBackgroundColor(Color.parseColor("#64B5F6"));
        }*/
    }


    private void showRatingAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.rate_bar_dailog, null);
        final RatingBar ratebar = (RatingBar) mView.findViewById(R.id.ratingBar);
        final EditText comments_edt = (EditText) mView.findViewById(R.id.comments_edt);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        tv_title.setText("Blog rating");
        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();
        //set the rating for the blog
        ratebar.setRating(Float.valueOf(rating));
        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                doRating("" + ratebar.getRating(), comments_edt.getText().toString());

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void doRating(String rating, String reviews) {
        if (isInternetOn()) {
            showProgressDialog();
            ratingBookmarkService(id, sessionManager.getUserId(), reviews, rating, "rating");
        } else {
            Toast.makeText(BlogDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void ratingBookmarkService(String blog_id, String userId, String reviews, String rating_bookmark, String flag) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<ResponsePojo> call;
        if (flag.equals("rating"))
            call = stationClient.createBlogRating(userId, blog_id, reviews, rating_bookmark);
        else
            call = stationClient.createBlogBookmark(userId, blog_id, rating_bookmark);
        call.enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                int response_code = response.code();
                ResponsePojo result = response.body();
                if (response_code == 200) {
                    if (result.getStatus()[0].getStatus().equals("1")) {
                        Toast.makeText(BlogDescriptionActivity.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(BlogDescriptionActivity.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Toast.makeText(BlogDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
