package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Adapter.AllreviewsAdapter;
import com.jireh.bounze.Fragment.SinglePassFragment;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.BookmarkPojo;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassBookingPojo;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.data.Service_details;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * <h2>PassDescriptionActivity</h2>
 * This activity is used for providing 1 single pass based on what we choosed in our previous page.
 *
 * @author Shubham
 */
public class PassDescriptionActivity extends AppCompatActivity implements View.OnClickListener {


    Service_details[] serviceDetails;
    LinearLayout mTabsLinearLayout;
    int currentPosition;
    //   private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    RecyclerView reviews_recycler;
    private SliderLayout sl_image;
    private ViewPager viewPager;
    private PagerSlidingTabStrip tabLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private ImageView back_img, bookmark_img;
    private TextView title_txt;
    private PassArr singlePassData;
    private SessionManager sessionManager;
    private LinearLayout ll_rating, ll_call, ll_information, ll_bookmark, ll_share, ll_back;
    private RatingBar ratingbar;
    private boolean bookmarkFlag;
    private ArrayList<BookmarkPojo> bookmarkList;
    List<Review_> myReview;
    int clickcount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pass);

        sessionManager = new SessionManager(PassDescriptionActivity.this);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        //get data from the url else load from the database
        Intent appLinkIntent = getIntent();

        String passId = appLinkIntent.getStringExtra("passId");
        if (passId != null) {

            //get all the pass result
            PassArr[] results = sessionManager.getPass_Result();
            //loop with the result and if the url pass id is equals to list result pass id then get the data of it
            for (int idx = 0; idx < results.length; idx++) {
                if (results[idx].getPass_id().equals(passId)) {
                    singlePassData = results[idx];
                    serviceDetails = singlePassData.getService_details();
                }
            }

        } else {
            //get pass details
            singlePassData = sessionManager.getSinglePass();
            serviceDetails = singlePassData.getService_details();
            Log.d("value of pass: ", " passarrr: " + singlePassData.getPass_name());
        }


        init();
        //get the user reviews for the pass
        setReviews();
        title_txt.setText(singlePassData.getPass_name());

        setupViewPager(viewPager);
        setImageScroll();
        setUpTabText();
    }

    private void setReviews() {

        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getPassReviews(singlePassData.getPass_id());
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    myReview = result.getReviews();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }

    /**
     * <h3>init</h3>
     * This method is used for initialising all of the views and other variables.
     */
    private void init() {
        sl_image = (SliderLayout) findViewById(R.id.sl_image);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (PagerSlidingTabStrip) findViewById(R.id.tabLayout);
        back_img = (ImageView) findViewById(R.id.back_img);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        title_txt = (TextView) findViewById(R.id.title_txt);


        ll_rating = (LinearLayout) findViewById(R.id.ll_rating);
        ll_call = (LinearLayout) findViewById(R.id.ll_call);
        ll_information = (LinearLayout) findViewById(R.id.ll_information);
        ll_bookmark = (LinearLayout) findViewById(R.id.ll_bookmark);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        bookmark_img = (ImageView) findViewById(R.id.bookmark_img);
        reviews_recycler = (RecyclerView) findViewById(R.id.reviews_recycler);

        LinearLayoutManager rlayoutManager = new LinearLayoutManager(PassDescriptionActivity.this, LinearLayout.HORIZONTAL, false);
        reviews_recycler.setLayoutManager(rlayoutManager);
        ll_back.setOnClickListener(this);
        ll_rating.setOnClickListener(this);
        ll_call.setOnClickListener(this);
        ll_information.setOnClickListener(this);
        ll_bookmark.setOnClickListener(this);
        ll_share.setOnClickListener(this);
        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabLayout.setTypeface(typeFace, 0);
        checkBookmarked();
    }

    /**
     * <h3>checkBookmarked</h3>
     * This method is used to check that the current pass is bookmarked or not.
     */
    private void checkBookmarked() {
        bookmarkList = sessionManager.getBookmarkPass();
        bookmarkFlag = true;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (bookmarkList.get(tempPos).getPassId().equals(singlePassData.getPass_id())) {
                bookmarkFlag = false;
                break;
            }
        }
        if (bookmarkFlag)       //Not yet bookmarked.
        {
            bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else                   //Already bookmarked.
        {
            bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                finish();
                break;


            case R.id.ll_rating:

                // TODO Auto-generated method stub
                clickcount = clickcount + 1;
                if (clickcount % 2 == 1) {
                    //SET visibility visible for the recycler review
                    reviews_recycler.setVisibility(View.VISIBLE);
                    //check for my reviews if available show else show no reviews ound
                    if (myReview != null) {   //on single click show the all review and on second click show the rating bar alert
                        AllreviewsAdapter adapter = new AllreviewsAdapter(PassDescriptionActivity.this, myReview);
                        reviews_recycler.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "No reviews found", Toast.LENGTH_LONG).show();
                    }
                    //first time clicked to do this
                    Toast.makeText(getApplicationContext(), "Click again to write a review", Toast.LENGTH_LONG).show();
                } else {
                    //show the rating alert in android  and close the recyclerview in android
                    reviews_recycler.setVisibility(View.GONE);
                    showRatingAlert();
                    //check how many times clicked and so on
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();
                }


            /*    //show the dialog with review and all reviews for the shop
                android.app.FragmentManager fm = getFragmentManager();
                ShowReviewFragment tv = new ShowReviewFragment(myReview, null, null, null, singlePassData, null);

                tv.show(fm, "TV_tag");*/
                // showRatingAlert();
                break;
            case R.id.ll_call:
                //show the alert dialog  for the confirmation
                showConfirmationDialog();
                break;

            case R.id.ll_information:
                showInformationAlert();
                break;

            case R.id.ll_share:
                //get the event description if length >40 show it else directly show it
                int desc_length = singlePassData.getDescription().length();
                String desc = singlePassData.getDescription();
                if (desc_length > 40) {
                    desc = singlePassData.getDescription().substring(0,40);
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, singlePassData.getPass_name() + "\n" + singlePassData.getLocation().trim() + "\n" + singlePassData.getService_details()[0].getService_name() + "\n" + desc + "..."
                        +"\n\n"+"https://www.bounze.in/pass_programs?pass_id="+ singlePassData.getPass_id()
                        + "\n\n" + "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n" + "https://play.google.com/store/apps/details?id=com.triton.bounze");
                //shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/passdescription/" + singlePassData.getPass_id());
                startActivity(Intent.createChooser(shareIntent, "Share Using "));
                break;

            case R.id.ll_bookmark:
                bookmarkList = sessionManager.getBookmarkPass();
                bookmarkFlag = true;
                for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                    if (bookmarkList.get(tempPos).getPassId().equals(singlePassData.getPass_id())) {
                        bookmarkFlag = false;
                        break;
                    }
                }
                if (bookmarkFlag) {
                    BookmarkPojo pojo = new BookmarkPojo();
                    pojo.setPassId(singlePassData.getPass_id());
                    pojo.setPassName(singlePassData.getPass_name());
                    bookmarkList.add(bookmarkList.size(), pojo);
                    sessionManager.setBookmarkPass(bookmarkList);
                    bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
                    doBookmark("1");
                } else {
                    showRemoveBookmarkAlert();
                }
                break;
        }
    }

    private void showConfirmationDialog() {


        AlertDialog.Builder mBuild = new AlertDialog.Builder(PassDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Call confirmation");
        tv_msg.setText("Are you sure, you want to call?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(PassDescriptionActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PassDescriptionActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                10);
                    }
                } else {

                    if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                }
                try {
                    callIntent.setData(Uri.parse("tel:" + "08026725115"));
                    startActivity(callIntent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    /**
     * <h3>bookPass</h3>
     * This method is used for booking the current pass.
     *
     * @param pass_id        contains the pass id.
     * @param userId         contains the user id.
     * @param booking_status booking status for our server maintenance.
     * @param cost           actual price of the pass.
     * @param payment_mode   cash/card.
     * @param payment_status payment successful or not.
     */
    private void bookPass(String pass_id, String userId, String booking_status, String cost, String payment_mode, String payment_status) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassBookingPojo> call = stationClient.bookPass(userId, pass_id, booking_status, cost, payment_mode, payment_status);

        call.enqueue(new Callback<PassBookingPojo>() {
            @Override
            public void onResponse(Call<PassBookingPojo> call, Response<PassBookingPojo> response) {
                int response_code = response.code();
                PassBookingPojo result = response.body();
                if (response_code == 200) {
                    if (result.getBooking()[0].getStatus() == 1) {
                        Toast.makeText(PassDescriptionActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    } else if (result.getBooking()[0].getStatus() == 0) {
                        Toast.makeText(PassDescriptionActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(PassDescriptionActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<PassBookingPojo> call, Throwable t) {
                Toast.makeText(PassDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }

    private void showRatingAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(PassDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.rate_bar_dailog, null);
        final RatingBar ratebar = (RatingBar) mView.findViewById(R.id.ratingBar);
        final EditText comments_edt = (EditText) mView.findViewById(R.id.comments_edt);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        //set the default ratings for the rating bar
        ratebar.setRating(Float.valueOf(singlePassData.getRatings()));
        mBuild.setView(mView);

        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                doRating("" + ratebar.getRating(), comments_edt.getText().toString());

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void doRating(String rating, String reviews) {
        if (isInternetOn()) {
            showProgressDialog();
            ratingBookmarkService(singlePassData.getPass_id(), sessionManager.getUserId(), reviews, rating, "rating");
        } else {
            Toast.makeText(PassDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * <h3>doBooking</h3>
     * This method just check internet connected or not, if it is connected so make a service call or else show
     * message that internet is not working.
     */
    private void doBooking() {
        if (isInternetOn()) {
            showProgressDialog();
            bookPass(singlePassData.getPass_id(), sessionManager.getUserId(), "1", singlePassData.getPrice(), "1", "1");
        } else {
            Toast.makeText(PassDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void doBookmark(String bookmark) {
        if (isInternetOn()) {
            showProgressDialog();
            ratingBookmarkService(singlePassData.getPass_id(), sessionManager.getUserId(), "", bookmark, "bookmark");
        } else {
            Toast.makeText(PassDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void ratingBookmarkService(String pass_id, String userId, String reviews, String rating_bookmark, String flag) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<ResponsePojo> call;
        if (flag.equals("rating"))
            call = stationClient.createRating(userId, pass_id, reviews, rating_bookmark);
        else
            call = stationClient.createBookmark(userId, pass_id, rating_bookmark);
        call.enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                int response_code = response.code();
                ResponsePojo result = response.body();
                if (response_code == 200) {
                    if (result.getStatus()[0].getStatus().equals("1")) {
                        Toast.makeText(PassDescriptionActivity.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    } else {
                        Toast.makeText(PassDescriptionActivity.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(PassDescriptionActivity.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Toast.makeText(PassDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void showInformationAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(PassDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setVisibility(View.GONE);
        bt_cancel.setText("OK");
        tv_msg.setText(singlePassData.getDescription());

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void showRemoveBookmarkAlert() {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(PassDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Bookmark");
        tv_msg.setText("Are you sure, you want to remove the Bookmark?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeBookmark();
                dialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void removeBookmark() {
        BookmarkPojo pojo = new BookmarkPojo();
        pojo.setPassId(singlePassData.getPass_id());
        pojo.setPassName(singlePassData.getPass_name());
        int removePos = 0;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (singlePassData.getPass_id().equals(bookmarkList.get(tempPos).getPassId())) {
                removePos = tempPos;
            }
        }
        bookmarkList.remove(removePos);
        sessionManager.setBookmarkPass(bookmarkList);
        bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
        doBookmark("2");
    }

    /**
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int tempPos = 0; tempPos < serviceDetails.length; tempPos++) {
            SinglePassFragment fragment = new SinglePassFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("Service_Detail", serviceDetails[tempPos]);
            fragment.setArguments(bundle);
//            viewPagerAdapter.addFrag(new SinglePassFragment(), serviceDetails[tempPos]);
            viewPagerAdapter.addFrag(fragment, serviceDetails[tempPos]);
        }
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setViewPager(viewPager);
    }

    /**
     * This method is used for setting the title on the Tabs.
     */
    private void setUpTabText() {
      /*  for (int tempPos=0; tempPos<serviceDetails.length; tempPos++)
        {
            TextView tv_title = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tv_title.setText(serviceDetails[tempPos].getSubservice_name());
            tabLayout.getTabAt(tempPos).setCustomView(tv_title);
        }*/


        mTabsLinearLayout = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);

            if (i == currentPosition) {
                tv.setTextColor(getResources().getColor(R.color.black));
            } else {
                tv.setTextColor(getResources().getColor(R.color.white));
            }
        }


        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    if (i == position) {
                        tv.setTextColor(getResources().getColor(R.color.black));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * <h3>setImageScroll</h3>
     * This method is used for setting the image that will be scrollable, and came from server.
     */
    private void setImageScroll() {
        String[] imageArr = singlePassData.getSlider_images();
        for (int tempPos = 0; tempPos < imageArr.length; tempPos++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(this);
            defaultSliderView
                    .image(BASE_URL + imageArr[tempPos])
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            defaultSliderView.bundle(new Bundle());
            sl_image.addSlider(defaultSliderView);
        }
        sl_image.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sl_image.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sl_image.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        sl_image.setDuration(4000);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
           /*  SparseArray<WeakReference<MyFragment>> mFragments = new SparseArray<WeakReference<MyFragment>>(3);
            myMemberSparseArrayObject.put(position, new WeakReference<MyFragment> (f))*/
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, Service_details service_details) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(service_details.getSubservice_name());
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
