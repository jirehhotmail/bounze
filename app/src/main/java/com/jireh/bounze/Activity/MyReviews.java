package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Adapter.MyReviewsAdapter;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 11-11-2017.
 */

public class MyReviews extends Fragment {
    RecyclerView my_review_recycler;
    TextView no_reviews;
    ProgressBar progress_bar;
    MyReviewsAdapter adapter;
    String currentDate;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_reviews, container, false);
        initVariable(view);

        if (isInternetOn()) {
            //get the current time from server
            gettheCurrentTimeFromServer();

        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void gettheCurrentTimeFromServer() {
        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.getCurrentTime();
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(final Call<LogIn> call, Response<LogIn> response) {

                LogIn result = response.body();

                currentDate = result.getStatus().get(0).getMessage();
               getReviewDetails();


            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }

    private void initVariable(View view) {
        my_review_recycler = (RecyclerView) view.findViewById(R.id.my_review_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        my_review_recycler.setLayoutManager(manager);
        no_reviews = (TextView) view.findViewById(R.id.no_reviews_text);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);

    }

    private boolean isInternetOn() {
        ConnectivityManager connectivity = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivity.getActiveNetworkInfo();
        return info != null & info.isConnected();
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }


    private void getReviewDetails() {
        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getMyReviewDetails(Prefs.getString(AppConstants.UserId, ""));
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    result.getReviews().get(0).getStatus();
                    List<Review_> myReview = result.getReviews();

                    //if the review has values then set the adapter
                    if (myReview.get(0).getCategory() != null) {
                        //Set visibility for the recycler and gone for no reviews
                        no_reviews.setVisibility(View.GONE);
                        my_review_recycler.setVisibility(View.VISIBLE);

                     //convert current date to date format and send to adapter
                        Date date1 = null;
                        try {
                            //convert gmt to ist time
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));  // set this if your timezone is different

                            date1 = sdf.parse(currentDate); // now time is Sun Jan 25 00:00:00 GMT 2015
                            sdf.setTimeZone(TimeZone.getTimeZone("IST"));
                            Log.d("date", sdf.format(date1)); // now time is Sun Jan 25 05:30:00 IST 2015

                            System.out.println("check..." + sdf.format(date1));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //set Adapter
                        adapter = new MyReviewsAdapter(getActivity(), myReview, date1, new MyReviewsAdapter.onItemClickListener() {
                            @Override
                            public void onItemClick(int view, int position) {
                                //call the server again when the ok button is clicked and set the adapter
                                getReviewDetails();
                            }
                        });
                        hideProgressDialog();
                        my_review_recycler.setAdapter(adapter);
                    } else {
                        //Set visibility for the no review  and gone for my recycler
                        no_reviews.setVisibility(View.VISIBLE);
                        my_review_recycler.setVisibility(View.GONE);
                        hideProgressDialog();
                    }

                }

            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }


}
