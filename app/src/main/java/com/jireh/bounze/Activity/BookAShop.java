package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Sessions;
import com.jireh.bounze.data.ShopSerializable;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 29-09-2017.
 */

public class BookAShop extends Activity {
    static final int STIME_DIALOG_ID = 999;
    static final int ETIME_DIALOG_ID = 111;
    TextView selected_date_txt, title_txt, selected_stime_txt, selected_etime_txt;
    Toolbar toolbar;
    Button book_btn;
    ShopSerializable sssDetails;
    String batchTime;
    MaterialCalendarView widget;
    Calendar calendar_onselect = null;
    Date date1 = null;
    String flag = "", selected_time = "", start_date = "", end_date = "";
    //   Spinner batch_time_spinner;
    Sessions sessionDetails;
    // int selected_position = 0;
    ImageView back_img;
    ArrayList<String> dates = new ArrayList<>();
    String selectedDate = "", selectedDateWithYear = "", selectedMinutes = "", selectedHours = "";
    ProgressBar progress_bar;
    Calendar instance1;
    //  TimePicker timePicker1;
    private int hour;
    private int minutes;
    private Date dateCompareOne;
    private Date dateCompareTwo;
    private Date dateSpaOne;
    private Date dateSpaTwo;
    private TimePickerDialog.OnTimeSetListener stimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    hour = hourOfDay;
                    minutes = minute;


                    String time = "" + hour + ":" + minutes;
                    //convert time from 24hrs to 12 hrs format


                    //get the selected time and add the no of hours/minutes to the selected time
                    String inputPattern = "K:mm a";

                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);


                    Date date = null;
                    try {
                        date = inputFormat.parse(ConvertTimeFormat(time));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);


                    //if the selected type is free trial or pay per use add one hour
                    if (flag.equalsIgnoreCase("Free Trial") || flag.equalsIgnoreCase("Pay per use")) {
                        calendar.add(Calendar.HOUR, 1);
                        //after adding get time convert to 12hrs format and set to the end time
                        SimpleDateFormat format1 = new SimpleDateFormat("K:mm a");

                        String formatted = format1.format(calendar.getTime());

                        //if shop timings is not empty then get the shop timings
                        if (!sssDetails.getShopTimings().equals("")) {

                            //check if the time is after shop time and befor shop end time
                            String[] shopTimings = sssDetails.getShopTimings().split(" to ");

                            dateCompareOne = parseDate(shopTimings[0]);
                            dateCompareTwo = parseDate(shopTimings[1]);

                            if ((dateCompareOne.before(date) || dateCompareOne.equals(date)) &&
                                    (dateCompareTwo.after(date) || dateCompareTwo.equals(date))) {
                                // set current time into textview and add one hour and set the end time else show toast message
                                //such that user select another time
                                selected_stime_txt.setText(ConvertTimeFormat(time));
                                selected_etime_txt.setText(formatted);
                            } else {
                                Toast.makeText(BookAShop.this, "Shop timings is between" + sssDetails.getShopTimings(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    //if the session is minutes add minutes
                    if (!selectedMinutes.equals("")) {


                        calendar.add(Calendar.MINUTE, Integer.parseInt(selectedMinutes));
                        //after adding get time convert to 12hrs format and set to the end time
                        SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");


                        String formatted_etime = format1.format(calendar.getTime());

                        //if shop timings is not empty then get the shop timings
                        if (!sssDetails.getShopTimings().equals("")) {

                            //check if the time is after shop time and before shop end time
                            String[] shopTimings = sssDetails.getShopTimings().split(" to ");

                            dateSpaOne = parseDate(shopTimings[0]);
                            dateSpaTwo = parseDate(shopTimings[1]);

                            if ((dateSpaOne.before(date) || dateSpaOne.equals(date)) &&
                                    (dateSpaTwo.after(date) || dateSpaTwo.equals(date))) {
                                // set current time into textview and add one hour and set the end time else show toast message
                                //such that user select another time
                                selected_stime_txt.setText(ConvertTimeFormat(time));
                                selected_etime_txt.setText(formatted_etime);
                            } else {
                                Toast.makeText(BookAShop.this, "Shop timings is between" + sssDetails.getShopTimings(), Toast.LENGTH_SHORT).show();
                            }
                        }


                    }
                    //if the session is hours add hours
                    if (!selectedHours.equals("")) {
                        // set current time into textview
                        selected_stime_txt.setText(ConvertTimeFormat(time));

                        calendar.add(Calendar.HOUR, Integer.parseInt(selectedHours));
                        //after adding get time convert to 12hrs format and set to the end time
                        SimpleDateFormat format1 = new SimpleDateFormat("K:mm a");


                        String formatted_etime = format1.format(calendar.getTime());

                        //if shop timings is not empty then get the shop timings
                        if (!sssDetails.getShopTimings().equals("")) {

                            //check if the time is after shop time and before shop end time
                            String[] shopTimings = sssDetails.getShopTimings().split(" to ");

                            dateSpaOne = parseDate(shopTimings[0]);
                            dateSpaTwo = parseDate(shopTimings[1]);

                            if ((dateSpaOne.before(date) || dateSpaOne.equals(date)) &&
                                    (dateSpaTwo.after(date) || dateSpaTwo.equals(date))) {
                                // set current time into textview and add one hour and set the end time else show toast message
                                //such that user select another time
                                selected_stime_txt.setText(ConvertTimeFormat(time));
                                selected_etime_txt.setText(formatted_etime);
                            } else {
                                Toast.makeText(BookAShop.this, "Shop timings is between" + sssDetails.getShopTimings(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }


                }


            };
    private TimePickerDialog.OnTimeSetListener etimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    hour = hourOfDay;
                    minutes = minute;

                    String time = "" + hour + ":" + minutes;
                    //convert time from 24hrs to 12 hrs format

                    // set current time into textview
                    selected_etime_txt.setText(ConvertTimeFormat(time));


                }


            };

    private Date parseDate(String date) {
        //after adding get time convert to 12hrs format and set to the end time
        SimpleDateFormat format1 = new SimpleDateFormat("K:mm a");
        try {
            return format1.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private String ConvertTimeFormat(String time) {
        Date dateObj = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            dateObj = sdf.parse(time);


        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("K:mm a").format(dateObj);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_a_shop);
        initVariable();
        //set clickable false for date and time before get current time

        selected_date_txt.setEnabled(false);
        selected_stime_txt.setEnabled(false);
        selected_etime_txt.setEnabled(false);
        //get the shop details
        try {
            //get the shop sub service details

            sssDetails = (ShopSerializable) getIntent().getSerializableExtra("ShopSubServices");

            //get the session details to book


            //get the batch details for the subservice of a shop
            sessionDetails = (Sessions) getIntent().getSerializableExtra("sessionDetails");

            //get the batch selected time
            batchTime = getIntent().getStringExtra("batchTime");
            flag = getIntent().getStringExtra("ShopFlag");
            // set text for title
            title_txt.setText("Book " + flag);


        } catch (Exception e) {
            e.printStackTrace();
        }
        setOnClickListener();


        //get the current time from server
        //check if the internet is there or not
        if (isInternetOn()) {
            gettheCurrentTimeFromServer();
        } else {
            Toast.makeText(BookAShop.this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
//set the selected time in the shop description page if the select type is session else select time between shop
// start and end time
        if (flag.equalsIgnoreCase("Session")) {
            //if the selected Batch time is not null and not empty then display the selected batch time
            //else make it as it is and set onclick listener for the start time
            if (batchTime != null && !batchTime.equals("")) {

                selected_stime_txt.setText(batchTime);
                selected_etime_txt.setText("");

            } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Day") ||
                    sessionDetails.getDaysUnit().equalsIgnoreCase("Days")) {
                // if session having days then set the default shop time else give the option to select time for user
                selected_stime_txt.setText(sssDetails.getShopTimings());

                selected_etime_txt.setText("");

            } else {
                //add listener to textview and make the endtime clickable false
                addListenerOnTV();
                selected_etime_txt.setClickable(false);

            }


        } else {
            // if session not having hours then if it is free trial or pay per use enable the start time
            // and disable the end time else show the shop timings

            //if the selected type is free trial or pay per use add one hour
            if (flag.equalsIgnoreCase("Free Trial") || flag.equalsIgnoreCase("Pay per use")) {
                //add listener to textview
                addListenerOnTV();
                selected_etime_txt.setClickable(false);
            }
            //if the shop timings is empty then add listener for the start time and end time
            else if (!sssDetails.getShopTimings().equals("")) {
                selected_stime_txt.setText(sssDetails.getShopTimings());
                selected_etime_txt.setText("");
            } else {
                //add listener to textview
                addListenerOnTV();
            }

        }


    }

    private void addListenerOnTV() {

        //set on click listener to show the time picker dialog to set the start time
        selected_stime_txt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(STIME_DIALOG_ID);

            }


        });

        selected_etime_txt
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        showDialog(ETIME_DIALOG_ID);
                    }


                });
    }

    @Override

    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case STIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(BookAShop.this, stimePickerListener, hour, minutes, false);
            case ETIME_DIALOG_ID:

                // set time picker as current time

                return new TimePickerDialog(BookAShop.this, etimePickerListener, hour, minutes, false);
        }

        return null;

    }


    private void gettheCurrentTimeFromServer() {
        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.getCurrentTime();
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(final Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                if (response_code == 200) {
                    //set clickable true for date and time before get current time

                    selected_date_txt.setEnabled(true);
                    selected_stime_txt.setEnabled(true);
                    selected_etime_txt.setEnabled(true);

                    hideProgressDialog();

                    //convert string to date
                    String dtStart = result.getStatus().get(0).getMessage().substring(0, 10);

                    try {
                        //convert gmt to ist time
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));  // set this if your timezone is different

                        date1 = sdf.parse(dtStart); // now time is Sun Jan 25 00:00:00 GMT 2015
                        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
                        Log.d("date", sdf.format(date1)); // now time is Sun Jan 25 05:30:00 IST 2015

                        System.out.println("check..." + sdf.format(date1));


                        //initialize single selection for the calendar for free trial
                        if (!flag.equals("") && flag != null) {
                            //set the current date from server to the calendar which is used for all selections
                            final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                            instance1 = Calendar.getInstance();
                            try {
                                instance1.setTime(df.parse(df.format(date1)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            instance1.set(instance1.get(Calendar.YEAR), instance1.get(Calendar.MONTH), instance1.get(Calendar.DAY_OF_MONTH));
                            instance1.setTime(instance1.getTime());
                            //set the selection color for the date
                            widget.setSelectionColor(getResources().getColor(R.color.toolbar_color));
                            if (flag.equalsIgnoreCase("Free Trial")) {

                                widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);

                                //set the start month for the calendar and set the start as today's date and disable previous dates
                                //set the end date as next 7 days


                                Calendar instance2 = Calendar.getInstance();

                                instance2.set(instance1.get(Calendar.YEAR), instance1.get(Calendar.MONTH), instance1.get(Calendar.DAY_OF_MONTH));
                                instance2.add(Calendar.DATE, 6);
                                instance2.setTime(instance2.getTime());

                                int showOtherDates = widget.getShowOtherDates();
                                MaterialCalendarView.showOutOfRange(showOtherDates);
                                showOtherDates |= MaterialCalendarView.SHOW_OUT_OF_RANGE;
                                widget.setShowOtherDates(showOtherDates);
                                widget.state().edit()
                                        .setFirstDayOfWeek(Calendar.SUNDAY)
                                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                                        .setMinimumDate(instance1.getTime())
                                        .setMaximumDate(instance2.getTime())
                                        .commit();
                            }
                            //set the calendar mode to multiple for the pay per use one month any date from today
                            else if (flag.equalsIgnoreCase("Pay per use")) {
                                widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
                                //set the start month for the calendar and set the start as today's date and disable previous dates
                                //set the end date as next 1 month
                                Calendar instance2 = Calendar.getInstance();
                                instance2.set(instance1.get(Calendar.YEAR), instance1.get(Calendar.MONTH), instance1.get(Calendar.DAY_OF_MONTH));
                                instance2.add(Calendar.MONTH, 1);
                                instance2.setTime(instance2.getTime());

                                int showOtherDates = widget.getShowOtherDates();
                                MaterialCalendarView.showOutOfRange(showOtherDates);
                                showOtherDates |= MaterialCalendarView.SHOW_OUT_OF_RANGE;
                                widget.setShowOtherDates(showOtherDates);
                                widget.state().edit()
                                        .setFirstDayOfWeek(Calendar.SUNDAY)
                                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                                        .setMinimumDate(instance1.getTime())
                                        .setMaximumDate(instance2.getTime())
                                        .commit();
                            } else if (flag.equalsIgnoreCase("Session")) {
                                //set the minimum date as current date to calendar
                                widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
                                //set the start month for the calendar and set the start as today's date and disable previous dates
                                int showOtherDates = widget.getShowOtherDates();
                                MaterialCalendarView.showOutOfRange(showOtherDates);
                                showOtherDates |= MaterialCalendarView.SHOW_OUT_OF_RANGE;
                                widget.setShowOtherDates(showOtherDates);
                                widget.state().edit()
                                        .setFirstDayOfWeek(Calendar.SUNDAY)
                                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                                        .setMinimumDate(instance1.getTime())
                                        .commit();

                            }
                        }
                        // widget.setSelectedDate(date1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    //get the selected dates
                    widget.setOnDateChangedListener(new OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {


                            calendar_onselect = date.getCalendar();
                            //get the selected date as the start date
                            Date startDate = date.getCalendar().getTime();
                            // Create a DateFormatter object for displaying date in specified format.
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            //assign the start date to a variable
                            start_date = formatter.format(startDate.getTime());

                            //based on user selection select the dates
                            //if the sessions not equal to null get the no of days and select the date based on user selection
                            if (!flag.equals("") && flag != null) {
                                if (flag.equalsIgnoreCase("Free Trial")) {
                                    //if flag is free trial then don't show the end date
                                    //select only one day for the free trial in next 7 days and display the date
                                    widget.setDateSelected(calendar_onselect, true);
                                    selected_date_txt.setText(start_date);

                                }
                                //select multiple days  and display the dates
                                //check if the string already exist replace with empty string else add the string

                                else if (flag.equalsIgnoreCase("Pay per use")) {
                                    //make the session details is null

                                    //Store the selected dates in AL if already exist remove else add it
                                    //if the date or month is in single digit add 0 before the day or month and display in the selected text area
                                    if (dates.contains(String.valueOf(date.getDay())
                                            + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear())) ||
                                            dates.contains("0" + String.valueOf(date.getDay())
                                                    + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear())
                                            ) ||

                                            dates.contains("0" + String.valueOf(date.getDay())
                                                    + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear())
                                            ) ||dates.contains(String.valueOf(date.getDay())
                                            + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()))) {
                                        for (int idx = 0; idx < dates.size(); idx++) {

                                            if ((dates.get(idx).equals(String.valueOf(date.getDay())
                                                    + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()))) ||

                                                    (dates.get(idx).equals(String.valueOf(date.getDay())
                                                            + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()))) ||

                                                    (dates.get(idx).equals("0" + String.valueOf(date.getDay())
                                                            + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear())))
                                                    ||
                                                    (dates.get(idx).equals("0" + String.valueOf(date.getDay())
                                                            + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear())))

                                                    ) {
                                                dates.remove(idx);
                                            }
                                        }

                                    } else {
                                        int day = date.getDay();
                                        int month = date.getMonth() + 1;
                                        boolean isDoubleDigitDay = (day > 9 && day < 100) || (day < -9 && day > -100);
                                        boolean isDoubleDigitMonth = (month > 9 && month < 100) || (month < -9 && month > -100);
                                        //check if the selected day is single or double if double check for month is single or double
                                        //if month is yes leave as it else else add zero
                                        if (isDoubleDigitDay) {

                                            if (isDoubleDigitMonth) {
                                                dates.add(String.valueOf(date.getDay())
                                                        + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()));
                                            } else {
                                                dates.add(String.valueOf(date.getDay())
                                                        + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()));
                                            }
                                        } else {
                                            //if the date is single digit add zero and if month is double leave it as it is else add zero to day and month
                                            if (isDoubleDigitMonth) {
                                                dates.add("0" + String.valueOf(date.getDay())
                                                        + "/" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()));
                                            } else {
                                                dates.add("0" + String.valueOf(date.getDay())
                                                        + "/0" + String.valueOf(date.getMonth() + 1) + "/" + String.valueOf(date.getYear()));
                                            }
                                        }
                                    }
                                    //sort the selected dates in the ascending order
                                    Collections.sort(dates, new Comparator<String>() {
                                        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");

                                        @Override
                                        public int compare(String o1, String o2) {
                                            try {
                                                return f.parse(o1).compareTo(f.parse(o2));
                                            } catch (ParseException e) {
                                                throw new IllegalArgumentException(e);
                                            }
                                        }
                                    });
                                    //display the selected dates by loop the arraylist
                                    selectedDate = "";
                                    selectedDateWithYear = "";

                                    //add the selected dates in string with the year
                                    for (int idx = 0; idx < dates.size(); idx++) {

                                        selectedDate = selectedDate + dates.get(idx).substring(0, 5) + ", ";
                                        selectedDateWithYear = selectedDateWithYear + dates.get(idx) + ", ";
                                    }

                                    selected_date_txt.setText(selectedDate);
                                    //if date size is zero then empty the string
                                    if (dates.size() == 0) {
                                        selected_date_txt.setText("");
                                    }
                                    int height_in_pixels = selected_date_txt.getLineCount() * selected_date_txt.getLineHeight(); //approx height text
                                    selected_date_txt.setHeight(height_in_pixels);


                                }

                                //check if the session details is not null


                                else if (sessionDetails != null) {
                                    //clear the widget selection
                                    widget.clearSelection();
                                    if (sessionDetails.getDaysUnit().equalsIgnoreCase("Min") || sessionDetails.getDaysUnit().equalsIgnoreCase("Mins")) {
                                        //if the selection is in minutes then select selection mode in single and set the selected date
                                        widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                                        widget.setDateSelected(calendar_onselect, true);
                                        selected_date_txt.setText(start_date);
                                        selectedMinutes = sessionDetails.getNoOfDays();
                                        //get the session selected  hours
                                    } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Hour") || sessionDetails.getDaysUnit().equalsIgnoreCase("Hours")) {
                                        //if the selection is in minutes then select selection mode in single and set the selected date
                                        widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                                        widget.setDateSelected(calendar_onselect, true);
                                        selected_date_txt.setText(start_date);
                                        selectedHours = sessionDetails.getNoOfDays();
                                    }
                                    //based on the sessions days or months or year set the calendar selections
                                    if (sessionDetails.getDaysUnit().equalsIgnoreCase("Min") ||
                                            sessionDetails.getDaysUnit().equalsIgnoreCase("Mins")) {
                                        //set the start date and end date as same
                                        selected_date_txt.setText(start_date);

                                    } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Hour")
                                            || sessionDetails.getDaysUnit().equalsIgnoreCase("Hours")) {
                                        //set the start date and end date as same
                                        selected_date_txt.setText(start_date);

                                    } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Day") ||
                                            sessionDetails.getDaysUnit().equalsIgnoreCase("Days")) {
                                        //get the calendar date and add based on no. of days and set the range
                                        calendar_onselect = date.getCalendar();
                                        int no_of_days = Integer.parseInt(sessionDetails.getNoOfDays()) - 1;
                                        calendar_onselect.add(Calendar.DATE, no_of_days);
                                        CalendarDay day = CalendarDay.from(calendar_onselect);
                                        widget.selectRange(date, day);
                                        //using calendar object get time and set the date
                                        calendar_onselect = date.getCalendar();
                                        Date instance2 = calendar_onselect.getTime();
                                        selected_date_txt.setText(start_date + " to " + formatter.format(instance2.getTime()));
                                        //Then subtract the value from the date since date in added to set the current selection point
                                        calendar_onselect.add(Calendar.DATE, -no_of_days);
                                        //if the user selected months then select one month from the select date
                                    } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Month") ||
                                            sessionDetails.getDaysUnit().equalsIgnoreCase("Months")) {
                                        //get the calendar date and add based on no. of days and set the range
                                        calendar_onselect = date.getCalendar();
                                        calendar_onselect.add(Calendar.MONTH, Integer.parseInt(sessionDetails.getNoOfDays()));
                                        CalendarDay day = CalendarDay.from(calendar_onselect);
                                        widget.selectRange(date, day);
                                        //using calendar object get time and set the date
                                        calendar_onselect = date.getCalendar();
                                        Date instance2 = calendar_onselect.getTime();
                                        selected_date_txt.setText(start_date + " to " + formatter.format(instance2.getTime()));
                                        //Then subtract the value from the date since date in added to set the current selection point
                                        calendar_onselect.add(Calendar.MONTH, -Integer.parseInt(sessionDetails.getNoOfDays()));

                                    } else if (sessionDetails.getDaysUnit().equalsIgnoreCase("Year") ||
                                            sessionDetails.getDaysUnit().equalsIgnoreCase("Years")) {

                                        //get the calendar date and add based on no. of days and set the range
                                        calendar_onselect = date.getCalendar();
                                        calendar_onselect.add(Calendar.YEAR, Integer.parseInt(sessionDetails.getNoOfDays()));
                                        CalendarDay day = CalendarDay.from(calendar_onselect);
                                        widget.selectRange(date, day);
                                        //using calendar object get time and set the date
                                        calendar_onselect = date.getCalendar();
                                        Date instance2 = calendar_onselect.getTime();
                                        selected_date_txt.setText(start_date + " to " + formatter.format(instance2.getTime()));
                                        //Then subtract the value from the date since date in added to set the current selection point
                                        calendar_onselect.add(Calendar.YEAR, -Integer.parseInt(sessionDetails.getNoOfDays()));
                                    }
                                }
                            }
                        }
                    });
                } else {
                    hideProgressDialog();
                    Toast.makeText(BookAShop.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }


    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //book the free trial
        book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check the internet connection
                if (isInternetOn()) {
                    //default set to error null for all

                    //validate if the user selected the dtartdate -enddate, start time and end -time
                    System.out.println("select date::" + selected_date_txt.getText().toString());
                    if (selected_date_txt.getText().toString().matches("")) {
                        selected_date_txt.setError("Please select a date");

                    } else {
                        //if the pay per user is checked make the session details as null else get the values
                        //so that if any batch select it will not consider
                        if (flag.equalsIgnoreCase("Pay per use")) {
                            sessionDetails = null;
                        } else {
                            //get the batch details for the subservice of a shop
                            sessionDetails = (Sessions) getIntent().getSerializableExtra("sessionDetails");
                        }

                        //if the batch time is not equal to empty then select the batch time as start time and end time
                        if (batchTime != null && !batchTime.equals("")) {
                            selected_stime_txt.setText(batchTime);
                            selected_etime_txt.setText("");
                            selected_time = selected_stime_txt.getText().toString();
                            //if the session details is in mins or hours then take the values from text else

                        } else if (sessionDetails != null) {
                            if (sessionDetails.getDaysUnit().equalsIgnoreCase("Min") ||
                                    sessionDetails.getDaysUnit().equalsIgnoreCase("Mins")
                                    || sessionDetails.getDaysUnit().equalsIgnoreCase("Hour") ||
                                    sessionDetails.getDaysUnit().equalsIgnoreCase("Hours")) {
                                selected_time = selected_stime_txt.getText().toString() + " to " + selected_etime_txt.getText().toString();
                            }
                        }
                       /* //if the shop timing it not available then get the user time else use the shop time
                        else if (!sssDetails.getShopTimings().equals("")) {
                            selected_stime_txt.setText(sssDetails.getShopTimings());
                            selected_etime_txt.setText("");
                            selected_time = selected_stime_txt.getText().toString();
                        }*/
                        else {
                            selected_time = selected_stime_txt.getText().toString() + " to " + selected_etime_txt.getText().toString();
                        }


                        //get the booking type and as per flag book the shop if free trial make booking without payment

                        if (flag.equalsIgnoreCase("Free Trial")) {
                            if (!selected_time.equals("Start time to End time")) {
                                if (!selected_time.contains("to End time")) {
                                    Intent intent = new Intent(BookAShop.this, Pay.class);
                                    intent.putExtra("PayFlag", "bookAShop");
                                    intent.putExtra("PayType", "FreeShop");
                                    intent.putExtra("shopDetails", (Serializable) sssDetails);
                                    intent.putExtra("selectedTime", selected_time);
                                    intent.putExtra("selectedDates", start_date);

                                    startActivity(intent);

                                } else {
                                    Toast.makeText(BookAShop.this, "Please select the End time", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(BookAShop.this, "Please select the time", Toast.LENGTH_SHORT).show();
                            }
                        } else if (flag.equalsIgnoreCase("Pay per use")) {
                            //Navigate to the payment summary page by sending the user details
                            //pass the date with year in the string format
                            if (!selected_time.equals("Start time to End time")) {
                                if (!selected_time.contains("to End time")) {
                                    Intent intent = new Intent(BookAShop.this, Pay.class);
                                    intent.putExtra("PayFlag", "bookAShop");
                                    intent.putExtra("PayType", "SPPU");
                                    intent.putExtra("shopDetails", (Serializable) sssDetails);
                                    intent.putExtra("selectedTime", selected_time);
                                    intent.putExtra("selectedDates", selected_date_txt.getText().toString());
                                    intent.putExtra("selectedDateWithYear", selectedDateWithYear);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(BookAShop.this, "Please select the End time", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(BookAShop.this, "Please select the time", Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            String session_cost;
                            //book using sessions
                            if (!selected_time.equals("Start time to End time")) {
                                if (!selected_time.contains("to End time")) {
                                    //if the session have the offer send offer cost else send normal cost
                                    if (sessionDetails.getSessionsOfferCost().equals("0")) {
                                        session_cost = sessionDetails.getSessionsCost();
                                    } else {
                                        session_cost = sessionDetails.getSessionsOfferCost();
                                    }
                                    Intent intent = new Intent(BookAShop.this, Pay.class);
                                    intent.putExtra("PayFlag", "bookAShop");
                                    intent.putExtra("PayType", "SSESSION");
                                    intent.putExtra("shopDetails", (Serializable) sssDetails);
                                    intent.putExtra("selectedTime", selected_time);
                                    intent.putExtra("selectedDates", selected_date_txt.getText().toString());
                                    intent.putExtra("sessionDetails", sessionDetails);
                                    intent.putExtra("sessionCost", session_cost);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(BookAShop.this, "Please select the End time", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(BookAShop.this, "Please select the time", Toast.LENGTH_SHORT).show();
                            }
                        }
                       /* } else {
                            Toast.makeText(BookAShop.this, "Please select a time", Toast.LENGTH_SHORT).show();
                        }*/
                    }
                } else {
                    Toast.makeText(BookAShop.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        selected_date_txt = (TextView) findViewById(R.id.selected_date_txt);

        book_btn = (Button) findViewById(R.id.book_btn);
        //batch_time_spinner = (Spinner) findViewById(R.id.batch_time_spinner);
        widget = (MaterialCalendarView) findViewById(R.id.calendarView);

        selected_stime_txt = (TextView) findViewById(R.id.selected_stime_txt);
        selected_etime_txt = (TextView) findViewById(R.id.selected_etime_txt);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        //timePicker1 = (TimePicker) findViewById(R.id.timePicker);
    }


}
