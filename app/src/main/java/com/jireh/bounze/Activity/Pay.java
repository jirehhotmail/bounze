package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.BatchTime;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Booking;
import com.jireh.bounze.data.BookingProcess;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachSerializable;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassBookingPojo;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.data.Reward;
import com.jireh.bounze.data.Sessions;
import com.jireh.bounze.data.SessionsDetail;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.ShopSerializable;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.payu.india.CallBackHandler.OnetapCallback;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Extras.PayUSdkDetails;
import com.payu.india.Interfaces.OneClickPaymentListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.Activity.PayUBaseActivity;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;
import static com.jireh.bounze.Activity.SplashScreenActivity.programrowid;


/**
 * This activity prepares PaymentParams, fetches hashes from server and send it to PayuBaseActivity.java.
 * <p>
 * Implement this activity with OneClickPaymentListener only if you are integrating One Tap payments.
 */
public class Pay extends AppCompatActivity implements OneClickPaymentListener {
    ImageView squareimage;
    TextView title, set_name, location_address_txt, booking_date_txt,
            booking_time_txt, booking_price_txt, booking_type_dtls, promocode;
    Event eventDetails;
    PassArr singlePassData;
    Bookhistory bookingHistory;
    BatchTime eventBatchDetails;
    Toolbar toolbar;
    ShopSerializable sssDetails;
    Sessions sessionDetails;
    SessionsDetail sessionCoachDetails;
    CoachSerializable cssDetails;
    DBFunctions dbObject;
    String batchTime, selectedTime, selectedDates, selectedDateWithYear, sessionCost, payflag, payType;
    ImageView back_img, book_image;
    View btnPayNow;
    int noOfDaysSelected;
    EditText voucher_code;
    TextView cancel_btn, apply_btn, total_points;
    List<Profile> login;
    double promoAmt = 0.0;
    String wallet;
    CheckBox utilize_points_chk;
    ProgressBar progress_bar;
    private double tcost, scost, pcost, pay_cost;
    private String merchantKey, userCredentials, rewardStatus;
    // These will hold all the payment parameters
    private PaymentParams mPaymentParams;
    // This sets the configuration
    private PayuConfig payuConfig;
    private Spinner environmentSpinner;
    // Used when generating hash from SDK
    private PayUChecksum checksum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        //init variable
        initVariable();
        setOnClickListener();
        //to hide the keyboard hide when open this activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        title.setText("Summary");
        payflag = getIntent().getStringExtra("PayFlag");
        payType = getIntent().getStringExtra("PayType");


        //set the book details for shop pay per use or session booking

        if (payflag.equals("bookAShop") && payType.equals("FreeShop")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.shop_image));
            Shop shopDetails = null;
            try {
                //get the extra values for pay per use from the book page
                sssDetails = (ShopSerializable) getIntent().getSerializableExtra("shopDetails");
                selectedTime = getIntent().getStringExtra("selectedTime");
                selectedDates = getIntent().getStringExtra("selectedDates");

                //get the shop details using the shopid
                shopDetails = dbObject.getShopListDetailbyId(sssDetails.getShopId());
                set_name.setText(shopDetails.getTitle());

                Picasso.with(Pay.this).load(BASE_URL + shopDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String strings[] = shopDetails.getFullAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(shopDetails.getFullAddress());
            booking_date_txt.setText(selectedDates);
            booking_time_txt.setText(selectedTime);

            noOfDaysSelected = 1;

            //pay per use is hard coded
            // booking_price_txt.setText(String.valueOf(Integer.valueOf(dates.length-1) * Integer.parseInt(sssDetails.getPayPerUse())));
            booking_price_txt.setText(String.valueOf(0));
            //set the booking type
            booking_type_dtls.setText("Free Trial");

        } else if (payflag.equals("bookAShop") && payType.equals("SPPU")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.shop_image));
            Shop shopDetails = null;
            try {
                //get the extra values for pay per use from the book page
                //get the selected years in string
                sssDetails = (ShopSerializable) getIntent().getSerializableExtra("shopDetails");
                selectedTime = getIntent().getStringExtra("selectedTime");
                selectedDates = getIntent().getStringExtra("selectedDates");
                selectedDateWithYear = getIntent().getStringExtra("selectedDateWithYear");
                //get the shop details using the shopid
                shopDetails = dbObject.getShopListDetailbyId(sssDetails.getShopId());
                set_name.setText(shopDetails.getTitle());

                Picasso.with(Pay.this).load(BASE_URL + shopDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String strings[] = shopDetails.getFullAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(shopDetails.getFullAddress());
            booking_date_txt.setText(selectedDates);
            booking_time_txt.setText(selectedTime);
            String dates[] = getIntent().getStringExtra("selectedDates").split(",");
            noOfDaysSelected = dates.length - 1;
            //pay per use is hard coded
            // booking_price_txt.setText(String.valueOf(Integer.valueOf(dates.length-1) * Integer.parseInt(sssDetails.getPayPerUse())));
            booking_price_txt.setText(String.valueOf(noOfDaysSelected * Integer.parseInt(sssDetails.getPayPerUse())));
            //set the booking type
            booking_type_dtls.setText("Pay per use");
        } else if (payflag.equals("bookAShop") && payType.equals("SSESSION")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.shop_image));
            Shop shopDetails = null;
            try {
                //get the extras details for session from the book a shop page
                sssDetails = (ShopSerializable) getIntent().getSerializableExtra("shopDetails");
                selectedTime = getIntent().getStringExtra("selectedTime");
                selectedDates = getIntent().getStringExtra("selectedDates");
                sessionDetails = (Sessions) getIntent().getSerializableExtra("sessionDetails");
                sessionCost = getIntent().getStringExtra("sessionCost");
                //get the shop details using the shopid
                shopDetails = dbObject.getShopListDetailbyId(sssDetails.getShopId());

                Picasso.with(Pay.this).load(BASE_URL + shopDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            set_name.setText(shopDetails.getTitle());
            String strings[] = shopDetails.getFullAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(shopDetails.getFullAddress());
            booking_date_txt.setText(selectedDates);
            booking_time_txt.setText(selectedTime);

            booking_price_txt.setText(sessionCost);
            //set the booking type
            //if sessions is empty then don't show session else show sesssion
            if (!sessionDetails.getSessions().equals("0")) {
                booking_type_dtls.setText(sessionDetails.getSessions() + " Sessions"
                        + " " + sessionDetails.getNoOfDays() + sessionDetails.getDaysUnit());
            } else {
                booking_type_dtls.setText(sessionDetails.getNoOfDays() + sessionDetails.getDaysUnit());
            }

        } else if (payflag.equals("BookACoach") && payType.equals("FreeCoach")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.coach_img));
            Coach coachDetails = null;
            try {
                cssDetails = (CoachSerializable) getIntent().getSerializableExtra("coachDetails");
                selectedDates = getIntent().getStringExtra("selectedDates");
                selectedTime = getIntent().getStringExtra("selectedTime");
                //get the shop details using the shopid
                coachDetails = dbObject.getCoachDetailsbyId(cssDetails.getCoachId());

                Picasso.with(getApplicationContext()).load(BASE_URL + coachDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            set_name.setText(coachDetails.getTitle());
            String strings[] = coachDetails.getFullAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(coachDetails.getFullAddress());
            booking_date_txt.setText(getIntent().getStringExtra("selectedDates"));
            booking_time_txt.setText(getIntent().getStringExtra("selectedTime"));
            noOfDaysSelected = 1;


            booking_price_txt.setText(String.valueOf(0));
            //set the booking type
            booking_type_dtls.setText("Free Trial");
        } else if (payflag.equals("BookACoach") && payType.equals("SPPU")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.coach_img));
            Coach coachDetails = null;
            try {
                cssDetails = (CoachSerializable) getIntent().getSerializableExtra("coachDetails");
                selectedDates = getIntent().getStringExtra("selectedDates");
                selectedTime = getIntent().getStringExtra("selectedTime");
                selectedDateWithYear = getIntent().getStringExtra("selectedDateWithYear");
                //get the shop details using the shopid
                coachDetails = dbObject.getCoachDetailsbyId(cssDetails.getCoachId());

                Picasso.with(getApplicationContext()).load(BASE_URL + coachDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            set_name.setText(coachDetails.getTitle());
            String strings[] = coachDetails.getFullAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(coachDetails.getFullAddress());
            booking_date_txt.setText(getIntent().getStringExtra("selectedDates"));
            booking_time_txt.setText(getIntent().getStringExtra("selectedTime"));
            String dates[] = getIntent().getStringExtra("selectedDates").split(",");
            noOfDaysSelected = dates.length - 1;

            booking_price_txt.setText(String.valueOf(noOfDaysSelected * Integer.parseInt(cssDetails.getPayPerUse())));
            //set the booking type
            booking_type_dtls.setText("Pay per use");
        } else if (payflag.equals("BookACoach") && payType.equals("SSESSION")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.coach_img));
            try {
                Coach coachDetails = null;
                cssDetails = (CoachSerializable) getIntent().getSerializableExtra("coachDetails");
                selectedTime = getIntent().getStringExtra("selectedTime");
                selectedDates = getIntent().getStringExtra("selectedDates");
                sessionCoachDetails = (SessionsDetail) getIntent().getSerializableExtra("sessionDetails");
                sessionCost = getIntent().getStringExtra("sessionCost");
                //get the shop details using the shopid
                coachDetails = dbObject.getCoachDetailsbyId(cssDetails.getCoachId());

                Picasso.with(getApplicationContext()).load(BASE_URL + coachDetails.getImageUrlSquare()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);

                set_name.setText(coachDetails.getTitle());
                String strings[] = coachDetails.getFullAddress().split(",");

                for (int i = 0; i < strings.length; i++) {
                    strings[i] = strings[i].trim();
                    strings[i] += ",\n";
                }
                String address = "";

                for (int i = 0; i < strings.length; i++)
                    address += strings[i];

                location_address_txt.setText(coachDetails.getFullAddress());
                booking_date_txt.setText(selectedDates);
                booking_time_txt.setText(selectedTime);

                booking_price_txt.setText(sessionCost);
                //set the booking type
                //if sessions is empty then don't show session else show sesssion
                if (!sessionDetails.getSessions().equals("0")) {
                    booking_type_dtls.setText(sessionCoachDetails.getSessions() + " Sessions"
                            + " " + sessionCoachDetails.getNoOfDays() + sessionCoachDetails.getDaysUnit());
                } else {
                    booking_type_dtls.setText(sessionCoachDetails.getNoOfDays() + sessionCoachDetails.getDaysUnit());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (payflag.equals("event")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.event_icon));
            book_image.setColorFilter(getResources().getColor(R.color.black));
            try {
                eventDetails = (Event) getIntent().getSerializableExtra("EventDetails");
                eventBatchDetails = (BatchTime) getIntent().getSerializableExtra("EventBatchTime");

                Picasso.with(getApplicationContext()).load(BASE_URL + eventDetails.getImage()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            set_name.setText(eventDetails.getName());
            String strings[] = eventDetails.getAddress().split(",");

            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];
            location_address_txt.setText(eventDetails.getAddress() + "\n" + eventDetails.getLocation());

            //if the event have no batch details then don't display anything else display
            if (eventBatchDetails != null) {
                booking_date_txt.setText(eventBatchDetails.getStartDate() + " to " + eventBatchDetails.getEndDate());
                booking_time_txt.setText(eventBatchDetails.getStartTime() + " to " + eventBatchDetails.getEndTime());
            } else {
                booking_date_txt.setText("");
                booking_time_txt.setText("");
            }

            booking_price_txt.setText(eventDetails.getCost());
            //set the booking type
            booking_type_dtls.setText("Event");
        } else if (payflag.equals("bookingDescEvent")) {

            book_image.setImageDrawable(getResources().getDrawable(R.drawable.event_icon));
            book_image.setColorFilter(getResources().getColor(R.color.black));
            //if booking an event from the booking description page show the details of it
            try {
                bookingHistory = (Bookhistory) getIntent().getSerializableExtra("EventDetails");
                batchTime = getIntent().getStringExtra("EventBatchTime");
                Picasso.with(getApplicationContext()).load(BASE_URL + eventDetails.getImage()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            set_name.setText(bookingHistory.getTitle());
            String strings[] = bookingHistory.getAddress().split(",");
            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(bookingHistory.getAddress() + "\n" + bookingHistory.getLocation());
            booking_date_txt.setText(batchTime);
            booking_time_txt.setText(bookingHistory.getSessionSTime());
            booking_price_txt.setText(bookingHistory.getCost());
            booking_type_dtls.setText("Event");
        } else if (payflag.equals("bookPass")) {
            book_image.setImageDrawable(getResources().getDrawable(R.drawable.id_card_black));
            singlePassData = getIntent().getParcelableExtra("passDetails");
//set the pass details
            Picasso.with(getApplicationContext()).load(BASE_URL + singlePassData.getLogo()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
    /*        String strings[] = singlePassData.getS.split(",");
            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();
                strings[i] += ",\n";
            }
            String address = "";

            for (int i = 0; i < strings.length; i++)
                address += strings[i];

            location_address_txt.setText(bookingHistory.getAddress() + "\n" + bookingHistory.getLocation());*/
            set_name.setText(singlePassData.getPass_name());

            booking_price_txt.setText(singlePassData.getPrice());
            booking_type_dtls.setText("Pass");
        }


        //display data commented
        //displaydata();
        //TODO Must write this code if integrating One Tap payments
        OnetapCallback.setOneTapCallback(this);

        //TODO Must write below code in your activity to set up initial context for PayU
        Payu.setInstance(this);

        // lets set up the tool bar;
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        toolBar.setTitle(" ");
        toolBar.setTitleTextColor(Color.WHITE);
        // setSupportActionBar(toolBar);


        // lets tell the people what version of sdk we are using
        PayUSdkDetails payUSdkDetails = new PayUSdkDetails();
        //cost is hardcoded
        pcost = Double.parseDouble(booking_price_txt.getText().toString());
        scost = pcost * (0.15);
        tcost = pcost + scost;
        SharedPreferences pred = Pay.this.getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
        pay_cost = tcost;
        // Toast.makeText(this, "Build No: " + payUSdkDetails.getSdkBuildNumber() + "\n Build Type: " + payUSdkDetails.getSdkBuildType() + " \n Build Flavor: " + payUSdkDetails.getSdkFlavor() + "\n Application Id: " + payUSdkDetails.getSdkApplicationId() + "\n Version Code: " + payUSdkDetails.getSdkVersionCode() + "\n Version Name: " + payUSdkDetails.getSdkVersionName(), Toast.LENGTH_LONG).show();
        setPrice();
/*        ((EditText) findViewById(R.id.editTextAmount)).setText("" + tcost);//tcost
        ((EditText) findViewById(R.id.editTextAmount)).setEnabled(false);
        ((EditText) findViewById(R.id.editTextEmail)).setText(pred.getString("emailid", ""));
        ((EditText) findViewById(R.id.editTextEmail)).setEnabled(false);*/
        //Lets setup the environment spinner
        environmentSpinner = (Spinner) findViewById(R.id.spinner_environment);
        //  List<String> list = new ArrayList<String>();
        String[] environmentArray = getResources().getStringArray(R.array.environment_array);
/*        list.add("Test");
        list.add("Production");*/
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, environmentArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        environmentSpinner.setAdapter(dataAdapter);
        environmentSpinner.setSelection(0);
        //       ((EditText) findViewById(R.id.editTextMerchantKey)).setText("MmQZF3");

        environmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (environmentSpinner.getSelectedItem().equals("Production")) {
                    // Toast.makeText(Pay.this, getString(R.string.use_live_key_in_production_environment), Toast.LENGTH_SHORT).show();

                    /* 0MQaQP is test key in PRODUCTION_ENv just for testing purpose in this app. Merchant should use their
                    * own key in PRODUCTION_ENV
                    */
                    //            ((EditText) findViewById(R.id.editTextMerchantKey)).setText("MmQZF3");

                } else {
                    //set the test key in test environment
                    //           ((EditText) findViewById(R.id.editTextMerchantKey)).setText("gtKFFx");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getTheWalletDetails(final String cost) {

        try {

            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
            Call<Reward> call = stationClient.getRewardDetails(Prefs.getString(AppConstants.UserId, ""));

            call.enqueue(new Callback<Reward>() {
                @Override
                public void onResponse(Call<Reward> call, Response<Reward> response) {
                    Reward result = response.body();

                }

                @Override
                public void onFailure(Call<Reward> call, Throwable t) {

                    Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.d("Registration_eqnn", t + "");
                    Log.d("Registration_eqnn", call + "");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void setOnClickListener() {

        //on click on the action done call the functionality
        voucher_code
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            //reset the value of the pay cost
                            tcost = pay_cost;

                            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                            Call<LogIn> call = stationClient.getPromocodePrice(Prefs.getString(AppConstants.UserId, ""), voucher_code.getText().toString());
                            call.enqueue(new Callback<LogIn>() {
                                @Override
                                public void onResponse(final Call<LogIn> call, Response<LogIn> response) {
                                    int response_code = response.code();
                                    LogIn result = response.body();
                                    if (response_code == 200) {
                                        login = result.getStatus();
                                        if (login.get(0).getStatus() == 1) {
                                            //clear the code if applied and clear promocode
                                            voucher_code.setText("");
                                            promocode.setText(getResources().getString(R.string.Rs) + "0");
                                            //based on the amount type
                                            if (login.get(0).getType().equals("Rs")) {
                                                promoAmt = Double.valueOf(login.get(0).getDiscount());
                                                promocode.setText(getResources().getString(R.string.Rs) + login.get(0).getDiscount());
                                            } else {

                                                promoAmt = tcost * (Integer.parseInt(login.get(0).getDiscount()) / 100.0);

                                                promocode.setText(getResources().getString(R.string.Rs) + new DecimalFormat("##.##").format(promoAmt));
                                            }
                                        } else {
                                            //clear the code if applied and clear promocode
                                            voucher_code.setText("");
                                            promoAmt = 0;
                                            promocode.setText(getResources().getString(R.string.Rs) + "0");
                                            Toast.makeText(Pay.this, login.get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    setPrice();
                                }

                                @Override
                                public void onFailure(Call<LogIn> call, Throwable t) {
                                    Log.d("Registration_eqnn", t + "");
                                    Log.d("Registration_eqnn", call + "");


                                }
                            });
                            handled = true;
                        }
                        return handled;
                    }
                });


        //set on click listener for the back image
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //set on click listener for pay button
        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                if (payflag.equals("bookAShop") && payType.equals("FreeShop")) {
                    checkDetails(programrowid, sssDetails.getShopId(), sssDetails.getSubServiceId(), "4",
                            "1", "1", selectedDates, selectedDates,
                            selectedTime, "1hr", "batch",
                            "", "1", "0", "Free Trial", "0", Prefs.getString(AppConstants.UserId, ""));
                } else if (payflag.equals("bookAShop") && payType.equals("SPPU")) {
                    checkDetails(programrowid, sssDetails.getShopId(), sssDetails.getSubServiceId(), "5",
                            noOfDaysSelected + " Days", noOfDaysSelected + " Sessions", selectedDateWithYear, selectedDateWithYear,
                            selectedTime, "1hr", "batch",
                            "", "1", String.valueOf(tcost), "1", "0", Prefs.getString(AppConstants.UserId, ""));

                } else if (payflag.equals("bookAShop") && payType.equals("SSESSION")) {
                    {
                        //seperate the string into two parts as sd&ed and send to server
                        String s = selectedDates;
                        String[] parts = s.split("to"); // escape
                        String startDate, endDate;

                        //if selected date is two split into two else use one as start date and end date
                        if (s.contains("to")) {
                            startDate = parts[0];
                            endDate = parts[1];
                        } else {
                            startDate = parts[0];
                            endDate = parts[0];
                        }
                        checkDetails(programrowid, sssDetails.getShopId(), sssDetails.getSubServiceId(), "1",
                                sessionDetails.getNoOfDays() + sessionDetails.getDaysUnit(), sessionDetails.getSessions(), startDate, endDate,
                                selectedTime, sessionDetails.getNoOfDays() + sessionDetails.getDaysUnit(), "batch",
                                "", "1", String.valueOf(tcost), "1", "0", Prefs.getString(AppConstants.UserId, ""));
                    }
                } else if (payflag.equals("BookACoach") && payType.equals("FreeCoach")) {

                    checkDetails(programrowid, cssDetails.getCoachId(), cssDetails.getCoachSubCategoryId(), "6",
                            "1", "1", selectedDates, selectedDates,
                            selectedTime, "1hr", "batch",
                            "", "1", "0", "Free Trial", "0", Prefs.getString(AppConstants.UserId, ""));


                } else if (payflag.equals("BookACoach") && payType.equals("SPPU")) {
                    checkDetails(programrowid, cssDetails.getCoachId(), cssDetails.getCoachSubCategoryId(), "7",
                            noOfDaysSelected + " Days", noOfDaysSelected + " Sessions", selectedDateWithYear, selectedDateWithYear,
                            selectedTime, "1hr", "batch",
                            "", "1", String.valueOf(tcost), "1", "0", Prefs.getString(AppConstants.UserId, ""));
                } else if (payflag.equals("BookACoach") && payType.equals("SSESSION")) {
                    //seperate the string into two parts as sd&ed and send to server
                    String s = selectedDates;
                    String[] parts = s.split("to"); // escape .
                    String startDate = parts[0];
                    String endDate = parts[1];
                    checkDetails(programrowid, cssDetails.getCoachId(), cssDetails.getCoachSubCategoryId(), "2",
                            sessionCoachDetails.getNoOfDays() + sessionCoachDetails.getDaysUnit(), sessionCoachDetails.getSessions(), startDate, endDate,
                            selectedTime, "1hr", "batch",
                            "", "1", String.valueOf(tcost), "1", "0", Prefs.getString(AppConstants.UserId, ""));
                } else if (payflag.equals("event")) {
                    checkDetails(eventDetails.getEid(), eventDetails.getEid(), "", "3", "", "",
                            eventBatchDetails.getStartDate(), eventBatchDetails.getEndDate(),
                            eventBatchDetails.getStartTime() + " to " + eventBatchDetails.getEndTime(), "", "0",
                            "", "1", String.valueOf(tcost), "1", "0", Prefs.getString(AppConstants.UserId, ""));

                } else if (payflag.equals("bookPass")) {
                    if (isInternetOn()) {

                        bookPass(singlePassData.getPass_id(), Prefs.getString(AppConstants.UserId, ""), "1", singlePassData.getPrice(), "1", "1");
                    } else {
                        Toast.makeText(Pay.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((TextView) findViewById(R.id.pacc)).setText(getResources().getString(R.string.Rs) + pcost);
                ((TextView) findViewById(R.id.serc)).setText(getResources().getString(R.string.Rs) + scost);
                promocode.setText(getResources().getString(R.string.Rs) + "0/-");
                ((TextView) findViewById(R.id.totalc)).setText(getResources().getString(R.string.Rs) + pay_cost);
            }
        });

        //by default set the reward status as No
        rewardStatus = "No";
        ///set on check listener for the checkbox reward points
        utilize_points_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rewardStatus = "Yes";
                } else {
                    rewardStatus = "No";
                }
            }
        });
    }

    private void bookPass(String pass_id, String userId, String booking_status, String cost, String payment_mode, String payment_status) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassBookingPojo> call = stationClient.bookPass(userId, pass_id, booking_status, cost, payment_mode, payment_status);

        call.enqueue(new Callback<PassBookingPojo>() {
            @Override
            public void onResponse(Call<PassBookingPojo> call, Response<PassBookingPojo> response) {
                int response_code = response.code();
                PassBookingPojo result = response.body();
                if (response_code == 200) {
                    if (result.getBooking()[0].getStatus() == 1) {
                        Toast.makeText(Pay.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();

                    } else if (result.getBooking()[0].getStatus() == 0) {
                        Toast.makeText(Pay.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(Pay.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<PassBookingPojo> call, Throwable t) {
                Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    private void setPrice() {

        ((TextView) findViewById(R.id.pacc)).setText(getResources().getString(R.string.Rs) + pcost);
        ((TextView) findViewById(R.id.serc)).setText(getResources().getString(R.string.Rs) + scost);
        //if applied second time then reset the price
        tcost = tcost - promoAmt;
        ((TextView) findViewById(R.id.totalc)).setText(getResources().getString(R.string.Rs) + tcost);
    }

    private void checkDetails(String program_id, String shopid, String serviceid, final String booking_type,
                              String no_of_days, String no_of_sessions, String start_date, String end_date,
                              String session_s_time, String session_hours, String is_batch,
                              String list_of_days, String approval_status,
                              final String cost, String Paymentmode, String payment_status,
                              String user_id) {
        //if the promoAmt is zero then make the voucher code as empty
        if (promoAmt == 0) {
            voucher_code.setText("");
        }
        System.out.println("Pay json" + program_id + "," + shopid + "," + serviceid + "," + booking_type + "," +
                no_of_days + "," + no_of_sessions + "," + start_date + "," + end_date + "," +
                session_s_time + "," + session_hours + "," + is_batch + "," +
                list_of_days + "," + approval_status + "," +
                String.valueOf(pcost) + "," + Paymentmode + "," + payment_status + "," +
                user_id + "," + rewardStatus + "," + voucher_code.getText().toString() + "," + String.valueOf(promoAmt) + "," + String.valueOf(scost));
        //change date format
        //  String sdate = changeFormatToSendServer(start_date);
        // String edate = changeFormatToSendServer(end_date);

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<BookingProcess> call = stationClient.booking(program_id, shopid, serviceid, booking_type,
                no_of_days, no_of_sessions, start_date, end_date,
                session_s_time, session_hours, is_batch,
                list_of_days, approval_status,
                String.valueOf(pcost), Paymentmode, payment_status,
                user_id, rewardStatus, voucher_code.getText().toString(), String.valueOf(promoAmt), String.valueOf(scost));


        call.enqueue(new Callback<BookingProcess>() {
            @Override
            public void onResponse(Call<BookingProcess> call, Response<BookingProcess> response) {
                int response_code = response.code();
                BookingProcess result = response.body();

                if (response_code == 200) {
                    hideProgressDialog();
                    Log.d("BookPage1", "Registration_eqnn" + response.code() + "");
                    List<Booking> check = result.getBooking();
                    Booking check1 = check.get(0);
                    String value = "" + check1.getStatus();

                    //set the payment unique id in the shared Preference
                    Prefs.putString(AppConstants.paymentuid, check1.getMessage());
                    if (value.equals("1")) {
                        SharedPreferences pred = getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editorp = pred.edit();
                        editorp.putString("status", value);
                        editorp.commit();
                        Toast.makeText(Pay.this, "Successfully booked", Toast.LENGTH_SHORT).show();
                        finish();
                        //after successfull book for free trial redirect to booking details activity page
                        Intent in = new Intent(Pay.this, BookingDetailsActivity.class);
                        in.putExtra("BookFlag", "BookAShop");
                        startActivity(in);
                        //get the wallet details from the server
                        getTheWalletDetails(cost);


                    } else {

                        Toast.makeText(Pay.this, check1.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                } else if (response_code == 400) {
                    Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("BookPage1", "400 error");
                    hideProgressDialog();
                } else if (response_code == 500) {
                    Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("BookPage1", "500 error");
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<BookingProcess> call, Throwable t) {
                Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });


    }

  /*  private void updateWallet(double currentGainedPoints) {


        String wallet = String.valueOf(currentGainedPoints);

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));

        RequestBody wallet_ = RequestBody.create(MediaType.parse("text/plain"), wallet);

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.updateWallet(userId, wallet_);

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                UserProfileService result = response.body();
                if (result.getResult().get(0).getStatus() == 0) {
                    //  Toast.makeText(Otp.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    //  Toast.makeText(Otp.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                Toast.makeText(Pay.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");


            }
        });

    }*/

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        back_img = (ImageView) findViewById(R.id.back_img);
        squareimage = (ImageView) findViewById(R.id.square_shop_image);
        book_image = (ImageView) findViewById(R.id.book_image);
        set_name = (TextView) findViewById(R.id.set_name);
        location_address_txt = (TextView) findViewById(R.id.location_address_txt);
        booking_date_txt = (TextView) findViewById(R.id.booking_date_txt);
        booking_time_txt = (TextView) findViewById(R.id.booking_time_txt);
        booking_price_txt = (TextView) findViewById(R.id.booking_price_txt);
        booking_type_dtls = (TextView) findViewById(R.id.booking_type_dtls);
        btnPayNow = findViewById(R.id.btnPayNow);
        dbObject = new DBFunctions(this);
        voucher_code = (EditText) findViewById(R.id.voucher_code);
        cancel_btn = (TextView) findViewById(R.id.cancel_btn);
        apply_btn = (TextView) findViewById(R.id.apply_btn);
        promocode = (TextView) findViewById(R.id.promocode);
        total_points = (TextView) findViewById(R.id.total_points);
        utilize_points_chk = (CheckBox) findViewById(R.id.utilize_points_chk);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {

                /**
                 * Here, data.getStringExtra("payu_response") ---> Implicit response sent by PayU
                 * data.getStringExtra("result") ---> Response received from merchant's Surl/Furl
                 *
                 * PayU sends the same response to merchant server and in app. In response check the value of key "status"
                 * for identifying status of transaction. There are two possible status like, success or failure
                 * */
                new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setMessage("Payu's Data : " + data.getStringExtra("payu_response") + "\n\n\n Merchant's Data: " + data.getStringExtra("result"))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();

                if (data.getStringExtra("paysucstu").equals("1")) {

                    paymentDetails();


                }
                if (data.getStringExtra("paysucstu").equals("2")) {
                    Toast.makeText(this, "Payment Failed, Try again", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This method prepares all the payments params to be sent to PayuBaseActivity.java
     */
    public void navigateToBaseActivity(View view) {

/*        merchantKey = ((EditText) findViewById(R.id.editTextMerchantKey)).getText().toString();
        String amount = "" + tcost;

        amount = ((EditText) findViewById(R.id.editTextAmount)).getText().toString();
        String email = ((EditText) findViewById(R.id.editTextEmail)).getText().toString();*/

        String value = environmentSpinner.getSelectedItem().toString();
        int environment;
        String TEST_ENVIRONMENT = getResources().getString(R.string.test);
        if (value.equals(TEST_ENVIRONMENT))
            environment = PayuConstants.STAGING_ENV;
        else
            environment = PayuConstants.PRODUCTION_ENV;

        //    userCredentials = merchantKey + ":" + email;

        //TODO Below are mandatory params for hash genetation
        mPaymentParams = new PaymentParams();
        /**
         * For Test Environment, merchantKey = "gtKFFx"
         * For Production Environment, merchantKey should be your live key or for testing in live you can use "0MQaQP"
         */
        //set the mobile no and emailid from the shared preference
        SharedPreferences pred = Pay.this.getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);

        mPaymentParams.setKey(merchantKey);
        //  mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo("product_info");
        mPaymentParams.setFirstName(pred.getString("name", ""));
        mPaymentParams.setEmail(pred.getString("emailid", ""));

        /*
        * Transaction Id should be kept unique for each transaction.
        * */
        mPaymentParams.setTxnId("" + System.currentTimeMillis());

        /**
         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
         */
        mPaymentParams.setSurl("https://payu.herokuapp.com/success");
        mPaymentParams.setFurl("https://payu.herokuapp.com/failure");

        /*
         * udf1 to udf5 are options params where you can pass additional information related to transaction.
         * If you don't want to use it, then send them as empty string like, udf1=""
         * */
        mPaymentParams.setUdf1(Prefs.getString(AppConstants.UserId, ""));
        mPaymentParams.setUdf2("udf2");
        mPaymentParams.setUdf3("udf3");
        mPaymentParams.setUdf4("udf4");
        mPaymentParams.setUdf5("udf5");

        /**
         * These are used for store card feature. If you are not using it then user_credentials = "default"
         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
         * here merchant_key = your merchant key,
         * user_id = unique id related to user like, email, phone number, etc.
         * */
        mPaymentParams.setUserCredentials(userCredentials);

        //TODO Pass this param only if using offer key
        //mPaymentParams.setOfferKey("cardnumber@8370");

        //TODO Sets the payment environment in PayuConfig object
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(environment);

        //TODO It is recommended to generate hash from server only. Keep your key and salt in server side hash generation code.
        //generateHashFromServer(mPaymentParams);

        /**
         * Below approach for generating hash is not recommended. However, this approach can be used to test in PRODUCTION_ENV
         * if your server side hash generation code is not completely setup. While going live this approach for hash generation
         * should not be used.
         * */
        String salt = "YIOBafjk";
        generateHashFromSDK(mPaymentParams, salt);

    }

    /******************************
     * Client hash generation
     ***********************************/
    // Do not use this, you may use this only for testing.
    // lets generate hashes.
    // This should be done from server side..
    // Do not keep salt anywhere in app.
    public void generateHashFromSDK(PaymentParams mPaymentParams, String salt) {
        PayuHashes payuHashes = new PayuHashes();
        PostData postData = new PostData();

        // payment Hash;
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setAmount(mPaymentParams.getAmount());
        checksum.setKey(mPaymentParams.getKey());
        checksum.setTxnid(mPaymentParams.getTxnId());
        checksum.setEmail(mPaymentParams.getEmail());
        checksum.setSalt(salt);
        checksum.setProductinfo(mPaymentParams.getProductInfo());
        checksum.setFirstname(mPaymentParams.getFirstName());
        checksum.setUdf1(mPaymentParams.getUdf1());
        checksum.setUdf2(mPaymentParams.getUdf2());
        checksum.setUdf3(mPaymentParams.getUdf3());
        checksum.setUdf4(mPaymentParams.getUdf4());
        checksum.setUdf5(mPaymentParams.getUdf5());

        postData = checksum.getHash();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setPaymentHash(postData.getResult());
        }

        // checksum for payemnt related details
        // var1 should be either user credentials or default
        String var1 = mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials();
        String key = mPaymentParams.getKey();

        if ((postData = calculateHash(key, PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // Assign post data first then check for success
            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(postData.getResult());
        //vas
        if ((postData = calculateHash(key, PayuConstants.VAS_FOR_MOBILE_SDK, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setVasForMobileSdkHash(postData.getResult());

        // getIbibocodes
        if ((postData = calculateHash(key, PayuConstants.GET_MERCHANT_IBIBO_CODES, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setMerchantIbiboCodesHash(postData.getResult());

        if (!var1.contentEquals(PayuConstants.DEFAULT)) {
            // get user card
            if ((postData = calculateHash(key, PayuConstants.GET_USER_CARDS, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // todo rename storedc ard
                payuHashes.setStoredCardsHash(postData.getResult());
            // save user card
            if ((postData = calculateHash(key, PayuConstants.SAVE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setSaveCardHash(postData.getResult());
            // delete user card
            if ((postData = calculateHash(key, PayuConstants.DELETE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setDeleteCardHash(postData.getResult());
            // edit user card
            if ((postData = calculateHash(key, PayuConstants.EDIT_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setEditCardHash(postData.getResult());
        }

        if (mPaymentParams.getOfferKey() != null) {
            postData = calculateHash(key, PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey(), salt);
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                payuHashes.setCheckOfferStatusHash(postData.getResult());
            }
        }

        if (mPaymentParams.getOfferKey() != null && (postData = calculateHash(key, PayuConstants.CHECK_OFFER_STATUS, mPaymentParams.getOfferKey(), salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setCheckOfferStatusHash(postData.getResult());
        }

        // we have generated all the hases now lest launch sdk's ui
        launchSdkUI(payuHashes);
    }

    // deprecated, should be used only for testing.
    private PostData calculateHash(String key, String command, String var1, String salt) {
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setKey(key);
        checksum.setCommand(command);
        checksum.setVar1(var1);
        checksum.setSalt(salt);
        return checksum.getHash();
    }

    /**
     * This method generates hash from server.
     *
     * @param mPaymentParams payments params used for hash generation
     */
    public void generateHashFromServer(PaymentParams mPaymentParams) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount()));
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        // for offer_key
        if (null != mPaymentParams.getOfferKey())
            postParamsBuffer.append(concatParams(PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey()));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();

        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }


    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    /**
     * This method adds the Payuhashes and other required params to intent and launches the PayuBaseActivity.java
     *
     * @param payuHashes it contains all the hashes generated from merchant server
     */
    public void launchSdkUI(PayuHashes payuHashes) {

        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);

        //Lets fetch all the one click card tokens first
        fetchMerchantHashes(intent);

    }

    /**
     * This method stores merchantHash and cardToken on merchant server.
     *
     * @param cardToken    card token received in transaction response
     * @param merchantHash merchantHash received in transaction response
     */
    private void storeMerchantHash(String cardToken, String merchantHash) {

        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials + "&card_token=" + cardToken + "&merchant_hash=" + merchantHash;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    //TODO Deploy a file on your server for storing cardToken and merchantHash nad replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/store_merchant_hash");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                    InputStream responseInputStream = conn.getInputStream();
                    StringBuffer responseStringBuffer = new StringBuffer();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }

                    JSONObject response = new JSONObject(responseStringBuffer.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                this.cancel(true);
            }
        }.execute();
    }

    //TODO This method is used if integrating One Tap Payments

    /**
     * This method fetches merchantHash and cardToken already stored on merchant server.
     */
    private void fetchMerchantHashes(final Intent intent) {
        // now make the api call.
        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials;
        final Intent baseActivityIntent = intent;
        new AsyncTask<Void, Void, HashMap<String, String>>() {

            @Override
            protected HashMap<String, String> doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/get_merchant_hashes");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                    InputStream responseInputStream = conn.getInputStream();
                    StringBuffer responseStringBuffer = new StringBuffer();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }

                    JSONObject response = new JSONObject(responseStringBuffer.toString());

                    HashMap<String, String> cardTokens = new HashMap<String, String>();
                    JSONArray oneClickCardsArray = response.getJSONArray("data");
                    int arrayLength;
                    if ((arrayLength = oneClickCardsArray.length()) >= 1) {
                        for (int i = 0; i < arrayLength; i++) {
                            cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
                        }
                        return cardTokens;
                    }
                    // pass these to next activity

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(HashMap<String, String> oneClickTokens) {
                super.onPostExecute(oneClickTokens);

                baseActivityIntent.putExtra(PayuConstants.ONE_CLICK_CARD_TOKENS, oneClickTokens);
                startActivityForResult(baseActivityIntent, PayuConstants.PAYU_REQUEST_CODE);
            }
        }.execute();
    }


    //TODO This method is used only if integrating One Tap Payments

    /**
     * This method deletes merchantHash and cardToken from server side file.
     *
     * @param cardToken cardToken of card whose merchantHash and cardToken needs to be deleted from merchant server
     */
    private void deleteMerchantHash(String cardToken) {

        final String postParams = "card_token=" + cardToken;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/delete_merchant_hash");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                this.cancel(true);
            }
        }.execute();
    }

    //TODO This method is used only if integrating One Tap Payments

    /**
     * This method prepares a HashMap of cardToken as key and merchantHash as value.
     *
     * @param merchantKey     merchant key used
     * @param userCredentials unique credentials of the user usually of the form key:userId
     */
    public HashMap<String, String> getAllOneClickHashHelper(String merchantKey, String userCredentials) {

        // now make the api call.
        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials;
        HashMap<String, String> cardTokens = new HashMap<String, String>();

        try {
            //TODO Replace below url with your server side file url.
            URL url = new URL("https://payu.herokuapp.com/get_merchant_hashes");

            byte[] postParamsByte = postParams.getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postParamsByte);

            InputStream responseInputStream = conn.getInputStream();
            StringBuffer responseStringBuffer = new StringBuffer();
            byte[] byteContainer = new byte[1024];
            for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                responseStringBuffer.append(new String(byteContainer, 0, i));
            }

            JSONObject response = new JSONObject(responseStringBuffer.toString());

            JSONArray oneClickCardsArray = response.getJSONArray("data");
            int arrayLength;
            if ((arrayLength = oneClickCardsArray.length()) >= 1) {
                for (int i = 0; i < arrayLength; i++) {
                    cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
                }

            }
            // pass these to next activity

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cardTokens;
    }

    //TODO This method is used only if integrating One Tap Payments

    /**
     * Returns a HashMap object of cardToken and one click hash from merchant server.
     * <p>
     * This method will be called as a async task, regardless of merchant implementation.
     * Hence, not to call this function as async task.
     * The function should return a cardToken and corresponding one click hash as a hashMap.
     *
     * @param userCreds a string giving the user credentials of user.
     * @return the Hash Map of cardToken and one Click hash.
     **/
    @Override
    public HashMap<String, String> getAllOneClickHash(String userCreds) {
        // 1. GET http request from your server
        // GET params - merchant_key, user_credentials.
        // 2. In response we get a
        // this is a sample code for fetching one click hash from merchant server.
        return getAllOneClickHashHelper(merchantKey, userCreds);
    }

    //TODO This method is used only if integrating One Tap Payments

    //TODO This method is used only if integrating One Tap Payments
    @Override
    public void getOneClickHash(String cardToken, String merchantKey, String userCredentials) {

    }

    /**
     * This method will be called as a async task, regardless of merchant implementation.
     * Hence, not to call this function as async task.
     * This function save the oneClickHash corresponding to its cardToken
     *
     * @param cardToken    a string containing the card token
     * @param oneClickHash a string containing the one click hash.
     **/

    @Override
    public void saveOneClickHash(String cardToken, String oneClickHash) {
        // 1. POST http request to your server
        // POST params - merchant_key, user_credentials,card_token,merchant_hash.
        // 2. In this POST method the oneclickhash is stored corresponding to card token in merchant server.
        // this is a sample code for storing one click hash on merchant server.

        storeMerchantHash(cardToken, oneClickHash);

    }


    //TODO This method is used only if integrating One Tap Payments

    /**
     * This method will be called as a async task, regardless of merchant implementation.
     * Hence, not to call this function as async task.
     * This function delete’s the oneClickHash from the merchant server
     *
     * @param cardToken       a string containing the card token
     * @param userCredentials a string containing the user credentials.
     **/

    @Override
    public void deleteOneClickHash(String cardToken, String userCredentials) {

        // 1. POST http request to your server
        // POST params  - merchant_hash.
        // 2. In this POST method the oneclickhash is deleted in merchant server.
        // this is a sample code for deleting one click hash from merchant server.

        deleteMerchantHash(cardToken);

    }

    //TODO This method is used only if integrating One Tap Payments

    private void paymentDetails() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<BookingProcess> call = stationClient.updatepayment(Prefs.getString(AppConstants.paymentuid, ""));
        call.enqueue(new Callback<BookingProcess>() {
            @Override
            public void onResponse(Call<BookingProcess> call, Response<BookingProcess> response) {
                int response_code = response.code();
                BookingProcess result = response.body();

                if (response_code == 200) {
                    Log.d("BookPage1", "Registration_eqnn" + response.code() + "");
                    List<Booking> check = result.getBooking();
                    Booking check1 = check.get(0);
                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    Toast.makeText(getApplicationContext(), "Payment Success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), PagerLayout.class));
                    if (value.equals("1")) {
                        Toast.makeText(getApplicationContext(), "updated Success", Toast.LENGTH_SHORT).show();
                    }

                } else if (response_code == 400) {

                } else if (response_code == 500) {

                }
            }

            @Override
            public void onFailure(Call<BookingProcess> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

   /* public void displaydata() {


        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "fonts/times.ttf");
        ImageView image = (ImageView) findViewById(R.id.serviceimagebook);
        TextView shopname = (TextView) findViewById(R.id.shopname);
        TextView dateday = (TextView) findViewById(R.id.dateday);
        TextView booktimeb = (TextView) findViewById(R.id.booktimeb);
        TextView bookrupees = (TextView) findViewById(R.id.bookrupees);
        TextView nodbook = (TextView) findViewById(R.id.nodbook);
        TextView nosbook = (TextView) findViewById(R.id.nosbook);
        TextView locationbook = (TextView) findViewById(R.id.locationbook);
        Button reviewhere = (Button) findViewById(R.id.reviewhere);

        reviewhere.setVisibility(View.GONE);


        Bookhistory check = new Bookhistory();
        check = PrePayDetails;
        if (("" + check.getBookingType()).equals("3")) {
            dateday.setText("" + check.getStartDate() + " to " + check.getNoOfDays());


        } else {
            dateday.setText("" + check.getStartDate());
        }


        booktimeb.setText("" + check.getSessionSTime());
        bookrupees.setText("" + check.getCost());
        nodbook.setText("" + check.getNoOfDays());
        nosbook.setText("" + check.getNoOfSessions());

        if (("" + check.getBookingType()).equals("1"))
            for (int i = 0; i < servicelist.size(); i++) {
                if (servicelist.get(i).getSid().equals(check.getServiceid())) {
                    Service check1 = servicelist.get(i);
                    int x = check1.getIconid();
                    try {
                        image.setImageResource(sercice_img[x - 1]);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        Picasso.with(getApplicationContext()).load(BASE_URL + check1.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(image);
                    }
                }
            }
        else if (("" + check.getBookingType()).equals("2")) {
            image.setImageResource(R.drawable.training);
        } else if (("" + check.getBookingType()).equals("3")) {
            image.setImageResource(R.drawable.callender);
        }

        if (("" + check.getBookingType()).equals("1"))
            for (int i = 0; i < shoplist.size(); i++) {
                if (shoplist.get(i).getSid().equals(check.getShopid())) {
                    Shop check1 = shoplist.get(i);
                    shopname.setText("" + check1.getTitle());
                    locationbook.setText("" + check1.getFullAddress());
                }
            }
        else if (("" + check.getBookingType()).equals("2")) {
            for (int i = 0; i < coachlist.size(); i++) {
                if (coachlist.get(i).getCid().equals(check.getShopid())) {
                    Coach check1 = coachlist.get(i);
                    shopname.setText("" + check1.getTitle());
                    locationbook.setText("" + check1.getFullAddress());

                }
            }

            //shopname.setText("Training");
            // locationbook.setText("--");
        } else if (("" + check.getBookingType()).equals("3")) {
            for (int i = 0; i < eventlist.size(); i++) {
                if (eventlist.get(i).getEid().equals(check.getShopid())) {
                    Event check1 = eventlist.get(i);
                    shopname.setText("" + check1.getName());
                    locationbook.setText("" + check1.getAddress());

                }

            }
            LinearLayout sesdaylr = (LinearLayout) findViewById(R.id.sesdaylr);
            LinearLayout timepricelr = (LinearLayout) findViewById(R.id.timepricelr);
            ImageView clockimages = (ImageView) findViewById(R.id.clockimages);
            clockimages.setVisibility(View.GONE);
            booktimeb.setVisibility(View.GONE);
            sesdaylr.setVisibility(View.GONE);
            //timepricelr.setVisibility(View.GONE);

            //shopname.setText("Training");
            // locationbook.setText("--");
        }


    }*/

    /**
     * This AsyncTask generates hash from server.
     */
    private class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Pay.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {

                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");

                // get the payuConfig first
                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }
                Log.v("pay", responseStringBuffer.toString());
                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        //TODO Below three hashes are mandatory for payment flow and needs to be generated at merchant server
                        /**
                         * Payment hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_hash -
                         *
                         * sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)
                         *
                         */
                        case "payment_hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;
                        /**
                         * vas_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating vas_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be "default"
                         *
                         */
                        case "vas_for_mobile_sdk_hash":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;
                        /**
                         * payment_related_details_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_related_details_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "payment_related_details_for_mobile_sdk_hash":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;

                        //TODO Below hashes only needs to be generated if you are using Store card feature
                        /**
                         * delete_user_card_hash is used while deleting a stored card.
                         * Below is formula for generating delete_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;
                        /**
                         * get_user_cards_hash is used while fetching all the cards corresponding to a user.
                         * Below is formula for generating get_user_cards_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;
                        /**
                         * edit_user_card_hash is used while editing details of existing stored card.
                         * Below is formula for generating edit_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;
                        /**
                         * save_user_card_hash is used while saving card to the vault
                         * Below is formula for generating save_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;

                        //TODO This hash needs to be generated if you are using any offer key
                        /**
                         * check_offer_status_hash is used while using check_offer_status api
                         * Below is formula for generating check_offer_status_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be Offer Key.
                         *
                         */
                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);

            progressDialog.dismiss();
            launchSdkUI(payuHashes);
        }
    }

}