package com.jireh.bounze.Activity;

/**
 * Created by Muthamizhan C on 20-10-2017.
 */

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.paolorotolo.appintro.AppIntro;
import com.jireh.bounze.R;


public class Helper extends AppIntro {
    int noOfTimesCalled = 0;

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(new firstSlide());
        addSlide(new secondSlide());
        addSlide(new thirdSlide());

        setSlideOverAnimation();
        setScrollDurationFactor(1);
        // SHOW or HIDE the statusbar
        showStatusBar(true);
    /*    setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));
*/
        showSkipButton(false);
    }

    @Override
    public void onSkipPressed() {
        if (noOfTimesCalled % 2 == 0) {
            Intent in = new Intent(Helper.this, SplashScreenActivity.class);
            startActivity(in);
            finish();
        }
        noOfTimesCalled++;
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        if (noOfTimesCalled % 2 == 0) {
            Intent in = new Intent(Helper.this, SplashScreenActivity.class);
            startActivity(in);
            finish();
        }
        noOfTimesCalled++;
    }

    public void getStarted() {

    }

    @Override
    public void onSlideChanged() {

    }

    @SuppressLint("ValidFragment")
    public static class firstSlide extends Fragment {


        public firstSlide() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.first_helper, container, false);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), R.drawable.bounze_app_01, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            String imageType = options.outMimeType;
            ImageView mImageView = (ImageView) v.findViewById(R.id.firstlayout);

            Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.bounze_app_01, imageWidth, imageHeight);
            mImageView.setImageBitmap(bitmap);

            return v;
        }

    }

    @SuppressLint("ValidFragment")
    public static class secondSlide extends Fragment {
        public secondSlide() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.second_helper, container, false);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), R.drawable.bounze_app_02, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            String imageType = options.outMimeType;
            ImageView mImageView = (ImageView) v.findViewById(R.id.secondlayout);
            Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.bounze_app_02, imageWidth, imageHeight);
            mImageView.setImageBitmap(bitmap);

            return v;
        }
    }

    @SuppressLint("ValidFragment")
    public static class thirdSlide extends Fragment {
        public thirdSlide()

        {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.third_helper, container, false);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), R.drawable.bounze_app_03, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            String imageType = options.outMimeType;
            ImageView mImageView = (ImageView) v.findViewById(R.id.thirdlayout);
            Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.bounze_app_03, imageWidth, imageHeight);
            mImageView.setImageBitmap(bitmap);

            return v;
        }
    }

}

