package com.jireh.bounze.Activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.Adapter.OnTheGoAdapter;
import com.jireh.bounze.Adapter.ShopListAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.data.City;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.data.Shop;

import java.util.ArrayList;
import java.util.List;


public class Shoplistactivity extends AppCompatActivity {
    public Location current_loc;
    public Profile logindet;
    public String current_service = "";
    public String current_shop = "";
    DBFunctions dbobject;
    TextView no_shop_txt, title_txt;
    Toolbar toolbar;
    List<Shop> current_shoplist = new ArrayList<Shop>();
    List<Shop> tcurrent_shoplist = new ArrayList<Shop>();
    RecyclerView shop_list_recycler;
    List<Shop> shoplist;
    List<City> citylist;
    ImageView backImg, filter_img, search_img;
    GPSTracker gpsTracker;
    EditText search_edit_text;
    ShopListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shoplist_layout);
        dbobject = new DBFunctions(getApplicationContext());
        initVariable();
        setOnClickListener();


        //set the visibility for the filter image and search image
        filter_img.setVisibility(View.VISIBLE);
        search_img.setVisibility(View.VISIBLE);
        //set the title for the service
        title_txt.setText(getIntent().getStringExtra("current_title"));

        gpsTracker = new GPSTracker(Shoplistactivity.this);

        if (!gpsTracker.canGetLocation()) {

            System.out.println("inside getlocation");
            gpsTracker.showSettingsAlert();
        } else {
            putdate();
        }

    }

    private void setOnClickListener() {
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Shoplistactivity.this, ShopFilter.class);
                startActivity(in);
            }
        });

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set the visibility for the edittext search and gone for filter and search img
                search_edit_text.setVisibility(View.VISIBLE);
                search_edit_text.setHint("Snap fitness");
                filter_img.setVisibility(View.GONE);
                search_img.setVisibility(View.GONE);
                addTextchangeListener();
            }
        });
    }

    private void addTextchangeListener() {


        search_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {


                ///get the shop list db and by name search and display the list

                OnTheGoAdapter searchadapter = null;
                if (query.toString().length() > 0) {

                    final List<Shop> shoplist = dbobject.getShopListDetailbyTitle(query.toString().toLowerCase().replaceAll(" ", "%"));

                    try {

                        //if the shop exist in search then set the adapter else don't do anything
                        if (shoplist != null) {


                            try {
                                //if shop available then set no shop to gone and recyclerview visible
                                no_shop_txt.setVisibility(View.GONE);
                                shop_list_recycler.setVisibility(View.VISIBLE);
                                //get the latitude and longitude from the gps tracker and set the distance
                                gpsTracker = new GPSTracker(Shoplistactivity.this);
                                if (!gpsTracker.canGetLocation()) {
                                    gpsTracker.showSettingsAlert();
                                } else {
                                    for (int idx = 0; idx < shoplist.size(); idx++) {
                                        Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(shoplist.get(idx).getLatitude()), Double.parseDouble(shoplist.get(idx).getLongitude()), shoplist.get(idx).getResults());
                                        shoplist.get(idx).setResults(shoplist.get(idx).getResults());
                                    }
                                }

                            } catch (Exception e) {
                            }
                            searchadapter = new OnTheGoAdapter(Shoplistactivity.this, shoplist, new OnTheGoAdapter.onItemClickListener() {
                                @Override
                                public void onItemClick(int view, int position) {
                                    //on click get the shop id and go to shop description details page
                                    Intent in = new Intent(Shoplistactivity.this, ShopDescriptionActivity.class);
                                    in.putExtra("ShopDetails", shoplist.get(position));
                                    startActivity(in);
                                }
                            });
                            shop_list_recycler.setAdapter(searchadapter);
                            adapter.notifyDataSetChanged();

                        } else {
                            //if no shop available then set no shop to visible and recyclerview gone
                            no_shop_txt.setVisibility(View.VISIBLE);
                            shop_list_recycler.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }


            }

        });

    }

    private void initVariable() {

        no_shop_txt = (TextView) findViewById(R.id.no_shop_txt);
        shop_list_recycler = (RecyclerView) findViewById(R.id.shop_list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        shop_list_recycler.setLayoutManager(layoutManager);
        toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        backImg = (ImageView) toolbar.findViewById(R.id.back_img);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        search_edit_text = (EditText) toolbar.findViewById(R.id.search_edit_text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        putdate();


    }

    void putdate() {

        if (shoplist != null) {
            shoplist.clear();
            current_shoplist.clear();
            tcurrent_shoplist.clear();
        }
        //get the shop details using service Id  if services is clicked if sub services is click get the sub services details

        String serviceId = getIntent().getStringExtra("current_service");
        String sub_serviceId = getIntent().getStringExtra("current_sub_service");
        if (serviceId != null) {
            shoplist = dbobject.getShopListByServiceId(serviceId);
        }
        if (sub_serviceId != null) {
            shoplist = dbobject.getShopListBySubServiceId(sub_serviceId);
        }


        try {
            for (int idx = 0; idx < shoplist.size(); idx++) {
                Shop shop = new Shop();
                Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(shoplist.get(idx).getLatitude()), Double.parseDouble(shoplist.get(idx).getLongitude()), shoplist.get(idx).getResults());
                shop.setResults(shoplist.get(idx).getResults());

                shoplist.add(shop);
                tcurrent_shoplist.add(shoplist.get(idx));
            }


        } catch (Exception e) {


            current_shoplist = shoplist;
        }


        //if there is not shop found then show the text with no Result found else don't show
        if (shoplist == null) {
            no_shop_txt.setText("No Result found");
            no_shop_txt.setVisibility(View.VISIBLE);
        } else {
            no_shop_txt.setVisibility(View.GONE);
            adapter = new ShopListAdapter(Shoplistactivity.this, tcurrent_shoplist, new ShopListAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {
                    Intent in = new Intent(Shoplistactivity.this, ShopDescriptionActivity.class);
                    in.putExtra("ShopDetails", tcurrent_shoplist.get(position));
                    startActivity(in);

                }
            });
            shop_list_recycler.setAdapter(adapter);
        }
    }


}
