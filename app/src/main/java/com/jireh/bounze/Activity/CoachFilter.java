package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.jireh.bounze.Adapter.FilterAdapter;
import com.jireh.bounze.Adapter.FilterByCoachAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Category;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.FilterModel;
import com.jireh.bounze.data.FilterService;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 06-10-2017.
 */

public class CoachFilter extends Activity {

    RecyclerView filter_recyclerview, filter_details_recycler;
    Toolbar toolbar;
    ImageView back_img;
    TextView title;
    DBFunctions dbFunctions;
    Button cancel_btn, apply_btn;
    ImageView reset_img;
    ArrayList<FilterModel> coachFree = new ArrayList<FilterModel>();
    ArrayList<FilterModel> coachService = new ArrayList<FilterModel>();
    ArrayList<FilterModel> coachLocations = new ArrayList<FilterModel>();
    ArrayList<FilterModel> coachRatings = new ArrayList<FilterModel>();
    EditText subservie_edit_text;
    CrystalRangeSeekbar rangeSeekbar;
    FilterByCoachAdapter filterByAdapter;
    RelativeLayout seekbar_layout;
    String minPrice = "";
    Integer maxPrice = 0,tempMaxPrice=0;
    boolean reset_price, reset_pressed;
    String groupPosition = "";
    int position = 0;
    ArrayList<String> filtertitle = new ArrayList<>();
    ArrayList<Integer> maxprice = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_layout);
        initVariable();

        //get the max price from the array list
        List<Coach> coachDetails = dbFunctions.getCoachListDB();
        for (int idx = 0; idx < coachDetails.size(); idx++) {
                  maxprice.add(Integer.parseInt(coachDetails.get(idx).getCost()));
        }
        maxPrice =  Collections.max(maxprice);
        //store the max price is temp to reset the price
        tempMaxPrice = maxPrice;
        setOnClickListener();
        //set the title
        title.setText("Coach Filter");
        //set the adapter for headings

        filtertitle.add("Coach");
        filtertitle.add("Services");
        filtertitle.add("Ratings");
        filtertitle.add("Price");
        filtertitle.add("Areas");
        //set the filter details layout and onclick the heading send position
        FilterAdapter adapter = new FilterAdapter(CoachFilter.this, filtertitle, new FilterAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {
                //call the method to get the filter details based on position
                setFilterDetails(position);


            }
        });
        filter_recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //initially set the group position to 0 and call the filterby  adapter
        setFilterDetails(position);


    }


    private void setFilterDetails(int pos) {

        //set visibility for the reset_btn
        reset_img.setVisibility(View.VISIBLE);

        //if the reset button is pressed then get the group position and reset the page else get the normal positon
        //and the call adapter accordingly
        if (reset_pressed == true) {
            position = pos;
            reset_pressed = false;
        } else {
            position = pos;
        }

        switch (position) {
            //case 0 for shop free
            case 0:
                //invisible rangeseek bar,actv, visible rv
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.GONE);


                //if user selected free trial then add two values in filter model and send to adapter to display
                ArrayList<String> allFree = new ArrayList<>();
                if (coachFree.size() > 0) {
                    coachFree.clear();

                }
                allFree.add("Free Trial");
                allFree.add("Bookmark");
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedItems = dbFunctions.getFilterCoachDetails("0");

                for (int idx = 0; idx < allFree.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(allFree.get(idx));
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedItems != null) {
                        for (int check = 0; check < checkedItems.size(); check++) {
                            if (idx == checkedItems.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    }
                    coachFree.add(model);
                }

                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "0";
                filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, groupPosition, coachFree, new FilterByCoachAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {


                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);

                break;

            case 1:
                //case 1 for shop sub services
                //visible autocomplete text view,rv gone for  rangebar
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.VISIBLE);
                subservie_edit_text.setHint(getResources().getString(R.string.cservice_hint));

                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedServiceItems = dbFunctions.getFilterCoachDetails("1");

                //get all category details and add in  filter model and send to adapter to display
                List<Category> coachcatlist = dbFunctions.getCoachCatAllListDB();


                if (coachService.size() > 0) {
                    coachService.clear();

                }

                for (int idx = 0; idx < coachcatlist.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(coachcatlist.get(idx).getTitle());
                    model.setSubServiceId(Integer.parseInt(coachcatlist.get(idx).getCid()));
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedServiceItems != null) {
                        for (int check = 0; check < checkedServiceItems.size(); check++) {
                            if (idx == checkedServiceItems.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    } else {
                        model.setCheckIt(false);
                    }
                    coachService.add(model);

                }


                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "1";
                filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, groupPosition, coachService, new FilterByCoachAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {
                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                //search based on the sub services and set the adatper
                addTextListener();
                break;
            case 2:
                //case 2 for ratings filter
                //invisible rangeseek bar,actv, visible rv
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.GONE);
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedRatings = dbFunctions.getFilterCoachDetails("2");

                //if user selected ratings then add two values  in filter model and send to adapter to display
                ArrayList<String> ratings = new ArrayList<>();
                if (coachRatings.size() > 0) {
                    coachRatings.clear();

                }
                ratings.add("Low - High");
                ratings.add("High - Low");
                for (int idx = 0; idx < ratings.size(); idx++) {
                    FilterModel model = new FilterModel();
                    model.setName(ratings.get(idx));
                    model.setPosition(idx);
                    //based on the checked position set true else make it false
                    //if the checkItems have values then make the loop
                    if (checkedRatings != null) {
                        for (int check = 0; check < checkedRatings.size(); check++) {
                            if (idx == checkedRatings.get(check).getCheckedPosition()) {
                                model.setCheckIt(true);
                            }
                        }
                    }
                    coachRatings.add(model);
                }
                //Send the flag to adapter to find whare it is called
                //set the filter by details
                groupPosition = "2";
                filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, groupPosition, coachRatings, new FilterByCoachAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {
                        filterByAdapter.notifyDataSetChanged();
                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                break;
            case 3:
                //case 3 for price
                //visible rangeseek bar and gone for rv and actv
                seekbar_layout.setVisibility(View.VISIBLE);
                filter_details_recycler.setVisibility(View.GONE);
                subservie_edit_text.setVisibility(View.GONE);
                rangeSeekbar.setMinValue(0.0f);
                rangeSeekbar.setMaxValue(Float.valueOf(maxPrice));
                getRangeSeekBar();
                //if reset pressed then reset the range seekbar set min and max value to default
                if (reset_price) {
                    rangeSeekbar.setMinStartValue(0.0f).setMaxStartValue(Float.valueOf(maxPrice)).apply();
                    reset_price = false;
                }
                break;

            case 4:
                //visible autocomplete text view,rv gone for  rangebar
                seekbar_layout.setVisibility(View.GONE);
                filter_details_recycler.setVisibility(View.VISIBLE);
                subservie_edit_text.setVisibility(View.VISIBLE);
                subservie_edit_text.setHint(getResources().getString(R.string.locations));
                setLocationsAdapter();
                //set text change listener for the location
                addLocationsTextListener();
                break;


        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        apply_btn.setEnabled(true);
    }

    private void getRangeSeekBar() {
        // get min and max text view
        final TextView tvMin = (TextView) findViewById(R.id.min_value_txt);
        final TextView tvMax = (TextView) findViewById(R.id.max_value_txt);

// set listener
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(getResources().getString(R.string.Rs) + minValue));
                tvMax.setText(String.valueOf(getResources().getString(R.string.Rs) + maxValue));
            }
        });

// set final value listener
        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
                minPrice = String.valueOf(minValue);
                tempMaxPrice = Integer.valueOf(String.valueOf(maxValue));
            }
        });
    }


    private void addTextListener() {


        subservie_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                //clear before adding the filter modellist
                if (coachService.size() > 0) {
                    coachService.clear();

                }
                query = query.toString().toLowerCase();
                List<Category> coachServiceList = dbFunctions.getCoachCatAllListDB();
                //get the selected values by the position from filter table
                ArrayList<FilterModel> checkedServiceItems = dbFunctions.getFilterCoachDetails("1");

                for (int idx = 0; idx < coachServiceList.size(); idx++) {
                    final String text = coachServiceList.get(idx).getTitle().toLowerCase();
                    if (text.contains(query)) {
                        FilterModel model = new FilterModel();
                        model.setName(coachServiceList.get(idx).getTitle());
                        model.setSubServiceId(Integer.parseInt(coachServiceList.get(idx).getCid()));
                        model.setPosition(idx);
                        //based on the checked position set true else make it false
                        //if the checkItems have values then make the loop
                        if (checkedServiceItems != null) {
                            for (int check = 0; check < checkedServiceItems.size(); check++) {
                                if (idx == checkedServiceItems.get(check).getCheckedPosition()) {
                                    model.setCheckIt(true);
                                }
                            }
                        }
                        coachService.add(model);

                    }
                }


                //set the filter by details
                filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, "1", coachService, new FilterByCoachAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, String groupPositon, int position) {

                    }
                });
                filter_details_recycler.setAdapter(filterByAdapter);
                filterByAdapter.notifyDataSetChanged();  // data set changed
            }
        });

    }


    private void addLocationsTextListener() {


        subservie_edit_text.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                if (query.length() > 0) {


                    if (coachLocations.size() > 0) {
                        coachLocations.clear();
                    }
                    //get all amenities and add in  filter model and send to adapter to display
                    List<ResultModel> locationsList = dbFunctions.getLocationsListDB();
                    //get the selected values by the position from filter table
                    ArrayList<FilterModel> checkedLocationsItems = dbFunctions.getFilterCoachDetails("4");

                    for (int idx = 0; idx < locationsList.size(); idx++) {

                        final String text = locationsList.get(idx).getLocation().toLowerCase();


                        if (text.contains(query)) {

                            FilterModel model = new FilterModel();
                            model.setLocation(locationsList.get(idx).getLocation());
                            model.setId(locationsList.get(idx).getId());
                            model.setPosition(idx);
                            //based on the checked position set true else make it false
                            //if the checkItems have values then make the loop
                            if (checkedLocationsItems != null) {
                                for (int check = 0; check < checkedLocationsItems.size(); check++) {
                                    if (idx == checkedLocationsItems.get(check).getCheckedPosition()) {
                                        model.setCheckIt(true);
                                    }
                                }
                            }
                            coachLocations.add(model);
                        }
                    }
                    //set the filter by details
                    groupPosition = "4";
                    filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, groupPosition, coachLocations, new FilterByCoachAdapter.onItemClickListener() {
                        @Override
                        public void onItemClick(int view, String groupPositon, int position) {

                        }
                    });
                    filter_details_recycler.setAdapter(filterByAdapter);
                    filterByAdapter.notifyDataSetChanged();  // data set changed

                } else {
                    setLocationsAdapter();
                }


            }
        });

    }

    private void setLocationsAdapter() {
        //clear if the shop subservice size is greater than 0
        if (coachLocations.size() > 0) {
            coachLocations.clear();

        }
        //get the selected values by the position from filter table
        ArrayList<FilterModel> checkedLocationsItems = dbFunctions.getFilterCoachDetails("4");

        //get all amenities and add in  filter model and send to adapter to display
        List<ResultModel> locationsList = dbFunctions.getLocationsListDB();
        for (int idx = 0; idx < locationsList.size(); idx++) {
            FilterModel model = new FilterModel();

            model.setId(locationsList.get(idx).getId());
            model.setLocation(locationsList.get(idx).getLocation());
            model.setPosition(idx);
            //based on the checked position set true else make it false
            //if the checkItems have values then make the loop
            if (checkedLocationsItems != null) {
                for (int check = 0; check < checkedLocationsItems.size(); check++) {
                    if (idx == checkedLocationsItems.get(check).getCheckedPosition()) {
                        model.setCheckIt(true);
                    }
                }
            }
            coachLocations.add(model);

        }
        //set the filter by details
        groupPosition = "4";
        filterByAdapter = new FilterByCoachAdapter(CoachFilter.this, groupPosition, coachLocations, new FilterByCoachAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, String groupPositon, int position) {

            }
        });
        filter_details_recycler.setAdapter(filterByAdapter);
        filterByAdapter.notifyDataSetChanged();  // data set changed
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        reset_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete all the details in the filter table
                //set the range seekbar to min and max values
                reset_price = true;
                reset_pressed = true;
                dbFunctions.deleteFilterCDBAllData();

                //set the filter details layout
                FilterAdapter adapter = new FilterAdapter(CoachFilter.this, filtertitle, new FilterAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, int position) {
                        setFilterDetails(position);


                    }
                });
                filter_recyclerview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                //reset the page which is visible to user.
                setFilterDetails(0);
                //show the reset toast
                Toast.makeText(CoachFilter.this,"Reset Successfully",Toast.LENGTH_SHORT).show();
            }
        });
        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set anable false to apply btn
                apply_btn.setEnabled(false);

                //check the internet connection if it is available process else show toast
                if (isInternetOn()) {

                    //get the values from the db to get filter based on shop
                    String freeTrial = "", freeReg = "", bookmark = "";
                    List<FilterModel> mlShop = dbFunctions.getFilterCoachDetails("0");
                    if (mlShop != null) {
                        for (int idx = 0; idx < mlShop.size(); idx++) {
                            if (mlShop.get(idx).getCheckedPosition() == 0) {
                                freeTrial = mlShop.get(idx).getValue();
                            } else if (mlShop.get(idx).getCheckedPosition() == 1) {
                                bookmark = mlShop.get(idx).getValue();
                            }

                        }
                    }
                    //get the values from the db to get filter based on serviceid's
                    String subServiceIds = "";
                    List<FilterModel> mlSubService = dbFunctions.getFilterCoachDetails("1");
                    if (mlSubService != null) {
                        for (int idx = 0; idx < mlSubService.size(); idx++) {
                            subServiceIds = subServiceIds + mlSubService.get(idx).getValue() + ",";

                        }
                        subServiceIds = subServiceIds.substring(0, subServiceIds.length() - 1);
                    }

                    //get the values from the db to get filter based on ratings
                    String ratings = "";
                    List<FilterModel> mlRatings = dbFunctions.getFilterCoachDetails("2");
                    if (mlRatings != null) {
                        for (int idx = 0; idx < mlRatings.size(); idx++) {
                            ratings = mlRatings.get(idx).getValue();

                        }
                    }

                    //get the values from the db to get filter based on locations
                    String locations = "";
                    List<FilterModel> mlLocations = dbFunctions.getFilterCoachDetails("4");
                    if (mlLocations != null) {
                        for (int idx = 0; idx < mlLocations.size(); idx++) {
                            locations = locations + mlLocations.get(idx).getValue() + ",";

                        }
                        locations = locations.substring(0, locations.length() - 1);
                    }

                    //call the service to get the filtered shops based on the selection
                    serviceCall(freeTrial, bookmark, subServiceIds, ratings, minPrice, tempMaxPrice, locations);

                } else {
                    Toast.makeText(CoachFilter.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void serviceCall(String freeTrial, String bookmark,
                             String subServiceIds, final String ratings, String price_min, Integer price_max, String locations) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<FilterService> call = stationClient.filterCoach(freeTrial, bookmark, subServiceIds, ratings, price_min, String.valueOf(price_max), locations, Prefs.getString(AppConstants.UserId, ""));
        System.out.println(" filter result::ft::" + freeTrial + "bm::" + bookmark
                + "ssv::" + subServiceIds + "rat::" + ratings + "prmax::" + price_min + "prmin::" + price_max + "am::" + "loc::" + locations);
        call.enqueue(new Callback<FilterService>() {
            @Override
            public void onResponse(Call<FilterService> call, Response<FilterService> response) {

                FilterService result = response.body();
                if (result.getResult().get(0).getStatus() == 0) {
                    Toast.makeText(CoachFilter.this, "No coach found", Toast.LENGTH_SHORT).show();
                    //set anable true to apply btn
                    apply_btn.setEnabled(true);
                } else {

                    //get the coach details based on the result and send to shopfilterlist class to display it
                    List<Coach> coachList = dbFunctions.getCoachListByIdDB(result.getResult().get(0).getCoach_id(), ratings);
                    Intent in = new Intent(CoachFilter.this, AllFilterResult.class);
                    in.putExtra("FilterFlag", "coach");
                    in.putExtra("CoachList", (Serializable) coachList);
                    startActivity(in);
                }

            }

            @Override
            public void onFailure(Call<FilterService> call, Throwable t) {
                Toast.makeText(CoachFilter.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }


    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        reset_img = (ImageView) toolbar.findViewById(R.id.reset_img);
        seekbar_layout = (RelativeLayout) findViewById(R.id.seekbar_layout);
        dbFunctions = new DBFunctions(CoachFilter.this);

        filter_recyclerview = (RecyclerView) findViewById(R.id.filter_recyclerview);
        filter_details_recycler = (RecyclerView) findViewById(R.id.filter_details_recycler);

        LinearLayoutManager manager = new LinearLayoutManager(CoachFilter.this);
        filter_recyclerview.setLayoutManager(manager);

        LinearLayoutManager filter_bymanager = new LinearLayoutManager(CoachFilter.this);
        filter_details_recycler.setLayoutManager(filter_bymanager);

        cancel_btn = (Button) findViewById(R.id.cancel_btn);
        apply_btn = (Button) findViewById(R.id.apply_btn);

        subservie_edit_text = (EditText) findViewById(R.id.subservie_edit_text);
        rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.range_seek_bar);
    }
}
