package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Adapter.AllreviewsAdapter;
import com.jireh.bounze.Adapter.EventBatchAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.BatchTime;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 07-10-2017.
 */

public class EventDescriptionActivity extends Activity {

    TextView location_address_txt, description_txt, event_name_txt, event_time_txt, event_otime_txt, event_price_txt;
    ImageView squareimage;
    Toolbar toolbar;
    TextView title_txt;
    ImageView back_img;
    ImageView direction_img, bookmark_img, share_img, rating_img, call_img;
    Button book_btn;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    boolean isBookMarked = false;
    LinearLayout ll_bookmark, ll_bookmark_select;
    // private ProgressDialog mProgressDialog;
    ProgressBar progressBar;
    RecyclerView reviews_recycler, event_batch_recycler;
    BatchTime sBatchDetails;
    List<Review_> myReview;
    Event eventDetails;
    LinearLayout event_details_layout;
    DBFunctions dbobject;
    int clickcount = 0;
    List<BatchTime> batchTDetails;
    private ArrayList<Event> bookmarkList;
    private SessionManager sessionManager;
    private boolean bookmarkFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_description);
        intiVariable();
        dbobject = new DBFunctions(getApplicationContext());
        //get data from the url else load from the database
        //get event details
        eventDetails = (Event) getIntent().getSerializableExtra("EventDetails");
        //get the user reviews for the event
        setReviews();


        //set the event batch adapter
        LinearLayoutManager manager = new LinearLayoutManager(EventDescriptionActivity.this, LinearLayoutManager.HORIZONTAL, false);
        event_batch_recycler.setLayoutManager(manager);
        //get the batch details using the query from db and set the adapter
        batchTDetails = dbobject.getEventTypeById(eventDetails.getEid());

        if (batchTDetails != null) {
            event_details_layout.setVisibility(View.VISIBLE);
            EventBatchAdapter adapter = new EventBatchAdapter(EventDescriptionActivity.this, batchTDetails, new EventBatchAdapter.onItemBatchClickListener() {
                @Override
                public void onItemBatchClick(CardView batch_layout, int position, Activity activity) {


                    sBatchDetails = batchTDetails.get(position);
                }
            });
            event_batch_recycler.setAdapter(adapter);
        }


        //set values
        title_txt.setText(eventDetails.getName());

        String strings[] = eventDetails.getAddress().split(",");

        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].trim();
            strings[i] += ",\n";
        }
        String address = "";

        for (int i = 0; i < strings.length; i++)
            address += strings[i];

        location_address_txt.setText(address + "\n" + eventDetails.getLocation());
        description_txt.setText(eventDetails.getDescription());
        event_name_txt.setText(eventDetails.getName());
        //change the date format before display
       /* String startTime = changeDateFormat(eventDetails.gets());
        String endTime = changeDateFormat(eventDetails.getEndDate());
        event_time_txt.setText(startTime + " to " + endTime);
        event_otime_txt.setText(eventDetails.getTime());*/
        Picasso.with(getApplicationContext()).load(BASE_URL + eventDetails.getImage()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);

        event_price_txt.setText(eventDetails.getCost());


        setOnClickListener();

        //initialize the session manager
        sessionManager = new SessionManager(EventDescriptionActivity.this);
        //if it is already bookmarked make the change bg color else don't
        checkBookmarked();
    }

    private void setReviews() {

        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.getEventReviews(eventDetails.getEid());
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(final Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    myReview = result.getReviews();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }

    private void checkBookmarked() {

        bookmarkList = sessionManager.getBookmarkEvent();
        bookmarkFlag = true;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (bookmarkList.get(tempPos).getEid().equals(eventDetails.getEid())) {
                bookmarkFlag = false;
                break;
            }
        }
        if (bookmarkFlag)       //Not yet bookmarked.
        {
            bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else                   //Already bookmarked.
        {
            bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
        }

    }


    private String changeDateFormat(String time) {

        String inputPattern = "dd/MM/yyyy";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;


    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        direction_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the mapview between your current location and the shop location
                GPSTracker gpsTracker = new GPSTracker(EventDescriptionActivity.this);
                if (!gpsTracker.canGetLocation())
                    gpsTracker.showSettingsAlert();
                else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + gpsTracker.getLatitude() + "," + gpsTracker.getLongitude() + "&daddr=" + eventDetails.getLatitude() + "," + eventDetails.getLongitude()));
                    startActivity(intent);
                }
            }
        });
        //send the bookmark details to server
        bookmark_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetOn()) {
                    //check if already it is bookmarked on click remove else bookmark

                    bookmarkList = sessionManager.getBookmarkEvent();
                    bookmarkFlag = true;
                    for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                        if (bookmarkList.get(tempPos).getEid().equals(eventDetails.getEid())) {
                            bookmarkFlag = false;
                            break;
                        }
                    }
                    if (bookmarkFlag) {
                        Event pojo = new Event();
                        pojo.setEid(eventDetails.getEid());
                        pojo.setName(eventDetails.getName());
                        bookmarkList.add(bookmarkList.size(), pojo);
                        sessionManager.setBookmarkEvent(bookmarkList);
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));

                        //showProgressDialog();
                        //bookmark the page
                        eventBookmark("1");

                    } else {
                        showRemoveBookmarkAlert();
                    }


                } else {
                    Toast.makeText(EventDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }


            }
        });
        //show the sharing page
        share_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the event description if length >40 show it else directly show it
                int desc_length = eventDetails.getDescription().length();
                String desc = eventDetails.getDescription();
                if (desc_length > 40) {
                    desc = eventDetails.getDescription().substring(0,40);
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, eventDetails.getName() + "\n" + eventDetails.getAddress().trim() + "\n" + eventDetails.getEventType().trim() + "\n" + desc + "..."
                        + "\n\n" + "https://www.bounze.in/event_programs.php?eid=" + eventDetails.getEid()
                        + "\n\n" + "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n" + "https://play.google.com/store/apps/details?id=com.triton.bounze" );
                //shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/eventdescription/" + eventDetails.getEid());
                startActivity(Intent.createChooser(shareIntent, "Share Using "));
            }
        });

        //post the ratings for the event

        rating_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                //show the dialog with review and all reviews for the shop
                android.app.FragmentManager fm = getFragmentManager();
                ShowReviewFragment tv = new ShowReviewFragment(myReview, null,null,eventDetails,null,null);

                tv.show(fm, "TV_tag");*/

                //showRatingAlert();

                // TODO Auto-generated method stub
                clickcount = clickcount + 1;
                if (clickcount % 2 == 1) {
                    //SET visibility visible for the recycler review
                    reviews_recycler.setVisibility(View.VISIBLE);
                    //check for my reviews if available show else show no reviews ound

                    if (myReview != null) {
                        //on single click show the all review and on second click show the rating bar alert
                        AllreviewsAdapter adapter = new AllreviewsAdapter(EventDescriptionActivity.this, myReview);
                        reviews_recycler.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "No reviews found", Toast.LENGTH_LONG).show();
                    }

                    //first time clicked to do this
                    Toast.makeText(getApplicationContext(), "Click again to write a review", Toast.LENGTH_LONG).show();
                } else {
                    //show the rating alert in android  and close the recyclerview in android
                    reviews_recycler.setVisibility(View.GONE);
                    showRatingAlert();
                    //check how many times clicked and so on
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();
                }

            }

        });
        //call the branch phone no
        call_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //show alert dialog to confirm call
                showConfirmationDialog();


            }
        });

        book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if the event has batch or not if the event has no batch then direclty show the dialog
                if (batchTDetails == null) {
                    //move to payment page
                    Intent intent = new Intent(EventDescriptionActivity.this, Pay.class);
                    intent.putExtra("PayFlag", "event");
                    intent.putExtra("EventDetails", (Serializable) eventDetails);
                    intent.putExtra("EventBatchTime", sBatchDetails);
                    startActivity(intent);



                   // showProcessDialog();
                }
                //check validation if the batch is selected and the event has batch else post a toast
                else if (sBatchDetails != null && batchTDetails != null) {

                    //move to payment page
                    Intent intent = new Intent(EventDescriptionActivity.this, Pay.class);
                    intent.putExtra("PayFlag", "event");
                    intent.putExtra("EventDetails", (Serializable) eventDetails);
                    intent.putExtra("EventBatchTime", sBatchDetails);
                    startActivity(intent);

                //    showProcessDialog();
                } else {
                    Toast.makeText(EventDescriptionActivity.this, "Please select a batch", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void showProcessDialog() {
        //show the dialog to share the event and then book it
        AlertDialog.Builder mBuild = new AlertDialog.Builder(EventDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.event_booking_dialog, null);

        Button proceed_btn = (Button) mView.findViewById(R.id.proceed_btn);

        final EditText username = (EditText) mView.findViewById(R.id.username_edt);
        final EditText emailid = (EditText) mView.findViewById(R.id.email_edt);
        final EditText mobile = (EditText) mView.findViewById(R.id.mobile_edt);


        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.setCancelable(true);
        dialog.show();

        proceed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getText().toString().matches("")) {
                    username.setError("Enter name");
                } else if (!(emailid.getText().toString().matches(emailPattern))) {
                    emailid.setError("Enter valid E-mail Id");
                    ;
                } else if (mobile.getText().toString().matches("") || mobile.getText().toString().length() < 10 || mobile.getText().toString().length() > 10) {
                    mobile.setError("Enter valid mobile no");
                } else {

                    dialog.dismiss();

                    //move to payment page
                    Intent intent = new Intent(EventDescriptionActivity.this, Pay.class);
                    intent.putExtra("PayFlag", "event");
                    intent.putExtra("EventDetails", (Serializable) eventDetails);
                    intent.putExtra("EventBatchTime", sBatchDetails);
                    startActivity(intent);


                }
            }
        });
    }

    private void showRatingAlert() {

        AlertDialog.Builder mBuild = new AlertDialog.Builder(EventDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.rate_bar_dailog, null);
        final RatingBar ratebar = (RatingBar) mView.findViewById(R.id.ratingBar);
        final EditText review = (EditText) mView.findViewById(R.id.comments_edt);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        tv_title.setText("Tell us what you think?");
        ratebar.setRating(Float.valueOf(eventDetails.getAvgratings()));
        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                doRating("" + ratebar.getRating(), review);

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    private void doRating(String rating, EditText review) {

        if (isInternetOn()) {
            showProgressDialog();
            event_ratings(rating, review);
        } else {
            Toast.makeText(EventDescriptionActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void event_ratings(String rating, EditText review) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), "", eventDetails.getEid(), "", "", review.getText().toString(), rating);
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();

                if (response_code == 200) {
                    //if submitted once show the reiew submitted toast else show the message from server
                    if (result.getStatus().get(0).getStatus().equals("1")) {
                        hideProgressDialog();
                        Toast.makeText(EventDescriptionActivity.this, "Review submitted successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(EventDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if (response_code == 400) {
                    Toast.makeText(EventDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(EventDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                    Log.e("m", "500 error");
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Toast.makeText(EventDescriptionActivity.this, "Server Auto Maintanence is on Progress! Please try after some times", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void showConfirmationDialog() {


        AlertDialog.Builder mBuild = new AlertDialog.Builder(EventDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Call confirmation");
        tv_msg.setText("Are you sure, you want to call?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(EventDescriptionActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(EventDescriptionActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                10);
                    }
                } else {

                    if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                }
                try {
                    callIntent.setData(Uri.parse("tel:" + "08026725115"));
                    startActivity(callIntent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    private void showRemoveBookmarkAlert() {

        AlertDialog.Builder mBuild = new AlertDialog.Builder(EventDescriptionActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Bookmark");
        tv_msg.setText("Are you sure, you want to remove the Bookmark?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //remove the bookmark event from the shared preference
                Event pojo = new Event();
                pojo.setEid(eventDetails.getEid());
                pojo.setName(eventDetails.getName());
                int removePos = 0;
                for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                    if (eventDetails.getEid().equals(bookmarkList.get(tempPos).getEid())) {
                        removePos = tempPos;
                    }
                }
                bookmarkList.remove(removePos);
                sessionManager.setBookmarkEvent(bookmarkList);
                bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));


                eventBookmark("2");
                dialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    private void eventBookmark(final String bookmark) {
        //bookmark the event idd by using user id
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call;

        call = stationClient.create_EventBookmark(Prefs.getString(AppConstants.UserId, ""), eventDetails.getEid(), bookmark);
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                final LogIn result = response.body();

                if (result.getStatus().get(0).getStatus() == 1) {

                    Toast.makeText(getBaseContext(), result.getStatus().get(0).getMessage().toString(), Toast.LENGTH_SHORT).show();
                    //make the book mark as true if it is remove make it false
                    if (bookmark.equals("1")) {
                        isBookMarked = true;
                        bookmark_img.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));
                    } else {
                        isBookMarked = false;
                        bookmark_img.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    }
                    hideProgressDialog();

                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(EventDescriptionActivity.this, result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(EventDescriptionActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 10: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(EventDescriptionActivity.this, "permission granted", Toast.LENGTH_SHORT).show();

                } else {

                    ActivityCompat.requestPermissions(EventDescriptionActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            10);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progressBar.setVisibility(View.GONE);
    }


    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    private void intiVariable() {
        toolbar = (Toolbar) findViewById(R.id.rl_toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) findViewById(R.id.title_txt);
        squareimage = (ImageView) findViewById(R.id.squareimage);
        direction_img = (ImageView) findViewById(R.id.direction_img);
        bookmark_img = (ImageView) findViewById(R.id.bookmark_img);
        share_img = (ImageView) findViewById(R.id.share_img);
        rating_img = (ImageView) findViewById(R.id.rating_img);
        call_img = (ImageView) findViewById(R.id.call_img);
        location_address_txt = (TextView) findViewById(R.id.location_address_txt);
        description_txt = (TextView) findViewById(R.id.description_txt);
        event_otime_txt = (TextView) findViewById(R.id.event_otime_txt);
        event_name_txt = (TextView) findViewById(R.id.event_name_txt);
        event_time_txt = (TextView) findViewById(R.id.event_time_txt);
        event_price_txt = (TextView) findViewById(R.id.event_price_txt);
        book_btn = (Button) findViewById(R.id.book_btn);
        ll_bookmark = (LinearLayout) findViewById(R.id.ll_bookmark);
        ll_bookmark_select = (LinearLayout) findViewById(R.id.ll_bookmark_select);
        event_batch_recycler = (RecyclerView) findViewById(R.id.event_batch_recycler);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        event_details_layout = (LinearLayout) findViewById(R.id.event_details_layout);
        reviews_recycler = (RecyclerView) findViewById(R.id.reviews_recycler);
        LinearLayoutManager rlayoutManager = new LinearLayoutManager(EventDescriptionActivity.this, LinearLayout.HORIZONTAL, false);
        reviews_recycler.setLayoutManager(rlayoutManager);
    }
}
