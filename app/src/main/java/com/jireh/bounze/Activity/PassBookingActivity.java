package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.jireh.bounze.Fragment.CurrentPassBookingFragment;
import com.jireh.bounze.Fragment.PastPassBookingFragment;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.OkHttp3Connection;
import com.jireh.bounze.Util.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PassBookingActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView title_txt;
    LinearLayout mTabsLinearLayout;
    int currentPosition;
    //private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    private SessionManager sessionManager;
    private PassBookingAdapter adapter;
    private ViewPager pager;
    private LinearLayout ll_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_booking);
        initVariable();
        getData();
    }

    private void initVariable() {
        sessionManager = new SessionManager(PassBookingActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        title_txt.setText("Bookings");
        // Initialize the ViewPager and set an adapter
        pager = (ViewPager) findViewById(R.id.pager);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        adapter = new PassBookingAdapter(getSupportFragmentManager());

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    /**
     * This method is used for setting the view pager and tabs.
     *
     * @param result
     */
    private void setViewPager(String result) {
        //Current pass booking history.
        CurrentPassBookingFragment currentPassFragment = new CurrentPassBookingFragment();
        Bundle currentBundle = new Bundle();
        currentBundle.putString("current_pass", result);
        currentPassFragment.setArguments(currentBundle);
        adapter.addFrag(currentPassFragment, "Going On");

        //Past pass booking history.
        PastPassBookingFragment pastPassFragment = new PastPassBookingFragment();
        Bundle pastBundle = new Bundle();
        pastBundle.putString("past_pass", result);
        pastPassFragment.setArguments(pastBundle);
        adapter.addFrag(pastPassFragment, "Completed");

        pager.setAdapter(adapter);

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tab_layout);
        tabs.setViewPager(pager);
        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");
        tabs.setTypeface(typeFace, 0);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    /**
     * <h3>getData</h3>
     * This method just check internet connected or not, if it is connected so make a service call or else show
     * message that internet is not working.
     */
    private void getData() {
        if (isInternetOn()) {
            showProgressDialog();
            getCompletePassData(sessionManager.getUserId());
        } else {
            Toast.makeText(PassBookingActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    private void showProgressDialog() {

        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    /**
     * <h3>bookPass</h3>
     * This method is used for booking the current pass.
     *
     * @param userId contains the user id.
     */
    /*private void getCompletePassData(String userId) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassCompleteHistoryPojo> call = stationClient.getAllPassHistory(userId);

        call.enqueue(new Callback<PassCompleteHistoryPojo>() {
            @Override
            public void onResponse(Call<PassCompleteHistoryPojo> call, Response<PassCompleteHistoryPojo> response) {
                int response_code = response.code();
                PassCompleteHistoryPojo result = response.body();
                if (response_code == 200) {
//                    if (result.getBooking()[0].getStatus() == 1) {
                    setViewPager(result);
//                        Toast.makeText(PassBookingActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                    *//*}
                    else if (result.getBooking()[0].getStatus() == 0) {
                        Toast.makeText(PassBookingActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }*//*
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(PassBookingActivity.this, "Response Code: "+response_code+" ,Error: Data not found.", Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<PassCompleteHistoryPojo> call, Throwable t) {
                Toast.makeText(PassBookingActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }*/
    private void getCompletePassData(String userId) {
        //String url = "https://bounze.in/demo/api/pass_user_history.php?user_id=" + sessionManager.getUserId();
        String url = "http://ec2-13-126-230-103.ap-south-1.compute.amazonaws.com/api/pass_user_history.php?user_id=" + sessionManager.getUserId();
        // String url = "https://192.168.1.111/bounze/api/pass_user_history.php?user_id=" + sessionManager.getUserId();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", sessionManager.getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttp3Connection.doOkHttp3Connection("", url, OkHttp3Connection.Request_type.GET, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d("value of ", " response: " + result);

                try {
                    JSONObject jsonRes = new JSONObject(result);
                    JSONArray array = jsonRes.getJSONArray("passhistory");
                    for (int idx = 0; idx < array.length(); idx++) {
                        if (array.getJSONObject(0).has("status")) {
                            Toast.makeText(PassBookingActivity.this, array.getJSONObject(0).getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            setViewPager(result);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideProgressDialog();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(PassBookingActivity.this, error, Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
        /*ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassCompleteHistoryPojo> call = stationClient.getAllPassHistory(userId);

        call.enqueue(new Callback<PassCompleteHistoryPojo>() {
            @Override
            public void onResponse(Call<PassCompleteHistoryPojo> call, Response<PassCompleteHistoryPojo> response) {
                int response_code = response.code();
                PassCompleteHistoryPojo result = response.body();
                if (response_code == 200) {
//                    if (result.getBooking()[0].getStatus() == 1) {
                    setViewPager(result);
//                        Toast.makeText(PassBookingActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                    *//*}
                    else if (result.getBooking()[0].getStatus() == 0) {
                        Toast.makeText(PassBookingActivity.this, result.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }*//*
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(PassBookingActivity.this, "Response Code: "+response_code+" ,Error: Data not found.", Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<PassCompleteHistoryPojo> call, Throwable t) {
                Toast.makeText(PassBookingActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });*/
    }

    private class PassBookingAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.CustomTabProvider {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int icons[] = {R.drawable.history, R.drawable.upcoming_event};

        public PassBookingAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            LinearLayout materialRippleLayout = (LinearLayout) LayoutInflater.from(PassBookingActivity.this).inflate(R.layout.pager_sliding_tab_custom_tab, parent, false);
            ((ImageView) materialRippleLayout.findViewById(R.id.image)).setImageResource(icons[position]);
            TextView title_txt = (TextView) materialRippleLayout.findViewById(R.id.title_txt);
            title_txt.setText(mFragmentTitleList.get(position));
            return materialRippleLayout;
        }

        @Override
        public void tabSelected(View tab) {
            LinearLayout layout = (LinearLayout) tab;
            TextView title = (TextView) layout.findViewById(R.id.title_txt);
            title.setTextColor(getResources().getColor(R.color.black));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.black),
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);
        }

        @Override
        public void tabUnselected(View tab) {
            LinearLayout layout = (LinearLayout) tab;
            TextView title = (TextView) layout.findViewById(R.id.title_txt);
            title.setTextColor(getResources().getColor(R.color.white));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);
        }
    }
}



