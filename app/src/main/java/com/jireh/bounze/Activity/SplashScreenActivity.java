package com.jireh.bounze.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.CheckConnectivity;
import com.jireh.bounze.Util.Config;
import com.jireh.bounze.Util.MyVideoView;
import com.jireh.bounze.Util.NotificationUtils;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.BlogData;
import com.jireh.bounze.data.BlogPojo;
import com.jireh.bounze.data.Category;
import com.jireh.bounze.data.City;
import com.jireh.bounze.data.CityList;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachList;
import com.jireh.bounze.data.Coachcategory;
import com.jireh.bounze.data.Content;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.Events;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassPojo;
import com.jireh.bounze.data.Program;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Search;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.Shoplist;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 16-08-2017.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private static final int REQUEST_PHONE_STATE = 1;
    /*
    * booking an program
    * */
    public static String programrowid = "";
    public static String cprogramrowid = "";
    // public static String BASE_URL = "https://bounze.in/demo/api/";
    // public static String BASE_URL = "https://192.168.1.111/bounze/api/";
    public static String BASE_URL = "http://ec2-13-126-230-103.ap-south-1.compute.amazonaws.com/api/";
    public static Location current_loc;
    public static String current_coachcat = "";
    public static String current_shop;
    public List<Coach> coachlist;
    public List<City> citylist;
    public List<Category> coachcatlist;
    public List<Program> coachprolist;
    public List<Event> eventlist;
    public List<Shop> shoplist;
    public List<Service> servicelist;
    TextView progress;
    DBFunctions dbobject;
    CheckConnectivity checkConnectivity;
    AlertDialog ad;
    SplashScreenActivity m;
    RelativeLayout no_connection_layout, connection_layout;
    // private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    ImageView bounze_image;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SessionManager sessionManager;
    private MyVideoView myVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.AppTheme1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        bounze_image = (ImageView) findViewById(R.id.bounze_image);
        no_connection_layout = (RelativeLayout) findViewById(R.id.no_connection_layout);
        connection_layout = (RelativeLayout) findViewById(R.id.connection_layout);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        try {
            if (!FirebaseApp.getApps(this).isEmpty()) {
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            }

            FirebaseMessaging.getInstance().subscribeToTopic("test");
            FirebaseInstanceId.getInstance().getToken();

        } catch (Exception e) {
            e.printStackTrace();
        }
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    //new Push notification is received.
                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        sessionManager = new SessionManager(SplashScreenActivity.this);
        storeFirebaseRegId();
        dbobject = new DBFunctions(getApplicationContext());
        m = this;
        //check internet connectivity
        progress = (TextView) findViewById(R.id.progress);
        checkConnectivity = CheckConnectivity.getInstance(getApplicationContext());
        if (checkConnectivity.isOnline(SplashScreenActivity.this)) {
            try {
                if (!CheckConnectivity.isfast) {
                    // show the first time helper screen when the internet is slow
                    SharedPreferences runCheck = getSharedPreferences("hasRunBefore", 0); //load the preferences
                    Boolean hasRun = runCheck.getBoolean("hasRun", false); //see if it's run before, default no
                    if (!hasRun) {
                        SharedPreferences settings = getSharedPreferences("hasRunBefore", 0);
                        SharedPreferences.Editor edit = settings.edit();
                        edit.putBoolean("hasRun", true); //set to has run
                        edit.commit(); //apply
                        new Thread(new Runnable() {
                            public void run() {
                                //    getVersion();
                            }
                        }).start();
                        //call the helper screen class
                        Intent in = new Intent(SplashScreenActivity.this, Helper.class);
                        startActivity(in);
                        finish();

                    }
                    showslowconnection(this);
                } else {

                    SharedPreferences runCheck = getSharedPreferences("hasRunBefore", 0); //load the preferences
                    Boolean hasRun = runCheck.getBoolean("hasRun", false); //see if it's run before, default no
                    if (!hasRun) {
                        SharedPreferences settings = getSharedPreferences("hasRunBefore", 0);
                        SharedPreferences.Editor edit = settings.edit();
                        edit.putBoolean("hasRun", true); //set to has run
                        edit.commit(); //apply
                        new Thread(new Runnable() {
                            public void run() {
                                //    getVersion();
                            }
                        }).start();
                        //call the helper screen class
                        Intent in = new Intent(SplashScreenActivity.this, Helper.class);
                        startActivity(in);
                        finish();

                    } else {

                        new CountDownTimer(2000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                //udate ratings from server and insert into reviews
                                getShopRatings();
                            }

                            @Override
                            public void onFinish() {
                                if (isInternetOn()) {
                                    //every time check the version after first download
                                    getVersion();


                                } else {
                                    no_connection_layout.setVisibility(View.VISIBLE);
                                    connection_layout.setVisibility(View.GONE);
                                    bounze_image.setVisibility(View.VISIBLE);
                                }
                            }
                        }.start();

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            no_connection_layout.setVisibility(View.VISIBLE);
            connection_layout.setVisibility(View.GONE);
        }

    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null)
                return netInfo.isConnected();
            else
                return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;

    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void storeFirebaseRegId() {
        SessionManager sessionManager = new SessionManager(this);
        String regId = sessionManager.getFCMId();
        System.out.println("registrationid::" + regId);

    }

    @Override
    protected void onResume() {
        super.onResume();
        doBroadCast();
    }

    private void doBroadCast() {
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

 /*   private void checkForPhoneStatePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(SplashScreenActivity.this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this,
                        Manifest.permission.READ_PHONE_STATE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    showPermissionMessage();

                } else {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(SplashScreenActivity.this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            REQUEST_PHONE_STATE);
                }
            }
        } else {

            getVersion();
        }
    }*/

    private void showPermissionMessage() {
        new AlertDialog.Builder(this)
                .setTitle("Read phone state")
                .setMessage("This app requires the permission to read phone state to continue")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(SplashScreenActivity.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                REQUEST_PHONE_STATE);
                    }
                }).create().show();
    }


    private void showslowconnection(Context context) {
        ad = new AlertDialog.Builder(SplashScreenActivity.this)
                .setMessage("Your Network connection is slow, some shops  may not load properly")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setContentView(R.layout.splash_screen);

                       /* Pushnotification push = new Pushnotification(SplashScreenActivity.this);
                        push.registerWithGCM();*/
                        if (isInternetOn()) {
                            //every time check the version after first download
                            getVersion();

                        }

                    }
                }).show();
    }

    private void getVersion() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Search> call = stationClient.getCurrentVersion();
        call.enqueue(new Callback<Search>() {

            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                int response_code = response.code();
                Search result = response.body();
                hideProgressDialog();
                if (response_code == 200) {
                    //get the result array and then get the version
                    List<ResultModel> check = result.getStatus();
                    ResultModel check1 = check.get(0);

                    String value = "" + check1.getStatus();

                    if (value.equals("1")) {
                        //check if the current version and before version are same if yes don't download data else download
                        if (!Prefs.getString(AppConstants.version, "").equals(check1.getVersion())) {
                            Prefs.putString(AppConstants.version, check1.getVersion());
                            //Hide the bounze logo and show the video layout
                            connection_layout.setVisibility(View.VISIBLE);
                            bounze_image.setVisibility(View.GONE);
                            try {
                                //on every time download show the video and in background download
                                myVideoView = (MyVideoView) findViewById(R.id.videoView);
                                myVideoView.setVisibility(View.VISIBLE);
                                progress = (TextView) findViewById(R.id.progress);
                                progress.setText("Loading...");
                                myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bounze_video));

                                myVideoView.requestFocus();
                                myVideoView.start();
                                myVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mediaPlayer) {
                                        if (!isInternetOn()) {
                                        }


                                    }
                                });
                            } catch (Exception e) {

                            }
                            DownloadAlldata();
                        } else {
                            //if already downloaded hide the video and show the splash screen
                            connection_layout.setVisibility(View.GONE);
                            bounze_image.setVisibility(View.VISIBLE);
                            navigateToHomescreen();

                        }

                    }
                } else if (response_code == 400) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                } else if (response_code == 500) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void getShopRatings() {


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Shoplist> call = stationClient.getShopRatings();
        call.enqueue(new Callback<Shoplist>() {
            @Override
            public void onResponse(Call<Shoplist> call, Response<Shoplist> response) {
                int response_code = response.code();
                Shoplist result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateShopListDB(result.getShoprating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");

                    getCoachRatings();

                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<Shoplist> call, Throwable t) {
                // shoplist = myDb.getSpecifyContent();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...1");
            }
        });

    }

    private void getCoachRatings() {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<CoachList> call = stationClient.getCoachRatings();
        call.enqueue(new Callback<CoachList>() {
            @Override
            public void onResponse(Call<CoachList> call, Response<CoachList> response) {
                int response_code = response.code();
                CoachList result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateCoachListDB(result.getCoachrating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");


                } else {

                }
            }

            @Override
            public void onFailure(Call<CoachList> call, Throwable t) {


            }
        });

    }


    private void DownloadAlldata() {

        getAllService();

    }


    public void getAllService() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Content> call = stationClient.getservicelist();
        call.enqueue(new Callback<Content>() {
            @Override
            public void onResponse(Call<Content> call, Response<Content> response) {
                int response_code = response.code();
                Content result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    List<Service> servicelist1 = result.getService();
                    //servicelist = result.getService();


                    dbobject.insertintoServiceListDB(servicelist1);
                    servicelist = dbobject.getServiceListDB();

                    getAllSubService();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }

            }

            @Override
            public void onFailure(Call<Content> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...2");
            }
        });
    }

    public void getAllSubService() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Content> call = stationClient.getsubservicelist();
        call.enqueue(new Callback<Content>() {
            @Override
            public void onResponse(Call<Content> call, Response<Content> response) {
                int response_code = response.code();
                Content result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    List<Service> servicelist1 = result.getService();
                    //servicelist = result.getService();
                    dbobject.insertintoServiceSubListDB(servicelist1);
                    servicelist = dbobject.getServiceSubListDB();

                    getAllPassList();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<Content> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...3");
            }
        });
    }

    public void getAllPassList() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassPojo> callPojo = stationClient.getPassPojo();
        callPojo.enqueue(new Callback<PassPojo>() {
            @Override
            public void onResponse(Call<PassPojo> call, Response<PassPojo> response) {
                int response_code = response.code();
                PassArr[] result = response.body().getPass();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");

                    sessionManager.setPass_Result(result);
                    PassArr[] passData = sessionManager.getPass_Result();
                    Log.d("", "value of pass data: " + passData.length);

                    getCoachlist();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }

            }

            @Override
            public void onFailure(Call<PassPojo> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...4");
            }
        });
    }


    public void getCoachlist() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<CoachList> call = stationClient.getCoachList();
        call.enqueue(new Callback<CoachList>() {
            @Override
            public void onResponse(Call<CoachList> call, Response<CoachList> response) {
                int response_code = response.code();
                CoachList result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // coachlist = result.getCoach();
                    dbobject.insertintoCoachListDB(result.getCoach());
                    coachlist = dbobject.getCoachListDB();
                    getCitylist();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }


            }

            @Override
            public void onFailure(Call<CoachList> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...5");
            }
        });
    }

    public void getCitylist() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<CityList> call = stationClient.getCitylist();
        call.enqueue(new Callback<CityList>() {
            @Override
            public void onResponse(Call<CityList> call, Response<CityList> response) {
                int response_code = response.code();
                CityList result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    //citylist = result.getCity();
                    dbobject.insertintoCityListDB(result.getCity());
                    citylist = dbobject.getCityListDB();

                    getCoachcatlist();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<CityList> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...6");
            }
        });
    }

    public void getCoachcatlist() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Coachcategory> call = stationClient.getCoachCatList();
        call.enqueue(new Callback<Coachcategory>() {
            @Override
            public void onResponse(Call<Coachcategory> call, Response<Coachcategory> response) {
                int response_code = response.code();
                Coachcategory result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    //coachcatlist = result.getCategory();
                    dbobject.insertintoCoachCatListDB(result.getCategory());
                    coachcatlist = dbobject.getCoachCatAllListDB();

                    getAllshop();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<Coachcategory> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...7");
            }
        });
    }


    public void getBlogList() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<BlogPojo> call = stationClient.getblogList();
        call.enqueue(new Callback<BlogPojo>() {
            @Override
            public void onResponse(Call<BlogPojo> call, Response<BlogPojo> response) {
                int response_code = response.code();

                BlogData[] result = response.body().getBlogData();
                response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    sessionManager.setBlogResult(result);
                    BlogData[] blogPojos = sessionManager.getBlogResult();
                    Log.d("", "value of blogPojos data: " + blogPojos.length);
                    getAllLocations();

                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<BlogPojo> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...8");
            }
        });
    }


    private void getAllLocations() {


        //get the locations list for the server and then set the values
        ApiInterface locations = ApiClient.createService(ApiInterface.class);
        Call<Search> locationsList = locations.getLocationsList();

        locationsList.enqueue(new Callback<Search>() {

            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                int response_code = response.code();
                Search result = response.body();

                if (response_code == 200) {
                    //get the result array and then get the version
                    List<ResultModel> check = result.getResult();

                    dbobject.insertintoLocationsListDB(check);
                    getAllAmenities();


                } else if (response_code == 400) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                } else if (response_code == 500) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void getAllAmenities() {
        //get the aminities list for the server and then set the values
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Search> call = stationClient.getAmenitiesList();

        call.enqueue(new Callback<Search>() {

            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                int response_code = response.code();
                Search result = response.body();

                if (response_code == 200) {
                    //get the result array and then get the version
                    List<ResultModel> check = result.getResult();
                    dbobject.insertintoAmenitiesListDB(check);
                    getEventList();


                } else if (response_code == 400) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                } else if (response_code == 500) {
                    Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Toast.makeText(SplashScreenActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    private void getEventList() {
        showProgressDialog();
        //    progress_bar.setVisibility(View.VISIBLE);
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Events> call = stationClient.getEventList();
        call.enqueue(new Callback<Events>() {
            @Override
            public void onResponse(Call<Events> call, Response<Events> response) {
                int response_code = response.code();

                if (response_code == 200) {
                    hideProgressDialog();
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    eventlist = response.body().getEvent();

                    dbobject.insertintoEventListDB(eventlist);
                    //make sure if every thing is download for the server
                    Prefs.putBoolean(AppConstants.isallDataDownloaded, true);
                    //blog list commented
                    navigateToHomescreen();
                } else {
                    hideProgressDialog();
                    //      progress_bar.setVisibility(View.GONE);
                    Toast.makeText(SplashScreenActivity.this, "Low Internet  /  Failed Connecting to server! please Try after some time...", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Events> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                //        progress_bar.setVisibility(View.GONE);
                Toast.makeText(SplashScreenActivity.this, "No Internet  /  Failed Connecting to server! please Try after some time...", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void getAllshop() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Shoplist> call = stationClient.getshoplist();
        call.enqueue(new Callback<Shoplist>() {
            @Override
            public void onResponse(Call<Shoplist> call, Response<Shoplist> response) {
                int response_code = response.code();
                Shoplist result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.insertintoShopListDB(result.getShop());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    getBlogList();
                } else {
                    progress.setText("Low Internet  /  Failed Connecting to server! please Try after some time...");
                }
            }

            @Override
            public void onFailure(Call<Shoplist> call, Throwable t) {
                // shoplist = myDb.getSpecifyContent();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress.setText("No Internet  /  Failed Connecting to server! please Try after some time...9");
            }
        });
    }

    private void navigateToHomescreen() {

        SharedPreferences settings = getSharedPreferences("hasRunBefore", 0);
        SharedPreferences.Editor edit = settings.edit();
        edit.putBoolean("hasRun", true); //set to has run
        edit.commit(); //apply

        hideProgressDialog();

        /**This method will be executed once the timer is over
         Start your app main activity*/
        if (Prefs.getBoolean(AppConstants.isUserLoggedIn, false)) {
            //make sure if every thing is downloaded if yes move to pager layout else display toast
            if (Prefs.getBoolean(AppConstants.isallDataDownloaded, false)) {
                Intent i = new Intent(SplashScreenActivity.this, PagerLayout.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(SplashScreenActivity.this, "Technical problem, please retry", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (Prefs.getBoolean(AppConstants.isallDataDownloaded, false)) {
                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            } else {

                //if all data not downloaded restart it again
                if (isInternetOn()) {
                    //every time check the version after first download
                    getVersion();

                }

            }
        }
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
