package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Booking;
import com.jireh.bounze.data.BookingProcess;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 12-09-2017.
 */

public class BookingDescription extends Activity {
    Bookhistory history;
    TextView shop_address_txt, city_txt, location_address_txt, description_txt, contact_no_txt, event_name_txt, event_time_txt, event_price_txt;
    ImageView squareimage;
    Toolbar toolbar;
    TextView title_txt;
    DBFunctions dbFunctions;
    ImageView back_img;
    Button book_btn;
    LinearLayout shop_details_layout, event_name_layout;
    SliderLayout booking_history_slider;
    Shop shopDetails;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_description);
        intiVariable();
        //if the intent is from booking history show the book button else don't show it
        String flag = getIntent().getStringExtra("flag");
        if (flag.equalsIgnoreCase("Booking history")) {
            book_btn.setVisibility(View.VISIBLE);
        }

        setOnClickListener();
        //get event details
        history = (Bookhistory) getIntent().getSerializableExtra("position");


        //get the shop details using shop id
        dbFunctions = new DBFunctions(BookingDescription.this);

        shopDetails = dbFunctions.getShopListDetailbyId(history.getShopid());
        try {
            //set values for title based on the booking type
            //if it is event don't show the shop image else show
            if (history.getBookingType().equals("3")) {
                title_txt.setText(history.getTitle());
                event_name_layout.setVisibility(View.VISIBLE);
            } else {
                title_txt.setText(shopDetails.getTitle() + " - " + shopDetails.getSmallAddress());
                shop_details_layout.setVisibility(View.VISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //if the booking type is 3 get the event details using history object else get from the shop details object
        if (history.getBookingType().equals("3")) {


            String strings[] = history.getAddress().split(",");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < strings.length; i++) {
                sb.append(strings[i].trim()).append("\n");
            }
            String address = sb.toString();
            location_address_txt.setText(address + "\n" + history.getLocation());
            event_name_txt.setText(history.getTitle());
            contact_no_txt.setText(history.getContactMobile());
            if (history.getBookingType().equals("5") || history.getBookingType().equals("7")) {
                event_time_txt.setText(history.getEventStartDate());
            }
            //set the slider image visibility gone and show the square image if it is event for the booking history

            squareimage.setVisibility(View.VISIBLE);
            booking_history_slider.setVisibility(View.GONE);
            Picasso.with(getApplicationContext()).load(BASE_URL + history.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(squareimage);
            event_price_txt.setText(history.getCost());
            description_txt.setText(history.getDescription());
        } else {

            shop_address_txt.setText(shopDetails.getTitle());
            String strings[] = shopDetails.getFullAddress().split(",");


            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < strings.length; i++) {
                sb.append(strings[i].trim()).append("\n");
            }

            String address = sb.toString();

            location_address_txt.setText(address + "\n" + history.getLocation());


            event_name_txt.setText(shopDetails.getTitle());
            event_time_txt.setText(history.getStartDate() + " to " + history.getEndDate());
            //get the contact details
            contact_no_txt.setText(shopDetails.getContactNumber());
            description_txt.setText(shopDetails.getDescription());

            event_price_txt.setText(shopDetails.getCost());
            //get the shop images and display it in the slider
            setSliderLayout();
        }


        city_txt.setText("Bengaluru");


    }

    private void setSliderLayout() {
        squareimage.setVisibility(View.GONE);
        booking_history_slider.setVisibility(View.VISIBLE);
        //seperare teh values of image url base on the comma
        String strings[] = shopDetails.getImageUrlRectangle().split(",");

        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].trim();

        }

        for (int idx = 0; idx < strings.length; idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(BookingDescription.this);
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(BASE_URL + strings[idx])

                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            booking_history_slider.addSlider(defaultSliderView);
        }
        booking_history_slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        booking_history_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        booking_history_slider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        booking_history_slider.setDuration(4000);
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //if the booking history type is 3 book the event else navigate to shop screen
        book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (history.getBookingType().equals("3")) {
                    //show the dialog to share the event and then book it
                    AlertDialog.Builder mBuild = new AlertDialog.Builder(BookingDescription.this);
                    View mView = getLayoutInflater().inflate(R.layout.event_booking_dialog, null);

                    Button proceed_btn = (Button) mView.findViewById(R.id.proceed_btn);

                    final EditText username = (EditText) mView.findViewById(R.id.username_edt);
                    final EditText emailid = (EditText) mView.findViewById(R.id.email_edt);
                    final EditText mobile = (EditText) mView.findViewById(R.id.mobile_edt);


                    mBuild.setView(mView);
                    final AlertDialog dialog = mBuild.create();
                    dialog.setCancelable(true);
                    dialog.show();

                    proceed_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (username.getText().toString().matches("")) {
                                username.setError("Enter name");
                            } else if (!(emailid.getText().toString().matches(emailPattern))) {
                                emailid.setError("Enter valid E-mail Id");
                                ;
                            } else if (mobile.getText().toString().matches("") || mobile.getText().toString().length() < 10 || mobile.getText().toString().length() > 10) {
                                mobile.setError("Enter valid mobile no");
                            } else {

                                dialog.dismiss();
                                checkDetails(history.getShopid(), history.getShopid(), "", "3", "", "",
                                        history.getEventStartDate(), history.getEventEndDate(),
                                        history.getSessionSTime(), "", "0",
                                        "", "1", history.getCost(), "1", "0", Prefs.getString(AppConstants.UserId, ""));
                            }
                        }
                    });
                } else {

                    Intent shop = new Intent(BookingDescription.this, ShopDescriptionActivity.class);
                    shop.putExtra("ShopDetails", shopDetails);
                    startActivity(shop);
                    finish();

                }
            }
        });
    }

    public void checkDetails(String program_id, String shopid, String serviceid, String booking_type,
                             String no_of_days, String no_of_sessions, String start_date, String end_date,
                             String session_s_time, String session_hours, String is_batch,
                             String list_of_days, String approval_status,
                             String cost, String Paymentmode, String payment_status,
                             String user_id) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<BookingProcess> call = stationClient.booking(program_id, shopid, serviceid, booking_type,
                no_of_days, no_of_sessions, start_date, end_date,
                session_s_time, session_hours, is_batch,
                list_of_days, approval_status,
                cost, Paymentmode, payment_status,
                user_id, "", "", "", "");
        call.enqueue(new Callback<BookingProcess>() {
            @Override
            public void onResponse(Call<BookingProcess> call, Response<BookingProcess> response) {
                int response_code = response.code();
                BookingProcess result = response.body();

                if (response_code == 200) {
                    Log.d("BookPage1", "Registration_eqnn" + response.code() + "");
                    List<Booking> check = result.getBooking();
                    Booking check1 = check.get(0);
                    String value = "" + check1.getStatus();
                    //    paymentuid = ""+check1.getMessage();

                    if (value.equals("1")) {
                        SharedPreferences pred = getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editorp = pred.edit();
                        editorp.putString("status", value);
                        editorp.commit();
                        Toast.makeText(BookingDescription.this, "Event booked successfully Do payment", Toast.LENGTH_SHORT).show();
                        //send the booked details of the event to the payment page
                        Intent intent = new Intent(BookingDescription.this, Pay.class);
                        intent.putExtra("PayFlag", "bookingDescEvent");
                        intent.putExtra("EventDetails", (Serializable) history);
                        intent.putExtra("EventBatchTime", event_time_txt.getText().toString());
                        startActivity(intent);
                    }

                } else if (response_code == 400) {
                    Toast.makeText(BookingDescription.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("BookPage1", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(BookingDescription.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("BookPage1", "500 error");
                }
            }

            @Override
            public void onFailure(Call<BookingProcess> call, Throwable t) {
                Toast.makeText(BookingDescription.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    private void intiVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) findViewById(R.id.title_txt);
        squareimage = (ImageView) findViewById(R.id.squareimage);
        shop_address_txt = (TextView) findViewById(R.id.shop_address_txt);
        location_address_txt = (TextView) findViewById(R.id.location_address_txt);
        contact_no_txt = (TextView) findViewById(R.id.contact_no_txt);
        event_name_txt = (TextView) findViewById(R.id.event_name_txt);
        event_time_txt = (TextView) findViewById(R.id.event_time_txt);
        event_price_txt = (TextView) findViewById(R.id.event_price_txt);
        city_txt = (TextView) findViewById(R.id.city_txt);
        description_txt = (TextView) findViewById(R.id.description_txt);
        book_btn = (Button) findViewById(R.id.book_btn);
        shop_details_layout = (LinearLayout) findViewById(R.id.shop_details_layout);
        event_name_layout = (LinearLayout) findViewById(R.id.event_name_layout);
        booking_history_slider = (SliderLayout) findViewById(R.id.booking_history_slider);

    }
}
