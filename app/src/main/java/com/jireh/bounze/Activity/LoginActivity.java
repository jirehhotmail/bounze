package com.jireh.bounze.Activity;


import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ServerValue;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.Constants;
import com.jireh.bounze.Util.CustomEditText;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.Util.SharedPrefManager;
import com.jireh.bounze.Util.Utils;
import com.jireh.bounze.Util.ValidationPoint;
import com.jireh.bounze.data.Forgotpwd;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Profile;
import com.jireh.bounze.data.User;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;*/

//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;

/**
 * Created by Shubham on 25-09-2017.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int RC_SIGN_IN_FACE = 9001;
    private static final int RC_SIGN_IN_GOOGLE = 9999;
    private static final String TAG = "MainActivity";
    static int openednow = 0;
    private final Context mContext = this;
    public SharedPrefManager sharedPrefManager;
    SharedPreferences pred;
    Dialog dialog;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    //    ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    CallbackManager callbackManager;
    TextView forgot;
    int count = 0;
    LoginButton facebookLogin;
    private ImageView facebook_img, google_img;
    private boolean isReady = false;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String idToken;
    private String name, email;
    private String photo;
    private Uri photoUri;
    private SignInButton mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                isReady = true;
            }
        });

        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.bounze",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        forgot = (TextView) findViewById(R.id.forget_txt);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                createForgetPasswordDialog();
            }
        });


        final CustomEditText userName = (CustomEditText) findViewById(R.id.username_edt);
        final CustomEditText passWord = (CustomEditText) findViewById(R.id.password_edt);
        Button logIn = (Button) findViewById(R.id.login_btn);
        TextView signIn = (TextView) findViewById(R.id.new_user);
        // TextView gPlus = (TextView) findViewById(R.id.googlePlus);
        facebook_img = (ImageView) findViewById(R.id.facebook_img);
        google_img = (ImageView) findViewById(R.id.google_img);
        facebookLogin = (LoginButton) findViewById(R.id.fb_login);
        mSignInButton = (SignInButton) findViewById(R.id.login_with_google);
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isValidUser = false, isValidPwd = false;
                if (userName.getText().toString().matches("")) {
                    userName.setError("Enter eMail Id or mobileno!");
                } else if (!(userName.getText().toString().matches(emailPattern) || userName.getText().toString().length() == 10)) {
                    userName.setError("Enter valid E-mail Id or mobileno !");
                    isValidUser = false;
                } else {
                    isValidUser = true;
                }
                if (passWord.getText().toString().matches("")) {
                    passWord.setError("Enter Password !");

              /*  } else if (!(passWord.getText().toString().length() >= 4)) {
                    passWord.setError("Minimum 4-char !");
                    isValidPwd = false;*/
                } else {
                    isValidPwd = true;
                }
                if (isValidUser && isValidPwd) {
                    String email_mobileno = userName.getText().toString().trim();
                    String password = passWord.getText().toString().trim();
                    if (isInternetOn()) {
                        showProgressDialog();

                        normalLoginProcess(email_mobileno, password);
                    } else {
                        Toast.makeText(LoginActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this, SignUp.class));

            }
        });
        //set the drawable click listener for the password right to show the password

        passWord.setDrawableClickListener(new CustomEditText.DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:

                        if (count % 2 == 0) {
                            passWord.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            count++;

                        } else {
                            passWord.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            count++;

                        }
                        System.out.println("password clicked");

                        break;

                    default:
                        break;
                }
            }

        });
        mSignInButton.setSize(SignInButton.SIZE_WIDE);

        mSignInButton.setOnClickListener(LoginActivity.this);

        configureSignIn();

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();

        //this is where we start the Auth state Listener to listen for whether the user is signed in or not
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Get signedIn user
                FirebaseUser user = firebaseAuth.getCurrentUser();

                //if user is signed in, we call a helper method to save the user details to Firebase
                if (user != null) {
                    // User is signed in
                    createUserInFirebaseHelper();
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };


        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //give the perform click for facebook login and check internet connection
                if (isInternetOn()) {


                    facebook_img.performClick();
                } else {
                    Toast.makeText(LoginActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        facebook_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog();

                refreshToken();
                final URL[] imageURL = new URL[1];


                //  facebookLogin.setReadPermissions("public_profile", "email", "user_birthday", "user_friends");
                LoginManager loginManager = LoginManager.getInstance();
                loginManager.logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));

                loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        hideProgressDialog();
                        String accessToken = loginResult.getAccessToken().getToken();
                        String accessToken1 = loginResult.getAccessToken().getUserId();

                        try {
                            imageURL[0] = new URL("https://graph.facebook.com/" + accessToken1 + "/picture?type=large");
                            Log.e("image url", imageURL[0] + "");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        String image_URL = imageURL[0] + "";
                        Log.d("FacebookOrGoogle", accessToken + " " + accessToken1);
                        Log.d("FacebookOrGoogle", loginResult.getAccessToken().getUserId() + "");
                        Log.d("FacebookOrGoogle", loginResult.getRecentlyDeniedPermissions() + "");
                        Log.d("FacebookOrGoogle", loginResult.getAccessToken().getApplicationId() + "");
                        Log.d("FacebookOrGoogle", loginResult.getAccessToken() + "");
                        handleFacebookAccessToken(loginResult, image_URL);
                    }

                    @Override
                    public void onCancel() {
                        hideProgressDialog();
                        Log.d("FacebookOrGoogle", " ");
                    }


                    @Override
                    public void onError(FacebookException error) {
                        hideProgressDialog();
                        Log.d("FacebookOrGoogle", " ");
                    }
                });
            }
        });

        google_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetOn()) {
                    showProgressDialog();

                    signIn();
                } else {
                    Toast.makeText(LoginActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    //This method creates a new user on our own Firebase database
    //after a successful Authentication on Firebase
    //It also saves the user info to SharedPreference
    private void createUserInFirebaseHelper() {

        //Since Firebase does not allow "." in the key name, we'll have to encode and change the "." to ","
        // using the encodeEmail method in class Utils
        final String encodedEmail = Utils.encodeEmail(email.toLowerCase());

        //create an object of Firebase database and pass the the Firebase URL
        final Firebase userLocation = new Firebase(Constants.FIREBASE_URL_USERS).child(encodedEmail);

        //Add a Listerner to that above location
        userLocation.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    /* Set raw version of date to the ServerValue.TIMESTAMP value and save into dateCreatedMap */
                    HashMap<String, Object> timestampJoined = new HashMap<>();
                    timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

                    // Insert into Firebase database
                    User newUser = new User(name, photo, encodedEmail, timestampJoined);
                    userLocation.setValue(newUser);

                    Toast.makeText(LoginActivity.this, "Account created!", Toast.LENGTH_SHORT).show();

                    // After saving data to Firebase, goto next activity
//                    Intent intent = new Intent(MainActivity.this, NavDrawerActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                Log.d(TAG, getString(R.string.log_error_occurred) + firebaseError.getMessage());
                //hideProgressDialog();
                if (firebaseError.getCode() == FirebaseError.EMAIL_TAKEN) {
                } else {
                    Toast.makeText(LoginActivity.this, firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    // This method configures Google SignIn
    public void configureSignIn() {

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.client_id))
                .requestEmail()
                .build();


        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }


    public void refreshToken() {
        if (isReady) {
            LoginManager.getInstance().logOut();
            /**
             * Refreshing the acess token of the Facebook*/
            Log.d(TAG, "Logout" + "Token Refreshed");
            AccessToken.refreshCurrentAccessTokenAsync();
        }
    }


    public void normalLoginProcess(String email_mobileno, String password) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        //if text contains at symbole pass as email id else pass mobile no

        Call<LogIn> call = stationClient.createLogIn(email_mobileno, password, "3", "");

        System.out.println("call " + call);
        Log.d("value of email: " + email, ", password: " + password);

        call.enqueue(new Callback<LogIn>() {

            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                hideProgressDialog();
                if (response_code == 200) {
                    Log.d(TAG, "Registration_eqnn" + response.code() + "");
                    List<Profile> check = result.getStatus();
                    Profile check1 = check.get(0);

                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    if (value.equals("1")) {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        String id = "" + check1.getId();
                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                        sessionManager.setUserId(id);
                        //store the values in the from the login details
                        pred = LoginActivity.this.getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pred.edit();
                        editor.putString("name", check1.getUserName());
                        editor.putString("emailid", check1.getEmailid());
                        editor.putString("mobile", check1.getMobileNumber());
                        editor.commit();
                        Prefs.putString(AppConstants.UserId, id);

                        Prefs.putBoolean(AppConstants.isUserLoggedIn, true);
                        Intent intent = new Intent(LoginActivity.this, PagerLayout.class);
                        startActivity(intent);
                        finish();

                        //update the profile inesert the token id into server using id
                        //call the server
                        updateProfile();
                    } else {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } else if (response_code == 400) {
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "500 error");
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void updateProfile() {
        //update the user data
        String firebasetokenId = Prefs.getString(AppConstants.tokenId, "");


        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));
        RequestBody tokenId = RequestBody.create(MediaType.parse("text/plain"), firebasetokenId);


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.updateProfileWithTokenId(userId, tokenId);

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                UserProfileService result = response.body();
                /*if (result.getResult().get(0).getStatus() == 0) {
                    Toast.makeText(LoginActivity.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(LoginActivity.this, result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                }*/
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    public void fbLoginProcess(String fbid, String name, String email, final String pic, String gender, String birthDay) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        System.out.println("id" + fbid + "pic" + pic + "gender" + gender + "birthday" + birthDay);
        Call<LogIn> call = stationClient.createfbLogIn("2", fbid, name, email, pic, gender, birthDay);
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                hideProgressDialog();
                if (response_code == 200) {
                    Log.d(TAG, "Registration_eqnn" + response.code() + "");
                    List<Profile> check = result.getStatus();
                    Profile check1 = check.get(0);

                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    if (value.equals("1")) {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        String id = "" + check1.getId();
                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                        sessionManager.setUserId(id);
                        //store the values in the from the login details
                        pred = LoginActivity.this.getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pred.edit();
                        editor.putString("name", check1.getUserName());
                        editor.putString("emailid", check1.getEmailid());
                        editor.putString("mobile", check1.getMobileNumber());
                        editor.putString("imageUrl", pic);
                        editor.commit();
                        Prefs.putString(AppConstants.UserId, id);
                        Prefs.putBoolean(AppConstants.isUserLoggedFacebook, true);
                        Prefs.putBoolean(AppConstants.isUserLoggedIn, true);
                        Intent intent = new Intent(LoginActivity.this, PagerLayout.class);
                        startActivity(intent);
                        finish();
                        //update the profile inesert the token id into server using id
                        //call the server
                        updateProfile();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }

                } else if (response_code == 400) {
                    hideProgressDialog();
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "400 error");
                } else if (response_code == 500) {
                    hideProgressDialog();
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "500 error");
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private boolean isInternetOn() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // This IS the method where the result of clicking the signIn button will be handled

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();

                firebaseAuthWithGoogle(account);
            } else {
                hideProgressDialog();
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);


        //facebook request code -> 2406 and result code -> -1
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.e("Google SigIn Result", acct.getDisplayName());
                            Log.e("Google SigIn Result", acct.getEmail() + "");
                            Log.e("Google SigIn Result", acct.getFamilyName() + "");
                            Log.e("Google SigIn Result", acct.getGivenName() + "");
                            Log.e("Google SigIn Result", acct.getId() + "");
                            Log.e("Google SigIn Result", acct.getIdToken() + "");
                            Log.e("Google SigIn Result", acct.getPhotoUrl() + "");
                            Log.e("Google SigIn Result", acct.getServerAuthCode() + "");
                            googleLoginProcess(acct.getId(), acct.getGivenName(), acct.getEmail(), acct.getPhotoUrl() + "", "", "");

                            //  updateUI(user);
                        } else {
                            Toast.makeText(LoginActivity.this, "Logged in fail", Toast.LENGTH_SHORT).show();
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //   updateUI(null);
                        }


                    }
                });
    }

    private void googleLoginProcess(String googleId, String gname, String gemailId, final String gpic, String ggender, String gbirthDay) {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);

        Call<LogIn> call = stationClient.createfbLogIn("4", googleId, gname, gemailId, gpic, ggender, gbirthDay);
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                hideProgressDialog();
                if (response_code == 200) {
                    Log.d(TAG, "Registration_eqnn" + response.code() + "");
                    List<Profile> check = result.getStatus();
                    Profile check1 = check.get(0);

                    String value = "" + check1.getStatus();
                    String message = "" + check1.getMessage();

                    if (value.equals("1")) {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        String id = "" + check1.getId();
                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                        sessionManager.setUserId(id);
                        //store the values in the from the login details
                        pred = LoginActivity.this.getSharedPreferences("Bounze", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pred.edit();
                        editor.putString("name", check1.getUserName());
                        editor.putString("emailid", check1.getEmailid());
                        editor.putString("mobile", check1.getMobileNumber());
                        editor.putString("imageUrl", gpic);
                        editor.commit();
                        Prefs.putString(AppConstants.UserId, id);
                        Prefs.putBoolean(AppConstants.isUserLoggedGoogle, true);
                        Prefs.putBoolean(AppConstants.isUserLoggedIn, true);
                        Intent intent = new Intent(LoginActivity.this, PagerLayout.class);
                        startActivity(intent);
                        finish();
                        //update the profile inesert the token id into server using id
                        //call the server
                        updateProfile();
                    } else {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }

                } else if (response_code == 400) {
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "500 error");
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }*/


    private void handleFacebookAccessToken(final LoginResult loginResult, final String imageUrl) {
        Log.d(TAG, "handleFacebookAccessToken:" + loginResult.getAccessToken());

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());

                        String name = object.optString("name");
                        String email = object.optString("email");
                        String gender = object.optString("gender");
                        String birthDay = object.optString("birthday");
                        String id = object.optString("id");
                        String picture = imageUrl;

                        fbLoginProcess(id, name, "" + email, picture, gender, birthDay);

                        Log.e("fb SigIn Result", name + " \n " + email + "\n " + gender + " \n" + birthDay + " \n" + id);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();

    }
   /* public static Bitmap getFacebookProfilePicture(String userID){
        Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
q
        return bitmap;
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

  /*
    * Dialogue for Forgot Password
    * */

    private void createForgetPasswordDialog() {
        final AlertDialog OptionDialog = new AlertDialog.Builder(this).create();
        //     AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.forget_dialog, null);
        OptionDialog.setView(dialogView);
        OptionDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final EditText edt = (EditText) dialogView.findViewById(R.id.email_edt);
        Button cancle_btn = (Button) dialogView.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialogView.findViewById(R.id.ok_btn);

        ok_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //check validation for email id
                if (ValidationPoint.isEmailIdValid(edt, true)) {
                    forgot(edt.getText().toString());
                    //Move the otp page to enter the password

                    Intent in = new Intent(LoginActivity.this, OtpLogin.class);
                    startActivity(in);
                    OptionDialog.dismiss();

                } else if (ValidationPoint.isValidPhone(edt, true)) {
                    forgot(edt.getText().toString());

                    Intent in = new Intent(LoginActivity.this, OtpLogin.class);
                    startActivity(in);
                    OptionDialog.dismiss();
                }

                return true;
            }
        });


        OptionDialog.show();
    }

    private void forgot(String s) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Forgotpwd> call = stationClient.forgotpassword(s);
        call.enqueue(new Callback<Forgotpwd>() {
            @Override
            public void onResponse(Call<Forgotpwd> call, Response<Forgotpwd> response) {
                int response_code = response.code();
                Forgotpwd result = response.body();

                if (response_code == 200) {
                    Toast.makeText(LoginActivity.this, "OTP has been sent to your registered mobile no. Use it to login.", Toast.LENGTH_LONG).show();
                    //store the user id in the shared prefernce
                    Prefs.putString(AppConstants.UserId, String.valueOf(result.getForgot().get(0).getUserid()));

                } else if (response_code == 400) {
                    Toast.makeText(LoginActivity.this, "Unable to Connect to server! Try later", Toast.LENGTH_SHORT).show();
                    Log.e("m", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(LoginActivity.this, "Unable to Connect to server! Try later", Toast.LENGTH_SHORT).show();
                    Log.e("m", "500 error");
                }
            }

            @Override
            public void onFailure(Call<Forgotpwd> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Kindly check your internet connection", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("value of ", "google connection error");
    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
