package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class PrivacyPolicy extends Activity {
    TextView privacy_txt;
    ImageView back_img;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);
        privacy_txt = (TextView) findViewById(R.id.privacy_txt);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) findViewById(R.id.back_img);

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        privacy_txt.setText(Html.fromHtml(getResources().getString(R.string.privacy_policy_bounze)));
    }
}
