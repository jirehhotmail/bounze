package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class AboutUs extends Activity {
    TextView about_us_txt;
    ImageView back_img;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) findViewById(R.id.back_img);
        about_us_txt = (TextView) findViewById(R.id.about_us_txt);
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        about_us_txt.setText(Html.fromHtml(getResources().getString(R.string.about_us_bounze)));
    }
}

