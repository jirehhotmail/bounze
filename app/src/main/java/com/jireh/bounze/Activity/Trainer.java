package com.jireh.bounze.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jireh.bounze.Adapter.CoachCategoryAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.data.Category;

import java.util.List;

/**
 * Created by Muthamizhan C on 09-08-2017.
 */

public class Trainer extends AppCompatActivity {
    // public static List<Category> ccoachcatlist = new ArrayList<Category>();
    //ListView listcoach;
    RecyclerView coach_category_recycler, search_trainer_recycler;
    DBFunctions dbobject;
    Toolbar toolbar;
    TextView title_txt;
    ImageView back_img, filter_img;
    List<Category> coachcatlist;
    LinearLayout trainer_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trainer);
        initVariable();

        title_txt.setText("Trainers");
        coach_category_recycler = (RecyclerView) findViewById(R.id.coach_category_recycler);
        dbobject = new DBFunctions(getApplicationContext());
        //set visibility for the filter image

        filter_img.setVisibility(View.VISIBLE);
        setOnClickListener();


        coachcatlist = dbobject.getCoachCatAllListDB();
      /*  ccoachcatlist = new ArrayList<Category>();
        for (int i = 0; i < coachcatlist.size(); i++) {
            if (coachcatlist.get(i).getPriority().equals("1"))
                if (coachcatlist.get(i).getActiveStatus().equals("1") && coachcatlist.get(i).getCid() != null) {
                    ccoachcatlist.add(coachcatlist.get(i));
                }
        }*/
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        coach_category_recycler.setLayoutManager(manager);

        CoachCategoryAdapter adapter = new CoachCategoryAdapter(this, coachcatlist, new CoachCategoryAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int item, int position) {
                //  current_coachcat = ccoachcatlist.get(position).getCid();
                Intent in = new Intent(Trainer.this, CoachListActivity.class);
                in.putExtra("CategoryName", coachcatlist.get(position).getTitle());
                in.putExtra("CategoryIdSelected", coachcatlist.get(position).getCid());
                startActivity(in);

            }
        });
        coach_category_recycler.setAdapter(adapter);


    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);

        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);

        trainer_layout = (LinearLayout) findViewById(R.id.trainer_layout);
        search_trainer_recycler = (RecyclerView) findViewById(R.id.search_trainer_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        search_trainer_recycler.setLayoutManager(manager);
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Trainer.this, CoachFilter.class);
                startActivity(in);
            }
        });
    }


}


