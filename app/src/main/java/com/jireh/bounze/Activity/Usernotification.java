package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Search;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MuthuBala on 2/5/2018.
 */

public class Usernotification extends Activity {

    RecyclerView notification_recycler;
    Toolbar toolbar;
    TextView title;
    ImageView back_img;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_notification);
        initVariable();
        //set the title for the notification
        title.setText("Notification");
        //set on click listener for back button
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getNotifications();
    }

    private void getNotifications() {

               //it is hardcoded for notification
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Search> call = stationClient.getHomeSliderList();


        call.enqueue(new Callback<Search>() {

            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                int response_code = response.code();
                Search result = response.body();

                if (response_code == 200) {


                } else if (response_code == 400) {
                    Toast.makeText(Usernotification.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();


                } else if (response_code == 500) {
                    Toast.makeText(Usernotification.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Toast.makeText(Usernotification.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");


            }
        });


    }

    private void initVariable() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        notification_recycler = new RecyclerView(Usernotification.this);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        notification_recycler.setLayoutManager(manager);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
    }
}
