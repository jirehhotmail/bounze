package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class NotificationSettings extends Activity {

    ImageView back_img;
    Toolbar toolbar;
    TextView notification_txt;
    Switch message_switch,message_offers,message_event;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);

        notification_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        notification_txt.setText("Settings");
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
