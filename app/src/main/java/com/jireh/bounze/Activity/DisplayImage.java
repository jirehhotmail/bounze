package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.RealPathUtil;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 24-10-2017.
 */

public class DisplayImage extends Activity {

    final int PIC_CROP = 1;
    Bitmap selectedBitmap;
    CropImageView captured_img;
    ImageView health_img,back_img;
    Button crop_btn, upload_btn;
    String imageUrl;
    Uri myUri;
    Toolbar toolbar;
    TextView title_txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_image_layout);
        captured_img = (CropImageView) findViewById(R.id.captured_img);
        crop_btn = (Button) findViewById(R.id.crop_btn);
        upload_btn = (Button) findViewById(R.id.upload_btn);
        health_img = (ImageView) findViewById(R.id.health_img);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        //set title for the page
        title_txt.setText("Image Upload");
        //if flag from myprofile get the  image path and then set visibility to crop,upload btn
        //else get the image url and show the image using picasso and the setvisiblity gone to crop, upload btn
        try {
            String flag = getIntent().getStringExtra("ImageFlag");
            if (flag.equals("MyProfile")) {
                myUri = Uri.parse(getIntent().getStringExtra("ImageUriAsync"));
                captured_img.setImageUriAsync(myUri);
                crop_btn.setVisibility(View.VISIBLE);
                upload_btn.setVisibility(View.VISIBLE);
            } else {
                crop_btn.setVisibility(View.GONE);
                upload_btn.setVisibility(View.GONE);
                imageUrl = getIntent().getStringExtra("profileImage");
                health_img.setVisibility(View.VISIBLE);
                Picasso.with(DisplayImage.this).load(BASE_URL + imageUrl).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(health_img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        setOnClickListener();

    }

    private void setOnClickListener() {

        crop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap cropped = captured_img.getCroppedImage(500, 500);
                if (cropped != null)
                    captured_img.setImageBitmap(cropped);
            }
        });

        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //upload the image
                uploadProfileImage();
            }
        });
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void uploadProfileImage() {/*


        //creating a file
        File file = new File(realPath);
        Uri uri = Uri.fromFile(file);
        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(uri)), file);*/


        //creating a file
        String realPath = RealPathUtil.getRealPath(this, myUri);
        final File file = new File(realPath);

        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getBaseContext().getContentResolver().getType(myUri)), file);
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.updateProfilePicture(userId,
                requestFile);

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                UserProfileService result = response.body();
                if (result.getResult().get(0).getStatus() == 1) {
                    Toast.makeText(getBaseContext(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PIC_CROP) {
            //set the cropped image to the imageview
            if (data != null) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                selectedBitmap = extras.getParcelable("data");

                captured_img.setImageBitmap(selectedBitmap);
            }
        }
    }
}
