package com.jireh.bounze.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 02-11-2017.
 */

public class InternetNoConnection extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_internet_connection);
    }
}
