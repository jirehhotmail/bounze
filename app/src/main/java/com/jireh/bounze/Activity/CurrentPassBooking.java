package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jireh.bounze.Adapter.CurrentPassGridAdapter;
import com.jireh.bounze.Adapter.PassHistoryAdapter;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.OkHttp3Connection;
import com.jireh.bounze.data.CurrentPassBookingPojo;
import com.jireh.bounze.data.PassCurrentHistory;
import com.jireh.bounze.data.PassHistoryPojo;
import com.jireh.bounze.data.QRCodeResponsePojo;
import com.jireh.bounze.data.QRServiceResponsePojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CurrentPassBooking extends AppCompatActivity {
    ProgressBar progress_bar;
    private GridView gv_service;
    private CurrentPassGridAdapter adapter;
    private ArrayList<CurrentPassBookingPojo> pojoArrayList;
    private CurrentPassBookingPojo pojo;
    private TextView tv_title, tv_history;
    private LinearLayout ll_back;
    private PassCurrentHistory currentPassData;
    private String selectedServiceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_pass_booking);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        currentPassData = getIntent().getParcelableExtra("data");
        initVar();
    }

    private void initVar() {
        gv_service = (GridView) findViewById(R.id.gv_service);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_history = (TextView) findViewById(R.id.tv_history);
        tv_history = (TextView) findViewById(R.id.tv_history);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);

        tv_title.setText(currentPassData.getPass_name());

        final String[] serviceIdArray = currentPassData.getSubservices_id().trim().split(",");
        String[] serviceNameArray = currentPassData.getSubservices().trim().split(",");
        final String[] serviceTotalSessionArray = currentPassData.getService_total_session().trim().split(",");
        final String[] servicePendingSessionArray = currentPassData.getService_pending_session().trim().split(",");
        pojoArrayList = new ArrayList<>();
        for (int tempPos = 0; tempPos < serviceTotalSessionArray.length; tempPos++) {
            pojo = new CurrentPassBookingPojo();
            pojo.setRemainingSessionCount(servicePendingSessionArray[tempPos]);
            pojo.setTotalSessionCount(serviceTotalSessionArray[tempPos]);
            pojo.setServiceName(serviceNameArray[tempPos]);
            pojoArrayList.add(pojo);
        }
        adapter = new CurrentPassGridAdapter(this, pojoArrayList);
        gv_service.setAdapter(adapter);

        gv_service.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Integer.parseInt(serviceTotalSessionArray[position]) > Integer.parseInt(servicePendingSessionArray[position])) {
                    Intent intent = new Intent(CurrentPassBooking.this, QrCodeScannerActivity.class);
                    selectedServiceId = serviceIdArray[position];
                    startActivityForResult(intent, 101);
                }
            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHistory();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("passbookingactivity", " onactivityresult requestcode: " + requestCode + " ,resultCode: " + resultCode + " ,selectedId: " + selectedServiceId + " ,bookingId: " + currentPassData.getBooking_id());
        if (data != null && requestCode == 101) {
            String message = data.getStringExtra("result");
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            Gson gson = new Gson();
            try {
                QRCodeResponsePojo responsePojo = gson.fromJson(message, QRCodeResponsePojo.class);
                if (responsePojo.getService_id().equals(selectedServiceId)) {
                    confirmUser(currentPassData.getBooking_id(), responsePojo.getShop_id(), responsePojo.getService_id());
                } else {
                    Toast.makeText(CurrentPassBooking.this, "Not appropriate data", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(CurrentPassBooking.this, "Not appropriate data", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * <h3>confirmUser</h3>
     * This method will just only ask the user that is he/she ready to take the current session or not.
     *
     * @param booking_id
     * @param shop_id
     * @param service_id
     */
    private void confirmUser(final String booking_id, final String shop_id, final String service_id) {
        final AlertDialog OptionDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.alert_confirm, null);
        OptionDialog.setView(dialogView);
        OptionDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Button bt_cancel = (Button) dialogView.findViewById(R.id.bt_cancel);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionDialog.dismiss();
            }
        });

        Button bt_ok = (Button) dialogView.findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBookSessionService(booking_id, shop_id, service_id);
                OptionDialog.dismiss();
            }
        });
        OptionDialog.show();
    }

    /**
     * <h3>getData</h3>
     * This method just check internet connected or not, if it is connected so make a service call or else show
     * message that internet is not working.
     *
     * @param booking_id
     * @param shop_id
     * @param service_id
     */
    private void callBookSessionService(String booking_id, String shop_id, String service_id) {
        if (isInternetOn()) {
            showProgressDialog();
            callService(booking_id, shop_id, service_id);
        } else {
            Toast.makeText(CurrentPassBooking.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void callService(String booking_id, String shop_id, String service_id) {
       // String url = "https://bounze.in/demo/api/pass_booking_history.php";
        String url = "http://ec2-13-126-230-103.ap-south-1.compute.amazonaws.com/api/pass_booking_history.php";
        //String url = "https://192.168.1.111/bounze/api/pass_booking_history.php";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
            jsonObject.put("shop_id", shop_id);
            jsonObject.put("service_id", service_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("url::" + url + "json" + jsonObject.toString());
        OkHttp3Connection.doOkHttp3Connection("", url, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d("value of ", " response: " + result);
                Gson gson = new Gson();
                QRServiceResponsePojo pojo = gson.fromJson(result, QRServiceResponsePojo.class);
                Toast.makeText(CurrentPassBooking.this, pojo.getBooking()[0].getMessage(), Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(CurrentPassBooking.this, error, Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }


    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void showHistory() {
        final AlertDialog OptionDialog = new AlertDialog.Builder(this).create();
        //     AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.alert_list, null);
        OptionDialog.setView(dialogView);
        OptionDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ArrayList<PassHistoryPojo> pojoArrayList = new ArrayList<>();
        PassHistoryPojo pojo;
        if (currentPassData.getSession_history() != null) {
            for (int tempPos = 0; tempPos < currentPassData.getSession_history().length; tempPos++) {
                pojo = new PassHistoryPojo();
                pojo.setServiceName(currentPassData.getSession_history()[tempPos].getService_name());
                pojo.setPassName(currentPassData.getSession_history()[tempPos].getShop_name());
                pojo.setDate(currentPassData.getSession_history()[tempPos].getDate_time());
                pojoArrayList.add(pojo);
            }
            PassHistoryAdapter adapter = new PassHistoryAdapter(CurrentPassBooking.this, pojoArrayList);
            RecyclerView rv_history = (RecyclerView) dialogView.findViewById(R.id.rv_history);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            rv_history.setLayoutManager(manager);
            rv_history.setAdapter(adapter);

            ImageView iv_back = (ImageView) dialogView.findViewById(R.id.iv_back);
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OptionDialog.dismiss();
                }
            });

            OptionDialog.show();
        } else
            Toast.makeText(CurrentPassBooking.this, "No history available.", Toast.LENGTH_SHORT).show();
    }
}
