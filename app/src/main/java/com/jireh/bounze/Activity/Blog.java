package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.data.BlogPojo;
import com.squareup.picasso.Picasso;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.BlogData;
import com.jireh.bounze.data.BookmarkPojo;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * <h2>Blog</h2>
 * This Class is used for showing the Blogs,
 * Created by Shubham on 06-10-2017.
 */

public class Blog extends AppCompatActivity implements View.OnClickListener {
    BlogData[] blogList;
    List<BlogData> blogArrayList;
    DBFunctions dbObject;
    LinearLayoutManager mLayoutManager;
    ProgressBar progress_bar;
    private RecyclerView mRecyclerView;
    private BlogAdapter mAdapter;
    private SessionManager sessionManager;
    private ImageView filter_img, iv_blog, iv_trending, iv_popular, iv_saved;
    Button btn_cancel, btn_save;
    private RelativeLayout rl_top_blogs, rl_trending, rl_most_popular, rl_saved;
    private LinearLayout ll_bottom, ll_back,filter_layout;
    private View invisible;
    private int filterFlag = 0;
    private ArrayList<BookmarkPojo> bookmarkList;
    private boolean bookmarkFlag;
    private String blogId, blogName;
    private OnItemClick mListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_layout);
        sessionManager = new SessionManager(this);
        blogList = sessionManager.getBlogResult();
        blogArrayList = new ArrayList<>();
        blogArrayList = Arrays.asList(blogList);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //on resume update the adapter with the bookmark
            callAdapter();
            //call the blog list api for the update of the blog list rating
            getBlogList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBlogList() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<BlogPojo> call = stationClient.getblogList();
        call.enqueue(new Callback<BlogPojo>() {
            @Override
            public void onResponse(Call<BlogPojo> call, Response<BlogPojo> response) {
                int response_code = response.code();

                BlogData[] result = response.body().getBlogData();
                response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    sessionManager.setBlogResult(result);
                    BlogData[] blogPojos = sessionManager.getBlogResult();
                    Log.d("", "value of blogPojos data: " + blogPojos.length);


                }
            }

            @Override
            public void onFailure(Call<BlogPojo> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }


    public void initView() {

        TextView title_txt = (TextView) findViewById(R.id.title_txt);
        title_txt.setText("Blogs");
        mRecyclerView = (RecyclerView) findViewById(R.id.pass_recycler);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        rl_top_blogs = (RelativeLayout) findViewById(R.id.rl_top_blogs);
        rl_trending = (RelativeLayout) findViewById(R.id.rl_trending);
        rl_most_popular = (RelativeLayout) findViewById(R.id.rl_most_populor);
        rl_saved = (RelativeLayout) findViewById(R.id.rl_saved);
        filter_img = (ImageView) findViewById(R.id.filter_img);
        iv_blog = (ImageView) findViewById(R.id.iv_blog);
        iv_trending = (ImageView) findViewById(R.id.iv_trending);
        iv_popular = (ImageView) findViewById(R.id.iv_popular);
        iv_saved = (ImageView) findViewById(R.id.iv_saved);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        filter_layout = (LinearLayout) findViewById(R.id.filter_layout);
        filter_img = (ImageView) findViewById(R.id.filter_img);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        invisible = findViewById(R.id.invisible);

        rl_top_blogs.setOnClickListener(this);
        rl_trending.setOnClickListener(this);
        rl_most_popular.setOnClickListener(this);
        rl_saved.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        filter_img.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        filter_layout.setOnClickListener(this);

        // use a linear layout manager
        dbObject = new DBFunctions(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        callAdapter();
    }

    /**
     * <h3>callAdapter</h3>
     * This method is used for itegrating the adapter with our Recycler view.
     */
    private void callAdapter() {
//        mAdapter = new BlogAdapter(this, blogList);
        mAdapter = new BlogAdapter(this, blogArrayList);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.setOnItemClickListener(new OnItemClick() {
            @Override
            public void onItemClicked(View view, int position, Object data) {
                //if the clicked item is book mark to insert on remove then call the adapter else move to next page
                if (view.getId() == R.id.iv_bookmark || view.getId() == R.id.bt_done) {
                    // specify an adapter
                    callAdapter();
                } else {
                    Intent intent = new Intent(Blog.this, BlogDescriptionActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", blogList[position].getBlog_id());
                    bundle.putString("date", blogList[position].getCreated_date());
                    bundle.putString("title", blogList[position].getBlog_title());
                    bundle.putString("image", blogList[position].getImage_url());
                    bundle.putString("description", blogList[position].getDescription());
                    bundle.putString("rating", blogList[position].getRatings());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:            //filterflag = 0
                filterFlag = 0;
                hideBottomLayout();
                break;
            case R.id.rl_top_blogs:         //filterflag = 1
                if (filterFlag != 1) {
                    filterFlag = 1;
                    iv_blog.setVisibility(View.VISIBLE);
                    iv_trending.setVisibility(View.GONE);
                    iv_popular.setVisibility(View.GONE);
                    iv_saved.setVisibility(View.GONE);
                } else {
                    filterFlag = 0;
                    iv_blog.setVisibility(View.GONE);
                }
                break;
            case R.id.rl_trending:          //filterflag = 2
                if (filterFlag != 2) {
                    filterFlag = 2;
                    iv_trending.setVisibility(View.VISIBLE);
                    iv_blog.setVisibility(View.GONE);
                    iv_popular.setVisibility(View.GONE);
                    iv_saved.setVisibility(View.GONE);
                } else {
                    filterFlag = 0;
                    iv_trending.setVisibility(View.GONE);
                }
                break;
            case R.id.rl_most_populor:     //filterflag = 3
                if (filterFlag != 3) {
                    iv_popular.setVisibility(View.VISIBLE);
                    iv_blog.setVisibility(View.GONE);
                    iv_trending.setVisibility(View.GONE);
                    iv_saved.setVisibility(View.GONE);
                    filterFlag = 3;
                } else {
                    iv_popular.setVisibility(View.GONE);
                    filterFlag = 0;
                }
                break;
            case R.id.rl_saved:            //filterflag = 4
                if (filterFlag != 4) {
                    iv_saved.setVisibility(View.VISIBLE);
                    iv_blog.setVisibility(View.GONE);
                    iv_trending.setVisibility(View.GONE);
                    iv_popular.setVisibility(View.GONE);
                    filterFlag = 4;
                } else {
                    iv_saved.setVisibility(View.GONE);
                    filterFlag = 0;
                }
                break;

            case R.id.btn_save:
                int pos = 0;
                if (filterFlag == 1) {
                    blogArrayList = new ArrayList<BlogData>();
                    for (pos = 0; pos < blogList.length; pos++) {
                        if (blogList[pos].getCategory_name().equalsIgnoreCase("Top Blog")) {
                            blogArrayList.add(blogList[pos]);
                        }
                    }
                } else if (filterFlag == 2) {
                    blogArrayList = new ArrayList<BlogData>();
                    for (pos = 0; pos < blogList.length; pos++) {
                        if (blogList[pos].getCategory_name().equalsIgnoreCase("Trending")) {
                            blogArrayList.add(blogList[pos]);
                        }
                    }
                } else if (filterFlag == 3) {
                    blogArrayList = new ArrayList<BlogData>();
                    for (pos = 0; pos < blogList.length; pos++) {
                        if (blogList[pos].getCategory_name().equalsIgnoreCase("Most Popular")) {
                            blogArrayList.add(blogList[pos]);
                        }
                    }
                }
                if (filterFlag == 0) {
                    blogList = new BlogData[sessionManager.getBlogResult().length];
                    blogList = sessionManager.getBlogResult();
                    blogArrayList = Arrays.asList(blogList);
                }
                if (filterFlag == 0)
                    Toast.makeText(this, "Choose an Option", Toast.LENGTH_SHORT).show();
                hideBottomLayout();
                callAdapter();
                break;

            case R.id.filter_img:

                showBottomLayout();
                break;

            case R.id.ll_back:
                finish();
                break;
            case R.id.filter_layout:
                filterFlag = 0;
                hideBottomLayout();
                break;
        }
    }

    private void showBottomLayout() {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);
        ll_bottom.startAnimation(bottomUp);
        ll_bottom.setVisibility(View.VISIBLE);
        invisible.setVisibility(View.VISIBLE);
    }

    private void hideBottomLayout() {
        Animation bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down);
        ll_bottom.startAnimation(bottomDown);
        ll_bottom.setVisibility(View.GONE);
        invisible.setVisibility(View.GONE);
    }

    private void checkBookmark(BlogAdapter.ViewHolder holder, BlogData blog) {
        bookmarkList = sessionManager.getBookmarkBlog();
        bookmarkFlag = true;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (bookmarkList.get(tempPos).getBlogId().equals(blog.getBlog_id())) {
                bookmarkFlag = false;
                break;
            }
        }
        if (bookmarkFlag)       //Not yet bookmarked.
        {
            holder.iv_bookmark.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmark));
        } else                   //Already bookmarked.
        {
            holder.iv_bookmark.setBackgroundColor(getResources().getColor(R.color.transparent));
            holder.iv_bookmark.setBackgroundDrawable(getResources().getDrawable(R.drawable.bookmar_select));

        }
    }

    private String changeDateFormat(String time) {

        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd-MM-yyyy, hh:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;

    }

    public void showRemoveBookmarkAlert(final int position) {
        AlertDialog.Builder mBuild = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Bookmark");
        tv_msg.setText("Are you sure, you want to remove the Bookmark?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClicked(view, position, "");
                removeBookmark();
                dialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void removeBookmark() {
        BookmarkPojo pojo = new BookmarkPojo();
        pojo.setBlogId(blogId);
        pojo.setBlogName(blogName);
        int removePos = 0;
        for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
            if (blogId.equals(bookmarkList.get(tempPos).getBlogId())) {
                removePos = tempPos;
            }
        }
        bookmarkList.remove(removePos);
        sessionManager.setBookmarkBlog(bookmarkList);
//        ll_bookmark.setBackgroundColor(Color.parseColor("#FFFFFF"));
        doBookmark("2");
    }

    private void doBookmark(String bookmark) {
        if (isInternetOn()) {
            showProgressDialog();
            ratingBookmarkService(blogId, sessionManager.getUserId(), bookmark, "bookmark");


        } else {
            Toast.makeText(Blog.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void ratingBookmarkService(String blogId, String userId, String rating_bookmark, String flag) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<ResponsePojo> call;
        if (flag.equals("rating"))
            call = stationClient.createBlogRating(userId, blogId, "",rating_bookmark);
        else
            call = stationClient.createBlogBookmark(userId, blogId, rating_bookmark);
        call.enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                int response_code = response.code();
                ResponsePojo result = response.body();
                if (response_code == 200) {
                    if (result.getStatus()[0].getStatus().equals("1")) {
                        Toast.makeText(Blog.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(Blog.this, result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Toast.makeText(Blog.this, "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }


    public interface OnItemClick {
        public void onItemClicked(View view, int position, Object data);
    }

    public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.ViewHolder> {
        List<BlogData> blogList;
        private Context mContext;


        // Provide a suitable constructor (depends on the kind of dataset)
        public BlogAdapter(Context context, List<BlogData> blogList) {
            this.mContext = context;
            this.blogList = blogList;
        }

        public void setOnItemClickListener(OnItemClick listener) {
            mListener = listener;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public BlogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_row_layout, parent, false);
            // set the view's size, margins, paddings and layout parameters
            BlogAdapter.ViewHolder vh = new BlogAdapter.ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final BlogAdapter.ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element


            final BlogData model = getItem(position);
            //change the date format
            String formattedDate = changeDateFormat(model.getCreated_date());
            holder.tv_blog_name.setText(model.getBlog_title());
            holder.tv_description.setText(model.getDescription());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, position, model);
                }
            });

            Picasso.with(mContext).load(BASE_URL + model.getImage_url()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.pass_logo_img);

            holder.tv_date.setText(formattedDate);
            //if it is book marked in the single blog get the id and book mark it in the adapter
            checkBookmark(holder, model);
            holder.iv_bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, position, model);
                    blogId = model.getBlog_id();
                    blogName = model.getBlog_title();
                    bookmarkList = sessionManager.getBookmarkBlog();
                    bookmarkFlag = true;
                    for (int tempPos = 0; tempPos < bookmarkList.size(); tempPos++) {
                        if (bookmarkList.get(tempPos).getBlogId().equals(model.getBlog_id())) {
                            bookmarkFlag = false;
                            break;
                        }
                    }
                    if (bookmarkFlag) {
                        BookmarkPojo pojo = new BookmarkPojo();
                        pojo.setBlogId(model.getBlog_id());
                        pojo.setBlogName(model.getBlog_title());
                        bookmarkList.add(bookmarkList.size(), pojo);
                        sessionManager.setBookmarkBlog(bookmarkList);
//                ll_bookmark.setBackgroundColor(Color.parseColor("#64B5F6"));

                        doBookmark("1");
                    } else {
                        showRemoveBookmarkAlert(position);
                    }
                }
            });
        }

        private BlogData getItem(int position) {
            return blogList.get(position);
        }

        private Context getContext() {
            return mContext;
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return blogList.size();
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            ImageView iv_bookmark, pass_logo_img;
            TextView tv_date, tv_blog_name, tv_description;

            public ViewHolder(View view) {
                super(view);
                pass_logo_img = (ImageView) view.findViewById(R.id.pass_logo_img);
                iv_bookmark = (ImageView) view.findViewById(R.id.iv_bookmark);
                tv_date = (TextView) view.findViewById(R.id.tv_date);
                tv_blog_name = (TextView) view.findViewById(R.id.tv_blog_name);
                tv_description = (TextView) view.findViewById(R.id.tv_description);
            }
        }
    }
}

  /*  public String convertDate(String dateTime)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm aa");
        String dateInString = "2015-07-16 17:07:21";
        Date date;
        try {

            date = formatter.parse(dateInString);

            System.out.println(date);
            System.out.println(formatter.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.toString();
    }*/
