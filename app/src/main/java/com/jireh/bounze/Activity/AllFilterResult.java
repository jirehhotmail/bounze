package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.jireh.bounze.Adapter.CoachAdapter;
import com.jireh.bounze.Adapter.OnTheGoAdapter;
import com.jireh.bounze.Adapter.PassLogoAdapter;
import com.jireh.bounze.Fragment.Pass;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassList;
import com.jireh.bounze.data.Service_logo;
import com.jireh.bounze.data.Shop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 12-10-2017.
 */

public class AllFilterResult extends Activity {
    RecyclerView all_filter_recycler;
    List<Shop> shopLists;
    List<Coach> coachList;
    ArrayList<PassArr> passArrList = new ArrayList<>();
    Toolbar toolbar;
    TextView title;
    ImageView back_img;
    OnTheGoAdapter shopAdapter;
    Multimap<Integer, String> servicLogo = ArrayListMultimap.create();
    ArrayList<PassList> passModels = new ArrayList<>();
    SessionManager sessionManager;
    PassArr[] passList;
    GPSTracker gpsTracker;
    private PassAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_filter_layout);
        initVariable();
        sessionManager = new SessionManager(AllFilterResult.this);
        //set the title,adapter  based on flag
        String flag = getIntent().getStringExtra("FilterFlag");
        if (flag.equals("shop")) {
            title.setText("Shop");
            //get the shoplist filter values
            shopLists = (List<Shop>) getIntent().getSerializableExtra("ShopList");

            //add the distane in it
            gpsTracker = new GPSTracker(AllFilterResult.this);

            if (!gpsTracker.canGetLocation()) {

                gpsTracker.showSettingsAlert();
            } else {

                //get the latitude and longitude and get the result and set it to shop list
                try {
                    for (int idx = 0; idx < shopLists.size(); idx++) {

                        Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(shopLists.get(idx).getLatitude()), Double.parseDouble(shopLists.get(idx).getLongitude()), shopLists.get(idx).getResults());
                        shopLists.get(idx).setResults(shopLists.get(idx).getResults());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                shopAdapter = new OnTheGoAdapter(AllFilterResult.this, shopLists, new OnTheGoAdapter.onItemClickListener() {
                    @Override
                    public void onItemClick(int view, int position) {
                        Intent in = new Intent(AllFilterResult.this, ShopDescriptionActivity.class);
                        in.putExtra("ShopDetails", shopLists.get(position));
                        startActivity(in);
                    }
                });
                all_filter_recycler.setAdapter(shopAdapter);
            }
        } else if (flag.equals("coach")) {
            title.setText("Coach");
            coachList = (List<Coach>) getIntent().getSerializableExtra("CoachList");
            CoachAdapter coachAdapter = new CoachAdapter(AllFilterResult.this, coachList, new CoachAdapter.onItemClickListener() {
                @Override
                public void onItemClick(int view, int position) {
                    startActivity(new Intent(AllFilterResult.this, CoachDescriptionActivity.class).putExtra("CoachId", coachList.get(position).getCid()));
                }
            });
            all_filter_recycler.setAdapter(coachAdapter);
        } else if (flag.equals("pass")) {
            title.setText("Pass");
            passList = sessionManager.getPass_Result();
            //get the pass ids from the previous activity

            //convert the string array to array list of the pass id's
            String[] passIds = getIntent().getStringArrayExtra("PassList");
            List<String> passIdList = Arrays.asList(passIds);
            //insert all pass id in an arraylist
            ArrayList<String> allPassId = new ArrayList<>();
            for (int idx = 0; idx < passList.length; idx++) {
                allPassId.add(passList[idx].getPass_id());
            }
            //loop with result pass id list and if all pass id contains this pass id then dispaly the pass details
            for (int idx = 0; idx < passIdList.size(); idx++) {
                if (allPassId.contains(passIdList.get(idx))) {
                    //Get the index of the pass id in all passid list and add the details in the list
                    int index = allPassId.indexOf(passIdList.get(idx));
                    passArrList.add(passList[index]);
                }
            }

            //add the service logo into models
            for (int idx = 0; idx < passArrList.size(); idx++) {
                PassList dm = new PassList();
                ArrayList<String> singleItem = new ArrayList<String>();

                for (Integer key : servicLogo.keySet()) {
                    if (key == idx) {
                        List<String> values = (List<String>) servicLogo.get(key);
                        singleItem.addAll(values);
                    }
                }

                dm.setAllItemsInSection(singleItem);

                passModels.add(dm);
            }
            // specify an adapter
            callAdapter();

        }
        setOnClickListener();


    }

    private void callAdapter() {

        //conver the passArraylist to passArra

        mAdapter = new PassAdapter(AllFilterResult.this, passArrList, passModels);
        all_filter_recycler.setAdapter(mAdapter);

        all_filter_recycler.setItemAnimator(new DefaultItemAnimator());

        mAdapter.setOnItemClickListener(new Pass.OnItemClick() {
            @Override
            public void onItemClicked(View view, int position, Object data) {
            }
        });
    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        all_filter_recycler = (RecyclerView) findViewById(R.id.all_filter_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        all_filter_recycler.setLayoutManager(manager);
    }

    public class PassAdapter extends RecyclerView.Adapter<PassAdapter.ViewHolder> {
        List<PassArr> passList;
        ArrayList<PassList> passModel;
        private Context mContext;
        private Pass.OnItemClick mListener;

        // Provide a suitable constructor (depends on the kind of dataset)
        public PassAdapter(Context context, List<PassArr> passList, ArrayList<PassList> passModel) {
            this.mContext = context;
            this.passList = passList;
            this.passModel = passModel;
        }

        public void setOnItemClickListener(Pass.OnItemClick listener) {
            mListener = listener;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pass_row_layout, parent, false);
            // set the view's size, margins, paddings and layout parameters

            PassAdapter.ViewHolder vh = new PassAdapter.ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final PassAdapter.ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final PassArr model = getItem(position);
            holder.pass_name.setText(model.getPass_name());


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, position, model);
                }
            });

            Picasso.with(mContext).load(BASE_URL + model.getLogo()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.pass_logo_img);


            if (!model.getRatings().equals(""))
                holder.ratingbar.setRating(Float.valueOf(String.valueOf(model.getRatings())));
            else
                holder.ratingbar.setRating(Float.valueOf(0.0f));

            holder.location_area_txt.setText(model.getLocation());
            holder.session_txt.setText(model.getSessions() + " Sessions");
            holder.price_txt.setText(mContext.getResources().getString(R.string.Rs) + model.getPrice());
            holder.bought_total_txt.setText(model.getTotal_bought() + "+ bought");


            holder.location_area_txt.setMovementMethod(new ScrollingMovementMethod());

            holder.book_pass_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AllFilterResult.this, PassDescriptionActivity.class);
                    sessionManager.setSinglePass(model);
                    startActivity(intent);
                }
            });

            Service_logo[] service_Logo_Arr = passList.get(position).getService_logo();
            ArrayList<String> serviceLogo = new ArrayList<>();
            for (int tempPos = 0; tempPos < service_Logo_Arr.length; tempPos++) {
                serviceLogo.add(tempPos, service_Logo_Arr[tempPos].getService_icon());
            }

            holder.horizontalList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            PassLogoAdapter horizontalAdapter = new PassLogoAdapter(mContext, serviceLogo);
            holder.horizontalList.setAdapter(horizontalAdapter);
        }

        private PassArr getItem(int position) {
            return passArrList.get(position);
        }

        private Context getContext() {
            return mContext;
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return passArrList.size();
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            ImageView pass_logo_img;
            TextView location_area_txt, session_txt, price_txt, bought_total_txt, pass_name;
            Button book_pass_btn;
            RatingBar ratingbar;
            private RecyclerView horizontalList;

            public ViewHolder(View view) {
                super(view);
                pass_logo_img = (ImageView) view.findViewById(R.id.pass_logo_img);
                pass_name = (TextView) view.findViewById(R.id.pass_name);
                ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
                location_area_txt = (TextView) view.findViewById(R.id.location_area_txt);
                session_txt = (TextView) view.findViewById(R.id.session_txt);
                price_txt = (TextView) view.findViewById(R.id.price_txt);
                bought_total_txt = (TextView) view.findViewById(R.id.bought_total_txt);
                book_pass_btn = (Button) view.findViewById(R.id.book_pass_btn);
                horizontalList = (RecyclerView) itemView.findViewById(R.id.horizontal_list);
            }
        }
    }
}

