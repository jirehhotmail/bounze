package com.jireh.bounze.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.jireh.bounze.Fragment.HowItWorks;
import com.jireh.bounze.Fragment.Rewards;
import com.jireh.bounze.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 13-12-2017.
 */

public class Wallet extends AppCompatActivity {

    Toolbar toolbar;
    TextView title_txt;
    ImageView back_img;
    EditText referral_code;
    ViewPager pager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_layout);
        //hide keyboard when open

        Wallet.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initVariable();
        setOnClickListener();


    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        referral_code = (EditText) findViewById(R.id.referral_code);
        title_txt.setText("Wallet");
        // Initialize the ViewPager and set an adapter
        pager = (ViewPager) findViewById(R.id.pager);
        WalletPagerAdapter adapter = new WalletPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new Rewards(), "Rewards");
        adapter.addFrag(new HowItWorks(), "How it works");

        pager.setAdapter(adapter);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tab_layout);

        //set custom font for the tab
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-SemiBold.otf");


        tabs.setTypeface(typeFace, 0);
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private class WalletPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public WalletPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("get item::" + position);
            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}




