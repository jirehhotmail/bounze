package com.jireh.bounze.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.Util.CustomEditText;

/**
 * Created by Muthamizhan C on 29-07-2017.
 */

public class SignUp extends Activity {
    EditText username_edt, email_edt, mobile_edt;
    CustomEditText password_edt;
    Button register_btn;
    ImageView back_img;
    CheckBox terms_checkbox;
    Toolbar signup;
    TextView title;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    // ProgressDialog mProgressDialog;

    String TAG = "Registration";
    int count = 0;
    ScrollView scroll_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
        initVariable();

        //set title for the page
        title.setText("Create Account");
        setOnClickListener();
        // check the textbox automatically
        terms_checkbox.setChecked(true);
      //hide the keypad when open the page

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ((ScrollView) findViewById(R.id.scroll_layout)).post(new Runnable() {
            public void run() {
                ((ScrollView) findViewById(R.id.scroll_layout)).scrollTo(0, 0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //set enabled false for the register button
            register_btn.setEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickListener() {
        terms_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                terms_checkbox.setChecked(true);
                Intent in = new Intent(SignUp.this, TermsAndConditions.class);
                startActivity(in);
            }
        });
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        //set font style for password

        password_edt.setTransformationMethod(new PasswordTransformationMethod());


        //set the drawable click listener for the password right to show the password

        password_edt.setDrawableClickListener(new CustomEditText.DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:

                        if (count % 2 == 0) {
                            password_edt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            count++;

                        } else {
                            password_edt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            count++;

                        }
                        System.out.println("password clicked");

                        break;

                    default:
                        break;
                }
            }

        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isValidName, isValidEmail = false, isValidPhone = false, isValidPassword = false;
                if (username_edt.getText().toString().matches("")) {
                    username_edt.setError("Enter first name !");
                    isValidName = false;
                } else {
                    isValidName = true;
                }

                if (email_edt.getText().toString().matches("")) {
                    email_edt.setError("Enter E-mail Id !");
                } else if (!(email_edt.getText().toString().matches(emailPattern))) {
                    email_edt.setError("Enter valid E-mail Id !");
                    isValidEmail = false;

                } else {
                    isValidEmail = true;
                }

                if (mobile_edt.getText().toString().length() == 10) {
                    isValidPhone = true;
                } else {
                    mobile_edt.setError("Enter Valid mobile number !");
                    isValidPhone = false;
                }

                if (password_edt.getText().toString().matches("")) {
                    password_edt.setError("Enter your password !");

                } else if (!(password_edt.getText().toString().length() >= 5)) {
                    password_edt.setError("Password should be minimum 5-char !");
                    isValidPassword = false;
                } else {
                    isValidPassword = true;
                }


                if (isValidName && isValidEmail && isValidPassword && isValidPhone) {
                    String name = username_edt.getText().toString().trim();
                    String email = email_edt.getText().toString().trim();
                    String phone_no = mobile_edt.getText().toString().trim();
                    String password = password_edt.getText().toString();
                    if (isInternetOn()) {
                        //set enabled false for the register button
                        register_btn.setEnabled(false);
                        //Navigate to otp page
                        Intent in = new Intent(SignUp.this, Otp.class);
                        in.putExtra("name", name);
                        in.putExtra("email", email);
                        in.putExtra("mobile_no", phone_no);
                        in.putExtra("password", password);
                        startActivity(in);


                    } else {
                        Toast.makeText(SignUp.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "check your details", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null)
                return netInfo.isConnected();
            else
                return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;

    }


    private void initVariable() {
        username_edt = (EditText) findViewById(R.id.username_edt);
        password_edt = (CustomEditText) findViewById(R.id.password_edt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        register_btn = (Button) findViewById(R.id.register_btn);
        terms_checkbox = (CheckBox) findViewById(R.id.terms_checkbox);

        signup = (Toolbar) findViewById(R.id.toolbar);
        back_img = (ImageView) signup.findViewById(R.id.back_img);
        title = (TextView) signup.findViewById(R.id.title_txt);
        scroll_layout = (ScrollView) findViewById(R.id.scroll_layout);
    }


}
