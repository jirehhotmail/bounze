package com.jireh.bounze.Activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jireh.bounze.Adapter.CoachCategoryAdapter;
import com.jireh.bounze.Adapter.OnTheGoAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.data.Content;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;


/**
 * Created by Muthamizhan C on 19-08-2017.
 */

public class ShopActivity extends AppCompatActivity {


    ImageView back_img, search_img, filter_img;

    DBFunctions dbobject;
    LocationManager manager;
    List<Service> servicelist_home;
    Toolbar toolbar;
    TextView title_txt;
    List<Service> servicelist;
    List<Service> search_servicelist = new ArrayList<>();
    LinearLayout shop_recycler_layout;
    List<Service> search_sub_servicelist = new ArrayList<>();
    GPSTracker gpsTracker;
    RecyclerView service_recycler, sub_service_recycler, shop_recycler;
    String current_service = "", current_sub_service = "", service_title = "";
    ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_layout);

        dbobject = new DBFunctions(getApplicationContext());


        initVariable();

        title_txt.setText("Shops");

        //set visibility to the filter image

        search_img.setVisibility(View.GONE);
        filter_img.setVisibility(View.VISIBLE);
        getAllContent();
        //get all the sub services for the server which should be show in the main page
        getAllsubServiceHome();
        setOnClickListener();

    }

    private void initVariable() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);

        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        shop_recycler_layout = (LinearLayout) findViewById(R.id.shop_recycler_layout);
        shop_recycler = (RecyclerView) findViewById(R.id.shop_recycler);
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void getAllsubServiceHome() {
        showProgressDialog();

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Content> call = stationClient.getsubservicelistHome();
        call.enqueue(new Callback<Content>() {
            @Override
            public void onResponse(Call<Content> call, Response<Content> response) {
                int response_code = response.code();
                Content result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    servicelist_home = result.getService();
                    hideProgressDialog();

                    //get the all sub services and send to image adapter and on click sub service get the current service
                    sub_service_recycler.setAdapter(new SubServiceImageAdapter(ShopActivity.this, servicelist_home, new CoachCategoryAdapter.onItemClickListener() {
                        @Override
                        public void onItemClick(int item, int position) {
                            current_sub_service = servicelist_home.get(position).getIconid().toString();
                            service_title = servicelist_home.get(position).getTitle().toString();
                            Intent in = new Intent(ShopActivity.this, Shoplistactivity.class);
                            in.putExtra("current_sub_service", current_sub_service);
                            in.putExtra("current_title", service_title);
                            startActivity(in);
                        }
                    }));

                } else {
                    hideProgressDialog();
                }

            }

            @Override
            public void onFailure(Call<Content> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });


    }

    private void setOnClickListener() {
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopActivity.this, ShopFilter.class);
                startActivity(in);
            }
        });


    }



    public void getAllContent() {
        //set the service manager
        service_recycler = (RecyclerView) findViewById(R.id.service_recycler);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        service_recycler.setLayoutManager(manager);

        //set the sub service manager
        sub_service_recycler = (RecyclerView) findViewById(R.id.sub_service_recycler);
        GridLayoutManager sub_manager = new GridLayoutManager(this, 3);
        sub_service_recycler.setLayoutManager(sub_manager);

        //set the shop recycler manager
        LinearLayoutManager shop_list_manager = new LinearLayoutManager(this);
        shop_recycler.setLayoutManager(shop_list_manager);


        servicelist = dbobject.getServiceListDB();


        //get the all services and send to image adapter and on click service get the current service
        service_recycler.setAdapter(new ImageAdapter(ShopActivity.this, servicelist, new CoachCategoryAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int item, int position) {
                current_service = servicelist.get(position).getSid().toString();
                service_title = servicelist.get(position).getTitle().toString();
                Intent in = new Intent(ShopActivity.this, Shoplistactivity.class);
                in.putExtra("current_service", current_service);
                in.putExtra("current_title", service_title);
                startActivity(in);
            }
        }));


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }


    public static class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

        Activity activity;
        List<Service> serviceList;
        CoachCategoryAdapter.onItemClickListener listener;

        public ImageAdapter(Activity activity, List<Service> serviceList, CoachCategoryAdapter.onItemClickListener listener) {
            this.activity = activity;
            this.serviceList = serviceList;
            this.listener = listener;
        }

        public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ImageAdapter.ViewHolder vh = new ImageAdapter.ViewHolder(v);
            return vh;
        }


        @Override
        public void onBindViewHolder(final ImageAdapter.ViewHolder holder, final int position) {
            final Service check1 = serviceList.get(position);


            holder.facility_title.setText("" + check1.getTitle());
            Picasso.with(activity).load(BASE_URL + check1.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.service_image);
            holder.facility_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(R.id.facility_layout, position);
                }
            });

        }


        @Override
        public int getItemCount() {
            return serviceList.size();
        }

        public interface onItemClickListener {
            void onItemClick(int item, int position);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView service_image;
            TextView facility_title;
            LinearLayout facility_layout;

            public ViewHolder(View view) {
                super(view);

                service_image = (ImageView) view.findViewById(R.id.service_image);
                facility_title = (TextView) view.findViewById(R.id.facility_title);
                facility_layout = (LinearLayout) view.findViewById(R.id.facility_layout);

            }
        }
    }


    public static class SubServiceImageAdapter extends RecyclerView.Adapter<SubServiceImageAdapter.ViewHolder> {

        Activity activity;
        List<Service> serviceList;
        CoachCategoryAdapter.onItemClickListener listener;

        public SubServiceImageAdapter(Activity activity, List<Service> serviceList, CoachCategoryAdapter.onItemClickListener listener) {
            this.activity = activity;
            this.serviceList = serviceList;
            this.listener = listener;
        }

        public SubServiceImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service, parent, false);
            // set the view's size, margins, paddings and layout parameters
            SubServiceImageAdapter.ViewHolder vh = new SubServiceImageAdapter.ViewHolder(v);
            return vh;
        }


        @Override
        public void onBindViewHolder(final SubServiceImageAdapter.ViewHolder holder, final int position) {
            final Service check1 = serviceList.get(position);


            holder.facility_title.setText("" + check1.getTitle());
            Picasso.with(activity).load(BASE_URL + check1.getImageUrl()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.service_image);
            holder.facility_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(R.id.facility_layout, position);
                }
            });

        }


        @Override
        public int getItemCount() {
            return serviceList.size();
        }

        public interface onItemClickListener {
            void onItemClick(int item, int position);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView service_image;
            TextView facility_title;
            LinearLayout facility_layout;

            public ViewHolder(View view) {
                super(view);

                service_image = (ImageView) view.findViewById(R.id.service_image);
                facility_title = (TextView) view.findViewById(R.id.facility_title);
                facility_layout = (LinearLayout) view.findViewById(R.id.facility_layout);

            }
        }
    }

}
