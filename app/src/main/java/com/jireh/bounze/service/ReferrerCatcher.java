package com.jireh.bounze.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Muthamizhan C on 08-01-2018.
 */

public class ReferrerCatcher extends BroadcastReceiver {

    private static String referrer = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        referrer = "";
        Bundle extras = intent.getExtras();
        if(extras != null){
            referrer = extras.getString("referrer");

        }

        Log.w("REFERRER","Referer is: "+referrer);
    }

    //...
}