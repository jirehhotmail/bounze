package com.jireh.bounze.Intents;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.jireh.bounze.Activity.BlogDescriptionActivity;
import com.jireh.bounze.Activity.CoachDescriptionActivity;
import com.jireh.bounze.Activity.EventDescriptionActivity;
import com.jireh.bounze.Activity.PassDescriptionActivity;
import com.jireh.bounze.Activity.ShopDescriptionActivity;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.Shop;


public class IntentHelper {
    DBFunctions dbobject;
    public static String EXTRA_A_QUERY = "com.jireh.bounze.Activity.ShopDescriptionActivity";
    public static String EXTRA_B_QUERY = "com.jireh.bounze.Activity.CoachDescriptionActivity";
    public static String EXTRA_C_QUERY = "com.jireh.bounze.Activity.EventDescriptionActivity";

    public Intent newAActivityIntent(Context context, String shopId) {
        Intent i = new Intent(context, ShopDescriptionActivity.class);
        dbobject = new DBFunctions(context);
        Shop shop = dbobject.getShopListDetailbyId(shopId);
        i.putExtra("ShopDetails", shop);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return i;
    }

    public Intent newBActivityIntent(Context context, String coachId) {
        Intent i = new Intent(context, CoachDescriptionActivity.class);
        dbobject = new DBFunctions(context);
        i.putExtra("CoachId", coachId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    public Intent newCActivityIntent(Context context, String eventId) {
        Intent i = new Intent(context, EventDescriptionActivity.class);
        dbobject = new DBFunctions(context);
        Event eventDetails = dbobject.getEventListById(eventId);
        i.putExtra("EventDetails", eventDetails);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    public Intent newDActivityIntent(Context context, String passId) {
        Intent i = new Intent(context, PassDescriptionActivity.class);
        i.putExtra("passId", passId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    public Intent newEActivityIntent(Context context, String blogId) {
        Intent i = new Intent(context, BlogDescriptionActivity.class);
        i.putExtra("blogId", blogId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

/*
    public Intent newAWebIntent(Context context, String query) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(query));

        return browserIntent;
    }

    public Intent newBWebIntent(Context context, String query) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(query));

        return browserIntent;
    }

    public Intent newCWebIntent(Context context, String query) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(query));

        return browserIntent;
    }*/



}
