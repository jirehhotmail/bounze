package com.jireh.bounze.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

/**
 * Created by Shubham on 25-09-2017.
 */

public class PassArr implements Parcelable {
    private String logo;

    private String remove_status;

    private String sessions;

    private String[] slider_images;

    private String status;

    private String location;

    private String pass_id;

    private Service_logo[] service_logo;

    private String active_status;

    private String message;

    private String price;

    private String pass_name;

    private String description;

    private Service_details[] service_details;

    private String ratings;

    private String total_bought;

    public String getLogo ()
    {
        return logo;
    }

    public void setLogo (String logo)
    {
        this.logo = logo;
    }

    public String getRemove_status ()
    {
        return remove_status;
    }

    public void setRemove_status (String remove_status)
    {
        this.remove_status = remove_status;
    }

    public String getSessions ()
    {
        return sessions;
    }

    public void setSessions (String sessions)
    {
        this.sessions = sessions;
    }

    public String[] getSlider_images ()
    {
        return slider_images;
    }

    public void setSlider_images (String[] slider_images)
    {
        this.slider_images = slider_images;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getPass_id ()
    {
        return pass_id;
    }

    public void setPass_id (String pass_id)
    {
        this.pass_id = pass_id;
    }

    public Service_logo[] getService_logo ()
    {
        return service_logo;
    }

    public void setService_logo (Service_logo[] service_logo)
    {
        this.service_logo = service_logo;
    }

    public String getActive_status ()
    {
        return active_status;
    }

    public void setActive_status (String active_status)
    {
        this.active_status = active_status;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getPass_name ()
    {
        return pass_name;
    }

    public void setPass_name (String pass_name)
    {
        this.pass_name = pass_name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Service_details[] getService_details ()
    {
        return service_details;
    }

    public void setService_details (Service_details[] service_details)
    {
        this.service_details = service_details;
    }

    public String getRatings ()
    {
        return ratings;
    }

    public void setRatings (String ratings)
    {
        this.ratings = ratings;
    }

    public String getTotal_bought ()
    {
        return total_bought;
    }

    public void setTotal_bought (String total_bought)
    {
        this.total_bought = total_bought;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [logo = "+logo+", remove_status = "+remove_status+", sessions = "+sessions+", slider_images = "+slider_images+", status = "+status+", location = "+location+", pass_id = "+pass_id+", service_logo = "+service_logo+", active_status = "+active_status+", message = "+message+", price = "+price+", pass_name = "+pass_name+", description = "+description+", service_details = "+service_details+", ratings = "+ratings+", total_bought = "+total_bought+"]";
    }

    public PassArr()
    {

    }

    protected PassArr(Parcel in) {
        logo = in.readString();
        remove_status = in.readString();
        sessions = in.readString();
        status = in.readString();
        location = in.readString();
        pass_id = in.readString();
        active_status = in.readString();
        message = in.readString();
        price = in.readString();
        pass_name = in.readString();
        description = in.readString();
        ratings = in.readString();
        total_bought = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(logo);
        dest.writeString(remove_status);
        dest.writeString(sessions);
        dest.writeString(status);
        dest.writeString(location);
        dest.writeString(pass_id);
        dest.writeString(active_status);
        dest.writeString(message);
        dest.writeString(price);
        dest.writeString(pass_name);
        dest.writeString(description);
        dest.writeString(ratings);
        dest.writeString(total_bought);
    }

    @SuppressWarnings("unused")
    public static final Creator<PassArr> CREATOR = new Creator<PassArr>() {
        @Override
        public PassArr createFromParcel(Parcel in) {
            return new PassArr(in);
        }

        @Override
        public PassArr[] newArray(int size) {
            return new PassArr[size];
        }
    };

    public static Comparator<PassArr> ratingHighToLowPass = new Comparator<PassArr>() {
        @Override
        public int compare(PassArr passObj1, PassArr passObj2) {
            float rating1 = Float.parseFloat(passObj1.getRatings());
            float rating2 = Float.parseFloat(passObj2.getRatings());
            if (rating1 > rating2)
                return -1;
            else if (rating1<rating2)
                return  1;
            else
            return 0;
        }
    };
    public static Comparator<PassArr> ratingLowToHighPass = new Comparator<PassArr>() {
        @Override
        public int compare(PassArr passObj1, PassArr passObj2) {
            float rating1 = Float.parseFloat(passObj1.getRatings());
            float rating2 = Float.parseFloat(passObj2.getRatings());
            if (rating1 < rating2)
                return -1;
            else if (rating1 > rating2)
                return  1;
            else
            return 0;
        }
    };
}