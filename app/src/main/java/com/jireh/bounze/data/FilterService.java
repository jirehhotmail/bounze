package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 12-10-2017.
 */

public class FilterService implements Serializable {
    @SerializedName("result")
    @Expose
    private List<FilterModel> result = new ArrayList<FilterModel>();

    /**
     *
     * @return
     * The status
     */
    public List<FilterModel> getResult() {
        return result;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setResult(List<FilterModel> status) {
        this.result = status;
    }
}
