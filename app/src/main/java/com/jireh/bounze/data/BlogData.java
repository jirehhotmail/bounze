package com.jireh.bounze.data;

/**
 * Created by Shubham on 07-10-2017.
 */

public class BlogData
{
    private String active_status;

    private String created_date;

    private String remove_status;

    private String category_name;

    private String video_url;

    private String image_url;

    private String description;

    private String blog_id;

    private String blog_title;

    private String ratings;

    public String getActive_status ()
    {
        return active_status;
    }

    public void setActive_status (String active_status)
    {
        this.active_status = active_status;
    }

    public String getCreated_date ()
    {
        return created_date;
    }

    public void setCreated_date (String created_date)
    {
        this.created_date = created_date;
    }

    public String getRemove_status ()
    {
        return remove_status;
    }

    public void setRemove_status (String remove_status)
    {
        this.remove_status = remove_status;
    }

    public String getCategory_name ()
    {
        return category_name;
    }

    public void setCategory_name (String category_name)
    {
        this.category_name = category_name;
    }

    public String getVideo_url ()
    {
        return video_url;
    }

    public void setVideo_url (String video_url)
    {
        this.video_url = video_url;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getBlog_id ()
    {
        return blog_id;
    }

    public void setBlog_id (String blog_id)
    {
        this.blog_id = blog_id;
    }

    public String getBlog_title ()
    {
        return blog_title;
    }

    public void setBlog_title (String blog_title)
    {
        this.blog_title = blog_title;
    }

    public String getRatings ()
    {
        return ratings;
    }

    public void setRatings (String ratings)
    {
        this.ratings = ratings;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [active_status = "+active_status+", created_date = "+created_date+", remove_status = "+remove_status+", category_name = "+category_name+", video_url = "+video_url+", image_url = "+image_url+", description = "+description+", blog_id = "+blog_id+", blog_title = "+blog_title+", ratings = "+ratings+"]";
    }
}
