package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 29-08-2017.
 */

public class PassList {

    public List<PassModel> getPasslist() {
        return passlist;
    }

    public void setPasslist(List<PassModel> passlist) {
        this.passlist = passlist;
    }

    @SerializedName("pass")
    @Expose
    public List<PassModel> passlist;

    private ArrayList<String> allItemsInSection;

    public ArrayList<String> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<String> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }

}
