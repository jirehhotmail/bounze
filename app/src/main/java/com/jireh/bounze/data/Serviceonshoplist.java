package com.jireh.bounze.data;

/**
 * Created by vishnu on 08-12-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Serviceonshoplist {

    @SerializedName("serviceonshop")
    @Expose
    private List<ServiceOnAShop> serviceonshop = null;

    /**
     *
     * @return
     * The serviceonshop
     */
    public List<ServiceOnAShop> getServiceonshop() {
        return serviceonshop;
    }

    /**
     *
     * @param serviceonshop
     * The serviceonshop
     */
    public void setServiceonshop(List<ServiceOnAShop> serviceonshop) {
        this.serviceonshop = serviceonshop;
    }

}