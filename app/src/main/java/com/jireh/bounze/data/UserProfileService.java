package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 12-10-2017.
 */

public class UserProfileService implements Serializable {
    @SerializedName("userdetails")
    @Expose
    private List<UserProfile> userdetails = new ArrayList<UserProfile>();

    @SerializedName("health_tracking")
    @Expose
    private List<UserProfile> health_tracking = new ArrayList<UserProfile>();

    public List<UserProfile> getHealth_tracking() {
        return health_tracking;
    }

    public void setHealth_tracking(List<UserProfile> health_tracking) {
        this.health_tracking = health_tracking;
    }

    /**
     * @return The status
     */
    public List<UserProfile> getResult() {
        return userdetails;
    }

    /**
     * @param status The status
     */
    public void setResult(List<UserProfile> status) {
        this.userdetails = status;
    }
}
