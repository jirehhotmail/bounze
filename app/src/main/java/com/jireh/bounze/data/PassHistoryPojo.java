package com.jireh.bounze.data;

/**
 * Created by Shubham on 04-10-2017.
 */

public class PassHistoryPojo {
    private String date, passName, serviceName;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPassName() {
        return passName;
    }

    public void setPassName(String passName) {
        this.passName = passName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
