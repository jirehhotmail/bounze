package com.jireh.bounze.data;

/**
 * Created by vishnu on 07-12-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Shop implements Serializable {

    public boolean clicked;
    float[] results = new float[1];
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sid")
    @Expose
    private String sid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("avgratings")
    @Expose
    private String avgratings;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("tagone")
    @Expose
    private String tagone;
    @SerializedName("tagtwo")
    @Expose
    private String tagtwo;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("subservice_id")
    @Expose
    private String subserviceId;
    @SerializedName("image_url_square")
    @Expose
    private String imageUrlSquare;
    @SerializedName("image_url_rectangle")
    @Expose
    private String imageUrlRectangle;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("full_address")
    @Expose
    private String fullAddress;
    @SerializedName("small_address")
    @Expose
    private String smallAddress;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("shop_timings")
    @Expose
    private String shopTimings;
    @SerializedName("book_online")
    @Expose
    private String bookOnline;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("admin_lock_status")
    @Expose
    private String adminLockStatus;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("isRegnAvailable")
    @Expose
    private String isRegnAvailable;
    @SerializedName("isFreeTrailAvailable")
    @Expose
    private String isFreeTrailAvailable;
    @SerializedName("isOfferAvailable")
    @Expose
    private String isOfferAvailable;
    @SerializedName("no_of_freetrial")
    @Expose
    private String no_of_freetrial;
    @SerializedName("services")
    @Expose
    private String services;
    @SerializedName("subservices")
    @Expose
    private String subservices;
    private List<Amenities> amenities;
    private List<ServiceOnAShop> serviceonshop;
    private Double distanceonthego;

    public String getBookOnline() {
        return bookOnline;
    }

    public void setBookOnline(String bookOnline) {
        this.bookOnline = bookOnline;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public String getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(String subserviceId) {
        this.subserviceId = subserviceId;
    }

    public String getShopTimings() {
        return shopTimings;
    }

    public void setShopTimings(String shopTimings) {
        this.shopTimings = shopTimings;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getNo_of_freetrial() {
        return no_of_freetrial;
    }

    public void setNo_of_freetrial(String no_of_freetrial) {
        this.no_of_freetrial = no_of_freetrial;
    }

    public List<Amenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenities> amenities) {
        this.amenities = amenities;
    }

    public List<ServiceOnAShop> getServiceonshop() {
        return serviceonshop;
    }

    public void setServiceonshop(List<ServiceOnAShop> serviceonshop) {
        this.serviceonshop = serviceonshop;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getSubservices() {
        return subservices;
    }

    public void setSubservices(String subservices) {
        this.subservices = subservices;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public float[] getResults() {
        return results;
    }

    public void setResults(float[] results) {
        this.results = results;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAvgratings() {
        return avgratings;
    }

    public void setAvgratings(String avgratings) {
        this.avgratings = avgratings;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTagone() {
        return tagone;
    }

    public void setTagone(String tagone) {
        this.tagone = tagone;
    }

    public String getTagtwo() {
        return tagtwo;
    }

    public void setTagtwo(String tagtwo) {
        this.tagtwo = tagtwo;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getImageUrlSquare() {
        return imageUrlSquare;
    }

    public void setImageUrlSquare(String imageUrlSquare) {
        this.imageUrlSquare = imageUrlSquare;
    }

    public String getImageUrlRectangle() {
        return imageUrlRectangle;
    }

    public void setImageUrlRectangle(String imageUrlRectangle) {
        this.imageUrlRectangle = imageUrlRectangle;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getSmallAddress() {
        return smallAddress;
    }

    public void setSmallAddress(String smallAddress) {
        this.smallAddress = smallAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAdminLockStatus() {
        return adminLockStatus;
    }

    public void setAdminLockStatus(String adminLockStatus) {
        this.adminLockStatus = adminLockStatus;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getIsRegnAvailable() {
        return isRegnAvailable;
    }

    public void setIsRegnAvailable(String isRegnAvailable) {
        this.isRegnAvailable = isRegnAvailable;
    }


    public String getIsFreeTrailAvailable() {
        return isFreeTrailAvailable;
    }

    public void setIsFreeTrailAvailable(String isFreeTrailAvailable) {
        this.isFreeTrailAvailable = isFreeTrailAvailable;
    }

    public String getIsOfferAvailable() {
        return isOfferAvailable;
    }

    public void setIsOfferAvailable(String isOfferAvailable) {
        this.isOfferAvailable = isOfferAvailable;
    }


    public Double getDistanceonthego() {
        return distanceonthego;
    }

    public void setDistanceonthego(Double distanceonthego) {
        this.distanceonthego = distanceonthego;
    }
}
