package com.jireh.bounze.data;

/**
 * Created by Muthamizhan C on 22-09-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Amenities implements Serializable {

    private String shop_id;
    @SerializedName("aid")
    @Expose
    private String aid;
    @SerializedName("amenities")
    @Expose
    private String amenities;
    @SerializedName("icon")
    @Expose
    private String icon;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}


