package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muthamizhan C on 31-01-2018.
 */

public class Reward implements Serializable {

    @SerializedName("reward")
    @Expose
    private List<RewardDetails> reward = new ArrayList<RewardDetails>();

    public List<RewardDetails> getReward() {
        return reward;
    }

    public void setReward(List<RewardDetails> reward) {
        this.reward = reward;
    }


}
