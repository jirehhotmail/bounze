package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */


public class CoachBatchTime implements Serializable {


    @SerializedName("coach_batch_time_id")
    @Expose
    private String coachBatchTimeId;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;

    public String getCoachBatchTimeId() {
        return coachBatchTimeId;
    }

    public void setCoachBatchTimeId(String coachBatchTimeId) {
        this.coachBatchTimeId = coachBatchTimeId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
