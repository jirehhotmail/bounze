package com.jireh.bounze.data;

/**
 * Created by Muthamizhan C on 22-09-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ServiceOnAShop implements Serializable {

    @SerializedName("subservice_id")
    @Expose
    private String subserviceId;
    @SerializedName("pay_per_use")
    @Expose
    private String payPerUse;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("subservice_name")
    @Expose
    private String subserviceName;
    private List<ServiceDetails> service_details;

    public String getSubserviceId() {
        return subserviceId;
    }

    public void setSubserviceId(String subserviceId) {
        this.subserviceId = subserviceId;
    }

    public String getPayPerUse() {
        return payPerUse;
    }

    public void setPayPerUse(String payPerUse) {
        this.payPerUse = payPerUse;
    }

    public List<ServiceDetails> getService_details() {
        return service_details;
    }

    public void setService_details(List<ServiceDetails> service_details) {
        this.service_details = service_details;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSubserviceName() {
        return subserviceName;
    }

    public void setSubserviceName(String subserviceName) {
        this.subserviceName = subserviceName;
    }

}