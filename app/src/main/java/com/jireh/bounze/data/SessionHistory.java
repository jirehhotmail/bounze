package com.jireh.bounze.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shubham on 05-10-2017.
 */

public class SessionHistory implements Parcelable {
    private String service_id;

    private String id;

    private String shop_id;

    private String booking_id;

    private String service_name;

    private String date_time;

    private String shop_name;

    public String getService_id ()
    {
        return service_id;
    }

    public void setService_id (String service_id)
    {
        this.service_id = service_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getShop_id ()
    {
        return shop_id;
    }

    public void setShop_id (String shop_id)
    {
        this.shop_id = shop_id;
    }

    public String getBooking_id ()
    {
        return booking_id;
    }

    public void setBooking_id (String booking_id)
    {
        this.booking_id = booking_id;
    }

    public String getService_name ()
    {
        return service_name;
    }

    public void setService_name (String service_name)
    {
        this.service_name = service_name;
    }

    public String getDate_time ()
    {
        return date_time;
    }

    public void setDate_time (String date_time)
    {
        this.date_time = date_time;
    }

    public String getShop_name ()
    {
        return shop_name;
    }

    public void setShop_name (String shop_name)
    {
        this.shop_name = shop_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [service_id = "+service_id+", id = "+id+", shop_id = "+shop_id+", booking_id = "+booking_id+", service_name = "+service_name+", date_time = "+date_time+", shop_name = "+shop_name+"]";
    }

    protected SessionHistory(Parcel in) {
        service_id = in.readString();
        id = in.readString();
        shop_id = in.readString();
        booking_id = in.readString();
        service_name = in.readString();
        date_time = in.readString();
        shop_name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(service_id);
        dest.writeString(id);
        dest.writeString(shop_id);
        dest.writeString(booking_id);
        dest.writeString(service_name);
        dest.writeString(date_time);
        dest.writeString(shop_name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SessionHistory> CREATOR = new Parcelable.Creator<SessionHistory>() {
        @Override
        public SessionHistory createFromParcel(Parcel in) {
            return new SessionHistory(in);
        }

        @Override
        public SessionHistory[] newArray(int size) {
            return new SessionHistory[size];
        }
    };
}