package com.jireh.bounze.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shubham on 25-09-2017.
 */

public class Service_details implements Parcelable {
    @SuppressWarnings("unused")
    public static final Creator<Service_details> CREATOR = new Creator<Service_details>() {
        @Override
        public Service_details createFromParcel(Parcel in) {
            return new Service_details(in);
        }

        @Override
        public Service_details[] newArray(int size) {
            return new Service_details[size];
        }
    };
    private String subservice_id;
    private String subservice_name;
    private Shop_details[] shop_details;
    private String no_of_sessions;
    private String service_name;
    private String service_icon;

    protected Service_details(Parcel in) {
        subservice_id = in.readString();
        subservice_name = in.readString();
        no_of_sessions = in.readString();
        service_name = in.readString();
        service_icon = in.readString();
    }

    public String getSubservice_id() {
        return subservice_id;
    }

    public void setSubservice_id(String subservice_id) {
        this.subservice_id = subservice_id;
    }

    public String getSubservice_name() {
        return subservice_name;
    }

    public void setSubservice_name(String subservice_name) {
        this.subservice_name = subservice_name;
    }

    public Shop_details[] getShop_details() {
        return shop_details;
    }

    public void setShop_details(Shop_details[] shop_details) {
        this.shop_details = shop_details;
    }

    public String getNo_of_sessions() {
        return no_of_sessions;
    }

    public void setNo_of_sessions(String no_of_sessions) {
        this.no_of_sessions = no_of_sessions;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_icon() {
        return service_icon;
    }

    public void setService_icon(String service_icon) {
        this.service_icon = service_icon;
    }

    @Override
    public String toString() {
        return "ClassPojo [subservice_id = " + subservice_id + ", subservice_name = " + subservice_name + ", shop_details = " + shop_details + ", no_of_sessions = " + no_of_sessions + ", service_name = " + service_name + ", service_icon = " + service_icon + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subservice_id);
        dest.writeString(subservice_name);
        dest.writeString(no_of_sessions);
        dest.writeString(service_name);
        dest.writeString(service_icon);
    }
}