package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */

public class CoachBatchDetail implements Serializable {


    @SerializedName("coach_batch_id")
    @Expose
    private String coachBatchId;
    @SerializedName("coach_batch_name")
    @Expose
    private String coachBatchName;
    @SerializedName("coach_batch_time")
    @Expose
    private List<CoachBatchTime> coachBatchTime = null;

    public String getCoachBatchId() {
        return coachBatchId;
    }

    public void setCoachBatchId(String coachBatchId) {
        this.coachBatchId = coachBatchId;
    }

    public String getCoachBatchName() {
        return coachBatchName;
    }

    public void setCoachBatchName(String coachBatchName) {
        this.coachBatchName = coachBatchName;
    }

    public List<CoachBatchTime> getCoachBatchTime() {
        return coachBatchTime;
    }

    public void setCoachBatchTime(List<CoachBatchTime> coachBatchTime) {
        this.coachBatchTime = coachBatchTime;
    }

}