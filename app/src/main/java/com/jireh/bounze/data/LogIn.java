package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Triton-PC on 05-12-2016.
 */

public class LogIn{

    @SerializedName("status")
    @Expose
    private List<Profile> status = new ArrayList<Profile>();

    /**
     *
     * @return
     * The status
     */
    public List<Profile> getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(List<Profile> status) {
        this.status = status;
    }



}
