package com.jireh.bounze.data;

/**
 * Created by Shubham on 28-09-2017.
 */

public class ResponsePojo
{
    private Status[] status;

    public Status[] getStatus ()
    {
        return status;
    }

    public void setStatus (Status[] status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+"]";
    }



}
