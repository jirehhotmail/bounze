package com.jireh.bounze.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jireh_16 on 05-10-2017.
 */

public class PassCurrentHistory implements Parcelable {
    private String id;

    private String subservices;

    private String booking_id;

    private String location;

    private String pass_logo;

    private String pass_name;

    private String pass_id;

    private String total_sessions;

    private String subservices_image;

    private SessionHistory[] session_history;

    private String subservices_id;

    private String used_sessions;

    private String service_total_session;

    private String service_pending_session;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubservices() {
        return subservices;
    }

    public void setSubservices(String subservices) {
        this.subservices = subservices;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPass_logo() {
        return pass_logo;
    }

    public void setPass_logo(String pass_logo) {
        this.pass_logo = pass_logo;
    }

    public String getPass_name() {
        return pass_name;
    }

    public void setPass_name(String pass_name) {
        this.pass_name = pass_name;
    }

    public String getPass_id() {
        return pass_id;
    }

    public void setPass_id(String pass_id) {
        this.pass_id = pass_id;
    }

    public String getTotal_sessions() {
        return total_sessions;
    }

    public void setTotal_sessions(String total_sessions) {
        this.total_sessions = total_sessions;
    }

    public String getSubservices_image() {
        return subservices_image;
    }

    public void setSubservices_image(String subservices_image) {
        this.subservices_image = subservices_image;
    }

    public SessionHistory[] getSession_history() {
        return session_history;
    }

    public void setSession_history(SessionHistory[] session_history) {
        this.session_history = session_history;
    }

    public String getSubservices_id() {
        return subservices_id;
    }

    public void setSubservices_id(String subservices_id) {
        this.subservices_id = subservices_id;
    }

    public String getUsed_sessions() {
        return used_sessions;
    }

    public void setUsed_sessions(String used_sessions) {
        this.used_sessions = used_sessions;
    }

    public String getService_total_session() {
        return service_total_session;
    }

    public void setService_total_session(String service_total_session) {
        this.service_total_session = service_total_session;
    }

    public String getService_pending_session() {
        return service_pending_session;
    }

    public void setService_pending_session(String service_pending_session) {
        this.service_pending_session = service_pending_session;
    }

    public static Creator<PassCurrentHistory> getCREATOR() {
        return CREATOR;
    }

    protected PassCurrentHistory(Parcel in) {
        id = in.readString();
        subservices = in.readString();
        booking_id = in.readString();
        location = in.readString();
        pass_logo = in.readString();
        pass_name = in.readString();
        pass_id = in.readString();
        total_sessions = in.readString();
        subservices_image = in.readString();
        subservices_id = in.readString();
        used_sessions = in.readString();
        service_total_session = in.readString();
        service_pending_session = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(subservices);
        dest.writeString(booking_id);
        dest.writeString(location);
        dest.writeString(pass_logo);
        dest.writeString(pass_name);
        dest.writeString(pass_id);
        dest.writeString(total_sessions);
        dest.writeString(subservices_image);
        dest.writeString(subservices_id);
        dest.writeString(used_sessions);
        dest.writeString(service_total_session);
        dest.writeString(service_pending_session);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PassCurrentHistory> CREATOR = new Parcelable.Creator<PassCurrentHistory>() {
        @Override
        public PassCurrentHistory createFromParcel(Parcel in) {
            return new PassCurrentHistory(in);
        }

        @Override
        public PassCurrentHistory[] newArray(int size) {
            return new PassCurrentHistory[size];
        }
    };
}