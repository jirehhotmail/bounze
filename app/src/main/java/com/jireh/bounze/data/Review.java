package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Review {

    @SerializedName("status")
    @Expose
    private List<Status> status = null;
    @SerializedName("reviews")
    @Expose
    private List<Review_> reviews = null;

    public List<Status> getStatus() {

        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

    public List<Review_> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review_> reviews) {
        this.reviews = reviews;
    }


}
