package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */

public class ChProgram implements Serializable {
    @SerializedName("program")
    @Expose
    private List<Program> program = null;

    public List<Program> getProgram() {
        return program;
    }

    public void setProgram(List<Program> program) {
        this.program = program;
    }
}
