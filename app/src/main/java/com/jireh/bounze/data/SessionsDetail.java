package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */

public class SessionsDetail implements Serializable
{

    @SerializedName("session_id")
    @Expose
    private String sessionId;
    @SerializedName("no_of_days")
    @Expose
    private String noOfDays;
    @SerializedName("days_unit")
    @Expose
    private String daysUnit;
    @SerializedName("sessions")
    @Expose
    private String sessions;
    @SerializedName("sessions_cost")
    @Expose
    private String sessionsCost;
    @SerializedName("sessions_offer_cost")
    @Expose
    private String sessionsOfferCost;


    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getDaysUnit() {
        return daysUnit;
    }

    public void setDaysUnit(String daysUnit) {
        this.daysUnit = daysUnit;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getSessionsCost() {
        return sessionsCost;
    }

    public void setSessionsCost(String sessionsCost) {
        this.sessionsCost = sessionsCost;
    }

    public String getSessionsOfferCost() {
        return sessionsOfferCost;
    }

    public void setSessionsOfferCost(String sessionsOfferCost) {
        this.sessionsOfferCost = sessionsOfferCost;
    }

}