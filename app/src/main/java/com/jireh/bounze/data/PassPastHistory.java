package com.jireh.bounze.data;

/**
 * Created by jireh_16 on 05-10-2017.
 */

public class PassPastHistory {
    private String id;

    private String subservices;

    private String booking_id;

    private String location;

    private String pass_logo;

    private String pass_name;

    private String pass_id;

    private String total_sessions;

    private String subservices_image;

    private SessionHistory[] session_history;

    private String subservices_id;

    private String used_sessions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubservices() {
        return subservices;
    }

    public void setSubservices(String subservices) {
        this.subservices = subservices;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPass_logo() {
        return pass_logo;
    }

    public void setPass_logo(String pass_logo) {
        this.pass_logo = pass_logo;
    }

    public String getPass_name() {
        return pass_name;
    }

    public void setPass_name(String pass_name) {
        this.pass_name = pass_name;
    }

    public String getPass_id() {
        return pass_id;
    }

    public void setPass_id(String pass_id) {
        this.pass_id = pass_id;
    }

    public String getTotal_sessions() {
        return total_sessions;
    }

    public void setTotal_sessions(String total_sessions) {
        this.total_sessions = total_sessions;
    }

    public String getSubservices_image() {
        return subservices_image;
    }

    public void setSubservices_image(String subservices_image) {
        this.subservices_image = subservices_image;
    }

    public SessionHistory[] getSession_history() {
        return session_history;
    }

    public void setSession_history(SessionHistory[] session_history) {
        this.session_history = session_history;
    }

    public String getSubservices_id() {
        return subservices_id;
    }

    public void setSubservices_id(String subservices_id) {
        this.subservices_id = subservices_id;
    }

    public String getUsed_sessions() {
        return used_sessions;
    }

    public void setUsed_sessions(String used_sessions) {
        this.used_sessions = used_sessions;
    }
}
