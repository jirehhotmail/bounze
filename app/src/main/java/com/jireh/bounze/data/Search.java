package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Search implements Serializable {

    @SerializedName("result")
    @Expose
    private List<ResultModel> result = null;
    @SerializedName("status")
    @Expose
    private List<ResultModel> status = new ArrayList<ResultModel>();





    public List<ResultModel> getResult() {
        return result;
    }

    public void setResult(List<ResultModel> result) {
        this.result = result;
    }

    public List<ResultModel> getStatus() {
        return status;
    }

    public void setStatus(List<ResultModel> status) {
        this.status = status;
    }


}
