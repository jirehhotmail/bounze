package com.jireh.bounze.data;

/**
 * Created by Muthamizhan C on 03-11-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CategoryDetail implements Serializable {

    private String unique_coach_id;
    private String unique_coach_cat_id;
    private List<CoachBatchTimeDetail> unique_ch_bt_tm_dtl;
    @SerializedName("coach_package")
    @Expose
    private String coachPackage;
    @SerializedName("coach_package_name")
    @Expose
    private String coachPackageName;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("anythings")
    @Expose
    private String anythings;
    @SerializedName("coach_batch_details")
    @Expose
    private List<CoachBatchDetail> coachBatchDetails = null;
    @SerializedName("sessions_details")
    @Expose
    private List<SessionsDetail> sessionsDetails = null;

    public String getUnique_coach_cat_id() {
        return unique_coach_cat_id;
    }

    public void setUnique_coach_cat_id(String unique_coach_cat_id) {
        this.unique_coach_cat_id = unique_coach_cat_id;
    }

    public List<CoachBatchTimeDetail> getUnique_ch_bt_tm_dtl() {
        return unique_ch_bt_tm_dtl;
    }

    public void setUnique_ch_bt_tm_dtl(List<CoachBatchTimeDetail> unique_ch_bt_tm_dtl) {
        this.unique_ch_bt_tm_dtl = unique_ch_bt_tm_dtl;
    }

    public String getUnique_coach_id() {
        return unique_coach_id;
    }

    public void setUnique_coach_id(String unique_coach_id) {
        this.unique_coach_id = unique_coach_id;
    }

    public String getCoachPackage() {
        return coachPackage;
    }

    public void setCoachPackage(String coachPackage) {
        this.coachPackage = coachPackage;
    }

    public String getCoachPackageName() {
        return coachPackageName;
    }

    public void setCoachPackageName(String coachPackageName) {
        this.coachPackageName = coachPackageName;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAnythings() {
        return anythings;
    }

    public void setAnythings(String anythings) {
        this.anythings = anythings;
    }

    public List<CoachBatchDetail> getCoachBatchDetails() {
        return coachBatchDetails;
    }

    public void setCoachBatchDetails(List<CoachBatchDetail> coachBatchDetails) {
        this.coachBatchDetails = coachBatchDetails;
    }

    public List<SessionsDetail> getSessionsDetails() {
        return sessionsDetails;
    }

    public void setSessionsDetails(List<SessionsDetail> sessionsDetails) {
        this.sessionsDetails = sessionsDetails;
    }

}