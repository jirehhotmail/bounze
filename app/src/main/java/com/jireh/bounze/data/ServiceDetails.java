package com.jireh.bounze.data;

/**
 * Created by Muthamizhan C on 22-09-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ServiceDetails implements Serializable {

    List<BatchDetails> batch_details;
    List<Sessions> sessions_details;
    private String shop_id;
    private String sub_service_id;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("age_types")
    @Expose
    private String ageTypes;
    @SerializedName("age_years")
    @Expose
    private String ageYears;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("anythings")
    @Expose
    private String anythings;

    public String getSub_service_id() {
        return sub_service_id;
    }

    public void setSub_service_id(String sub_service_id) {
        this.sub_service_id = sub_service_id;
    }

    public List<BatchDetails> getBatch_details() {
        return batch_details;
    }

    public void setBatch_details(List<BatchDetails> batch_details) {
        this.batch_details = batch_details;
    }

    public List<Sessions> getSessions_details() {
        return sessions_details;
    }

    public void setSessions_details(List<Sessions> sessions_details) {
        this.sessions_details = sessions_details;
    }


    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getAgeTypes() {
        return ageTypes;
    }

    public void setAgeTypes(String ageTypes) {
        this.ageTypes = ageTypes;
    }

    public String getAgeYears() {
        return ageYears;
    }

    public void setAgeYears(String ageYears) {
        this.ageYears = ageYears;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAnythings() {
        return anythings;
    }

    public void setAnythings(String anythings) {
        this.anythings = anythings;
    }


}
