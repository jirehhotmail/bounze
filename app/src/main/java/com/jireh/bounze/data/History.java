
package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class History {

    @SerializedName("bookhistory")
    @Expose
    private List<Bookhistory> bookhistory;

    public List<Bookhistory> getBookhistory() {
        return bookhistory;
    }

    public void setBookhistory(List<Bookhistory> bookhistory) {
        this.bookhistory = bookhistory;
    }

}
