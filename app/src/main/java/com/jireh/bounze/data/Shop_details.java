package com.jireh.bounze.data;

/**
 * Created by Shubham on 25-09-2017.
 */

public class Shop_details {
    private String contact_number;

    private String location;

    private String shop_timings;

    private String full_address;

    private String longitude;

    private String latitude;

    private String availability;

    private String shop_name;

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getShop_timings() {
        return shop_timings;
    }

    public void setShop_timings(String shop_timings) {
        this.shop_timings = shop_timings;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [contact_number = " + contact_number + ", location = " + location + ", shop_timings = " + shop_timings + ", full_address = " + full_address + ", longitude = " + longitude + ", latitude = " + latitude + ", availability = " + availability + ", shop_name = " + shop_name + "]";
    }
}