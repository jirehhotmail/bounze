package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sessions implements Serializable {

    private String shop_id;
    private String sub_service_id;
    @SerializedName("session_id")
    @Expose
    private String sessionId;
    @SerializedName("no_of_days")
    @Expose
    private String noOfDays;
    @SerializedName("days_unit")
    @Expose
    private String daysUnit;
    @SerializedName("sessions")
    @Expose
    private String sessions;
    @SerializedName("sessions_cost")
    @Expose
    private String sessionsCost;
    @SerializedName("sessions_offer_cost")
    @Expose
    private String sessionsOfferCost;

    public String getSub_service_id() {
        return sub_service_id;
    }

    public void setSub_service_id(String sub_service_id) {
        this.sub_service_id = sub_service_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }


    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getDaysUnit() {
        return daysUnit;
    }

    public void setDaysUnit(String daysUnit) {
        this.daysUnit = daysUnit;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getSessionsCost() {
        return sessionsCost;
    }

    public void setSessionsCost(String sessionsCost) {
        this.sessionsCost = sessionsCost;
    }

    public String getSessionsOfferCost() {
        return sessionsOfferCost;
    }

    public void setSessionsOfferCost(String sessionsOfferCost) {
        this.sessionsOfferCost = sessionsOfferCost;
    }

}