package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BatchDetails implements Serializable {

    private String sub_service_id;
    @SerializedName("batch_id")
    @Expose
    private String batchId;
    @SerializedName("batch_name")
    @Expose
    private String batchName;
    private List<BatchTime> batch_time;

    public String getSub_service_id() {
        return sub_service_id;
    }

    public void setSub_service_id(String sub_service_id) {
        this.sub_service_id = sub_service_id;
    }

    public List<BatchTime> getBatch_time() {
        return batch_time;
    }

    public void setBatch_time(List<BatchTime> batch_time) {
        this.batch_time = batch_time;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

}