package com.jireh.bounze.data;

/**
 * Created by jireh_16 on 05-10-2017.
 */

public class PassBookingPojo {
    private Booking[] booking;

    public Booking[] getBooking ()
    {
        return booking;
    }

    public void setBooking (Booking[] booking)
    {
        this.booking = booking;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [booking = "+booking+"]";
    }
}
