package com.jireh.bounze.data;

/**
 * Created by vishnu on 07-12-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Shoplist {

    @SerializedName("shop")
    @Expose
    private List<Shop> shop = null;

    /**
     *
     * @return
     * The shop
     */
    public List<Shop> getShop() {
        return shop;
    }

    /**
     *
     * @param shop
     * The shop
     */
    public void setShop(List<Shop> shop) {
        this.shop = shop;
    }

    public List<Shop> getShoprating() {
        return shoprating;
    }

    public void setShoprating(List<Shop> shoprating) {
        this.shoprating = shoprating;
    }

    @SerializedName("shoprating")
    @Expose
    private List<Shop> shoprating = null;
}