package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Coach implements Serializable {

    float[] results = new float[1];
    public boolean clicked;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("avgratings")
    @Expose
    private String avgratings;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("image_url_square")
    @Expose
    private String imageUrlSquare;
    @SerializedName("image_url_rectangle")
    @Expose
    private String imageUrlRectangle;
    @SerializedName("active_status")
    @Expose
    private String activeStatus;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("full_address")
    @Expose
    private String fullAddress;
    @SerializedName("small_address")
    @Expose
    private String smallAddress;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("admin_lock_status")
    @Expose
    private String adminLockStatus;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("isFreeTrailAvailable")
    @Expose
    private String isFreeTrailAvailable;
    @SerializedName("no_of_freetrial")
    @Expose
    private String noOfFreetrial;
    @SerializedName("coach_program")
    @Expose
    private List<CoachProgram> coachProgram = null;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;

    public String getAvgratings() {
        return avgratings;
    }

    public void setAvgratings(String avgratings) {
        this.avgratings = avgratings;
    }

    public float[] getResults() {
        return results;
    }

    public void setResults(float[] results) {
        this.results = results;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageUrlSquare() {
        return imageUrlSquare;
    }

    public void setImageUrlSquare(String imageUrlSquare) {
        this.imageUrlSquare = imageUrlSquare;
    }

    public String getImageUrlRectangle() {
        return imageUrlRectangle;
    }

    public void setImageUrlRectangle(String imageUrlRectangle) {
        this.imageUrlRectangle = imageUrlRectangle;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getSmallAddress() {
        return smallAddress;
    }

    public void setSmallAddress(String smallAddress) {
        this.smallAddress = smallAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAdminLockStatus() {
        return adminLockStatus;
    }

    public void setAdminLockStatus(String adminLockStatus) {
        this.adminLockStatus = adminLockStatus;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getIsFreeTrailAvailable() {
        return isFreeTrailAvailable;
    }

    public void setIsFreeTrailAvailable(String isFreeTrailAvailable) {
        this.isFreeTrailAvailable = isFreeTrailAvailable;
    }

    public String getNoOfFreetrial() {
        return noOfFreetrial;
    }

    public void setNoOfFreetrial(String noOfFreetrial) {
        this.noOfFreetrial = noOfFreetrial;
    }

    public List<CoachProgram> getCoachProgram() {
        return coachProgram;
    }

    public void setCoachProgram(List<CoachProgram> coachProgram) {
        this.coachProgram = coachProgram;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}

