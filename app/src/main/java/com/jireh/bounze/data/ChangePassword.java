package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ChangePassword {

    public List<Response> getStatus() {
        return status;
    }

    public void setStatus(List<Response> status) {
        this.status = status;
    }

    @SerializedName("status")
    @Expose
    private List<Response> status = new ArrayList<Response>();



}
