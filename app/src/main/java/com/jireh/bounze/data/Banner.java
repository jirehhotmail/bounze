package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Muthamizhan C on 02-11-2017.
 */

public class  Banner implements Serializable{
    @SerializedName("target_link")
    @Expose
    private String targetLink;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getTargetLink() {
        return targetLink;
    }

    public void setTargetLink(String targetLink) {
        this.targetLink = targetLink;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
