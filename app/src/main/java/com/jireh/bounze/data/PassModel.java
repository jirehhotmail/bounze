package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 14-08-2017.
 */

public class PassModel {
    @SerializedName("areas")
    @Expose
    private String areas;
    @SerializedName("total_bought")
    @Expose
    private String total_bought;
    @SerializedName("remove_status")
    @Expose
    private Integer remove_status;
    @SerializedName("active_status")
    @Expose
    private Integer active_status;
    @SerializedName("pass_id")
    @Expose
    private Integer pass_id;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pass_image")
    @Expose
    private String pass_image;
    @SerializedName("service_logo1")
    @Expose
    private String service_logo1;
    @SerializedName("service_logo2")
    @Expose
    private String service_logo2;
    @SerializedName("service_logo3")
    @Expose
    private String service_logo3;
    @SerializedName("service_logo4")
    @Expose
    private String service_logo4;
    @SerializedName("service_logo5")
    @Expose
    private String service_logo5;
    @SerializedName("ratings")
    @Expose
    private Double ratings;
    @SerializedName("sessions")
    @Expose
    private String sessions;

    public PassModel(String s, String s1) {
    }
    public PassModel() {
    }

    public String getService_logo() {
        return service_logo;
    }

    public void setService_logo(String service_logo) {
        this.service_logo = service_logo;
    }

    private String service_logo;
    @SerializedName("price")
    @Expose




    private Double price;

    public String getTotal_bought() {
        return total_bought;
    }

    public void setTotal_bought(String total_bought) {
        this.total_bought = total_bought;
    }

    public Integer getPass_id() {
        return pass_id;
    }

    public void setPass_id(Integer pass_id) {
        this.pass_id = pass_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPass_image() {
        return pass_image;
    }

    public void setPass_image(String pass_image) {
        this.pass_image = pass_image;
    }

    public String getService_logo1() {
        return service_logo1;
    }

    public void setService_logo1(String service_logo1) {
        this.service_logo1 = service_logo1;
    }

    public String getService_logo2() {
        return service_logo2;
    }

    public void setService_logo2(String service_logo2) {
        this.service_logo2 = service_logo2;
    }

    public String getService_logo3() {
        return service_logo3;
    }

    public void setService_logo3(String service_logo3) {
        this.service_logo3 = service_logo3;
    }

    public String getService_logo4() {
        return service_logo4;
    }

    public void setService_logo4(String service_logo4) {
        this.service_logo4 = service_logo4;
    }

    public String getService_logo5() {
        return service_logo5;
    }

    public void setService_logo5(String service_logo5) {
        this.service_logo5 = service_logo5;
    }

    public Double getRatings() {
        return ratings;
    }

    public void setRatings(Double ratings) {
        this.ratings = ratings;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas;
    }

    public Integer getRemove_status() {
        return remove_status;
    }

    public void setRemove_status(Integer remove_status) {
        this.remove_status = remove_status;
    }

    public Integer getActive_status() {
        return active_status;
    }

    public void setActive_status(Integer active_status) {
        this.active_status = active_status;
    }

}
