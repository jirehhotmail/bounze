package com.jireh.bounze.data;

/**
 * Created by vishnu on 08-12-2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Serviceonshop {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("no_of_days")
    @Expose
    private String noOfDays;
    @SerializedName("no_of_sessions")
    @Expose
    private String noOfSessions;
    @SerializedName("sessions")
    @Expose
    private String sessions;
    @SerializedName("session_hour")
    @Expose
    private String sessionHour;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("offer_cost")
    @Expose
    private String offerCost;
    @SerializedName("is_batch")
    @Expose
    private String isBatch;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("active_status")
    @Expose
    private String activeStatus;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("package_name")
    @Expose
    private String createdDate;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The shopId
     */
    public String getShopId() {
        return shopId;
    }

    /**
     *
     * @param shopId
     * The shop_id
     */
    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     *
     * @return
     * The serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     *
     * @param serviceId
     * The service_id
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     *
     * @return
     * The noOfDays
     */
    public String getNoOfDays() {
        return noOfDays;
    }

    /**
     *
     * @param noOfDays
     * The no_of_days
     */
    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    /**
     *
     * @return
     * The noOfSessions
     */
    public String getNoOfSessions() {
        return noOfSessions;
    }

    /**
     *
     * @param noOfSessions
     * The no_of_sessions
     */
    public void setNoOfSessions(String noOfSessions) {
        this.noOfSessions = noOfSessions;
    }

    /**
     *
     * @return
     * The sessions
     */
    public String getSessions() {
        return sessions;
    }

    /**
     *
     * @param sessions
     * The sessions
     */
    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    /**
     *
     * @return
     * The sessionHour
     */
    public String getSessionHour() {
        return sessionHour;
    }

    /**
     *
     * @param sessionHour
     * The session_hour
     */
    public void setSessionHour(String sessionHour) {
        this.sessionHour = sessionHour;
    }

    /**
     *
     * @return
     * The cost
     */
    public String getCost() {
        return cost;
    }

    /**
     *
     * @param cost
     * The cost
     */
    public void setCost(String cost) {
        this.cost = cost;
    }


    /**
     *
     * @return
     * The cost
     */
    public String getOfferCost() {
        return offerCost;
    }

    /**
     *
     * @param cost
     * The cost
     */
    public void setOfferCost(String cost) {
        this.offerCost = cost;
    }

    /**
     *
     * @return
     * The isBatch
     */
    public String getIsBatch() {
        return isBatch;
    }

    /**
     *
     * @param isBatch
     * The is_batch
     */
    public void setIsBatch(String isBatch) {
        this.isBatch = isBatch;
    }

    /**
     *
     * @return
     * The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     *
     * @param startDate
     * The start_date
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     *
     * @return
     * The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     *
     * @param endDate
     * The end_date
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     *
     * @return
     * The activeStatus
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     *
     * @param activeStatus
     * The active_status
     */
    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     *
     * @return
     * The updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }

    /**
     *
     * @param updatedDate
     * The updated_date
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     *
     * @return
     * The createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     *
     * @param createdDate
     * The created_date
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}