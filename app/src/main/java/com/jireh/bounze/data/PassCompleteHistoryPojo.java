package com.jireh.bounze.data;

/**
 * Created by jireh_16 on 05-10-2017.
 */

public class PassCompleteHistoryPojo {
    private PassHistory[] passhistory;

    public PassHistory[] getPassHistory()
    {
        return passhistory;
    }

    public void setPassHistory(PassHistory[] passhistory)
    {
        this.passhistory = passhistory;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [passHistory = "+ passhistory +"]";
    }
}
