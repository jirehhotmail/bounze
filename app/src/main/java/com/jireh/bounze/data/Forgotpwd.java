
package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Forgotpwd {

    @SerializedName("forgot")
    @Expose
    private List<Forgot> forgot = null;

    public List<Forgot> getForgot() {
        return forgot;
    }

    public void setForgot(List<Forgot> forgot) {
        this.forgot = forgot;
    }

}
