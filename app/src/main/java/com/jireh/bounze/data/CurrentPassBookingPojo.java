package com.jireh.bounze.data;

/**
 * Created by Shubham on 03-10-2017.
 */

public class CurrentPassBookingPojo {
    private String totalSessionCount;
    private String remainingSessionCount;
    private String serviceName;

    public String getTotalSessionCount() {
        return totalSessionCount;
    }

    public void setTotalSessionCount(String totalSessionCount) {
        this.totalSessionCount = totalSessionCount;
    }

    public String getRemainingSessionCount() {
        return remainingSessionCount;
    }

    public void setRemainingSessionCount(String remainingSessionCount) {
        this.remainingSessionCount = remainingSessionCount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
