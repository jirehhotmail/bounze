
package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Analytics {

    @SerializedName("analytic")
    @Expose
    private List<Analytic> analytic = null;

    public List<Analytic> getAnalytic() {
        return analytic;
    }

    public void setAnalytic(List<Analytic> analytic) {
        this.analytic = analytic;
    }

}
