package com.jireh.bounze.data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 03-10-2017.
 */

public class ShopSerializable implements Serializable {
    String shopId;
    String shopTimings;
    String SubServiceName;
    String payPerUse;
    String subServiceId;
    String no_of_freetrial;
    List<ShopServiceDetails> serviceDetails;
    String packageName;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getNo_of_freetrial() {
        return no_of_freetrial;
    }

    public void setNo_of_freetrial(String no_of_freetrial) {
        this.no_of_freetrial = no_of_freetrial;
    }

    public String getShopTimings() {
        return shopTimings;
    }

    public void setShopTimings(String shopTimings) {
        this.shopTimings = shopTimings;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getSubServiceId() {
        return subServiceId;
    }

    public void setSubServiceId(String subServiceId) {
        this.subServiceId = subServiceId;
    }
/*    List<BatchTime> batchTimes;
    List<Sessions> sessionDetails;*/

    public List<ShopServiceDetails> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ShopServiceDetails> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

  /*  public List<BatchTime> getBatchTimes() {
        return batchTimes;
    }

    public void setBatchTimes(List<BatchTime> batchTimes) {
        this.batchTimes = batchTimes;
    }

    public List<Sessions> getSessionDetails() {
        return sessionDetails;
    }

    public void setSessionDetails(List<Sessions> sessionDetails) {
        this.sessionDetails = sessionDetails;
    }*/


    public String getPayPerUse() {
        return payPerUse;
    }

    public void setPayPerUse(String payPerUse) {
        this.payPerUse = payPerUse;
    }

    public String getSubServiceName() {
        return SubServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        SubServiceName = subServiceName;
    }
}
