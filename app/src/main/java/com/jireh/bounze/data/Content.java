package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vishnu on 06-12-2016.
 */
public class Content {

    @SerializedName("service")
    @Expose
    private List<Service> service = null;

    /**
     *
     * @return
     * The service
     */
    public List<Service> getService() {
        return service;
    }

    /**
     *
     * @param service
     * The service
     */
    public void setService(List<Service> service) {
        this.service = service;
    }

}
