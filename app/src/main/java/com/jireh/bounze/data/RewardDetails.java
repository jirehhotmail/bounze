package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Muthamizhan C on 31-01-2018.
 */

public class RewardDetails implements Serializable {


    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("currentreward")
    @Expose
    private String currentreward;
    @SerializedName("currentpointsgained")
    @Expose
    private String currentpointsgained;
    @SerializedName("currentrank")
    @Expose
    private String currentrank;
    @SerializedName("referalpoints")
    @Expose
    private String referalpoints;
    @SerializedName("usedpoints")
    @Expose
    private String usedpoints;
    @SerializedName("totalreward")
    @Expose
    private String totalreward;

    public String getTotalreward() {
        return totalreward;
    }

    public void setTotalreward(String totalreward) {
        this.totalreward = totalreward;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getCurrentreward() {
        return currentreward;
    }

    public void setCurrentreward(String currentreward) {
        this.currentreward = currentreward;
    }

    public String getCurrentpointsgained() {
        return currentpointsgained;
    }

    public void setCurrentpointsgained(String currentpointsgained) {
        this.currentpointsgained = currentpointsgained;
    }

    public String getCurrentrank() {
        return currentrank;
    }

    public void setCurrentrank(String currentrank) {
        this.currentrank = currentrank;
    }

    public String getReferalpoints() {
        return referalpoints;
    }

    public void setReferalpoints(String referalpoints) {
        this.referalpoints = referalpoints;
    }

    public String getUsedpoints() {
        return usedpoints;
    }

    public void setUsedpoints(String usedpoints) {
        this.usedpoints = usedpoints;
    }

}
