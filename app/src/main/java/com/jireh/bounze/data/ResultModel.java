package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 26-10-2017.
 */

public class ResultModel implements Serializable {

    @SerializedName("position")
    @Expose
    private List<Banner> position = null;

    @SerializedName("max_cost")
    @Expose
    private Integer maxCost;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sid")
    @Expose
    private String sid;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("amenities")
    @Expose
    private String amenities;

    public List<Banner> getPosition() {
        return position;
    }

    public void setPosition(List<Banner> position) {
        this.position = position;
    }


    public Integer getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(Integer maxCost) {

    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }
}
