
package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Program {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("coach_id")
    @Expose
    private String coachId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("no_of_days")
    @Expose
    private String noOfDays;
    @SerializedName("no_of_sessions")
    @Expose
    private String noOfSessions;
    @SerializedName("sessions")
    @Expose
    private String sessions;
    @SerializedName("session_hour")
    @Expose
    private String sessionHour;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("offer_cost")
    @Expose
    private String offerCost;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("active_status")
    @Expose
    private String activeStatus;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("package_name")
    @Expose
    private String createdDate = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCoachId() {
        return coachId;
    }

    public void setCoachId(String coachId) {
        this.coachId = coachId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getNoOfSessions() {
        return noOfSessions;
    }

    public void setNoOfSessions(String noOfSessions) {
        this.noOfSessions = noOfSessions;
    }

    public String getSessions() {
        return sessions;
    }

    public void setSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getSessionHour() {
        return sessionHour;
    }

    public void setSessionHour(String sessionHour) {
        this.sessionHour = sessionHour;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getOfferCost() {
        return offerCost;
    }

    /**
     *
     * @param cost
     * The cost
     */
    public void setOfferCost(String cost) {
        this.offerCost = cost;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}
