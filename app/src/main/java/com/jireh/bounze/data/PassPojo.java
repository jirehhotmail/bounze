package com.jireh.bounze.data;

import java.io.Serializable;

/**
 * Created by Shubham on 25-09-2017.
 */

public class PassPojo implements Serializable
{
    private PassArr[] pass;

    public PassArr[] getPass ()
    {
        return pass;
    }

    public void setPass (PassArr[] pass)
    {
        this.pass = pass;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pass = "+pass+"]";
    }
}
