package com.jireh.bounze.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoachList {

    @SerializedName("coach")
    @Expose
    private List<Coach> coach = null;
    @SerializedName("coachrating")
    @Expose
    private List<Coach> coachrating = null;

    public List<Coach> getCoach() {
        return coach;
    }

    public void setCoach(List<Coach> coach) {
        this.coach = coach;
    }

    public CoachList withCoach(List<Coach> coach) {
        this.coach = coach;
        return this;
    }

    public List<Coach> getCoachrating() {
        return coachrating;
    }

    public void setCoachrating(List<Coach> coachrating) {
        this.coachrating = coachrating;
    }

}
