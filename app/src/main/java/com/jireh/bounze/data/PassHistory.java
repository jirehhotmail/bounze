package com.jireh.bounze.data;

/**
 * Created by jireh_16 on 05-10-2017.
 */

public class PassHistory {
    private PassCurrentHistory[] currenthistory;

    private PassPastHistory[] pasthistory;

    public PassCurrentHistory[] getCurrenthistory ()
    {
        return currenthistory;
    }

    public void setCurrenthistory (PassCurrentHistory[] currenthistory)
    {
        this.currenthistory = currenthistory;
    }

    public PassPastHistory[] getPasthistory ()
    {
        return pasthistory;
    }

    public void setPasthistory (PassPastHistory[] pasthistory)
    {
        this.pasthistory = pasthistory;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [currenthistory = "+currenthistory+", pasthistory = "+pasthistory+"]";
    }
}
