package com.jireh.bounze.data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Muthamizhan C on 08-11-2017.
 */

public class CoachSerializable implements Serializable {
    String coachId;
    String coachSubCategoryId;
    List<CoachProgram> coachPrograms;
    String subCategoryName;
    String payPerUse;
    String noOfFreeTrial;

    List<CategoryDetail> categoryDetails;

    public String getNoOfFreeTrial() {
        return noOfFreeTrial;
    }

    public void setNoOfFreeTrial(String noOfFreeTrial) {
        this.noOfFreeTrial = noOfFreeTrial;
    }

    public String getCoachSubCategoryId() {
        return coachSubCategoryId;
    }

    public void setCoachSubCategoryId(String coachSubCategoryId) {
        this.coachSubCategoryId = coachSubCategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getPayPerUse() {
        return payPerUse;
    }

    public void setPayPerUse(String payPerUse) {
        this.payPerUse = payPerUse;
    }

    public List<CategoryDetail> getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(List<CategoryDetail> categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public String getCoachId() {
        return coachId;
    }

    public void setCoachId(String coachId) {
        this.coachId = coachId;
    }

    public List<CoachProgram> getCoachPrograms() {
        return coachPrograms;
    }

    public void setCoachPrograms(List<CoachProgram> coachPrograms) {
        this.coachPrograms = coachPrograms;
    }
}
