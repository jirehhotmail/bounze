package com.jireh.bounze.data;

/**
 * Created by Shubham on 07-10-2017.
 */

public class BlogPojo
{
    private BlogData[] blog;

    public BlogData[] getBlogData()
    {
        return blog;
    }

    public void setBlogData(BlogData[] blog)
    {
        this.blog = blog;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [blogData = "+ blog +"]";
    }
}
