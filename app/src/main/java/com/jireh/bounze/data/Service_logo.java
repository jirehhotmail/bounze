package com.jireh.bounze.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shubham on 25-09-2017.
 */

public class Service_logo implements Parcelable {
    private String subservice_id;

    private String subservice_name;

    private String service_name;

    private String service_icon;

    public String getSubservice_id ()
    {
        return subservice_id;
    }

    public void setSubservice_id (String subservice_id)
    {
        this.subservice_id = subservice_id;
    }

    public String getSubservice_name ()
    {
        return subservice_name;
    }

    public void setSubservice_name (String subservice_name)
    {
        this.subservice_name = subservice_name;
    }

    public String getService_name ()
    {
        return service_name;
    }

    public void setService_name (String service_name)
    {
        this.service_name = service_name;
    }

    public String getService_icon ()
    {
        return service_icon;
    }

    public void setService_icon (String service_icon)
    {
        this.service_icon = service_icon;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [subservice_id = "+subservice_id+", subservice_name = "+subservice_name+", service_name = "+service_name+", service_icon = "+service_icon+"]";
    }

    protected Service_logo(Parcel in) {
        subservice_id = in.readString();
        subservice_name = in.readString();
        service_name = in.readString();
        service_icon = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subservice_id);
        dest.writeString(subservice_name);
        dest.writeString(service_name);
        dest.writeString(service_icon);
    }

    @SuppressWarnings("unused")
    public static final Creator<Service_logo> CREATOR = new Creator<Service_logo>() {
        @Override
        public Service_logo createFromParcel(Parcel in) {
            return new Service_logo(in);
        }

        @Override
        public Service_logo[] newArray(int size) {
            return new Service_logo[size];
        }
    };
}