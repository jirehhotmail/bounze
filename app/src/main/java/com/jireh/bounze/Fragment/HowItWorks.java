package com.jireh.bounze.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jireh.bounze.R;

/**
 * Created by Muthamizhan C on 13-12-2017.
 */

public class HowItWorks extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.how_it_works, container, false);
        return root;
    }

}
