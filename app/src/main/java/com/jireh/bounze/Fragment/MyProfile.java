package com.jireh.bounze.Fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.Toolbar;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.jireh.bounze.Activity.Blog;
import com.jireh.bounze.Activity.BookingDetailsActivity;
import com.jireh.bounze.Activity.DisplayImage;
import com.jireh.bounze.Activity.EventActivity;
import com.jireh.bounze.Activity.LoginActivity;
import com.jireh.bounze.Activity.NotificationSettings;
import com.jireh.bounze.Activity.PagerLayout;
import com.jireh.bounze.Activity.PassBookingActivity;
import com.jireh.bounze.Activity.ProfileDetailsActivity;
import com.jireh.bounze.Activity.ShopActivity;
import com.jireh.bounze.Activity.Trainer;
import com.jireh.bounze.Activity.Usernotification;
import com.jireh.bounze.Activity.Wallet;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.ogaclejapan.arclayout.ArcLayout;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 09-08-2017.
 */

public class MyProfile extends Fragment {
    ImageView back_img, iv_setting, iv_notification;
    CircleImageView profile_user_img;
    TextView title_txt, settings_txt, about_us_txt, privacy_txt, user_name_txt;
    LinearLayout settings_layout;
    SharedPreferences pref;
    String id;
    // ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    ImageButton friend_imgbtn, wallet_imgbtn, pass_imgbtn, bookings_imgbtn, me_imgbtn;
    Toolbar toolbar;
    EditText username_edt, email_edt, mobile_edt;
    String profileUrl;
    ArcLayout arc_layout;
    ImageView logout_img;
    ImageButton fab1_image, fab2_image, fab3_image, fab4_image;
    private Uri mCropImageUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_profile, container, false);
        initVariable(view);
        pref = getActivity().getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
        SharedPreferences pred = getActivity().getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
        username_edt.setText(pred.getString("name", ""));
        mobile_edt.setText(pred.getString("mobile", ""));
        email_edt.setText(pred.getString("emailid", ""));
        profileUrl = pred.getString("imageUrl", "");
        id = pref.getString("id", "");
        title_txt.setText("My Profile");
        setOnClickListener();
        //setvisibility for the logout button
        logout_img.setVisibility(view.VISIBLE);

        //if user logged in using facebook then user this profile picture
        if (Prefs.getBoolean(AppConstants.isUserLoggedFacebook, false)) {

            String str = pred.getString("name", "");
            String[] splited = str.split("\\s+");
            user_name_txt.setText(splited[0]);
            Picasso.with(getActivity()).load(profileUrl).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(profile_user_img);

        } else if (Prefs.getBoolean(AppConstants.isUserLoggedGoogle, false)) {

            String str = pred.getString("name", "");
            String[] splited = str.split("\\s+");
            user_name_txt.setText(splited[0]);
            Picasso.with(getActivity()).load(profileUrl).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(profile_user_img);


        } else {
            //set the username

            user_name_txt.setText(pred.getString("name", ""));
            //set the proile image
            getProfile();
        }
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.white),
                PorterDuff.Mode.SRC_ATOP);

        me_imgbtn.setColorFilter(porterDuffColorFilter);
        //set the my profile details

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pred = getActivity().getApplicationContext().getSharedPreferences("Bounze", MODE_PRIVATE);
        username_edt.setText(pred.getString("name", ""));
        mobile_edt.setText(pred.getString("mobile", ""));
        email_edt.setText(pred.getString("emailid", ""));
        profileUrl = pred.getString("imageUrl", "");

        //set the username


    }
    //get the path of the attached image


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//if the request code is 200 and permmission give move to upload page else move to this page
        if ((resultCode == Activity.RESULT_OK) && (requestCode == 200)) {
            Uri imageUri = getPickImageResultUri(data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                //intent to display image and start for result
                Intent in = new Intent(getActivity(), DisplayImage.class);
                in.putExtra("ImageUriAsync", imageUri.toString());
                in.putExtra("ImageFlag", "MyProfile");
                startActivityForResult(in, 500);

            }
        } else if (requestCode == 500) {

            getProfile();

        }


    }

    private void getProfile() {

        try {
            showProgressDialog();
            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
            Call<UserProfileService> call = stationClient.getProfile(Prefs.getString(AppConstants.UserId, ""));

            call.enqueue(new Callback<UserProfileService>() {
                @Override
                public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                    UserProfileService result = response.body();
                    if (result.getResult().get(0).getStatus() == 0) {
                        hideProgressDialog();
                        Toast.makeText(getActivity(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Picasso.with(getActivity()).load(BASE_URL + result.getResult().get(0).getPicture()).placeholder(R.drawable.profile).error(R.drawable.profile).into(profile_user_img);
                    }
                }

                @Override
                public void onFailure(Call<UserProfileService> call, Throwable t) {
                    hideProgressDialog();
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.d("Registration_eqnn", t + "");
                    Log.d("Registration_eqnn", call + "");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Required permissions granted", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getActivity(), "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /*
    * This method is fetching the absolute path of the image file
    * if you want to upload other kind of files like .pdf, .docx
    * you need to make changes on this method only
    * Rest part will be the same
    * */
    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PagerLayout.viewPager.setCurrentItem(0);
            }
        });


        fab1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ShopActivity.class);
                startActivity(in);
            }
        });

        fab2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Trainer.class);
                startActivity(in);
            }
        });

        fab3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), EventActivity.class);
                startActivity(in);

            }
        });

        fab4_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Blog.class);
                startActivity(in);
            }
        });

        //set profile image on click listener to upload image

        profile_user_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //on click profile image open the choose on activity result open the display image page
                startActivityForResult(getPickImageChooserIntent(), 200);


            }
        });
        wallet_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Wallet.class);
                startActivity(in);
            }
        });
        bookings_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), BookingDetailsActivity.class);
                startActivity(in);
            }
        });
        me_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ProfileDetailsActivity.class);
                startActivity(in);
            }
        });

        pass_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), PassBookingActivity.class);
                startActivity(in);
            }
        });
        iv_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), NotificationSettings.class);
                startActivity(in);
            }
        });
        iv_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in =new Intent(getActivity(),Usernotification.class);
                startActivity(in);
            }
        });
        logout_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //make the shared preference userid variable to 0 and intent to login screen
                Prefs.putString(AppConstants.UserId, "");
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                Toast.makeText(getActivity(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                //set the shared preference value to false for isUserLogged in so that it will open the login page
                Prefs.putBoolean(AppConstants.isUserLoggedIn, false);
                Prefs.putBoolean(AppConstants.isUserLoggedFacebook, false);
                //sign out the google
                if (Prefs.getBoolean(AppConstants.isUserLoggedGoogle, false)) {
                    Prefs.putBoolean(AppConstants.isUserLoggedGoogle, false);
                    FirebaseAuth.getInstance().signOut();
                }

            }
        });

    }

    /* Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
          * Will return the correct URI for camera and gallery image.
          *
          * @param data the returned data of the activity result
   */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

     /*   // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
*/
        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath()));
        }
        return outputFileUri;
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    @SuppressWarnings("NewApi")
    public void showMenu() {

        try {
            arc_layout.setVisibility(View.VISIBLE);
            List<Animator> animList = new ArrayList<>();


            for (int i = 0; i < arc_layout.getChildCount(); i++) {
                View child = arc_layout.getChildAt(i);
                child.setVisibility(View.VISIBLE);
            }


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("NewApi")
    public void hideMenu() {
        try {
            arc_layout.setVisibility(View.GONE);
            List<Animator> animList = new ArrayList<>();
            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    for (int i = 0; i < arc_layout.getChildCount(); i++) {
                        View child = arc_layout.getChildAt(i);
                        child.setVisibility(View.INVISIBLE);
                    }
                    // menu_layout.setVisibility(View.INVISIBLE);
                }
            });
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void initVariable(View view) {
        title_txt = (TextView) view.findViewById(R.id.title_txt);
        user_name_txt = (TextView) view.findViewById(R.id.user_name_txt);
        back_img = (ImageView) view.findViewById(R.id.back_img);
        profile_user_img = (CircleImageView) view.findViewById(R.id.profile_user_img);

        about_us_txt = (TextView) view.findViewById(R.id.about_us_txt);
        privacy_txt = (TextView) view.findViewById(R.id.privacy_txt);

        iv_setting = (ImageView) view.findViewById(R.id.iv_setting);
        iv_notification = (ImageView) view.findViewById(R.id.iv_notification);
        friend_imgbtn = (ImageButton) view.findViewById(R.id.friend_imgbtn);
        wallet_imgbtn = (ImageButton) view.findViewById(R.id.wallet_imgbtn);
        pass_imgbtn = (ImageButton) view.findViewById(R.id.pass_imgbtn);
        bookings_imgbtn = (ImageButton) view.findViewById(R.id.bookings_imgbtn);
        me_imgbtn = (ImageButton) view.findViewById(R.id.me_imgbtn);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        mobile_edt = (EditText) view.findViewById(R.id.mobile_edt);
        email_edt = (EditText) view.findViewById(R.id.email_edt);
        username_edt = (EditText) view.findViewById(R.id.username_edt);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        arc_layout = (ArcLayout) view.findViewById(R.id.arc_layout);
        logout_img = (ImageView) toolbar.findViewById(R.id.logout_img);
        fab1_image = (ImageButton) view.findViewById(R.id.fab1_image);
        fab2_image = (ImageButton) view.findViewById(R.id.fab2_image);
        fab3_image = (ImageButton) view.findViewById(R.id.fab3_image);
        fab4_image = (ImageButton) view.findViewById(R.id.fab4_image);
    }
}
