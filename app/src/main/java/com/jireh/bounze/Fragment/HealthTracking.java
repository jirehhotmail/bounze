package com.jireh.bounze.Fragment;

import android.animation.ObjectAnimator;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Activity.PhotoTransformation;
import com.jireh.bounze.Adapter.HealthTrackingAdapter;
import com.jireh.bounze.BuildConfig;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.RealPathUtil;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.UserProfile;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;


/**
 * Created by Muthamizhan C on 09-09-2017.
 */

public class HealthTracking extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    Button calculate_bmi, add_entry_btn;
    EditText height_edt, weight_edt;
    TextView result_txt;
    List<UserProfile> healthmodels = new ArrayList<>();
    List<UserProfile> healthmodelsPeriod = new ArrayList<>();

    ArrayList<UserProfile> graphPoints = new ArrayList<>();
    RecyclerView health_tracking_recycler;
    String mCurrentPhotoPath = "", lastHeight = "", lastWeight = "", secondLastHeight, secondLastWeight, currentDate = "";
    ArrayAdapter<String> dataAdapter;
    ImageView captured_img;
    LinearLayout heading_row_layout, health_layout;
    ArrayList<String> periodList = new ArrayList<>();
    ProgressBar progress_bar;
    Uri imageUri;
    GraphView graphView;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
    HealthTrackingAdapter adapter;
    Spinner select_period_spinner;
    String selectedPeriod = "";
    Date date1 = null;
    private boolean Validation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.health_tracking, container, false);

        //initvariable
        initVariable(view);
        //set the spinner adapter
        //clear the adapter before adding
        if (periodList.size() > 0) {
            periodList.clear();
        }
        periodList.add("Select a period");
        periodList.add("One Week");
        periodList.add("One Month");
        periodList.add("Three Months");
        periodList.add("Six Months");
        periodList.add("One Year");
        //set the spinner adapter
        dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_row, R.id.tv_data, periodList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_row);
        select_period_spinner.setAdapter(dataAdapter);


        //set on click listener
        setOnClickListener();

        //get the details from the server and set to the adapter
        getHealthTrackDetails("normalEntry");
        gettheCurrentTimeFromServer();
        return view;
    }

    private void gettheCurrentTimeFromServer() {
        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.getCurrentTime();
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(final Call<LogIn> call, Response<LogIn> response) {

                LogIn result = response.body();

                currentDate = result.getStatus().get(0).getMessage();
                setTimePeriodAdapter();
                //first time load call the 7 days graph
                //     showSevenDays for the first time
                showTheGraph();
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });
    }

    private void showTheGraph() {

        try {
            //convert gmt to ist time
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));  // set this if your timezone is different

            date1 = sdf.parse(currentDate); // now time is Sun Jan 25 00:00:00 GMT 2015
            sdf.setTimeZone(TimeZone.getTimeZone("IST"));
            Log.d("date", sdf.format(date1)); // now time is Sun Jan 25 05:30:00 IST 2015

            System.out.println("check..." + sdf.format(date1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        calendar.add(Calendar.DAY_OF_YEAR, -7);

        Date setDate = calendar.getTime();

        //if health models period greater than 0 clear it
        if (healthmodelsPeriod.size() > 0) {
            healthmodelsPeriod.clear();
        }
        //get the dates between last one week using data from server
        for (int idx = 0; idx < healthmodels.size(); idx++) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date allDates = null;
            try {
                allDates = sdf.parse(healthmodels.get(idx).getUpdateDate());
            } catch (ParseException ex) {

            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


            //get the dates after the user selected dates to till dates
            if (setDate.before(allDates)) {
                healthmodelsPeriod.add(healthmodels.get(idx));
                System.out.println("Result dates::" + healthmodels.get(idx));
            }
        }

        //by default set the values as one week
        //  selectedPeriod ="One Week";

        //setGraph(healthmodelsPeriod);
    }

    private void setTimePeriodAdapter() {


        select_period_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPeriod = select_period_spinner.getSelectedItem().toString();
                //based on selection set the graph
                //set the graph
                try {
                    //convert gmt to ist time
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));  // set this if your timezone is different

                    date1 = sdf.parse(currentDate); // now time is Sun Jan 25 00:00:00 GMT 2015
                    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
                    Log.d("date", sdf.format(date1)); // now time is Sun Jan 25 05:30:00 IST 2015

                    System.out.println("check..." + sdf.format(date1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date1);
                //get the previous dates based on the selection
                if (parent.getSelectedItem().equals("One Week")) {
                    calendar.add(Calendar.DAY_OF_YEAR, -7);
                } else if (parent.getSelectedItem().equals("One Month")) {
                    calendar.add(Calendar.DAY_OF_YEAR, -30);
                } else if (parent.getSelectedItem().equals("Three Months")) {
                    calendar.add(Calendar.DAY_OF_YEAR, -90);
                } else if (parent.getSelectedItem().equals("Six Months")) {
                    calendar.add(Calendar.DAY_OF_YEAR, -180);
                } else if (parent.getSelectedItem().equals("One Year")) {
                    calendar.add(Calendar.DAY_OF_YEAR, -365);
                }


                Date setDate = calendar.getTime();

                //if health models period greater than 0 clear it
                if (graphPoints.size() > 0) {
                    healthmodelsPeriod.clear();
                    graphPoints.clear();
                }
                //get the dates between last one week using data from server
                for (int idx = 0; idx < healthmodels.size(); idx++) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date allDates = null;
                    try {
                        allDates = sdf.parse(healthmodels.get(idx).getUpdateDate());
                    } catch (ParseException ex) {

                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }


                    //get the dates after the user selected dates to till dates
                    if (setDate.before(allDates)) {
                        healthmodelsPeriod.add(healthmodels.get(idx));
                        graphPoints.add(healthmodels.get(idx));
                        System.out.println("Result dates::" + healthmodelsPeriod.size());
                    }
                }

                //if selected item is not select a period then proceed

                if (!selectedPeriod.equals("Select a period") && healthmodels.size() > 0) {
                    setGraph(graphPoints);
                }
                // }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        System.out.println("is fragment visible::" + isFragmentVisible_ + " is visible" + isVisible());
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_) {
                execute();
            }
            //if the permission is granted set the on go details


        }
    }

    private void execute() {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if the result is ok then pass the image path to intent crop to crop and then display the result in dialog

        if (resultCode == RESULT_OK && requestCode == 100) {


            try {
                captured_img.setVisibility(View.VISIBLE);
                imageUri = Uri.parse(mCurrentPhotoPath);
                File file = new File(imageUri.getPath());
                try {
                    InputStream ims = new FileInputStream(file);
                    captured_img.setImageBitmap(BitmapFactory.decodeStream(ims));
                } catch (FileNotFoundException e) {
                    return;
                }
            }
            // respond to users whose devices do not support the crop action
            catch (ActivityNotFoundException anfe) {
                // display an error message
                String errorMessage = "Whoops - your device doesn't support the crop action!";
                Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
                toast.show();
            }

        }

    }

    private void initVariable(View view) {

        calculate_bmi = (Button) view.findViewById(R.id.calculate_bmi);
        add_entry_btn = (Button) view.findViewById(R.id.add_entry_btn);
        graphView = (GraphView) view.findViewById(R.id.graph);
        select_period_spinner = (Spinner) view.findViewById(R.id.select_period_spinner);
        health_tracking_recycler = (RecyclerView) view.findViewById(R.id.health_tracking_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        health_tracking_recycler.setLayoutManager(manager);
        heading_row_layout = (LinearLayout) view.findViewById(R.id.heading_row_layout);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        health_layout = (LinearLayout) view.findViewById(R.id.health_tracking);
    }

    private LineGraphSeries<DataPoint> getDataPoint(ArrayList<UserProfile> healthmodelsPeriod) {
        //set the points
        // declare an array of DataPoint objects with the same size as your list
        DataPoint[] dataPoints = new DataPoint[healthmodelsPeriod.size()];
        for (int idx = 0; idx < healthmodelsPeriod.size(); idx++) {
            //convert string to date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = null;
            try {
                d = sdf.parse(healthmodelsPeriod.get(idx).getUpdateDate());
            } catch (ParseException ex) {

            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            // add new DataPoint object to the array for each of your list entries
            dataPoints[idx] = new DataPoint(d, Double.parseDouble(healthmodelsPeriod.get(idx).getWeight())); // not sure but I think the second argument should be of type double
        }
        //set the data points for the graph
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(dataPoints);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10f);


        // set date label formatter for 7 points and the add x as dates and y as weight manually

        return series;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean write = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean read = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted && write && read) {
                        Toast.makeText(getActivity(), "Permission Granted, Now you can access camera, read and write external storage", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void setOnClickListener() {
        //calculate bmi of the user using height and weight
        calculate_bmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuild = new AlertDialog.Builder(getActivity());
                View mView = getActivity().getLayoutInflater().inflate(R.layout.calculate_bmi_dialog, null);
                Button bt_cal = (Button) mView.findViewById(R.id.btn_calculate);
                result_txt = (TextView) mView.findViewById(R.id.result_txt);
                height_edt = (EditText) mView.findViewById(R.id.height_edt);
                weight_edt = (EditText) mView.findViewById(R.id.weight_edt);


                mBuild.setView(mView);
                final AlertDialog dialog = mBuild.create();
                dialog.getWindow().setLayout(600, 400);
                dialog.setCancelable(true);
                dialog.show();

                bt_cal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (validation()) {

                            String heightStr = height_edt.getText().toString();
                            String weightStr = weight_edt.getText().toString();

                            if (heightStr != null && !"".equals(heightStr)
                                    && weightStr != null && !"".equals(weightStr)) {
                                float heightValue = Float.parseFloat(heightStr) / 100;
                                float weightValue = Float.parseFloat(weightStr);

                                float bmi = weightValue / (heightValue * heightValue);

                                displayBMI(bmi);

                            }
                        }
                    }
                });

                //validation is userEntered values


            }
        });

        add_entry_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the alert dialog to insert the user details

                final AlertDialog OptionDialog = new AlertDialog.Builder(getActivity()).create();
                //     AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View mView = inflater.inflate(R.layout.health_tracking_dialog, null);
                OptionDialog.setView(mView);
                OptionDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


                //  AlertDialog.Builder mBuild = new AlertDialog.Builder(getActivity());
                // View mView = getActivity().getLayoutInflater().inflate(R.layout.health_tracking_dialog, null);
                Button submit_btn = (Button) mView.findViewById(R.id.submit_btn);
                Button photo_btn = (Button) mView.findViewById(R.id.photo_btn);
                mView.setBackgroundResource(android.R.color.transparent);
                final EditText height = (EditText) mView.findViewById(R.id.height_edt);
                final EditText weight = (EditText) mView.findViewById(R.id.weight_edt);
                captured_img = (ImageView) mView.findViewById(R.id.captured_img);
                //set the value for height and weight
                height.setText(lastHeight);
                weight.setText(lastWeight);

                OptionDialog.setView(mView);

                OptionDialog.setCancelable(true);
                OptionDialog.show();
                //open camera
                photo_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                            if (checkPermission()) {
                                Toast.makeText(getActivity(), "Permission already granted", Toast.LENGTH_LONG).show();
                                //open the camera
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                // Ensure that there's a camera activity to handle the intent
                                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                    // Create the File where the photo should go
                                    File photoFile = null;
                                    photoFile = getOutputMediaFile();
                                    // Continue only if the File was successfully created
                                    if (photoFile != null) {
                                        //   Uri photoURI = Uri.fromFile(getOutputMediaFile());
                                        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                                BuildConfig.APPLICATION_ID + ".provider",
                                                getOutputMediaFile());
                                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                        startActivityForResult(takePictureIntent, 100);
                                    }
                                }
                            } else {
                                requestPermission();
                            }
                        }
                    }
                });
                submit_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (height.getText().toString().matches("")) {
                            height.setError("Enter height");

                        } else if (weight.getText().toString().matches("")) {
                            weight.setError("Enter weight");

                        } else if (height.getText().toString().matches("") && weight.getText().toString().matches("")) {
                            height.setError("Enter height");
                            weight.setError("Enter weight");

                        } else {

                            //check the permission at run time
                            showProgressDialog();

                            OptionDialog.dismiss();

                            //send the health details  to the server

                            String height_ = height.getText().toString();
                            String weight_ = weight.getText().toString();
                            //creating a file
                            //if the user select an image then get the value for real path else make the request body null
                            RequestBody mFile;
                            if (imageUri != null) {
                                String path = RealPathUtil.getRealPath(getActivity(), imageUri);
                                File file = new File(path);

                                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            } else {
                                mFile = null;
                            }



                      /*      RequestBody mFile = RequestBody.create(MediaType.parse("image*//*"), file);
                            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
*/


                            //   }
                            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));
                            RequestBody uheight_ = RequestBody.create(MediaType.parse("text/plain"), height_);
                            RequestBody uweight_ = RequestBody.create(MediaType.parse("text/plain"), weight_);


                            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                            Call<UserProfileService> call = stationClient.addHealthTrack(userId, uheight_, uweight_, mFile);

                            call.enqueue(new Callback<UserProfileService>() {
                                @Override
                                public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                                    UserProfileService result = response.body();
                                    if (result.getHealth_tracking().get(0).getStatus() == 1) {
                                        //when one entry is there show the heading
                                        heading_row_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), result.getHealth_tracking().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                                        //get the details from the server and set to the adapter
                                        getHealthTrackDetails("newEntry");
                                        showProgressDialog();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UserProfileService> call, Throwable t) {
                                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                                    Log.d("Registration_eqnn", t + "");
                                    Log.d("Registration_eqnn", call + "");
                                    hideProgressDialog();

                                }
                            });


                            //set the adapter
                            // setAdapter();

                        }
                    }
                });
            }
        });
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void getHealthTrackDetails(final String entryType) {


        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.getHealthTrack(Prefs.getString(AppConstants.UserId, ""));

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {
                hideProgressDialog();
                UserProfileService result = response.body();

                if (result.getHealth_tracking().get(0).getStatus() == 1) {
                    Toast.makeText(getActivity(), result.getHealth_tracking().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    healthmodels = result.getHealth_tracking();
                    //get the values and set the adapter if the user have entries and set the visibility for header
                    if (healthmodels.size() > 0) {
                        heading_row_layout.setVisibility(View.VISIBLE);
                        setAdapter();
                        //get the last entered height and weight to display in entry popup
                        lastHeight = healthmodels.get(healthmodels.size() - 1).getHeight();
                        lastWeight = healthmodels.get(healthmodels.size() - 1).getWeight();

                        //if entry is greater than 2 then compare two weight if it is less than one then show the transformation
                        if (healthmodels.size() > 2) {
                            secondLastHeight = healthmodels.get(healthmodels.size() - 2).getHeight();
                            secondLastWeight = healthmodels.get(healthmodels.size() - 2).getWeight();

                            if (Integer.parseInt(secondLastWeight) - Integer.parseInt(lastWeight) >= 1) {
                                //if entry type is new Entry then show the transformation else don't show
                                //show the photo transpormation
                                if (entryType.equals("newEntry")) {
                                    Intent in = new Intent(getActivity(), PhotoTransformation.class);
                                    in.putExtra("photoTransformation", (Serializable) healthmodels);
                                    startActivity(in);
                                }

                            }
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setGraph(List<UserProfile> healthmodelsPeriod) {
//set the graph values based on the selection and also add the days for the week
        String endDate;


        Calendar mCalendar = Calendar.getInstance();

        mCalendar.setTime(date1);
        //get the end date that is the current date
        DateFormat originalFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT'z yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = originalFormat.parse(mCalendar.getTime().toString());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        Date req_current_date = null;
        try {
            req_current_date = targetFormat.parse(formattedDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        endDate = targetFormat.format(req_current_date);

        System.out.println("health end date::" + endDate);


        String startDate = null;
        Calendar mCalendarStart = Calendar.getInstance();
        if (selectedPeriod.equals("One Week")) {

//get the start date that is the first date of the graph

            mCalendarStart.setTime(date1);
            mCalendarStart.add(Calendar.DAY_OF_YEAR, -7);

            startDate = getTheStartDate(mCalendarStart.getTime().toString());
            //if there is entry is a week get the start date and set the first entry else use the last entry
            if (healthmodelsPeriod.size() > 0) {

                UserProfile profile = new UserProfile();
                profile.setWeight(healthmodelsPeriod.get(0).getWeight());
                profile.setUpdateDate(startDate);
                graphPoints.add(profile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);

            } else {
                UserProfile sprofile = new UserProfile();
                sprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                sprofile.setUpdateDate(startDate);
                graphPoints.add(sprofile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);
            }


            System.out.println("health start date::" + startDate);


        } else if (selectedPeriod.equals("One Month")) {
            // Calendar mCalendar = Calendar.getInstance();
            //   Calendar mCalendarStart = Calendar.getInstance();
            mCalendarStart.setTime(date1);
            mCalendarStart.add(Calendar.DAY_OF_YEAR, -30);
            startDate = getTheStartDate(mCalendarStart.getTime().toString());
            //if there is entry is a month get the start date and set the first entry else use the last entry
            if (healthmodelsPeriod.size() > 0) {

                UserProfile profile = new UserProfile();
                profile.setWeight(healthmodelsPeriod.get(0).getWeight());
                profile.setUpdateDate(startDate);
                graphPoints.add(profile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);

            } else {
                UserProfile sprofile = new UserProfile();
                sprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                sprofile.setUpdateDate(startDate);
                graphPoints.add(sprofile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);
            }


            System.out.println("health start date::" + startDate);
        } else if (selectedPeriod.equals("Three Months")) {
            // Calendar mCalendar = Calendar.getInstance();

            //   Calendar mCalendarStart = Calendar.getInstance();
            mCalendarStart.setTime(date1);
            mCalendarStart.add(Calendar.DAY_OF_YEAR, -90);
            startDate = getTheStartDate(mCalendarStart.getTime().toString());


            //if there is entry is a three months get the start date and set the first entry else use the last entry
            if (healthmodelsPeriod.size() > 0) {

                UserProfile profile = new UserProfile();
                profile.setWeight(healthmodelsPeriod.get(0).getWeight());
                profile.setUpdateDate(startDate);
                graphPoints.add(profile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);

            } else {
                UserProfile sprofile = new UserProfile();
                sprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                sprofile.setUpdateDate(startDate);
                graphPoints.add(sprofile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);
            }


            System.out.println("health start date::" + startDate);
        } else if (selectedPeriod.equals("Six Months")) {
            //Calendar mCalendar = Calendar.getInstance();


            //    Calendar mCalendarStart = Calendar.getInstance();
            mCalendarStart.setTime(date1);
            mCalendarStart.add(Calendar.DAY_OF_YEAR, -180);
            startDate = getTheStartDate(mCalendarStart.getTime().toString());
            //if there is entry is a six months get the start date and set the first entry else use the last entry
            if (healthmodelsPeriod.size() > 0) {

                UserProfile profile = new UserProfile();
                profile.setWeight(healthmodelsPeriod.get(0).getWeight());
                profile.setUpdateDate(startDate);
                graphPoints.add(profile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);

            } else {
                UserProfile sprofile = new UserProfile();
                sprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                sprofile.setUpdateDate(startDate);
                graphPoints.add(sprofile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);
            }


            System.out.println("health start date::" + startDate);
        } else if (selectedPeriod.equals("One Year")) {

            //   Calendar mCalendarStart = Calendar.getInstance();
            mCalendarStart.setTime(date1);
            mCalendarStart.add(Calendar.DAY_OF_YEAR, -365);

            startDate = getTheStartDate(mCalendarStart.getTime().toString());

            //if there is entry is a year get the start date and set the first entry else use the last entry
            if (healthmodelsPeriod.size() > 0) {

                UserProfile profile = new UserProfile();
                profile.setWeight(healthmodelsPeriod.get(0).getWeight());
                profile.setUpdateDate(startDate);
                graphPoints.add(profile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);

            } else {
                UserProfile sprofile = new UserProfile();
                sprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                sprofile.setUpdateDate(startDate);
                graphPoints.add(sprofile);
                UserProfile eprofile = new UserProfile();
                eprofile.setWeight(healthmodels.get(healthmodels.size() - 1).getWeight());
                eprofile.setUpdateDate(endDate);
                graphPoints.add(eprofile);
            }


            System.out.println("health start date::" + startDate);
        }
        try {
            //remove the all series and then add on every selection
            graphView.removeAllSeries();
            graphView.setTitleTextSize(1.0f);


            //store weight in array list and set the min and max value
            ArrayList<Integer> weightArray = new ArrayList<>();
            for (int idx = 0; idx < healthmodels.size(); idx++) {
                weightArray.add(Integer.parseInt(healthmodels.get(idx).getWeight()));
            }
            int min = Collections.min(weightArray);
            int max = Collections.max(weightArray);


            SimpleDateFormat original_Format = new SimpleDateFormat("yyyy-MM-dd");
            //   SimpleDateFormat target_Format = new SimpleDateFormat("dd/MM/yy");
            Date min_date = null;
            try {
                min_date = original_Format.parse(startDate);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            Date max_date = null;
            try {
                max_date = original_Format.parse(endDate);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            //set the x axis values manually for the min and max dates such that based on the selection
            graphView.getViewport().setXAxisBoundsManual(true);
            graphView.getViewport().setMinX(min_date.getTime());
            graphView.getViewport().setMaxX(max_date.getTime());
            graphView.getViewport().setScrollable(true);
            //  set manual Y bounds substract one value for the min and add one values to the max
            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMinY(min - 1);
            graphView.getViewport().setMaxY(max + 1);
            // graphView.set(300.0d, -30.0d);
            //sort the graph points and based on the updated date
            Collections.sort(graphPoints, new Comparator<UserProfile>() {
                DateFormat f = new SimpleDateFormat("yyyy-MM-dd");

                @Override
                public int compare(UserProfile lhs, UserProfile rhs) {
                    try {
                        return f.parse(lhs.getUpdateDate()).compareTo(f.parse(rhs.getUpdateDate()));
                    } catch (java.text.ParseException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            });

            graphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
            graphView.getGridLabelRenderer().setHumanRounding(true);

            if (selectedPeriod.equals("One Week")) {
                graphView.getGridLabelRenderer().setNumHorizontalLabels(7); // only 4 because of the space
            } else if (selectedPeriod.equals("One Month")) {
                graphView.getGridLabelRenderer().setNumHorizontalLabels(4); // only 4 because of the space
            } else if (selectedPeriod.equals("Three Months")) {
                graphView.getGridLabelRenderer().setNumHorizontalLabels(6); // only 4 because of the space
            } else if (selectedPeriod.equals("Six Months")) {
                graphView.getGridLabelRenderer().setNumHorizontalLabels(5); // only 4 because of the space
            } else if (selectedPeriod.equals("One Year")) {
                graphView.getGridLabelRenderer().setNumHorizontalLabels(6); // only 4 because of the space
            }
            // graphView.getGridLabelRenderer().setNumHorizontalLabels(5);
            //set the graph with the entry points
            graphView.addSeries(getDataPoint(graphPoints));
            graphView.setHorizontalScrollBarEnabled(true);
            graphView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            // set date label formatter for 7 points and the add x as dates and y as weight manually
            graphView.getGridLabelRenderer().setLabelFormatter(
                    new DateAsXAxisLabelFormatter(getActivity(),
                            SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT)) {
                        @Override
                        public String formatLabel(double value, boolean isValueX) {
                            System.out.println("double value" + value + "boolean" + isValueX);
                            if (isValueX) {
                                return sdf.format(new Date((long) value));
                            } else {
                                return super.formatLabel(value, isValueX);
                            }

                        }
                    });


        } catch (ParseException e) {
            e.printStackTrace();
        }



/*
        //to round of the y axis values
        graphView.getGridLabelRenderer().setHumanRounding(true);
        //to set the labels in the horizontal wise no of labels
        graphView.getGridLabelRenderer().setNumHorizontalLabels(3);*/


    }

    private String getTheStartDate(String startTime) {
        String startDate;
        DateFormat originalFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT'z yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date sdate = null;
        try {
            sdate = originalFormat.parse(startTime);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        String formatted_Date = targetFormat.format(sdate);
        Date req_start_date = null;
        try {
            req_start_date = targetFormat.parse(formatted_Date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        startDate = targetFormat.format(req_start_date);
        return startDate;
    }

    private File getOutputMediaFile() {
        //create a directory and store the taken picture
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED

                && ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, REQUEST_CAMERA);
    }

    private boolean validation() {
        Validation = true;
        if (height_edt.getText().toString().matches("")) {
            height_edt.setError("Enter height");
            Validation = false;
        } else if (weight_edt.getText().toString().matches("")) {
            weight_edt.setError("Enter weight");
            Validation = false;
        } else if (height_edt.getText().toString().matches("") && weight_edt.getText().toString().matches("")) {
            height_edt.setError("Enter height");
            weight_edt.setError("Enter weight");
            Validation = false;
        }
        return Validation;
    }


    private void setAdapter() {

        adapter = new HealthTrackingAdapter(getActivity(), healthmodels, new HealthTrackingAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {

            }
        });
        health_tracking_recycler.setAdapter(adapter);
    }

    private void displayBMI(float bmi) {

        String bmiLabel = "";

        if (Float.compare(bmi, 15f) <= 0) {
            bmiLabel = getString(R.string.very_severely_underweight);
        } else if (Float.compare(bmi, 15f) > 0 && Float.compare(bmi, 16f) <= 0) {
            bmiLabel = getString(R.string.severely_underweight);
        } else if (Float.compare(bmi, 16f) > 0 && Float.compare(bmi, 18.5f) <= 0) {
            bmiLabel = getString(R.string.underweight);
        } else if (Float.compare(bmi, 18.5f) > 0 && Float.compare(bmi, 25f) <= 0) {
            bmiLabel = getString(R.string.normal);
        } else if (Float.compare(bmi, 25f) > 0 && Float.compare(bmi, 30f) <= 0) {
            bmiLabel = getString(R.string.overweight);
        } else if (Float.compare(bmi, 30f) > 0 && Float.compare(bmi, 35f) <= 0) {
            bmiLabel = getString(R.string.obese_class_i);
        } else if (Float.compare(bmi, 35f) > 0 && Float.compare(bmi, 40f) <= 0) {
            bmiLabel = getString(R.string.obese_class_ii);
        } else {
            bmiLabel = getString(R.string.obese_class_iii);
        }

        bmiLabel = bmi + "\n\n" + bmiLabel;
        result_txt.setText(bmiLabel);
    }

}
