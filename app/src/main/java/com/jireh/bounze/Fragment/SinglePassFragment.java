package com.jireh.bounze.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.jireh.bounze.Activity.Pay;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.CustomThinFont;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassList;
import com.jireh.bounze.data.Service_details;
import com.jireh.bounze.data.Shop_details;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Shubham on 22-09-2017.
 */
public class SinglePassFragment extends Fragment {
    //    List<PassModel> passList;
//    DBFunctions dbObject;
    LinearLayoutManager mLayoutManager;
    //    HashMap<Integer, ArrayList<String>> servicLogo;
    Multimap<Integer, String> servicLogo = ArrayListMultimap.create();
    ArrayList<PassList> passModels = new ArrayList<>();
    ArrayList<String> timingList = new ArrayList<>();
    Button tv_buy;
    // HashMap<Integer, String> serviceLogoPath =new  HashMap<Integer,String>();
    private RecyclerView mRecyclerView;
    private SinglePassAdapter mAdapter;
    private Service_details serviceDetails;
    private PassArr singlePassData;
    private SessionManager sessionManager;

    public SinglePassFragment() {
       /* Bundle bundle = getArguments();
        serviceDetails = bundle.getParcelable("Service_Detail");*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sessionManager = new SessionManager(getActivity());
        singlePassData = sessionManager.getSinglePass();
        return inflater.inflate(R.layout.single_pass_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.pass_recycler);
        tv_buy = (Button) view.findViewById(R.id.tv_buy);

        // use a linear layout manager

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Bundle bundle = getArguments();
        serviceDetails = bundle.getParcelable("Service_Detail");
        // specify an adapter (see also next example)
        mAdapter = new SinglePassAdapter(getActivity(), serviceDetails, passModels);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.setOnItemClickListener(new OnItemClick() {
            @Override
            public void onItemClicked(View view, int position, Object data) {
            }
        });
        tv_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                doBooking();
//                Toast.makeText(PassDescriptionActivity.this, "Buy", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void doBooking() {

        //move to the payment pagement to display the summary
        Intent intent = new Intent(getActivity(), Pay.class);
        intent.putExtra("PayFlag", "bookPass");
        intent.putExtra("passDetails", (Parcelable) singlePassData);
        startActivity(intent);

        //to be implemented after payment

   /*     if (isInternetOn()) {

            bookPass(singlePassData.getPass_id(), sessionManager.getUserId(), "1", singlePassData.getPrice(), "1", "1");
        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }*/
    }


    private boolean isInternetOn() {
        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    public interface OnItemClick {
        public void onItemClicked(View view, int position, Object data);
    }

    public class SinglePassAdapter extends RecyclerView.Adapter<SinglePassAdapter.ViewHolder> {
        Service_details service_details;
        ArrayList<PassList> passLists;
        private Context mContext;
        private OnItemClick mListener;
        private GPSTracker gpsTracker;


        // Provide a suitable constructor (depends on the kind of dataset)
        public SinglePassAdapter(Context context, Service_details service_details, ArrayList<PassList> passLists) {
            this.service_details = service_details;
            this.mContext = context;
            this.passLists = passLists;


        }

        public void setOnItemClickListener(OnItemClick listener) {
            mListener = listener;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public SinglePassAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_pass_row_layout, parent, false);
            // set the view's size, margins, paddings and layout parameters

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final Shop_details shop_details = getItem(position);
            //add the shop timings in the arraylist
            timingList.add(shop_details.getShop_timings());
            holder.tv_shop_name.setText(shop_details.getShop_name());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mListener.onItemClicked(v, position, model);
                }
            });
            holder.iv_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gpsTracker = new GPSTracker(getActivity());
                    if (gpsTracker.canGetLocation()) {
                        Log.d("value of lat", "and long: " + gpsTracker.getLatitude() + " ,lng: " + gpsTracker.getLongitude() + " ,lat1: " + shop_details.getLatitude() + " ,lng2: " + shop_details.getLongitude());
                        String src_lat = String.valueOf(gpsTracker.getLatitude());
                        String src_ltg = String.valueOf(gpsTracker.getLongitude());
                        String des_lat = shop_details.getLatitude();
                        String des_ltg = shop_details.getLongitude();
                        if (des_lat == null) {
                            des_lat = "13.77";
                            des_ltg = "77.06";
                        }
                        Uri uri = Uri.parse("http://maps.google.com/maps?saddr=" + src_lat + "," + src_ltg + "&daddr=" + des_lat + "," + des_ltg);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException ex) {
                            try {
                                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(unrestrictedIntent);
                            } catch (ActivityNotFoundException exp) {
                                Toast.makeText(getActivity(), "Please install a maps application", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        gpsTracker.showSettingsAlert();
                    }
                }
            });

            holder.tv_session.setText(serviceDetails.getNo_of_sessions() + " Sessions");

            // Spinner Drop down elements
            List<String> categories = new ArrayList<String>();
            categories.add(mContext.getString(R.string.view_more));
            categories.add(shop_details.getFull_address());
            categories.add(shop_details.getContact_number());

            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_row, R.id.tv_data, categories);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(R.layout.spinner_row);
            // attaching data adapter to spinner
            holder.spinner.setAdapter(dataAdapter);

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("click done", " click on list11: " + position);
                    TextView tv_spinner_data = (TextView) parent.findViewById(R.id.tv_data);
                    tv_spinner_data.setText(mContext.getString(R.string.view_more));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.d("click done", " click on list22");
                }
            });


            //set the timings for the shop

            holder.shop_timing.setText(shop_details.getShop_timings());

            //set the availability for the shop


            String str = shop_details.getAvailability();
            List<String> availabilityList = Arrays.asList(str.split(","));

            if (availabilityList.contains("M")) {
                holder.tv_mon.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_mon.setTextColor(getResources().getColor(R.color.black));

                holder.tv_mon.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(_context.getResources().getDrawable(R.drawable.outer_unselected_box));
            }

            if (availabilityList.contains("T")) {
                holder.tv_tues.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_tues.setTextColor(getResources().getColor(R.color.black));

                holder.tv_tues.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }


            if (availabilityList.contains("W")) {
                holder.tv_wed.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_wed.setTextColor(getResources().getColor(R.color.black));

                holder.tv_wed.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }


            if (availabilityList.contains("Th")) {
                holder.tv_thurs.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_thurs.setTextColor(getResources().getColor(R.color.black));

                holder.tv_thurs.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }


            if (availabilityList.contains("F")) {
                holder.tv_fri.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_fri.setTextColor(getResources().getColor(R.color.black));

                holder.tv_fri.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }


            if (availabilityList.contains("Sa")) {
                holder.tv_satur.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_satur.setTextColor(getResources().getColor(R.color.black));

                holder.tv_satur.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }


            if (availabilityList.contains("S")) {
                holder.tv_sun.setTextColor(getResources().getColor(R.color.white));
                //   tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_selected_box));
            } else {
                holder.tv_sun.setTextColor(getResources().getColor(R.color.black));

                holder.tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.day_oval_unselect));
                //  tv_sun.setBackgroundDrawable(getResources().getDrawable(R.drawable.outer_unselected_box));
            }

        }

        private Shop_details getItem(int position) {
            return serviceDetails.getShop_details()[position];
        }

        private Context getContext() {
            return mContext;
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            if (serviceDetails.getShop_details() != null)
                return serviceDetails.getShop_details().length;
            else
                return 0;
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            ImageView pass_logo_img, iv_location;
            TextView tv_shop_name, shop_timing;
            CustomThinFont tv_session, tv_timing_label, tv_service_label, tv_service;
            // private RecyclerView rv_timing;
            Button tv_mon, tv_tues, tv_wed, tv_thurs, tv_fri, tv_satur, tv_sun;
            private Spinner spinner;

            public ViewHolder(View view) {
                super(view);
                pass_logo_img = (ImageView) view.findViewById(R.id.pass_logo_img);
                iv_location = (ImageView) view.findViewById(R.id.iv_location);
                tv_shop_name = (TextView) view.findViewById(R.id.tv_shop_name);
                tv_session = (CustomThinFont) view.findViewById(R.id.tv_session);
                tv_timing_label = (CustomThinFont) view.findViewById(R.id.tv_timing_label);
                tv_service_label = (CustomThinFont) view.findViewById(R.id.tv_service_label);
                tv_service = (CustomThinFont) view.findViewById(R.id.tv_service);
                spinner = (Spinner) view.findViewById(R.id.spinner);
                shop_timing = (TextView) view.findViewById(R.id.shop_timing);
                tv_mon = (Button) itemView.findViewById(R.id.tv_mon);
                tv_tues = (Button) itemView.findViewById(R.id.tv_tues);
                tv_wed = (Button) itemView.findViewById(R.id.tv_wed);
                tv_thurs = (Button) itemView.findViewById(R.id.tv_thurs);
                tv_fri = (Button) itemView.findViewById(R.id.tv_fri);
                tv_satur = (Button) itemView.findViewById(R.id.tv_satur);
                tv_sun = (Button) itemView.findViewById(R.id.tv_sun);
            }
        }
    }
}
