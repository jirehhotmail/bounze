package com.jireh.bounze.Fragment;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.data.Reward;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 13-12-2017.
 */

public class Rewards extends Fragment {

    TextView current_rewardpoints, current_rank, total_points,
            current_gained, used_points, ref_points, refer_a_friend;
    EditText referral_code;
    ProgressBar progress_bar;
    String myReferralCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.rewards_layout, container, false);
        initVariable(root);
        //get the wallet details from the server
        getTheWalletDetails();
        getProfile();
        setOnClickListener();
        return root;
    }

    private void getProfile() {

        try {
            showProgressDialog();
            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
            Call<UserProfileService> call = stationClient.getProfile(Prefs.getString(AppConstants.UserId, ""));

            call.enqueue(new Callback<UserProfileService>() {
                @Override
                public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                    UserProfileService result = response.body();
                    if (result.getResult().get(0).getStatus() == 1) {
                        myReferralCode = result.getResult().get(0).getMy_referral_code();
                        hideProgressDialog();
                    }


                }

                @Override
                public void onFailure(Call<UserProfileService> call, Throwable t) {
                    hideProgressDialog();
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.d("Registration_eqnn", t + "");
                    Log.d("Registration_eqnn", call + "");

                }
            });
        } catch (Exception e) {
            hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void checkTheCode() {
// it is hard coded
        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<ResponsePojo> call;

        call = stationClient.addRewardReference(Prefs.getString(AppConstants.UserId, ""), referral_code.getText().toString());


        call.enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                int response_code = response.code();
                ResponsePojo result = response.body();
                if (response_code == 200) {
                    if (result.getStatus()[0].getStatus().equals("1")) {
                        Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    } else {
                        Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void setOnClickListener() {
        //on click on the action done call the functionality
        referral_code
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                            Call<LogIn> call = stationClient.getPromocodePrice(Prefs.getString(AppConstants.UserId, ""), referral_code.getText().toString());
                            call.enqueue(new Callback<LogIn>() {
                                @Override
                                public void onResponse(final Call<LogIn> call, Response<LogIn> response) {
                                    int response_code = response.code();
                                    LogIn result = response.body();
                                    if (response_code == 200) {
                                        //check the referral code
                                        checkTheCode();
                                    }

                                }

                                @Override
                                public void onFailure(Call<LogIn> call, Throwable t) {
                                    Log.d("Registration_eqnn", t + "");
                                    Log.d("Registration_eqnn", call + "");


                                }
                            });
                            handled = true;
                        }
                        return handled;
                    }
                });


        refer_a_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");

                shareIntent.putExtra(Intent.EXTRA_TEXT, "Download the Bounze App to enhance your online shopping experience on your Android devices." +
                        "\n\n" + "https://play.google.com/store/apps/details?id=com.triton.bounze " + "\n\n" +
                        "Your Referral code is " + myReferralCode + ". If anyone use your code in the wallet page you will get 50 reward points.");

                //  shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.bounze.in/blogdescription/" + id);
                startActivity(Intent.createChooser(shareIntent, "Share using "));

            }
        });


    }

    private void initVariable(View root) {

        progress_bar = (ProgressBar) root.findViewById(R.id.progress_bar);
        current_rewardpoints = (TextView) root.findViewById(R.id.current_rewardpoints);
        current_rank = (TextView) root.findViewById(R.id.current_rank);
        total_points = (TextView) root.findViewById(R.id.total_points);
        current_gained = (TextView) root.findViewById(R.id.current_gained);
        ref_points = (TextView) root.findViewById(R.id.ref_points);
        refer_a_friend = (TextView) root.findViewById(R.id.refer_a_friend);
        referral_code = (EditText) root.findViewById(R.id.referral_code);
        used_points = (TextView) root.findViewById(R.id.used_points);
    }

    private void getTheWalletDetails() {

        try {

            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
            Call<Reward> call = stationClient.getRewardDetails(Prefs.getString(AppConstants.UserId, ""));

            call.enqueue(new Callback<Reward>() {
                @Override
                public void onResponse(Call<Reward> call, Response<Reward> response) {

                    Reward result = response.body();

                    //set the reward points in the current reward points
                    current_rewardpoints.setText(result.getReward().get(0).getCurrentreward());
                    //get the rank based on the points

                    current_rank.setText(result.getReward().get(0).getCurrentrank());

                    // Prefs.getInt(AppConstants.reward_points,0);
                    current_gained.setText("");

                    ref_points.setText(result.getReward().get(0).getReferalpoints());

                    used_points.setText(result.getReward().get(0).getUsedpoints());

                    total_points.setText(result.getReward().get(0).getTotalreward());
                }

                @Override
                public void onFailure(Call<Reward> call, Throwable t) {

                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.d("Registration_eqnn", t + "");
                    Log.d("Registration_eqnn", call + "");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
