package com.jireh.bounze.Fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jireh.bounze.Activity.Blog;
import com.jireh.bounze.Activity.EventActivity;
import com.jireh.bounze.Activity.PagerLayout;
import com.jireh.bounze.Activity.SearchActivity;
import com.jireh.bounze.Activity.ShopActivity;
import com.jireh.bounze.Activity.ShopDescriptionActivity;
import com.jireh.bounze.Activity.ShopFilter;
import com.jireh.bounze.Activity.Trainer;
import com.jireh.bounze.Adapter.OnTheGoAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.GPSTracker;
import com.jireh.bounze.data.Shop;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


/**
 * Created by Muthamizhan C on 09-08-2017.
 */

public class OnTheGo extends Fragment {

    TextView title_txt, request_txt;
    DBFunctions dbobject;

    RecyclerView list_shopos_recycler;
    LinearLayout activity_on_the_go, ll_back;
    GPSTracker gpsTracker;
    List<Shop> shoplist;
    Toolbar toolbar;
    ImageView filter_img, search_img, back_img;
    RelativeLayout spinner_layout;
    ArcLayout arc_layout;
    ImageButton fab1_image, fab2_image, fab3_image, fab4_image;
    LinearLayoutManager manager;
    int topView, positionIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.onthego_layout, container, false);


        initVariable(view);
        title_txt.setVisibility(View.VISIBLE);
        title_txt.setText("On The Go");
        spinner_layout.setVisibility(View.GONE);
        //if the user mobile version is greater than m show the permission request dialog
        // else check if it location is enabled or not

        SharedPreferences runCheck = getActivity().getSharedPreferences("hasPermission", 0); //load the preferences
        Boolean hasPermission = runCheck.getBoolean("hasPermission", false); //see if it's run before, default no
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (hasPermission) {
                checkPermission();
            }
        } else {
            checkPermission();
        }

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //store the position of the recylcerview and set to  the layout manager
            positionIndex = manager.findFirstVisibleItemPosition();
            View startView = list_shopos_recycler.getChildAt(0);
            topView = (startView == null) ? 0 : (startView.getTop() - list_shopos_recycler.getPaddingTop());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            execute();
            //retrieve the position of the recylcerview and set to  the layout manager
            if (positionIndex != -1) {
                manager.scrollToPositionWithOffset(positionIndex, topView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        System.out.println("permission granted" + requestCode);
        if (requestCode == 65535) {

            if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) &&
                    (grantResults[1] == PackageManager.PERMISSION_GRANTED) &&
                    (grantResults[2] == PackageManager.PERMISSION_GRANTED) &&
                    (grantResults[3] == PackageManager.PERMISSION_GRANTED)) {

                //if the permission is granted set the on go details
                checkPermission();

                SharedPreferences runCheck = getActivity().getSharedPreferences("hasPermission", 0); //load the preferences
                Boolean hasRun = runCheck.getBoolean("hasPermission", false); //see if it's run before, default no
                if (!hasRun) {
                    SharedPreferences settings = getActivity().getSharedPreferences("hasPermission", 0);
                    SharedPreferences.Editor edit = settings.edit();
                    edit.putBoolean("hasPermission", true); //set to has run
                    edit.commit(); //apply


                }
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        System.out.println("is fragment visible::" + isFragmentVisible_ + " is visible" + isVisible());
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_) {
                execute();
            }
            //if the permission is granted set the on go details


        }
    }

    public void execute() {

        //get user permission for location for the first tiem
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {

            if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                System.out.println("is fragment executed");
                requestPermissions(
                        new String[]{ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                                , Manifest.permission.READ_EXTERNAL_STORAGE
                        },
                        65535);
            } else {
                checkPermission();
            }


        }
    }

    private void requestPermission() {
        requestPermissions(new String[]{ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                },
                65535);

    }

   /* private boolean checkUserPermission() {
        return (     );
    }*/

    public void checkPermission() {


        System.out.println("inside check permission");
        gpsTracker = new GPSTracker(getActivity());

        if (!gpsTracker.canGetLocation()) {
            request_txt.setVisibility(View.VISIBLE);
            System.out.println("inside getlocation");
            gpsTracker.showSettingsAlert();
        } else {

            request_txt.setVisibility(View.GONE);
            System.out.println("outside getlocation");
            //if the shoplist list is not null then clear the array
            if (shoplist != null) {
                shoplist.clear();

            }
            //get the shoplist details from the localdb

            shoplist = dbobject.getShopListDB();
            setSpinner();
            //get the latitude and longitude and get the result and set it to shop list
            try {
                for (int idx = 0; idx < shoplist.size(); idx++) {

                    Location.distanceBetween(gpsTracker.getLatitude(), gpsTracker.getLongitude(), Double.parseDouble(shoplist.get(idx).getLatitude()), Double.parseDouble(shoplist.get(idx).getLongitude()), shoplist.get(idx).getResults());
                    shoplist.get(idx).setResults(shoplist.get(idx).getResults());


                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            getNearMeAndSetAdapter(shoplist);
        }

    }

    private void setSpinner() {


        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PagerLayout.viewPager.setCurrentItem(0);
            }
        });

        //open filter page for filter
        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent nextActivity = new Intent(getActivity(), ShopFilter.class);
                startActivity(nextActivity);
                //push from bottom to top
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);

            }
        });

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), SearchActivity.class);
                startActivity(in);
            }
        });
        fab1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ShopActivity.class);
                startActivity(in);
            }
        });

        fab2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Trainer.class);
                startActivity(in);
            }
        });
        fab3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), EventActivity.class);
                startActivity(in);

            }
        });

        fab4_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Blog.class);
                startActivity(in);
            }
        });

    }

    private void getNearMeAndSetAdapter(final List<Shop> tcurrent_shoplist) {

        Collections.sort(tcurrent_shoplist, new Comparator<Shop>() {
            @Override
            public int compare(Shop lhs, Shop rhs) {
                return Float.valueOf(lhs.getResults()[0]).compareTo(rhs.getResults()[0]);
            }
        });

        manager = new LinearLayoutManager(getActivity());
        list_shopos_recycler.setLayoutManager(manager);
        OnTheGoAdapter adapter = new OnTheGoAdapter(getActivity(), tcurrent_shoplist, new OnTheGoAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int view, int position) {

                //if the view is shop view clayout layout open description page else open the animation of shop images
                if (view == R.id.shop_view_clayout) {
                    Intent in = new Intent(getActivity(), ShopDescriptionActivity.class);
                    in.putExtra("ShopDetails", tcurrent_shoplist.get(position));
                    startActivity(in);

                }

            }
        });

        list_shopos_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }


    @SuppressWarnings("NewApi")
    public void showMenu() {

        try {
            arc_layout.setVisibility(View.VISIBLE);
            List<Animator> animList = new ArrayList<>();


            for (int i = 0; i < arc_layout.getChildCount(); i++) {
                View child = arc_layout.getChildAt(i);
                child.setVisibility(View.VISIBLE);
            }


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("NewApi")
    public void hideMenu() {
        try {
            arc_layout.setVisibility(View.GONE);

            List<Animator> animList = new ArrayList<>();


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    for (int i = 0; i < arc_layout.getChildCount(); i++) {
                        View child = arc_layout.getChildAt(i);
                        child.setVisibility(View.INVISIBLE);
                    }
                    // menu_layout.setVisibility(View.INVISIBLE);
                }
            });
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initVariable(View view) {
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        arc_layout = (ArcLayout) view.findViewById(R.id.arc_layout);
        title_txt = (TextView) view.findViewById(R.id.title_txt);
        request_txt = (TextView) view.findViewById(R.id.request_txt);
        back_img = (ImageView) view.findViewById(R.id.back_img);
        dbobject = new DBFunctions(getActivity().getApplicationContext());
        activity_on_the_go = (LinearLayout) view.findViewById(R.id.activity_on_the_go);
        ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
        list_shopos_recycler = (RecyclerView) view.findViewById(R.id.list_shopos_recycler);
        spinner_layout = (RelativeLayout) view.findViewById(R.id.spinner_layout);
        fab1_image = (ImageButton) view.findViewById(R.id.fab1_image);
        fab2_image = (ImageButton) view.findViewById(R.id.fab2_image);
        fab3_image = (ImageButton) view.findViewById(R.id.fab3_image);
        fab4_image = (ImageButton) view.findViewById(R.id.fab4_image);
    }


}
