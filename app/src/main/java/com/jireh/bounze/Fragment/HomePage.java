package com.jireh.bounze.Fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Activity.Blog;
import com.jireh.bounze.Activity.EventActivity;
import com.jireh.bounze.Activity.SearchActivity;
import com.jireh.bounze.Activity.ShopActivity;
import com.jireh.bounze.Activity.ShopFilter;
import com.jireh.bounze.Activity.SplashScreenActivity;
import com.jireh.bounze.Activity.Trainer;
import com.jireh.bounze.R;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Search;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 29-07-2017.
 */

public class HomePage extends Fragment {
    boolean doubleBackToExitPressedOnce = false;
    ArcLayout arc_layout;
    ImageButton fab1_image, fab2_image, fab3_image, fab4_image;
    TextView title_txt;
    //create arraylist for the four banners
    ArrayList<String> offersImagelist = new ArrayList<>();
    ArrayList<String> eventsImagelist = new ArrayList<>();
    ArrayList<String> dailyImagelist = new ArrayList<>();
    ArrayList<String> blogsImagelist = new ArrayList<>();


    ArrayList<String> offersTargetUrl = new ArrayList<>();
    ArrayList<String> eventsTargetUrl = new ArrayList<>();
    ArrayList<String> dailyTargetUrl = new ArrayList<>();
    ArrayList<String> blogsTargetUrl = new ArrayList<>();
    ProgressBar progress_bar;
    View viewCreated;
    LinearLayout ll_back;
    private SliderLayout offers_slider_layout, events_slider_layout, dailycabs_slider_layout, bounze_blogs_slider_layout;
    private Spinner city_spinner;
    private ImageView filter_img, search_img, back_img;
    private Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout resource that'll be returned
        View rootView = inflater.inflate(R.layout.home_layout, container,
                false);
        initVariable(rootView);

        //set visibility gone for the back button
        ll_back.setVisibility(View.GONE);

        setSpinner();

        title_txt.setText("Bengaluru");
        title_txt.setTextColor(getResources().getColor(R.color.white));
        //setvisibility to spinner
        city_spinner.setVisibility(View.VISIBLE);
        //get the banner details
        getBannerDetails();


        setOnClickListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewCreated = view;

    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }

    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void getBannerDetails() {
        showProgressDialog();

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Search> call = stationClient.getHomeSliderList();


        call.enqueue(new Callback<Search>() {

            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                int response_code = response.code();
                Search result = response.body();

                if (response_code == 200) {
                    //get the result array and then get the version
                    List<ResultModel> check = result.getStatus();
                    //if arraylist size > 0 then clear it before adding
                    if (offersImagelist.size() > 0 || eventsImagelist.size() > 0 ||
                            dailyImagelist.size() > 0 || blogsImagelist.size() > 0) {
                        offersImagelist.clear();
                        eventsImagelist.clear();
                        dailyImagelist.clear();
                        blogsImagelist.clear();

                        offersTargetUrl.clear();
                        eventsTargetUrl.clear();
                        dailyTargetUrl.clear();
                        blogsTargetUrl.clear();

                    }
                    //set the slider images for the 4 rows
                    for (int idx = 0; idx < check.size(); idx++) {
                        for (int position = 0; position < check.get(idx).getPosition().size(); position++) {
                            //base on the  position of the arrray add in the array list
                            if (idx == 0) {
                                offersImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());
                                offersTargetUrl.add(check.get(idx).getPosition().get(position).getTargetLink());
                            } else if (idx == 1) {
                                eventsImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());
                                eventsTargetUrl.add(check.get(idx).getPosition().get(position).getTargetLink());
                            } else if (idx == 2) {
                                dailyImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());
                                dailyTargetUrl.add(check.get(idx).getPosition().get(position).getTargetLink());
                            } else if (idx == 3) {

                                blogsImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());
                                blogsTargetUrl.add(check.get(idx).getPosition().get(position).getTargetLink());
                            }

                        }

                        //if the first,second,third,fourth position array size ends then set the adapter
                        if (idx == 0) {
                            setOffersBanner();
                        } else if (idx == 1) {
                            setEventsBanner();
                        } else if (idx == 2) {
                            setDailyDcab();
                        } else if (idx == 3) {
                            setBounzeBlogs();
                        }
                    }


                    hideProgressDialog();

                } else if (response_code == 400) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    hideProgressDialog();

                } else if (response_code == 500) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    hideProgressDialog();

                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                hideProgressDialog();

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressWarnings("NewApi")
    public void showMenu() {

        try {
            arc_layout = (ArcLayout) viewCreated.findViewById(R.id.arc_layout);
            arc_layout.setVisibility(View.VISIBLE);
            List<Animator> animList = new ArrayList<>();


            for (int i = 0; i < arc_layout.getChildCount(); i++) {
                View child = arc_layout.getChildAt(i);
                child.setVisibility(View.VISIBLE);
            }


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showArchLayout() {
        arc_layout.setVisibility(View.VISIBLE);
    }

    public void hideArchLayout() {
        arc_layout.setVisibility(View.GONE);
    }

    @SuppressWarnings("NewApi")
    public void hideMenu() {
        try {
            arc_layout = (ArcLayout) viewCreated.findViewById(R.id.arc_layout);
            arc_layout.setVisibility(View.GONE);

            List<Animator> animList = new ArrayList<>();


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    for (int i = 0; i < arc_layout.getChildCount(); i++) {
                        View child = arc_layout.getChildAt(i);
                        child.setVisibility(View.INVISIBLE);
                    }
                    // menu_layout.setVisibility(View.INVISIBLE);
                }
            });
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setOnClickListener() {
        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), SearchActivity.class);
                startActivity(in);
            }
        });
        //open filter page for filter
        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent nextActivity = new Intent(getActivity(), ShopFilter.class);
                startActivity(nextActivity);
                //push from bottom to top
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);

            }
        });
        //double click the close the app
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (doubleBackToExitPressedOnce) {

                    return;
                }

                doubleBackToExitPressedOnce = true;
                Toast.makeText(getActivity(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        });

        fab1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ShopActivity.class);
                startActivity(in);
            }
        });

        fab2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Trainer.class);
                startActivity(in);
            }
        });
        fab3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), EventActivity.class);
                startActivity(in);

            }
        });

        fab4_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Blog.class);
                startActivity(in);
            }
        });
    }

    private void setSpinner() {
        String city[] = {"Bengaluru"};
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_adapter, city);
        dataAdapter2.setDropDownViewResource(R.layout.simple_spinner_adapter);
        city_spinner.setAdapter(dataAdapter2);
    }


    private void setEventsBanner() {

        for (int idx = 0; idx < eventsImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + eventsImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            //set the slider click listener and on click redirect to that url in browser based on the slider position
            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {

                    if (!eventsTargetUrl.get(events_slider_layout.getCurrentPosition()).equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eventsTargetUrl.get(events_slider_layout.getCurrentPosition())));
                        startActivity(browserIntent);
                        System.out.println("event position" + events_slider_layout.getCurrentPosition());
                    }
                }
            });

            events_slider_layout.addSlider(defaultSliderView);
        }
        events_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        events_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        events_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        events_slider_layout.setDuration(4000);
    }

    private void setOffersBanner() {
        //  int arrayImages[] = {R.drawable.banner_one, R.drawable.banner_three};
        for (int idx = 0; idx < offersImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            System.out.println("banner image url::"+SplashScreenActivity.BASE_URL + offersImagelist.get(idx));
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + offersImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);


            //add your extra information
            defaultSliderView.bundle(new Bundle());
            //set the slider click listener and on click redirect to that url in browser based on the slider position
            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    if (!offersTargetUrl.get(offers_slider_layout.getCurrentPosition()).equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(offersTargetUrl.get(offers_slider_layout.getCurrentPosition())));
                        startActivity(browserIntent);
                    }

                }
            });

            offers_slider_layout.addSlider(defaultSliderView);
        }
        offers_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        offers_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        offers_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        offers_slider_layout.setDuration(4000);


    }

    private void setDailyDcab() {

        for (int idx = 0; idx < dailyImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + dailyImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            //set the slider click listener and on click redirect to that url in browser based on the slider position
            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {

                    if (!dailyTargetUrl.get(dailycabs_slider_layout.getCurrentPosition()).equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dailyTargetUrl.get(dailycabs_slider_layout.getCurrentPosition())));
                        startActivity(browserIntent);
                        System.out.println("event position" + dailycabs_slider_layout.getCurrentPosition());
                    }
                }
            });
            dailycabs_slider_layout.addSlider(defaultSliderView);
        }
        dailycabs_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        dailycabs_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        dailycabs_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        dailycabs_slider_layout.setDuration(4000);
    }

    private void setBounzeBlogs() {

        for (int idx = 0; idx < blogsImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + blogsImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            //set the slider click listener and on click redirect to that url in browser based on the slider position
            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {

                    if (!blogsTargetUrl.get(bounze_blogs_slider_layout.getCurrentPosition()).equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(blogsTargetUrl.get(bounze_blogs_slider_layout.getCurrentPosition())));
                        startActivity(browserIntent);
                        System.out.println("event position" + bounze_blogs_slider_layout.getCurrentPosition());
                    }
                }
            });
            bounze_blogs_slider_layout.addSlider(defaultSliderView);
        }
        bounze_blogs_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        bounze_blogs_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        bounze_blogs_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        bounze_blogs_slider_layout.setDuration(4000);
    }

    private void initVariable(View rootView) {
        offers_slider_layout = (SliderLayout) rootView.findViewById(R.id.offer_slider_layout);
        events_slider_layout = (SliderLayout) rootView.findViewById(R.id.events_slider_layout);
        dailycabs_slider_layout = (SliderLayout) rootView.findViewById(R.id.dailycabs_slider_layout);
        bounze_blogs_slider_layout = (SliderLayout) rootView.findViewById(R.id.bounze_blogs_slider_layout);

        city_spinner = (Spinner) rootView.findViewById(R.id.city_spinner);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title_txt = (TextView) toolbar.findViewById(R.id.title_txt);
        arc_layout = (ArcLayout) rootView.findViewById(R.id.arc_layout);
        fab1_image = (ImageButton) rootView.findViewById(R.id.fab1_image);
        fab2_image = (ImageButton) rootView.findViewById(R.id.fab2_image);
        fab3_image = (ImageButton) rootView.findViewById(R.id.fab3_image);
        fab4_image = (ImageButton) rootView.findViewById(R.id.fab4_image);
        progress_bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        ll_back = (LinearLayout) toolbar.findViewById(R.id.ll_back);
    }
}
