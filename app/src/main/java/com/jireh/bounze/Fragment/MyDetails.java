package com.jireh.bounze.Fragment;

import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.UserProfileService;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Muthamizhan C on 09-09-2017.
 */

public class MyDetails extends Fragment {
    EditText username_edt, email_edt, mobile_edt, height_edt, weight_edt, dob_edt;
    // SharedPreferences pred;
    View root;
    Spinner bloodgroup_spinner;
    ArrayList<String> bloodGroup = new ArrayList<>();
    Button update_profile_btn;
    RadioGroup radio_gender_group;
    RadioButton radioMale, radioFemale;
    ArrayAdapter<String> dataAdapter;
    RelativeLayout spinner_layout;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Calendar myCalendar = Calendar.getInstance();
    ImageView edit_profile;
    int edit_count;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };
    // private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    private View.OnTouchListener otl = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            return true; // the listener has consumed the event
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.profile_details_layout, container, false);
        //hide keyboard when open

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        //initialize variable
        initVariable();
        //set clickable false to mobileno, emailid ,height and weight
        disableTheDetails();


        //get the user details from server
        getUserDetails();
        //set the default image to edit profile
        edit_profile.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_unselect));
        //add blood group to the array list and set the adapter
        bloodGroup.add("A+");
        bloodGroup.add("O+");
        bloodGroup.add("B+");
        bloodGroup.add("AB+");
        bloodGroup.add("A-");
        bloodGroup.add("O-");
        bloodGroup.add("B-");
        bloodGroup.add("AB-");

        dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_row, R.id.tv_data, bloodGroup);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_row);
        bloodgroup_spinner.setAdapter(dataAdapter);

        //set on click listener for the views
        setOnClickListener();
        return root;
    }

    private void disableTheDetails() {
        username_edt.setFocusable(false);
        username_edt.setClickable(false);
        username_edt.setFocusableInTouchMode(false);

        mobile_edt.setFocusable(false);
        mobile_edt.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
        mobile_edt.setClickable(false);

        radio_gender_group.setEnabled(false);


        bloodgroup_spinner.setEnabled(false);


        email_edt.setFocusable(false);
        email_edt.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
        email_edt.setClickable(false);

        dob_edt.setEnabled(false);


        height_edt.setFocusable(false);
        height_edt.setClickable(false);
        height_edt.setFocusableInTouchMode(false);

        weight_edt.setFocusable(false);
        weight_edt.setClickable(false);
        weight_edt.setFocusableInTouchMode(false);
    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void getUserDetails() {

        showProgressDialog();
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.getProfile(Prefs.getString(AppConstants.UserId, ""));

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {
                hideProgressDialog();
                UserProfileService result = response.body();

                if (result.getResult().get(0).getStatus() == 0) {
                    Toast.makeText(getActivity(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    //get the values and set to the list

                    username_edt.setText(result.getResult().get(0).getFirstName());
                    email_edt.setText(result.getResult().get(0).getEmailId());
                    mobile_edt.setText(result.getResult().get(0).getMobileNumber());
                    //if the server value is male set the first index male else set female
                    if (result.getResult().get(0).getGender().equalsIgnoreCase("Male")) {
                        ((RadioButton) radio_gender_group.getChildAt(0)).setChecked(true);

                    } else if ((result.getResult().get(0).getGender().equalsIgnoreCase("Female"))) {
                        ((RadioButton) radio_gender_group.getChildAt(1)).setChecked(true);
                    }
                    //get the spinner value from server and set it
                    String bloodGroup = result.getResult().get(0).getBloodGroup();
                    if (!bloodGroup.equals(null) && !bloodGroup.equals("")) {
                        int spinnerPosition = dataAdapter.getPosition(bloodGroup);
                        bloodgroup_spinner.setSelection(spinnerPosition);
                    }
                    //set the date of birth
                    dob_edt.setText(result.getResult().get(0).getDateOfBirth());

                    //set the height and weight
                    height_edt.setText(result.getResult().get(0).getHeight());
                    weight_edt.setText(result.getResult().get(0).getWeight());
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });

    }

    private void setOnClickListener() {
        //on spinner layout click open spinner
        spinner_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bloodgroup_spinner.getSelectedItem() == null) { // user selected nothing...
                    bloodgroup_spinner.performClick();
                }

            }
        });
        update_profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //user details validation

                if (username_edt.getText().toString().matches("")) {
                    username_edt.setError("Enter Username");
                } else if (!email_edt.getText().toString().matches(emailPattern)) {
                    email_edt.setError("Enter valid emailid");
                } else if (mobile_edt.getText().toString().length() != 10) {
                    mobile_edt.setError("Enter valid mobileno");

                } else {
                    //call the server
                    updateProfile();
                }

            }
        });

        dob_edt.setOnTouchListener(otl);


        //get the birthday calendar
        dob_edt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        dob_edt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int inType = dob_edt.getInputType(); // backup the input type
                dob_edt.setInputType(InputType.TYPE_NULL); // disable soft input
                dob_edt.onTouchEvent(event); // call native handler
                dob_edt.setInputType(inType); // restore input type


                return true; // consume touch event


            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //based on the selection enable and disable the user details
                if (edit_count % 2 == 0) {
                    edit_profile.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_select));
                    edit_count++;
                    enableThedetails();
                } else {
                    edit_profile.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_unselect));
                    edit_count++;
                    disableTheDetails();
                }

            }
        });

    }

    private void enableThedetails() {

        username_edt.setFocusable(true);
        username_edt.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
        username_edt.setClickable(true);

        mobile_edt.setFocusable(true);
        mobile_edt.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
        mobile_edt.setClickable(true);

        email_edt.setFocusable(true);
        email_edt.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
        email_edt.setClickable(true);

        radio_gender_group.setEnabled(true);


        bloodgroup_spinner.setEnabled(true);

        height_edt.setFocusable(true);
        height_edt.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
        height_edt.setClickable(true);

        weight_edt.setFocusable(true);
        weight_edt.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
        weight_edt.setClickable(true);

        dob_edt.setEnabled(true);

    }

    private void updateProfile() {
        //update the user data
        final String userName = username_edt.getText().toString();
        final String emailId = email_edt.getText().toString();
        final String phoneNo = mobile_edt.getText().toString();
        //get the radio button text which is checked
        int radioButtonID = radio_gender_group.getCheckedRadioButtonId();
        View radioButton = radio_gender_group.findViewById(radioButtonID);
        int idx = radio_gender_group.indexOfChild(radioButton);
        RadioButton r = (RadioButton) radio_gender_group.getChildAt(idx);
        String gender = r.getText().toString();

        String bloodGroup = bloodgroup_spinner.getSelectedItem().toString();
        String dob = dob_edt.getText().toString();
        String height = height_edt.getText().toString();
        String weight = weight_edt.getText().toString();

        RequestBody requestBody = null;

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConstants.UserId, ""));
        RequestBody user_Name = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody email_Id = RequestBody.create(MediaType.parse("text/plain"), emailId);
        RequestBody phone_No = RequestBody.create(MediaType.parse("text/plain"), phoneNo);
        RequestBody gender_ = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody blood_Group = RequestBody.create(MediaType.parse("text/plain"), bloodGroup);
        RequestBody dob_ = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody height_ = RequestBody.create(MediaType.parse("text/plain"), height);
        RequestBody weight_ = RequestBody.create(MediaType.parse("text/plain"), weight);


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<UserProfileService> call = stationClient.updateProfile(userId,
                user_Name, requestBody, email_Id, phone_No, gender_, blood_Group, dob_, height_, weight_);

        call.enqueue(new Callback<UserProfileService>() {
            @Override
            public void onResponse(Call<UserProfileService> call, Response<UserProfileService> response) {

                UserProfileService result = response.body();
                if (result.getResult().get(0).getStatus() == 0) {
                    Toast.makeText(getActivity(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences pred = getActivity().getSharedPreferences("Bounze", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pred.edit();
                    editor.putString("name", userName);
                    editor.putString("emailid", emailId);
                    editor.putString("mobile", phoneNo);
                    editor.commit();
                    Toast.makeText(getActivity(), result.getResult().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<UserProfileService> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob_edt.setText(sdf.format(myCalendar.getTime()));
    }

    private void initVariable() {
        username_edt = (EditText) root.findViewById(R.id.username_edt);
        email_edt = (EditText) root.findViewById(R.id.email_edt);
        mobile_edt = (EditText) root.findViewById(R.id.mobile_edt);
        bloodgroup_spinner = (Spinner) root.findViewById(R.id.bloodgroup_spinner);
        height_edt = (EditText) root.findViewById(R.id.height_edt);
        weight_edt = (EditText) root.findViewById(R.id.weight_edt);
        update_profile_btn = (Button) root.findViewById(R.id.update_profile_btn);
        radio_gender_group = (RadioGroup) root.findViewById(R.id.radio_gender_group);
        radioMale = (RadioButton) root.findViewById(R.id.radioMale);
        radioFemale = (RadioButton) root.findViewById(R.id.radioFemale);
        dob_edt = (EditText) root.findViewById(R.id.dob_edt);
        spinner_layout = (RelativeLayout) root.findViewById(R.id.spinner_layout);
        edit_profile = (ImageView) root.findViewById(R.id.edit_profile);
        progress_bar = (ProgressBar) root.findViewById(R.id.progress_bar);
    }
}
