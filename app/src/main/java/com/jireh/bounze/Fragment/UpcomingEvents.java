package com.jireh.bounze.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Activity.BookingDescription;
import com.jireh.bounze.Adapter.BookingAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.Events;
import com.jireh.bounze.data.History;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 12-09-2017.
 */

public class UpcomingEvents extends Fragment {
    List<Event> eventlist;
    ProgressBar progress_bar;
    List<Bookhistory> historylist;
    List<Bookhistory> upcoming;
    List<Coach> coachlist;
    List<Service> servicelist;
    List<Shop> shoplist;
    RecyclerView upcoming_event_recycler;
    DBFunctions dbobject;
    TextView status_txt;
    String payPerUseDates = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.upcoming_events_layout, container, false);
        initVariable(view);
        //get the current time from server

        gettheCurrentTimeFromServer();


        //initialize the db object
        dbobject = new DBFunctions(getActivity());

        //get shop list details
        shoplist = dbobject.getShopListDB();
        //get the service list details
        servicelist = dbobject.getServiceListDB();
        coachlist = dbobject.getCoachListDB();
        return view;
    }

    private void gettheCurrentTimeFromServer() {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.getCurrentTime();
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(final Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                if (response_code == 200) {
                    System.out.println("current Time::" + result.getStatus().get(0).getMessage());


                    //convert string to date
                    String currentDate = result.getStatus().get(0).getMessage().substring(0, 10);


                    //get the event list from the local db
                    eventlist = dbobject.getEventListDB();
                    checkDetails(currentDate);
                }

            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }

    private void checkDetails(final String currentDate) {

        //get the booking events
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        System.out.println("My profile::UserId" + Prefs.getString(AppConstants.UserId, "") + "");
        Call<History> call = stationClient.historyget(Prefs.getString(AppConstants.UserId, "") + "");
        call.enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call<History> call, Response<History> response) {
                int response_code = response.code();
                History result = response.body();
                //check if the booking is there are not if yes show else show not found
                if (result.getBookhistory().get(0).getStatus() == 1) {
                    if (response_code == 200) {
                        Log.d("Me", "Registration_eqnn" + response.code() + "");

                        //Insert all booking history in the array list
                        historylist = result.getBookhistory();


                        long date = System.currentTimeMillis();


                        try {
                            //if the booking type is shop freetrial or pay per use or shop session book get the shop  image url
                            for (int idx = 0; idx < historylist.size(); idx++) {
                                if (historylist.get(idx).getBookingType().equals("1") ||
                                        historylist.get(idx).getBookingType().equals("4") ||
                                        historylist.get(idx).getBookingType().equals("5")
                                        ) {

                                    //set the shop image to the result history object
                                    Shop shopDetails = dbobject.getShopListDetailbyId(historylist.get(idx).getShopid());


                                    try {
                                        //get the shopdetails of the booking shop using the shop id
                                        historylist.get(idx).setImageUrl(shopDetails.getImageUrlSquare());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    //if the booking type is coach freetrial or pay per use or coach session book get the coach  image url
                                } else if (historylist.get(idx).getBookingType().equals("2") ||
                                        historylist.get(idx).getBookingType().equals("6") ||
                                        historylist.get(idx).getBookingType().equals("7")) {


                                    //get the coach image based on the coach id and set to history list array
                                    Coach coachDetails = dbobject.getCoachDetailsbyId(historylist.get(idx).getShopid());

                                    historylist.get(idx).setImageUrl(coachDetails.getImageUrlSquare());
                                    //  holder.squareimage.setImageResource(R.drawable.training);
                                } else if (historylist.get(idx).getBookingType().equals("3")) {
                                    historylist.get(idx).setImageUrl(String.valueOf(R.drawable.callender));
                                }


                                //set the title for the shop if freet,ppc or session
                                if (historylist.get(idx).getBookingType().equals("1") ||
                                        historylist.get(idx).getBookingType().equals("4") ||
                                        historylist.get(idx).getBookingType().equals("5")) {
                                    for (int i = 0; i < shoplist.size(); i++) {
                                        if (shoplist.get(i).getSid().equals(historylist.get(idx).getShopid())) {
                                            Shop check3 = shoplist.get(i);
                                            historylist.get(idx).setShop_name(check3.getTitle());

                                        }
                                    }



                                    //set the start dates which is going to come for pay per use

                                    if (historylist.get(idx).getBookingType().equals("5")) {
                                        String startDate = historylist.get(idx).getStartDate();
                                        String[] datesInString = startDate.substring(0, startDate.length() - 1).split(",");


                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                        List<String> type = Arrays.asList(datesInString);

                                        //add the start date details in the histroy
                                        for (int lidx = 0; lidx < type.size(); lidx++) {
                                            String dateInString = type.get(lidx);

                                            Calendar tc = Calendar.getInstance();
                                            tc.setTimeInMillis(date);
                                            Calendar c = Calendar.getInstance();
                                            try {
                           /*     if (historylist.get(idx).getBookingType().equals("3"))
                                    c.setTime(formatter.parse(dateInString));
                                else
                                    c.setTime(formatter2.parse(dateInString));

*/
                                                c.setTime(sdf.parse(dateInString));
                                                String output = sdf.format(c.getTime());

                                                //get the booking before the current date and store in the history
                                                Date strDate = sdf.parse(output);
                                                //convert current date from string to date format
                                                Date current_Date = null;
                                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                                try {
                                                    current_Date = format.parse(currentDate);
                                                    System.out.println(date);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                if (current_Date.before(strDate)) {
                                                    payPerUseDates = payPerUseDates + "," + output;

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        //    bookhistory.setStartDate();
                                        if (!payPerUseDates.equals("")) {
                                            historylist.get(idx).setStartDate(payPerUseDates.substring(1, payPerUseDates.length()));
                                            payPerUseDates = "";
                                        }
                                    }
                                } else if (historylist.get(idx).getBookingType().equals("2") ||
                                        historylist.get(idx).getBookingType().equals("6") ||
                                        historylist.get(idx).getBookingType().equals("7")) {
                                    if (coachlist != null) {
                                        for (int i = 0; i < coachlist.size(); i++) {
                                            if (coachlist.get(i).getCid().equals(historylist.get(idx).getShopid())) {
                                                Coach check4 = coachlist.get(i);
                                                historylist.get(idx).setShop_name(check4.getTitle());


                                            }
                                        }


                                        //set the start dates which is going to come for pay per use
                                        if (historylist.get(idx).getBookingType().equals("7")) {
                                            String startDate = historylist.get(idx).getStartDate();
                                            String[] datesInString = startDate.substring(0, startDate.length() - 1).split(",");

                                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                            List<String> type = Arrays.asList(datesInString);

                                            //add the start date details in the histroy
                                            for (int lidx = 0; lidx < type.size(); lidx++) {
                                                String dateInString = type.get(lidx);

                                                Calendar tc = Calendar.getInstance();
                                                tc.setTimeInMillis(date);
                                                Calendar c = Calendar.getInstance();
                                                try {
                           /*     if (historylist.get(idx).getBookingType().equals("3"))
                                    c.setTime(formatter.parse(dateInString));
                                else
                                    c.setTime(formatter2.parse(dateInString));

*/
                                                    c.setTime(sdf.parse(dateInString));
                                                    String output = sdf.format(c.getTime());

                                                    //get the booking before the current date and store in the history
                                                    Date strDate = sdf.parse(output);
                                                    //convert current date from string to date format
                                                    Date current_Date = null;
                                                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                                    try {
                                                        current_Date = format.parse(currentDate);
                                                        System.out.println(date);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                    if (current_Date.before(strDate)) {
                                                        payPerUseDates = payPerUseDates + "," + output;

                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (!payPerUseDates.equals("")) {
                                                historylist.get(idx).setStartDate(payPerUseDates.substring(1, payPerUseDates.length()));
                                                payPerUseDates = "";
                                            }
                                        }
                                    }

                                } else if (historylist.get(idx).getBookingType().equals("3")) {
                                    System.out.println("event list size::booking adapter" + eventlist.size());
                                    for (int i = 0; i < eventlist.size(); i++) {
                                        if (eventlist.get(i).getEid().equals(historylist.get(idx).getShopid())) {
                                            Event check5 = eventlist.get(i);
                                            historylist.get(idx).setTitle(check5.getName());
                                            historylist.get(idx).setEventStartDate(historylist.get(idx).getStartDate());
                                            historylist.get(idx).setEventEndDate(historylist.get(idx).getEndDate());
                                            historylist.get(idx).setImageUrl(check5.getImage());
                                            historylist.get(idx).setDescription(check5.getDescription());
                                            historylist.get(idx).setLocation(check5.getLocation());
                                            historylist.get(idx).setAddress(check5.getAddress());
                                            historylist.get(idx).setMessage(check5.getMessage());
                               /*         //add the shop name inthe array list
                                        for (int idx_shop = 0; idx_shop < shoplist.size(); idx_shop++) {
                                            if (shoplist.get(idx_shop).getSid().equals(historylist.get(idx).getShopid())) {
                                                Shop check3 = shoplist.get(idx_shop);
                                                historylist.get(idx).setShop_name(check3.getTitle());
                                                System.out.println("shop name::" + check3.getTitle());


                                            }
                                        }*/

                                        }
                                    }

                                }


                            }

                            upcoming = new ArrayList<Bookhistory>();

                            for (int idx = 0; idx < historylist.size(); idx++) {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                String dateInString = historylist.get(idx).getStartDate();

                                Calendar tc = Calendar.getInstance();
                                tc.setTimeInMillis(date);
                                Calendar c = Calendar.getInstance();
                                try {
                                    c.setTime(sdf.parse(dateInString));
                                    String output = sdf.format(c.getTime());
                                    //get the booking after the current date and store in the history
                                    Date strDate = sdf.parse(output);

                                    //convert current date from string to date format
                                    Date current_Date = null;
                                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                    try {
                                        current_Date = format.parse(currentDate);
                                        System.out.println(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if (current_Date.before(strDate)) {
                                        upcoming.add(historylist.get(idx));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                            //if there is no upcoming bookings show no upcomings found else get the upcoming details
                            if (upcoming.isEmpty()) {
                                status_txt.setVisibility(View.VISIBLE);

                            } else {
                                status_txt.setVisibility(View.GONE);

                                upcoming_event_recycler.setAdapter(new BookingAdapter(getActivity(), upcoming, eventlist, new BookingAdapter.onItemClickListener() {
                                    @Override
                                    public void onItemClick(int item, int position) {
                                        Intent in = new Intent(getActivity(), BookingDescription.class);
                                        in.putExtra("position", (Serializable) upcoming.get(position));
                                        in.putExtra("flag", "Upcoming Events");
                                        startActivity(in);
                                    }
                                }));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (response_code == 400) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                        Log.e("m", "400 error");
                    } else if (response_code == 500) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                        Log.e("m", "500 error");
                    }
                } else {
                    status_txt.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onFailure(Call<History> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    private void initVariable(View view) {
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        upcoming_event_recycler = (RecyclerView) view.findViewById(R.id.upcoming_event_recycler);
        LinearLayoutManager layoutmanager = new LinearLayoutManager(getActivity());
        upcoming_event_recycler.setLayoutManager(layoutmanager);
        status_txt = (TextView) view.findViewById(R.id.status_txt);
    }

    private void getEventlist(final String currentDate) {

        progress_bar.setVisibility(View.VISIBLE);
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Events> call = stationClient.getEventList();
        call.enqueue(new Callback<Events>() {
            @Override
            public void onResponse(Call<Events> call, Response<Events> response) {
                int response_code = response.code();
                response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    eventlist = response.body().getEvent();
                    System.out.println("event list size::splash screen" + eventlist.size());
                    progress_bar.setVisibility(View.GONE);

                } else {
                    progress_bar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Low Internet  /  Failed Connecting to server! please Try after some time...", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Events> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");
                progress_bar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "No Internet  /  Failed Connecting to server! please Try after some time...", Toast.LENGTH_SHORT).show();

            }
        });
    }


}
