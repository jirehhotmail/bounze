package com.jireh.bounze.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.CoachList;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.ResponsePojo;
import com.jireh.bounze.data.Review;
import com.jireh.bounze.data.Review_;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.Shoplist;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 14-12-2017.
 */

@SuppressLint("ValidFragment")
public class ShowReviewFragment extends DialogFragment {

    RecyclerView rv;
    MyAdapter adapter;
    List<Review_> reviews;
    Button bt_done, bt_cancel;
    TextView tv_title;
    RatingBar ratingBar;
    EditText review;
    DBFunctions dbobject;
    Shop shopDetails;
    Coach coachDetails;
    Event eventDetails;
    PassArr singlePass;
    String blog_id;

    public ShowReviewFragment(List<Review_> myReview, Shop shopDetails, Coach coachDetails, Event eventDetails,
                              PassArr singlePass, String blog_id) {
        this.reviews = myReview;
        //based on the review type get the shop/coach/event/pass/blog details and display the review
        if (shopDetails != null) {
            this.shopDetails = shopDetails;
        }
        if (coachDetails != null) {
            this.coachDetails = coachDetails;
        }
        if (eventDetails != null) {
            this.eventDetails = eventDetails;
        }
        if (singlePass != null) {
            this.singlePass = singlePass;
        }
        if (blog_id != null) {
            this.blog_id = blog_id;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.rate_bar_dailog, container);
        dbobject = new DBFunctions(getActivity());
        bt_done = (Button) rootView.findViewById(R.id.bt_done);
        bt_cancel = (Button) rootView.findViewById(R.id.bt_cancel);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);
        review = (EditText) rootView.findViewById(R.id.comments_edt);
        //RECYCER
        rv = (RecyclerView) rootView.findViewById(R.id.mRecyerID);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        //set visibility visible for the recycler view
        rv.setVisibility(View.VISIBLE);
        tv_title.setText("Tell us what you think?");
        review.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);
        //ADAPTER
        adapter = new MyAdapter(this.getActivity(), reviews);
        rv.setAdapter(adapter);

        setOnClickListener();
        return rootView;
    }

    private void setOnClickListener() {


//        ratingBar.setRating(Float.valueOf(shopDetails.getAvgratings()));


        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //

                doRating("" + ratingBar.getRating(), review);
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

    }

    private void doRating(String rating, EditText review) {
        if (isInternetOn()) {

            shop_ratings(rating, review);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void shop_ratings(String rating, EditText review) {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Review> call = null;
        if (shopDetails != null) {
            call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), shopDetails.getSid(), "", "", "", review.getText().toString(), rating);
        }
        if (coachDetails != null) {
            call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), "", "", coachDetails.getCid(), "", review.getText().toString(), rating);
        }
        if (eventDetails != null) {
            call = stationClient.ratings(Prefs.getString(AppConstants.UserId, ""), "", eventDetails.getEid(), "", "", review.getText().toString(), rating);
        }
        if (singlePass != null) {

            Call<ResponsePojo> call_pass;

            call_pass = stationClient.createRating(Prefs.getString(AppConstants.UserId, ""), singlePass.getPass_id(), review.getText().toString(), rating);

            call_pass.enqueue(new Callback<ResponsePojo>() {
                @Override
                public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                    int response_code = response.code();
                    ResponsePojo result = response.body();
                    if (response_code == 200) {
                        if (result.getStatus()[0].getStatus().equals("1")) {
                            Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    } else if (response_code == 400 || response_code == 500) {
                        Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponsePojo> call, Throwable t) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();

                }
            });
        }
        if (blog_id != null) {

            Call<ResponsePojo> call_blog;

            call_blog = stationClient.createBlogRating(Prefs.getString(AppConstants.UserId, ""), blog_id, review.getText().toString(), rating);

            call_blog.enqueue(new Callback<ResponsePojo>() {
                @Override
                public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                    int response_code = response.code();
                    ResponsePojo result = response.body();
                    if (response_code == 200) {
                        if (result.getStatus()[0].getStatus().equals("1")) {
                            Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    } else if (response_code == 400 || response_code == 500) {
                        Toast.makeText(getActivity(), result.getStatus()[0].getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponsePojo> call, Throwable t) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();

                }
            });
        }
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(Call<Review> call, Response<Review> response) {
                int response_code = response.code();
                Review result = response.body();
                if (response_code == 200) {
                    //if submitted once show the reiew submitted toast else show the message from server
                    if (result.getStatus().get(0).getStatus().equals("1")) {

                        Toast.makeText(getActivity(), "Review submitted successfully", Toast.LENGTH_SHORT).show();
                        //udate ratings from server
                        if (shopDetails != null) {
                            getShopRatings();
                        }
                        if (coachDetails != null) {
                            getCoachRatings();
                        }
                    } else {
                        Toast.makeText(getActivity(), result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if (response_code == 400) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("m", "400 error");
                } else if (response_code == 500) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.e("m", "500 error");
                }
            }

            @Override
            public void onFailure(Call<Review> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });


    }

    private void getCoachRatings() {

        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<CoachList> call = stationClient.getCoachRatings();
        call.enqueue(new Callback<CoachList>() {
            @Override
            public void onResponse(Call<CoachList> call, Response<CoachList> response) {
                int response_code = response.code();
                CoachList result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateCoachListDB(result.getCoachrating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");


                } else {

                }
            }

            @Override
            public void onFailure(Call<CoachList> call, Throwable t) {


            }
        });


    }

    private void getShopRatings() {


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<Shoplist> call = stationClient.getShopRatings();
        call.enqueue(new Callback<Shoplist>() {
            @Override
            public void onResponse(Call<Shoplist> call, Response<Shoplist> response) {
                int response_code = response.code();
                Shoplist result = response.body();
                if (response_code == 200) {
                    Log.d("service list", "Registration_eqnn " + response.code() + "");
                    // shoplist = result.getShop();
                    dbobject.updateShopListDB(result.getShoprating());
                    Log.d("service list", "Registration_eqnn " + response.code() + "");


                } else {

                }
            }

            @Override
            public void onFailure(Call<Shoplist> call, Throwable t) {


            }
        });

    }

    private boolean isInternetOn() {

        boolean isConnectionAvail = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null) return netInfo.isConnected();
            else return isConnectionAvail;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnectionAvail;
    }

    class MyAdapter extends RecyclerView.Adapter<MyHolder> {

        Activity activity;

        List<Review_> reviews;

        public MyAdapter(Activity activity, List<Review_> reviews) {
            this.activity = activity;
            this.reviews = reviews;

        }

        //INITIALIE VH
        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_reviews, parent, false);
            MyHolder holder = new MyHolder(v);
            return holder;
        }

        //BIND DATA
        @Override
        public void onBindViewHolder(MyHolder holder, int position) {
            Review_ review = reviews.get(position);
            holder.ratingBar.setRating(Float.valueOf(review.getRating()));
            holder.person_name.setText(review.getFirstName());
            holder.reviews_text.setText(review.getReview());
        }

        @Override
        public int getItemCount() {
            return reviews.size();
        }
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        CardView all_review_layout;
        RatingBar ratingBar;
        TextView person_name, reviews_text;

        public MyHolder(View view) {
            super(view);
            all_review_layout = (CardView) view.findViewById(R.id.all_review_layout);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            person_name = (TextView) view.findViewById(R.id.person_name);
            reviews_text = (TextView) view.findViewById(R.id.reviews_text);
        }
    }

}