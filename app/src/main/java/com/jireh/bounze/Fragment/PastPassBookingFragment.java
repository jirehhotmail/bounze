package com.jireh.bounze.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jireh.bounze.Activity.CurrentPassBooking;
import com.jireh.bounze.Adapter.PassLogoAdapter;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.PassCompleteHistoryPojo;
import com.jireh.bounze.data.PassPastHistory;
import com.jireh.bounze.data.Service;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * <h2>PastPassBookingFragment</h2>
 * This class is used for showing pass booking history,
 * When user done the pass booking, then he/she can see their pass booking related information
 * using page, here we are showing OUTGOING and COMPLETED pass history
 * and this is a fragment calling from pass booking activity.
 *
 * @author Shubham
 * @Since 05-10-2017
 */

public class PastPassBookingFragment extends Fragment {

    List<Event> eventlist;
    ProgressBar progress_bar;
    List<Bookhistory> historylist;
    List<Bookhistory> history;
    RecyclerView rv_pass;
    //    DBFunctions dbobject;
    List<Service> servicelist;
    //    List<Shop> shoplist;
    private PassHistoryAdapter mAdapter;
    private SessionManager sessionManager;
    //    private PassArr[] passList;
    private PassPastHistory[] pastPassArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booking_pass_history, container, false);

        Bundle bundle = getArguments();
        String result = bundle.getString("past_pass");
        Gson gson = new Gson();
        PassCompleteHistoryPojo historyPojo = gson.fromJson(result, PassCompleteHistoryPojo.class);
        pastPassArray = historyPojo.getPassHistory()[0].getPasthistory();

        initVariable(view);
        return view;
    }


    private void initVariable(View view) {
        rv_pass = (RecyclerView) view.findViewById(R.id.rl_pass_history);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_pass.setLayoutManager(manager);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        sessionManager = new SessionManager(getActivity());
//        passList = sessionManager.getPass_Result();
        // specify an adapter
        callAdapter();
    }

    /**
     * <h3>callAdapter</h3>
     * This method is used for itegrating the adapter with our Recycler view.
     */
    private void callAdapter() {
        mAdapter = new PassHistoryAdapter(getActivity(), pastPassArray);
        rv_pass.setAdapter(mAdapter);

        rv_pass.setItemAnimator(new DefaultItemAnimator());

        mAdapter.setOnItemClickListener(new OnItemClick() {
            @Override
            public void onItemClicked(View view, int position, Object data) {
                Log.d("value of position: ", "Position of adapter: " + position);
                Intent intent = new Intent(getActivity(), CurrentPassBooking.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("passbookinghistory", " onactivityresult requestcode: " + requestCode + " ,resultCode: " + resultCode);
        if (requestCode == 101) {
            String message = data.getStringExtra("result");
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }


    public interface OnItemClick {
        public void onItemClicked(View view, int position, Object data);
    }

    public class PassHistoryAdapter extends RecyclerView.Adapter<PassHistoryAdapter.ViewHolder> {
        PassPastHistory[] passList;
        private Context mContext;
        private OnItemClick mListener;

        // Provide a suitable constructor (depends on the kind of dataset)
        public PassHistoryAdapter(Context context, PassPastHistory[] passList) {
            this.mContext = context;
            this.passList = passList;
            progress_bar.setVisibility(View.GONE);
        }

        public void setOnItemClickListener(OnItemClick listener) {
            mListener = listener;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public PassHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_pass_history_row, parent, false);
            // set the view's size, margins, paddings and layout parameters

            PassHistoryAdapter.ViewHolder vh = new PassHistoryAdapter.ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final PassHistoryAdapter.ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final PassPastHistory model = getItem(position);
            holder.pass_name.setText(model.getPass_name());


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, position, model);
                }
            });

            Picasso.with(mContext).load(BASE_URL + model.getPass_logo()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.pass_logo_img);


            /*if (!model.getRatings().equals(""))
                holder.ratingbar.setRating(Float.valueOf(String.valueOf(model.getRatings())));
            else
                holder.ratingbar.setRating(Float.valueOf(0.0f));*/

            holder.location_area_txt.setText(model.getLocation());
            holder.session_txt.setText(model.getUsed_sessions() + " Session Remaining");
            holder.price_txt.setText(mContext.getResources().getString(R.string.Rs) + "100");//model.getPrice());
//            holder.bought_total_txt.setText(model.get() + "+ bought");


            holder.location_area_txt.setMovementMethod(new ScrollingMovementMethod());

            /*holder.book_pass_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), PassDescriptionActivity.class);
                    sessionManager.setSinglePass(model);
                    startActivity(intent);
                }
            });*/
            String serviceImg = passList[position].getSubservices_image().trim();
            String[] service_Logo_Arr = serviceImg.split(",");
            ArrayList<String> serviceLogo = new ArrayList<>();
            for (int tempPos = 0; tempPos < service_Logo_Arr.length; tempPos++) {
                serviceLogo.add(tempPos, service_Logo_Arr[tempPos]);
            }

            holder.horizontalList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            PassLogoAdapter horizontalAdapter = new PassLogoAdapter(mContext, serviceLogo);
            holder.horizontalList.setAdapter(horizontalAdapter);
        }

        private PassPastHistory getItem(int position) {
            return passList[position];
        }

        private Context getContext() {
            return mContext;
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return passList.length;
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            ImageView pass_logo_img;
            TextView location_area_txt, session_txt, price_txt, bought_total_txt, pass_name;
            Button book_pass_btn;
            //            RatingBar ratingbar;
            private RecyclerView horizontalList;

            public ViewHolder(View view) {
                super(view);
                pass_logo_img = (ImageView) view.findViewById(R.id.pass_logo_img);
                pass_name = (TextView) view.findViewById(R.id.pass_name);
//                ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
                location_area_txt = (TextView) view.findViewById(R.id.location_area_txt);
                session_txt = (TextView) view.findViewById(R.id.session_txt);
                price_txt = (TextView) view.findViewById(R.id.price_txt);
                bought_total_txt = (TextView) view.findViewById(R.id.bought_total_txt);
                book_pass_btn = (Button) view.findViewById(R.id.book_pass_btn);
                horizontalList = (RecyclerView) itemView.findViewById(R.id.horizontal_list);
            }
        }
    }
}
