package com.jireh.bounze.Fragment;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.ChangePassword;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 09-09-2017.
 */

public class Change_Password extends Fragment {
    EditText old_password_edt;
    EditText new_password_edt;
    EditText confirm_new_pwd;
    Button submit_pwd_btn;
    ProgressBar progress_bar;
    private boolean Validation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password_dialog, container, false);
        //initialize variable

        old_password_edt = (EditText) view.findViewById(R.id.old_password_edt);
        new_password_edt = (EditText) view.findViewById(R.id.new_password_edt);
        confirm_new_pwd = (EditText) view.findViewById(R.id.confirm_new_pwd);
        submit_pwd_btn = (Button) view.findViewById(R.id.submit_pwd_btn);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);

        submit_pwd_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //before submit check the internet connection
                if (isInternetOn()) {
                    //check the validation in all field is filled
                    if (validation()) {
                        showProgressDialog();
                        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
                        Call<ChangePassword> call = stationClient.changepassword(Integer.parseInt(Prefs.getString(AppConstants.UserId, "")), old_password_edt.getText().toString(), new_password_edt.getText().toString());
                        call.enqueue(new Callback<ChangePassword>() {
                            @Override
                            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                                int response_code = response.code();
                                ChangePassword result = response.body();

                                if (response_code == 200) {


                                    if (result.getStatus().get(0).getStatus() == 1) {
                                        Toast.makeText(getActivity(), "" + result.getStatus().get(0).getMessage(), Toast.LENGTH_LONG).show();
                                        hideProgressDialog();
                                        getActivity().finish();
                                    } else {
                                        Toast.makeText(getActivity(), "Invalid current password", Toast.LENGTH_SHORT).show();
                                        hideProgressDialog();
                                    }


                                    //    hideProgressDialog();
                                } else if (response_code == 400) {
                                    Toast.makeText(getActivity(), "Unable to Connect to server! Try later", Toast.LENGTH_SHORT).show();
                                    hideProgressDialog();
                                    Log.e("m", "400 error");
                                } else if (response_code == 500) {
                                    Toast.makeText(getActivity(), "Unable to Connect to server! Try later", Toast.LENGTH_SHORT).show();
                                    hideProgressDialog();
                                    Log.e("m", "500 error");
                                }
                            }

                            @Override
                            public void onFailure(Call<ChangePassword> call, Throwable t) {
                                Toast.makeText(getActivity(), "Kindly check your internet connection", Toast.LENGTH_SHORT).show();
                                Log.d("Registration_eqnn", t + "");
                                Log.d("Registration_eqnn", call + "");

                            }
                        });
                    }
                } else {
                    Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private boolean isInternetOn() {
        ConnectivityManager connectivity = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivity.getActiveNetworkInfo();
        return info != null & info.isConnected();
    }

    private boolean validation() {


        Validation = true;
        if (old_password_edt.getText().toString().matches("")) {
            old_password_edt.setError("Enter old password");
            Validation = false;
        } else if (new_password_edt.getText().toString().matches("")) {
            new_password_edt.setError("Enter new password");
            Validation = false;
        } else if (!(new_password_edt.getText().toString().length() >= 5)) {
            new_password_edt.setError("Password should be minimum 5-char !");
            Validation = false;
        }
        else if(!(new_password_edt.getText().toString().equals(confirm_new_pwd.getText().toString())))
        {
            confirm_new_pwd.setError("password mismatch !");
            Validation = false;
        }

        return Validation;
    }
}
