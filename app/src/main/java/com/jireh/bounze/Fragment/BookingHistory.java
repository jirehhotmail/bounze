package com.jireh.bounze.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Activity.BookingDescription;
import com.jireh.bounze.Adapter.BookingAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.data.Bookhistory;
import com.jireh.bounze.data.Coach;
import com.jireh.bounze.data.Event;
import com.jireh.bounze.data.History;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Service;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 12-09-2017.
 */

public class BookingHistory extends Fragment {
    List<Event> eventlist;
    ProgressBar progress_bar;
    List<Bookhistory> historylist;
    List<Bookhistory> history;
    RecyclerView booking_history_recycler;
    DBFunctions dbobject;
    List<Coach> coachlist;
    List<Service> servicelist;
    List<Shop> shoplist;
    TextView status_txt;
    String payPerUseDates = "";
    Bookhistory bookhistory = new Bookhistory();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bookinghistory_layout, container, false);
        initVariable(view);
        dbobject = new DBFunctions(getActivity());
        //get shop list details
        shoplist = dbobject.getShopListDB();

        //get the coach details

        coachlist = dbobject.getCoachListDB();

        //get the service list details
        servicelist = dbobject.getServiceListDB();
        //get the current time from server

        gettheCurrentTimeFromServer();


        return view;
    }


    private void gettheCurrentTimeFromServer() {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call = stationClient.getCurrentTime();
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(final Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                LogIn result = response.body();
                if (response_code == 200) {
                    System.out.println("current Time::" + result.getStatus().get(0).getMessage());


                    //convert string to date
                    String currentDate = result.getStatus().get(0).getMessage().substring(0, 10);

                    //get the evenat list from the local db
                    eventlist = dbobject.getEventListDB();
                    checkDetails(currentDate);
                }

            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });
    }


    private void initVariable(View view) {

        booking_history_recycler = (RecyclerView) view.findViewById(R.id.booking_history_recycler);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        booking_history_recycler.setLayoutManager(manager);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        status_txt = (TextView) view.findViewById(R.id.status_txt);
    }

    private void checkDetails(final String currentDate) {
        //get the booking history of the user
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);

        Call<History> call = stationClient.historyget(Prefs.getString(AppConstants.UserId, ""));
        call.enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call<History> call, Response<History> response) {
                int response_code = response.code();
                History result = response.body();
//check if the booking is there are not if yes show else show not found
                if (result.getBookhistory().get(0).getStatus() == 1) {
                    if (response_code == 200) {

                        //get history list from server
                        //Insert all booking history in the array list
                        historylist = result.getBookhistory();

                        //get current time from the phone
                        long date = System.currentTimeMillis();

                        try {
                            //if the booking type is shop freetrial or pay per use or shop session book get the shop  image url
                            for (int idx = 0; idx < historylist.size(); idx++) {
                                if (historylist.get(idx).getBookingType().equals("1") ||
                                        historylist.get(idx).getBookingType().equals("4") ||
                                        historylist.get(idx).getBookingType().equals("5")
                                        ) {

                                    //set the shop image to the result history object
                                    Shop shopDetails = dbobject.getShopListDetailbyId(historylist.get(idx).getShopid());


                                    try {
                                        //get the shopdetails of the booking shop using the shop id
                                        historylist.get(idx).setImageUrl(shopDetails.getImageUrlSquare());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }


                                if ((historylist.get(idx).getBookingType().equals("2") ||
                                        historylist.get(idx).getBookingType().equals("6") ||
                                        historylist.get(idx).getBookingType().equals("7"))) {


                                    //get the coach image based on the coach id and set to history list array
                                    Coach coachDetails = dbobject.getCoachDetailsbyId(historylist.get(idx).getShopid());

                                    historylist.get(idx).setImageUrl(coachDetails.getImageUrlSquare());
                                    //  holder.squareimage.setImageResource(R.drawable.training);
                                }


                                if (historylist.get(idx).getBookingType().equals("3")) {
                                    historylist.get(idx).setImageUrl(String.valueOf(R.drawable.callender));
                                }


                                //set the title for the shop if freet,ppu or session
                                if (historylist.get(idx).getBookingType().equals("1") ||
                                        historylist.get(idx).getBookingType().equals("4") ||
                                        historylist.get(idx).getBookingType().equals("5")) {
                                    for (int i = 0; i < shoplist.size(); i++) {
                                        if (shoplist.get(i).getSid().equals(historylist.get(idx).getShopid())) {
                                            Shop check3 = shoplist.get(i);
                                            historylist.get(idx).setShop_name(check3.getTitle());

                                        }
                                    }
                                    //set the start dates which is went for pay per use

                                    if (historylist.get(idx).getBookingType().equals("5")) {
                                        String startDate = historylist.get(idx).getStartDate();
                                        String[] datesInString = startDate.substring(0, startDate.length() - 1).split(",");


                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                        List<String> type = Arrays.asList(datesInString);

                                        //add the start date details in the histroy
                                        for (int lidx = 0; lidx < type.size(); lidx++) {
                                            String dateInString = type.get(lidx);

                                            Calendar tc = Calendar.getInstance();
                                            tc.setTimeInMillis(date);
                                            Calendar c = Calendar.getInstance();
                                            try {
                           /*     if (historylist.get(idx).getBookingType().equals("3"))
                                    c.setTime(formatter.parse(dateInString));
                                else
                                    c.setTime(formatter2.parse(dateInString));

*/
                                                c.setTime(sdf.parse(dateInString));
                                                String output = sdf.format(c.getTime());

                                                //get the booking before the current date and store in the history
                                                Date strDate = sdf.parse(output);
                                                //convert current date from string to date format
                                                Date current_Date = null;
                                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                                try {
                                                    current_Date = format.parse(currentDate);
                                                    System.out.println(date);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                if (current_Date.after(strDate)) {
                                                    payPerUseDates = payPerUseDates + "," + output;

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        //    bookhistory.setStartDate();
                                        if (!payPerUseDates.equals("")) {
                                            historylist.get(idx).setStartDate(payPerUseDates.substring(1, payPerUseDates.length()));
                                            payPerUseDates = "";
                                        }
                                    }


                                }

                                //set the title for the coach if freet,ppu or session
                                if ((historylist.get(idx).getBookingType().equals("2") ||
                                        historylist.get(idx).getBookingType().equals("6") ||
                                        historylist.get(idx).getBookingType().equals("7"))) {
                                    for (int i = 0; i < coachlist.size(); i++) {
                                        if (coachlist.get(i).getCid().equals(historylist.get(idx).getShopid())) {
                                            Coach check4 = coachlist.get(i);
                                            historylist.get(idx).setShop_name(check4.getTitle());


                                        }
                                    }

                                    //set the start dates which is went for pay per use
                                    if (historylist.get(idx).getBookingType().equals("7")) {
                                        String startDate = historylist.get(idx).getStartDate();
                                        String[] datesInString = startDate.substring(0, startDate.length() - 1).split(",");

                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                        List<String> type = Arrays.asList(datesInString);

                                        //add the start date details in the histroy
                                        for (int lidx = 0; lidx < type.size(); lidx++) {
                                            String dateInString = type.get(lidx);

                                            Calendar tc = Calendar.getInstance();
                                            tc.setTimeInMillis(date);
                                            Calendar c = Calendar.getInstance();
                                            try {
                           /*     if (historylist.get(idx).getBookingType().equals("3"))
                                    c.setTime(formatter.parse(dateInString));
                                else
                                    c.setTime(formatter2.parse(dateInString));

*/
                                                c.setTime(sdf.parse(dateInString));
                                                String output = sdf.format(c.getTime());

                                                //get the booking before the current date and store in the history
                                                Date strDate = sdf.parse(output);
                                                //convert current date from string to date format
                                                Date current_Date = null;
                                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                                try {
                                                    current_Date = format.parse(currentDate);
                                                    System.out.println(date);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                if (current_Date.after(strDate)) {
                                                    payPerUseDates = payPerUseDates + "," + output;

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (!payPerUseDates.equals("")) {
                                            historylist.get(idx).setStartDate(payPerUseDates.substring(1, payPerUseDates.length()));
                                            payPerUseDates = "";
                                        }
                                    }


                                }

                                //set the title for the event
                                if (historylist.get(idx).getBookingType().equals("3")) {
                                    System.out.println("event list size::booking adapter" + eventlist.size());
                                    for (int eidx = 0; eidx < eventlist.size(); eidx++) {
                                        if (eventlist.get(eidx).getEid().equals(historylist.get(idx).getShopid())) {
                                            Event check5 = eventlist.get(eidx);
                                            historylist.get(idx).setTitle(check5.getName());
                                            historylist.get(idx).setEventStartDate(historylist.get(idx).getStartDate());
                                            historylist.get(idx).setEventEndDate(historylist.get(idx).getEndDate());
                                            historylist.get(idx).setContactMobile(check5.getContactMobile());
                                            historylist.get(idx).setImageUrl(check5.getImage());
                                            historylist.get(idx).setDescription(check5.getDescription());
                                            historylist.get(idx).setLocation(check5.getLocation());
                                            historylist.get(idx).setAddress(check5.getAddress());
                                            historylist.get(idx).setMessage(check5.getMessage());

                                        }
                                    }

                                }


                            }

                            history = new ArrayList<Bookhistory>();
                            System.out.println("history list::" + historylist.size());

                            //based on the booking type separate  the pay per use dates into history

                            for (int idx = 0; idx < historylist.size(); idx++) {


                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                String dateInString = historylist.get(idx).getStartDate();

                                Calendar tc = Calendar.getInstance();
                                tc.setTimeInMillis(date);
                                Calendar c = Calendar.getInstance();
                                try {
                           /*     if (historylist.get(idx).getBookingType().equals("3"))
                                    c.setTime(formatter.parse(dateInString));
                                else
                                    c.setTime(formatter2.parse(dateInString));

*/
                                    c.setTime(sdf.parse(dateInString));
                                    String output = sdf.format(c.getTime());

                                    //get the booking before the current date and store in the history
                                    Date strDate = sdf.parse(output);
                                    //convert current date from string to date format
                                    Date current_Date = null;
                                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                    try {
                                        current_Date = format.parse(currentDate);
                                        System.out.println(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }


                                    if (current_Date.after(strDate)) {
                                        history.add(historylist.get(idx));
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            //if there is no bookings show no booking found else get the booking details

                            if (!history.isEmpty()) {
                                status_txt.setVisibility(View.GONE);

                                //add all values to historyfinal list
                                booking_history_recycler.setAdapter(new BookingAdapter(getActivity(), history, eventlist, new BookingAdapter.onItemClickListener() {
                                    @Override
                                    public void onItemClick(int item, int position) {
                                        Intent in = new Intent(getActivity(), BookingDescription.class);
                                        in.putExtra("position", (Serializable) history.get(position));
                                        in.putExtra("flag", "Booking history");
                                        startActivity(in);
                                    }
                                }));

                            } else {
                                status_txt.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (response_code == 400) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                        Log.e("m", "400 error");
                    } else if (response_code == 500) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                        Log.e("m", "500 error");
                    }
                } else {
                    status_txt.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<History> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
