package com.jireh.bounze.Fragment;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jireh.bounze.Activity.BookAShop;
import com.jireh.bounze.Adapter.ExpandablePackageAdapter;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AppConstants;
import com.jireh.bounze.Util.NonScrollableExpandableListView;
import com.jireh.bounze.data.LogIn;
import com.jireh.bounze.data.Sessions;
import com.jireh.bounze.data.Shop;
import com.jireh.bounze.data.ShopSerializable;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 21-09-2017.
 */

public class ShopServiceDescription extends Fragment {

    LinearLayout free_trial_layout, pay_per_use_layout;
    List<String> listDataHeader;
    TextView pay_per_cost_txt, free_trial_txt;
    RadioButton free_trial_radio, pay_per_use_radio;
    NonScrollableExpandableListView expandableListView;
    ExpandablePackageAdapter listAdapter;
    HashMap<String, List<String>> listDataChild;
    Shop shop;
    ShopSerializable subServices;
    Button book_btn;
    String selectedBatchTime = "";
    Sessions sessionDetails;
    int lastExpandedPosition = -1;
    String noOfBatches = "";
    RadioButton SessionView;
    Point p;
    //  private ProgressDialog mProgressDialog;
    ProgressBar progress_bar;
    boolean expanded = true;
    private Animation animShow, animHide;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.shop_service_desc_layout, container, false);

        initVariable(view);


        //get the value from the bundle
        subServices = (ShopSerializable) getArguments().getSerializable("ShopDetails");


        shop = (Shop) getArguments().getSerializable("shop");

        //check if the free trial available if yes show else don't show
        if (shop.getIsFreeTrailAvailable().equals("0")) {

            free_trial_layout.setVisibility(View.GONE);
        } else {
            //FREE TRIAL IS  hardcoded
            free_trial_layout.setVisibility(View.VISIBLE);
        }
        //set pay per cost is greater than 0 or empty show else don't show
        if (subServices.getPayPerUse().equals("0") || subServices.getPayPerUse().equals("")) {
            pay_per_use_layout.setVisibility(View.GONE);
        } else {
            pay_per_use_layout.setVisibility(View.VISIBLE);
            pay_per_cost_txt.setText(getActivity().getResources().getString(R.string.Rs) + subServices.getPayPerUse());
        }
        //check for this shop registration fee is applicable or not if there show the toast
        if (!shop.getIsRegnAvailable().equals("1")) {
            Toast.makeText(getActivity(), "Registration fee is applicable for this shop", Toast.LENGTH_SHORT).show();
        }

        prepareListData(subServices);

        //pass the values to the expandable adapter for header and child and get the item click listener  of the item selected in the session adapter
        listAdapter = new ExpandablePackageAdapter(getActivity(), listDataHeader, listDataChild, subServices, new ExpandablePackageAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int groupPosition, RadioButton view, int position, Activity activity) {
                //if the view is sesssion radio get the selected position of the session

                //get the position of the session selected
                System.out.println("session position clicked::" + position);
                //get the subservies details  based on group position selection
                sessionDetails = subServices.getServiceDetails().get(groupPosition).getSessions_details().get(position);
                //to check if the package have batches.
                if (subServices.getServiceDetails().get(groupPosition).getBatchTimes() == null) {
                    noOfBatches = "0";
                }
                SessionView = view;

                setOnClickListener();
                //display the book button as pop up
                showPopup(getActivity());


            }


        }, new ExpandablePackageAdapter.onItemBatchClickListener() {
            @Override
            public void onItemBatchClick(CardView batch_layout, int group_position, int position, Activity activity) {
                //if the view is batch layout get the selected batch time
                String starTime = subServices.getServiceDetails().get(group_position).getBatchTimes().get(position).getStartTime();
                String endTime = subServices.getServiceDetails().get(group_position).getBatchTimes().get(position).getEndTime();

                selectedBatchTime = starTime + " to " + endTime;
                try {
                    //if the session view is check and on click batch show the pop up
                    if (SessionView.isChecked()) {
                        //show the pop up on click the
                        showPopup(getActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });

        // setting list adapter
        expandableListView.setAdapter(listAdapter);

        if (subServices.getPackageName().equals("")) {
            expandableListView.expandGroup(0);
        }

        setOnClickListener();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            free_trial_radio.setChecked(false);
            pay_per_use_radio.setChecked(false);
            SessionView.setChecked(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareListData(ShopSerializable shop) {

        listDataHeader = new ArrayList<String>();
        //adding the package name for the subservice
        for (int idx = 0; idx < shop.getServiceDetails().size(); idx++) {
            listDataHeader.add(shop.getServiceDetails().get(idx).getPackageName());

        }


        listDataChild = new HashMap<String, List<String>>();

        List<String> childData = new ArrayList<>();
        childData.add("idx");
        //add the subservice has two packages
        for (int idx = 0; idx < shop.getServiceDetails().size(); idx++) {
            listDataChild.put(listDataHeader.get(idx), childData); // Header, Child data
        }
    }


    private void setOnClickListener() {
        try {


            SessionView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                }


            });

            //set on click listner for free trial layout make the free trial radio checked
            free_trial_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("SSD: free trial layout clicked");
                    free_trial_radio.setChecked(true);
                }
            });

            //set on click listner for pay per use layout make the pay per use radio checked
            pay_per_use_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("SSD: pay per use layout clicked");
                    pay_per_use_radio.setChecked(true);
                }
            });


            //set on check listener for free trial radio if checked make the sessionview and pay per radio false

            free_trial_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    System.out.println("is checked::radio" + SessionView.isChecked());

                    if (isChecked) {

                        SessionView.setChecked(false);
                        pay_per_use_radio.setChecked(false);
                    }

                }

/*

                Intent bookATrail = new Intent(getActivity(), BookAShop.class);
                bookATrail.putExtra("ShopSubServices", subServices);
                bookATrail.putExtra("Flag", "Free Trial");
                startActivity(bookATrail);
*/


            });
            //set on check listener for pay per use radio if checked make the sessionview and free trial false
            pay_per_use_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        SessionView.setChecked(false);
                        free_trial_radio.setChecked(false);
                    }

                }

     /*        Intent bookATrail = new Intent(getActivity(), BookAShop.class);
                bookATrail.putExtra("ShopSubServices", subServices);
                bookATrail.putExtra("Flag", "Free Trial");
                startActivity(bookATrail);
*/

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        //close the previously expanded group position
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
                try {
                    SessionView.setChecked(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
// out of the radio button which is checked please check it and get the details to send to Book a trial page
        book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        //if the free trial radio is clicked make the pay per use checked false

        free_trial_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay_per_use_radio.setChecked(false);
                //display the book button as pop up
                showPopup(getActivity());

            }
        });


        //if the pay per use  is clicked make the free trial checked false

        pay_per_use_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                free_trial_radio.setChecked(false);
                //display the book button as pop up
                showPopup(getActivity());

            }
        });
    }

    // The method that displays the popup.
    private void showPopup(final Activity context) {


        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup =  new PopupWindow(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);

        popup.setFocusable(false);
        popup.setTouchable(true);

        popup.setOutsideTouchable(false);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
      //  int OFFSET_X = 30;
    //    int OFFSET_Y = 30;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//check if the shop have the booking online option is ther show pop up else show toast
        if (shop.getBookOnline().equals("1")) {
            // Displaying the popup at the specified location, + offsets.
            popup.showAtLocation(layout, Gravity.BOTTOM, 0, 0);
        } else {
            Toast.makeText(getActivity(), "Please visit the shop for booking", Toast.LENGTH_SHORT).show();
        }
        // Getting a reference to Close button, and close the popup when clicked.
        Button close = (Button) layout.findViewById(R.id.book_btn);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();

                try {

                    //check which radio button is checked is free trial or pay per use or session view
                    if (free_trial_radio.isChecked()) {

                        //get the user already booked
                        getFreeTrialDetails();


                    } else if (pay_per_use_radio.isChecked()) {
                        Intent pay_per_use = new Intent(getActivity(), BookAShop.class);
                        pay_per_use.putExtra("sessionDetails", sessionDetails);
                        pay_per_use.putExtra("ShopSubServices", subServices);
                        pay_per_use.putExtra("ShopFlag", "Pay Per Use");
                        startActivity(pay_per_use);
                    } else if (SessionView != null) {
                        //if the package have no batch time and no selected batch time proceed
                        if (noOfBatches.equals("0") && selectedBatchTime.equals("")) {
                            //pass the selected batch time and the sessiondetails of the check position and pass to book a shop
                            Intent session = new Intent(getActivity(), BookAShop.class);
                            session.putExtra("sessionDetails", sessionDetails);
                            session.putExtra("ShopSubServices", subServices);
                            session.putExtra("batchTime", "");
                            session.putExtra("ShopFlag", "Session");
                            startActivity(session);
                            //if the package have  batch time and no selected batch time then show toast to select a batch else proceed
                        } else if (!noOfBatches.equals("0") && !selectedBatchTime.equals("")) {
                            Intent session = new Intent(getActivity(), BookAShop.class);
                            session.putExtra("sessionDetails", sessionDetails);
                            session.putExtra("ShopSubServices", subServices);
                            session.putExtra("batchTime", selectedBatchTime);
                            session.putExtra("ShopFlag", "Session");
                            startActivity(session);
                        } else {
                            Toast.makeText(getActivity(), "Please select a batch", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please select your choice", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void getFreeTrialDetails() {
        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<LogIn> call;

        call = stationClient.getFreeTrialStatus(Prefs.getString(AppConstants.UserId, ""), shop.getSid(), "4");
        call.enqueue(new Callback<LogIn>() {
            @Override
            public void onResponse(Call<LogIn> call, Response<LogIn> response) {
                int response_code = response.code();
                final LogIn result = response.body();

                if (result.getStatus().get(0).getStatus() == 1) {
                    //if user selected freeTrial and validate no. of free trial if more than one show pop up

                    //Sent the free trail sub service details
                    Intent bookaShop = new Intent(getActivity(), BookAShop.class);
                    bookaShop.putExtra("ShopSubServices", subServices);
                    bookaShop.putExtra("ShopFlag", "Free Trial");
                    bookaShop.putExtra("SelectedBatchTime", selectedBatchTime);
                    startActivity(bookaShop);


                } else if (result.getStatus().get(0).getStatus() == 0) {
                    //show the alert dialog
                    AlertDialog.Builder mBuild = new AlertDialog.Builder(getActivity());
                    View mView = getActivity().getLayoutInflater().inflate(R.layout.free_trial_dialog, null);

                    Button close_btn = (Button) mView.findViewById(R.id.close_btn);

                    TextView textView = (TextView) mView.findViewById(R.id.dialog_alert_info);

                    mBuild.setView(mView);
                    final AlertDialog dialog = mBuild.create();
                    dialog.setCancelable(true);
                    dialog.show();

                    //set the number clickable
                    SpannableString ss = new SpannableString(getResources().getString(R.string.free_trial_alert));
                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            //call the number which was underline show confirmation dialog before call and close the previous dialog
                            dialog.dismiss();
                            showConfirmationDialog();
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            super.updateDrawState(ds);
                            ds.setUnderlineText(false);
                        }
                    };
                    ss.setSpan(clickableSpan, 36, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    textView.setText(ss);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setHighlightColor(Color.TRANSPARENT);


                    close_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });


                } else if (response_code == 400 || response_code == 500) {
                    Toast.makeText(getActivity(), result.getStatus().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<LogIn> call, Throwable t) {
                Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }
        });

    }

    private void showConfirmationDialog() {


        AlertDialog.Builder mBuild = new AlertDialog.Builder(getActivity());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.information_dialog, null);
        Button bt_done = (Button) mView.findViewById(R.id.bt_done);
        Button bt_cancel = (Button) mView.findViewById(R.id.bt_cancel);
        TextView tv_msg = (TextView) mView.findViewById(R.id.tv_msg);
        TextView tv_title = (TextView) mView.findViewById(R.id.tv_title);
        bt_done.setText("YES");
        bt_cancel.setText("NO");
        tv_title.setText("Call confirmation");
        tv_msg.setText("Are you sure, you want to call?");

        mBuild.setView(mView);
        final AlertDialog dialog = mBuild.create();
        dialog.getWindow().setLayout(600, 400);
        dialog.setCancelable(true);
        dialog.show();

        bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CALL_PHONE},
                                10);
                    }
                } else {

                    if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                }
                try {
                    callIntent.setData(Uri.parse("tel:" + "08026725115"));
                    startActivity(callIntent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }


    private void showProgressDialog() {
        progress_bar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(progress_bar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
    }


    private void hideProgressDialog() {
        progress_bar.setVisibility(View.GONE);
    }

    private void initVariable(View view) {
        free_trial_layout = (LinearLayout) view.findViewById(R.id.free_trial_layout);
        pay_per_use_layout = (LinearLayout) view.findViewById(R.id.pay_per_use_layout);
        pay_per_cost_txt = (TextView) view.findViewById(R.id.pay_per_cost_txt);
        expandableListView = (NonScrollableExpandableListView) view.findViewById(R.id.expandable_package_listview);
        free_trial_radio = (RadioButton) view.findViewById(R.id.free_trial_radio);
        pay_per_use_radio = (RadioButton) view.findViewById(R.id.pay_per_use_radio);
        book_btn = (Button) view.findViewById(R.id.book_btn);
        free_trial_txt = (TextView) view.findViewById(R.id.free_trial_txt);

        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_show);
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_hide);
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
    }
}