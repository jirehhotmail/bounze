package com.jireh.bounze.Fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.jireh.bounze.Activity.Blog;
import com.jireh.bounze.Activity.EventActivity;
import com.jireh.bounze.Activity.ShopActivity;
import com.jireh.bounze.Activity.PagerLayout;
import com.jireh.bounze.Activity.SplashScreenActivity;
import com.jireh.bounze.Activity.Trainer;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.AnimatorUtils;
import com.ogaclejapan.arclayout.ArcLayout;
import com.jireh.bounze.data.ResultModel;
import com.jireh.bounze.data.Search;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muthamizhan C on 09-08-2017.
 */


public class Explore extends Fragment {

    View rootView;
    ArcLayout arc_layout;
    FrameLayout menu_layout;
    FloatingActionButton tabThree;
    LinearLayout home_layout;
    ImageButton fab1_image, fab2_image, fab3_image, fab4_image;
    ImageView back_img, filter_img, search_img;
    Toolbar toolbar;
    TextView title;
    private SliderLayout offers_slider_layout, events_slider_layout, dailycabs_slider_layout, bounze_blogs_slider_layout;
    private Spinner city_spinner;
    RelativeLayout spinner_layout;
    //create arraylist for the four banners
    ArrayList<String> offersImagelist = new ArrayList<>();
    ArrayList<String> eventsImagelist = new ArrayList<>();
    ArrayList<String> dailyImagelist = new ArrayList<>();
    ArrayList<String> blogsImagelist = new ArrayList<>();





    @SuppressWarnings("NewApi")
    public void showMenu() {

        try {
            arc_layout.setVisibility(View.VISIBLE);
            List<Animator> animList = new ArrayList<>();


            for (int i = 0; i < arc_layout.getChildCount(); i++) {
                View child = arc_layout.getChildAt(i);
                child.setVisibility(View.VISIBLE);
            }


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void hideMenu() {
        try {
            arc_layout.setVisibility(View.GONE);

            List<Animator> animList = new ArrayList<>();


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    for (int i = 0; i < arc_layout.getChildCount(); i++) {
                        View child = arc_layout.getChildAt(i);
                        child.setVisibility(View.INVISIBLE);
                    }
                    // menu_layout.setVisibility(View.INVISIBLE);
                }
            });
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout resource that'll be returned
        rootView = inflater.inflate(R.layout.explore, container,
                false);
        Log.d("Explore", "open");

        initVariable(rootView);

        //set Title
        title.setText("Explore");
        title.setVisibility(View.VISIBLE);
        filter_img.setVisibility(View.GONE);
        search_img.setVisibility(View.GONE);
        spinner_layout.setVisibility(View.GONE);
        setSpinner();
        //get the banner details
        getBannerDetails();



        setOnClickListener();

        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.explore_layout);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            child.setEnabled(false);
        }
        showMenu();
        return rootView;
    }

    private void getBannerDetails() {



            ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
            Call<Search> call = stationClient.getHomeSliderList();


            call.enqueue(new Callback<Search>() {

                @Override
                public void onResponse(Call<Search> call, Response<Search> response) {
                    int response_code = response.code();
                    Search result = response.body();

                    if (response_code == 200) {
                        //get the result array and then get the version
                        List<ResultModel> check = result.getStatus();
                        //if arraylist size > 0 then clear it before adding
                        if (offersImagelist.size() > 0 || eventsImagelist.size() > 0 ||
                                dailyImagelist.size() > 0 || blogsImagelist.size() > 0) {
                            offersImagelist.clear();
                            eventsImagelist.clear();
                            dailyImagelist.clear();
                            blogsImagelist.clear();
                        }
                        //set the slider images for the 4 rows
                        for (int idx = 0; idx < check.size(); idx++) {
                            for (int position = 0; position < check.get(idx).getPosition().size(); position++) {
                                //base on the  position of the arrray add in the array list
                                if (idx == 0) {
                                    offersImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());

                                } else if (idx == 1) {
                                    eventsImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());

                                } else if (idx == 2) {
                                    dailyImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());

                                } else if (idx == 3) {

                                    blogsImagelist.add(check.get(idx).getPosition().get(position).getImageUrl());

                                }

                            }

                            //if the first,second,third,fourth position array size ends then set the adapter
                            if (idx == 0) {
                                setOffersBanner();
                            } else if (idx == 1) {
                                setEventsBanner();
                            } else if (idx == 2) {
                                setDailyDcab();
                            } else if (idx == 3) {
                                setBounzeBlogs();
                            }
                        }


                        System.out.println("Total value::" + check.size());

                    } else if (response_code == 400) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();

                    } else if (response_code == 500) {
                        Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<Search> call, Throwable t) {
                    Toast.makeText(getActivity(), "Technical error, please retry", Toast.LENGTH_SHORT).show();
                    Log.d("Registration_eqnn", t + "");
                    Log.d("Registration_eqnn", call + "");

                }
            });



    }

    private void setOnClickListener() {

        System.out.println("Inside :: child count:" + arc_layout.getChildCount());
        System.out.println("Inside :: child id:" + R.id.fab2_image);

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PagerLayout.viewPager.setCurrentItem(0);
            }
        });

        fab1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ShopActivity.class);
                startActivity(in);
            }
        });

        fab2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Trainer.class);
                startActivity(in);
            }
        });
        fab3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), EventActivity.class);
                startActivity(in);

            }
        });

        fab4_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Blog.class);
                startActivity(in);
            }
        });
    }

    private void setSpinner() {
        String city[] = {"Bengaluru"};
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_adapter, city);
        dataAdapter2.setDropDownViewResource(R.layout.simple_spinner_adapter);
        city_spinner.setAdapter(dataAdapter2);
    }
    private void setEventsBanner() {

        for (int idx = 0; idx < eventsImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + eventsImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            events_slider_layout.addSlider(defaultSliderView);
        }
        events_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        events_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        events_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        events_slider_layout.setDuration(4000);
    }

    private void setOffersBanner() {
        //  int arrayImages[] = {R.drawable.banner_one, R.drawable.banner_three};
        for (int idx = 0; idx < offersImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + offersImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);


            //add your extra information
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {

                    System.out.println("event position" + offers_slider_layout.getCurrentPosition());
                }
            });

            offers_slider_layout.addSlider(defaultSliderView);
        }
        offers_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        offers_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        offers_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        offers_slider_layout.setDuration(4000);


    }

    private void setDailyDcab() {

        for (int idx = 0; idx < dailyImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + dailyImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            dailycabs_slider_layout.addSlider(defaultSliderView);
        }
        dailycabs_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        dailycabs_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        dailycabs_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        dailycabs_slider_layout.setDuration(4000);
    }

    private void setBounzeBlogs() {

        for (int idx = 0; idx < blogsImagelist.size(); idx++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    //   .description(name)
                    .image(SplashScreenActivity.BASE_URL + blogsImagelist.get(idx))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            bounze_blogs_slider_layout.addSlider(defaultSliderView);
        }
        bounze_blogs_slider_layout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        bounze_blogs_slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        bounze_blogs_slider_layout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        // slider_layout.setCustomAnimation(new DescriptionAnimation());
        bounze_blogs_slider_layout.setDuration(4000);
    }
    private void initVariable(View rootView) {
        offers_slider_layout = (SliderLayout) rootView.findViewById(R.id.offer_slider_layout);
        events_slider_layout = (SliderLayout) rootView.findViewById(R.id.events_slider_layout);
        dailycabs_slider_layout = (SliderLayout) rootView.findViewById(R.id.dailycabs_slider_layout);
        bounze_blogs_slider_layout = (SliderLayout) rootView.findViewById(R.id.bounze_blogs_slider_layout);
        home_layout = (LinearLayout) rootView.findViewById(R.id.home_layout);
        city_spinner = (Spinner) rootView.findViewById(R.id.city_spinner);
        //  fab = (FloatingActionButton) rootView.findViewById(R.id.fab);

        arc_layout = (ArcLayout) rootView.findViewById(R.id.arc_layout);
        fab1_image = (ImageButton) rootView.findViewById(R.id.fab1_image);
        fab2_image = (ImageButton) rootView.findViewById(R.id.fab2_image);
        fab3_image = (ImageButton) rootView.findViewById(R.id.fab3_image);
        fab4_image = (ImageButton) rootView.findViewById(R.id.fab4_image);

        menu_layout = (FrameLayout) rootView.findViewById(R.id.menu_layout);
        // animateFAB();
        tabThree = (FloatingActionButton) LayoutInflater.from(getActivity()).inflate(R.layout.floating_button_icon, null);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        title = (TextView) toolbar.findViewById(R.id.title_txt);
        filter_img = (ImageView) toolbar.findViewById(R.id.filter_img);
        search_img = (ImageView) toolbar.findViewById(R.id.search_img);
        spinner_layout = (RelativeLayout) toolbar.findViewById(R.id.spinner_layout);
    }


}
