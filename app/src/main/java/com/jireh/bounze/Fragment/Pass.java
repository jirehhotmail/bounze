package com.jireh.bounze.Fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.jireh.bounze.Activity.Blog;
import com.jireh.bounze.Activity.EventActivity;
import com.jireh.bounze.Activity.PagerLayout;
import com.jireh.bounze.Activity.PassDescriptionActivity;
import com.jireh.bounze.Activity.PassFilter;
import com.jireh.bounze.Activity.ShopActivity;
import com.jireh.bounze.Activity.Trainer;
import com.jireh.bounze.Adapter.FilterByPassAdapter;
import com.jireh.bounze.Adapter.PassLogoAdapter;
import com.jireh.bounze.Db.DBFunctions;
import com.jireh.bounze.R;
import com.jireh.bounze.Util.SessionManager;
import com.jireh.bounze.data.PassArr;
import com.jireh.bounze.data.PassList;
import com.jireh.bounze.data.PassPojo;
import com.jireh.bounze.data.Service_logo;
import com.jireh.bounze.rest.ApiClient;
import com.jireh.bounze.rest.ApiInterface;
import com.ogaclejapan.arclayout.ArcLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jireh.bounze.Activity.SplashScreenActivity.BASE_URL;

/**
 * Created by Muthamizhan C on 09-08-2017.
 */

public class Pass extends Fragment implements View.OnClickListener {
    public static boolean[] checkSelected;
    public static ArrayList<String> dataList;
    //    List<PassModel> passList;
    PassArr[] passList;
    DBFunctions dbObject;
    LinearLayoutManager mLayoutManager;
    //    HashMap<Integer, ArrayList<String>> servicLogo;
    Multimap<Integer, String> servicLogo = ArrayListMultimap.create();
    ArrayList<PassList> passModels = new ArrayList<>();
    Toolbar toolbar;
    ImageView back_img;
    ArcLayout arc_layout;
    ImageButton fab1_image, fab2_image, fab3_image, fab4_image;

    FilterByPassAdapter adapter;
    // HashMap<Integer, String> serviceLogoPath =new  HashMap<Integer,String>();
    private RecyclerView mRecyclerView;
    private PassAdapter mAdapter;
    private SessionManager sessionManager;
    private ImageView filter_img;


    private LinearLayout ll_back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        sessionManager = new SessionManager(getActivity());
        passList = sessionManager.getPass_Result();
        return inflater.inflate(R.layout.pass_layout, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            //get pass list details from server for the updation
            getAllPassList();
            mAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllPassList() {


        ApiInterface stationClient = ApiClient.createService(ApiInterface.class);
        Call<PassPojo> callPojo = stationClient.getPassPojo();
        callPojo.enqueue(new Callback<PassPojo>() {
            @Override
            public void onResponse(Call<PassPojo> call, Response<PassPojo> response) {
                try {
                    int response_code = response.code();
                    PassArr[] result = response.body().getPass();
                    if (response_code == 200) {
                        Log.d("service list", "Registration_eqnn " + response.code() + "");

                        sessionManager.setPass_Result(result);
                        passList = sessionManager.getPass_Result();


                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<PassPojo> call, Throwable t) {
                Log.d("Registration_eqnn", t + "");
                Log.d("Registration_eqnn", call + "");

            }
        });

    }

    @SuppressWarnings("NewApi")
    public void showMenu() {

        try {
            arc_layout.setVisibility(View.VISIBLE);
            List<Animator> animList = new ArrayList<>();


            for (int i = 0; i < arc_layout.getChildCount(); i++) {
                View child = arc_layout.getChildAt(i);
                child.setVisibility(View.VISIBLE);
            }


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("NewApi")
    public void hideMenu() {
        try {
            arc_layout.setVisibility(View.GONE);
            List<Animator> animList = new ArrayList<>();


            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(400);
            animSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animSet.playTogether(animList);
            animSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                    for (int i = 0; i < arc_layout.getChildCount(); i++) {
                        View child = arc_layout.getChildAt(i);
                        child.setVisibility(View.INVISIBLE);
                    }
                    // menu_layout.setVisibility(View.INVISIBLE);
                }
            });
            animSet.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title_txt = (TextView) view.findViewById(R.id.title_txt);
        title_txt.setText("Pass");
        mRecyclerView = (RecyclerView) view.findViewById(R.id.pass_recycler);
        ll_back = (LinearLayout) view.findViewById(R.id.ll_back);


        filter_img = (ImageView) view.findViewById(R.id.filter_img);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar_layout);
        back_img = (ImageView) toolbar.findViewById(R.id.back_img);
        arc_layout = (ArcLayout) view.findViewById(R.id.arc_layout);
        fab1_image = (ImageButton) view.findViewById(R.id.fab1_image);
        fab2_image = (ImageButton) view.findViewById(R.id.fab2_image);
        fab3_image = (ImageButton) view.findViewById(R.id.fab3_image);
        fab4_image = (ImageButton) view.findViewById(R.id.fab4_image);


        filter_img.setOnClickListener(this);
        ll_back.setOnClickListener(this);

        // use a linear layout manager
        dbObject = new DBFunctions(getActivity());

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (servicLogo.size() > 0) {
            servicLogo.clear();
            passModels.clear();
        }

        for (int idx = 0; idx < passList.length; idx++) {
            PassList dm = new PassList();
            ArrayList<String> singleItem = new ArrayList<String>();

            for (Integer key : servicLogo.keySet()) {
                if (key == idx) {
                    List<String> values = (List<String>) servicLogo.get(key);
                    singleItem.addAll(values);
                }
            }

            dm.setAllItemsInSection(singleItem);

            passModels.add(dm);
        }
        // specify an adapter
        callAdapter();

        setOnClickListener();

    }

    private void setOnClickListener() {

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PagerLayout.viewPager.setCurrentItem(0);
            }
        });

        fab1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), ShopActivity.class);
                startActivity(in);
            }
        });

        fab2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Trainer.class);
                startActivity(in);
            }
        });
        fab3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), EventActivity.class);
                startActivity(in);
            }
        });

        fab4_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Blog.class);
                startActivity(in);
            }
        });
    }

    /**
     * <h3>callAdapter</h3>
     * This method is used for itegrating the adapter with our Recycler view.
     */
    private void callAdapter() {
        mAdapter = new PassAdapter(getActivity(), passList, passModels);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.setOnItemClickListener(new OnItemClick() {
            @Override
            public void onItemClicked(View view, int position, Object data) {
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.filter_img:

                Intent in = new Intent(getActivity(), PassFilter.class);
                startActivity(in);

                break;
            case R.id.ll_back:

                break;

        }
    }


    public interface OnItemClick {
        public void onItemClicked(View view, int position, Object data);
    }

    public class PassAdapter extends RecyclerView.Adapter<PassAdapter.ViewHolder> {
        PassArr[] passList;
        ArrayList<PassList> passModel;
        private Context mContext;
        private OnItemClick mListener;

        // Provide a suitable constructor (depends on the kind of dataset)
        public PassAdapter(Context context, PassArr[] passList, ArrayList<PassList> passModel) {
            this.mContext = context;
            this.passList = passList;
            this.passModel = passModel;
        }

        public void setOnItemClickListener(OnItemClick listener) {
            mListener = listener;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public PassAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pass_row_layout, parent, false);
            // set the view's size, margins, paddings and layout parameters

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            final PassArr model = getItem(position);
            holder.pass_name.setText(model.getPass_name());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClicked(v, position, model);
                }
            });

            Picasso.with(mContext).load(BASE_URL + model.getLogo()).placeholder(R.drawable.loadingcoa).error(R.drawable.noimagefound).into(holder.pass_logo_img);


            if (!model.getRatings().equals(""))
                holder.ratingbar.setRating(Float.valueOf(String.valueOf(model.getRatings())));
            else
                holder.ratingbar.setRating(Float.valueOf(0.0f));

            holder.location_area_txt.setText(model.getLocation());
            holder.session_txt.setText(model.getSessions() + " Sessions");
            holder.price_txt.setText(mContext.getResources().getString(R.string.Rs) + model.getPrice());
            holder.bought_total_txt.setText(model.getTotal_bought() + "+ bought");


            holder.location_area_txt.setMovementMethod(new ScrollingMovementMethod());
              //set on click listener for whole layout and also the book button
            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), PassDescriptionActivity.class);
                    sessionManager.setSinglePass(model);
                    startActivity(intent);
                }
            });

            holder.book_pass_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), PassDescriptionActivity.class);
                    sessionManager.setSinglePass(model);
                    startActivity(intent);
                }
            });


            Service_logo[] service_Logo_Arr = Pass.this.passList[position].getService_logo();
            ArrayList<String> serviceLogo = new ArrayList<>();
            for (int tempPos = 0; tempPos < service_Logo_Arr.length; tempPos++) {
                serviceLogo.add(tempPos, service_Logo_Arr[tempPos].getService_icon());
            }

            holder.horizontalList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            PassLogoAdapter horizontalAdapter = new PassLogoAdapter(mContext, serviceLogo);
            holder.horizontalList.setAdapter(horizontalAdapter);
        }

        private PassArr getItem(int position) {
            return passList[position];
        }

        private Context getContext() {
            return mContext;
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return passList.length;
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            ImageView pass_logo_img;
            TextView location_area_txt, session_txt, price_txt, bought_total_txt, pass_name;
            Button book_pass_btn;
            RatingBar ratingbar;
            CardView card_view;
            private RecyclerView horizontalList;

            public ViewHolder(View view) {
                super(view);
                pass_logo_img = (ImageView) view.findViewById(R.id.pass_logo_img);
                pass_name = (TextView) view.findViewById(R.id.pass_name);
                ratingbar = (RatingBar) view.findViewById(R.id.ratingbar);
                location_area_txt = (TextView) view.findViewById(R.id.location_area_txt);
                session_txt = (TextView) view.findViewById(R.id.session_txt);
                price_txt = (TextView) view.findViewById(R.id.price_txt);
                bought_total_txt = (TextView) view.findViewById(R.id.bought_total_txt);
                book_pass_btn = (Button) view.findViewById(R.id.book_pass_btn);
                horizontalList = (RecyclerView) itemView.findViewById(R.id.horizontal_list);
                card_view = (CardView) view.findViewById(R.id.card_view);
            }
        }
    }
}