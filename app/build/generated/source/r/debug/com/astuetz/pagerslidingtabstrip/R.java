/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.astuetz.pagerslidingtabstrip;

public final class R {
    public static final class attr {
        public static final int pstsDividerColor = 0x7f0101a0;
        public static final int pstsDividerPadding = 0x7f0101a2;
        public static final int pstsDividerWidth = 0x7f0101a1;
        public static final int pstsIndicatorColor = 0x7f01019c;
        public static final int pstsIndicatorHeight = 0x7f01019d;
        public static final int pstsPaddingMiddle = 0x7f0101a5;
        public static final int pstsScrollOffset = 0x7f0101a3;
        public static final int pstsShouldExpand = 0x7f0101a4;
        public static final int pstsTabBackground = 0x7f0101a7;
        public static final int pstsTabPaddingLeftRight = 0x7f0101a6;
        public static final int pstsTabTextAllCaps = 0x7f0101ab;
        public static final int pstsTabTextAlpha = 0x7f0101ac;
        public static final int pstsTabTextColor = 0x7f0101a9;
        public static final int pstsTabTextFontFamily = 0x7f0101ad;
        public static final int pstsTabTextSize = 0x7f0101a8;
        public static final int pstsTabTextStyle = 0x7f0101aa;
        public static final int pstsUnderlineColor = 0x7f01019e;
        public static final int pstsUnderlineHeight = 0x7f01019f;
    }
    public static final class color {
        public static final int psts_background_tab_pressed = 0x7f0c00be;
        public static final int psts_background_tab_pressed_ripple = 0x7f0c00bf;
    }
    public static final class drawable {
        public static final int psts_background_tab = 0x7f020182;
    }
    public static final class id {
        public static final int bold = 0x7f0e0071;
        public static final int italic = 0x7f0e0072;
        public static final int normal = 0x7f0e0028;
        public static final int psts_tab_title = 0x7f0e02cf;
    }
    public static final class layout {
        public static final int psts_tab = 0x7f0400b2;
    }
    public static final class styleable {
        public static final int[] PagerSlidingTabStrip = { 0x7f01019c, 0x7f01019d, 0x7f01019e, 0x7f01019f, 0x7f0101a0, 0x7f0101a1, 0x7f0101a2, 0x7f0101a3, 0x7f0101a4, 0x7f0101a5, 0x7f0101a6, 0x7f0101a7, 0x7f0101a8, 0x7f0101a9, 0x7f0101aa, 0x7f0101ab, 0x7f0101ac, 0x7f0101ad };
        public static final int PagerSlidingTabStrip_pstsDividerColor = 4;
        public static final int PagerSlidingTabStrip_pstsDividerPadding = 6;
        public static final int PagerSlidingTabStrip_pstsDividerWidth = 5;
        public static final int PagerSlidingTabStrip_pstsIndicatorColor = 0;
        public static final int PagerSlidingTabStrip_pstsIndicatorHeight = 1;
        public static final int PagerSlidingTabStrip_pstsPaddingMiddle = 9;
        public static final int PagerSlidingTabStrip_pstsScrollOffset = 7;
        public static final int PagerSlidingTabStrip_pstsShouldExpand = 8;
        public static final int PagerSlidingTabStrip_pstsTabBackground = 11;
        public static final int PagerSlidingTabStrip_pstsTabPaddingLeftRight = 10;
        public static final int PagerSlidingTabStrip_pstsTabTextAllCaps = 15;
        public static final int PagerSlidingTabStrip_pstsTabTextAlpha = 16;
        public static final int PagerSlidingTabStrip_pstsTabTextColor = 13;
        public static final int PagerSlidingTabStrip_pstsTabTextFontFamily = 17;
        public static final int PagerSlidingTabStrip_pstsTabTextSize = 12;
        public static final int PagerSlidingTabStrip_pstsTabTextStyle = 14;
        public static final int PagerSlidingTabStrip_pstsUnderlineColor = 2;
        public static final int PagerSlidingTabStrip_pstsUnderlineHeight = 3;
    }
}
